<?xml version="1.0" encoding="windows-1252"?>
<NAXML-POSJournal xmlns="http://www.naxml.org/POSBO/Vocabulary/2003-10-16"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.naxml.org/POSBO/Vocabulary/2003-10-16
\PCATSschemas\naxml-posjournal33.xsd">
	<TransmissionHeader>
		<StoreLocationID>1</StoreLocationID>
		<VendorName>Hungary</VendorName>
		<VendorModelVersion>1.0</VendorModelVersion>
	<Extension/>
	</TransmissionHeader>
	<JournalReport>
		<SaleEvent>
			<EventSequenceID>1</EventSequenceID>
			<RegisterID>201</RegisterID>
			<TransactionID>2618</TransactionID>
			<EventEndDate>2014-11-21</EventEndDate>
			<EventEndTime>13:47:12</EventEndTime>
			<ReceiptType>9</ReceiptType>
			<ReceiptDate>2014-11-21</ReceiptDate>
			<ReceiptTime>13:47:12</ReceiptTime>
			<TicketPrinted value="yes"/>
			<PaymentId>1</PaymentId>
			<TransactionFiscalID>00026001/44/2618</TransactionFiscalID>
			<InvoiceFlag value="no"/>
			<TransactionDetailGroup>
				<TransactionLine>
					<TransactionLineSequenceNumber>1</TransactionLineSequenceNumber>
					<FuelLine>
						<FuelGradeID>15</FuelGradeID>
						<FuelPumpID>1</FuelPumpID>
						<FuelNozzleID>3</FuelNozzleID>
						<FuelPositionID>13</FuelPositionID>
						<PumpMeterQuantity>23376.08</PumpMeterQuantity>
						<PumpMeterAmount>8189719.00</PumpMeterAmount>
						<Description>ESZ95/E5 4</Description>
						<RegularSellPrice>360.00</RegularSellPrice>
						<SalesQuantity>236.11</SalesQuantity>
						<SalesAmount>85000.00</SalesAmount>
					</FuelLine>
					<TenderInfo>
						<Tender>
							<TenderCode>manager</TenderCode>
							<TenderSubCode>manager</TenderSubCode>
						</Tender>
						<TenderAmount>85000.00</TenderAmount>
						<RefundCodeAmountConsumed>0.00</RefundCodeAmountConsumed>
						<Authorization>
							<ReferenceNumber></ReferenceNumber>
							<SlipNumber></SlipNumber>
							<AuthorizationDate></AuthorizationDate>
							<AuthorizationTime></AuthorizationTime>
							<AuthorizingTerminalID></AuthorizingTerminalID>
							<AuthorizedChargeAmount>85000.00</AuthorizedChargeAmount>
						</Authorization>

						<PartialDeliveryInformation />

						<PAN></PAN>
						<ExpDate></ExpDate>
					</TenderInfo>
				</TransactionLine>
			</TransactionDetailGroup>
			<Extension/>
		</SaleEvent>
	</JournalReport>
</NAXML-POSJournal>

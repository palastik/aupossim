# Microsoft Developer Studio Project File - Name="AuPosSim" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=AuPosSim - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "AuPosSim.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "AuPosSim.mak" CFG="AuPosSim - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "AuPosSim - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "AuPosSim - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "AuPosSim - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /Zp1 /MT /W3 /GX /O2 /I "..\borne.all\issh" /I "..\common" /I "X:\Program Files\MySQL\MySQL Server 5.1\include" /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /Fr /FD /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x40e /d "NDEBUG"
# ADD RSC /l 0x40e /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# SUBTRACT BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 Psapi.Lib wsock32.lib libmysql.lib mysys.lib gdecimal.lib threegli.lib hlogia46.lib hlogia.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386 /nodefaultlib:"LIBCMTD.lib" /libpath:"..\borne.all\isslib" /libpath:"X:\Program Files\MySQL\MySQL Server 5.1\lib\debug"

!ELSEIF  "$(CFG)" == "AuPosSim - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /Zp1 /MT /W3 /Gm /GX /ZI /Od /I "..\borne.all\issh" /I "..\common" /I "X:\Program Files\MySQL\MySQL Server 5.1\include" /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /FR /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x40e /d "_DEBUG"
# ADD RSC /l 0x40e /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 Psapi.Lib wsock32.lib libmysql.lib mysys.lib gdecimal.lib threegli.lib hlogia46.lib hlogia.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /nodefaultlib:"LIBCMTD.lib" /pdbtype:sept /libpath:"..\borne.all\isslib" /libpath:"X:\Program Files\MySQL\MySQL Server 5.1\lib\debug"

!ENDIF 

# Begin Target

# Name "AuPosSim - Win32 Release"
# Name "AuPosSim - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AuPosSim.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\FlexArray.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\GenLog2File.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\LogElements.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\MyArray.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\MyGDec.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\ReadIssDB.cpp
# End Source File
# Begin Source File

SOURCE=..\Common\REG.CPP
# End Source File
# Begin Source File

SOURCE=.\tinystr.cpp
# End Source File
# Begin Source File

SOURCE=.\tinyxml.cpp
# End Source File
# Begin Source File

SOURCE=.\tinyxmlerror.cpp
# End Source File
# Begin Source File

SOURCE=.\tinyxmlparser.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\Common\FlexArray.h
# End Source File
# Begin Source File

SOURCE=..\Common\GenLog2File.h
# End Source File
# Begin Source File

SOURCE=..\Common\LogElements.h
# End Source File
# Begin Source File

SOURCE=..\Common\Logmeta.h
# End Source File
# Begin Source File

SOURCE=..\Common\MyArray.h
# End Source File
# Begin Source File

SOURCE=..\Common\MyGDec.h
# End Source File
# Begin Source File

SOURCE=..\Common\MyIssLog46.h
# End Source File
# Begin Source File

SOURCE=..\Common\ReadIssDB.h
# End Source File
# Begin Source File

SOURCE=..\Common\REG.H
# End Source File
# Begin Source File

SOURCE=.\tinystr.h
# End Source File
# Begin Source File

SOURCE=.\tinyxml.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\AuPosSim.rc
# End Source File
# Begin Source File

SOURCE=.\GASPUMP.ICO
# End Source File
# End Group
# End Target
# End Project

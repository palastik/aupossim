
	/* Revision history

		ISS calls with retries
		Empty RVM vouchers are not errors (Optimistic voucher handling)
		Empty Fuel transactions are not errors
1.9.4	Added:	Round voucher sum to next 5

1.9.3	BugFix:	PreFill21001 changed to have no weight and eantype=1
				PreFill1021 also changed, but caused no error
  
1.9.2	Added:	VAT rate conversion for the incoming item update files

1.9.1	BugFix: Rounding applied to Fuel tender counters causing invalid data for comparing
  
1.9.0	Added:	PETLOG return functionality handling
		BugFix:	Limiting continuous performance slot logging to 5 days
		BugFix:	Some errors not caused exiting from ProcSale causing trouble/false data
		BugFix:	Continuous transaction processing involved delay (Sleep())

1.8.1	BugFix:	Tran process retry must seek back to the beginning of the tran file

1.8.0	Added:	RVM item update sends item link data too

1.7.1	Bugfix:	RVM and Fuel item updates were mixed onto one list, not two separate
		Added:	Item update can send linked item records

1.7.0	Added:	Cash Rounding can be handled as it is in the new ISS version

1.6.0	Added:	Filtering of price updates by barcode from registry setting
		Change:	Autologon of (automatic) fuel vending machines is longer an error
		bugfix:	Minor log text formatting problems at fuel station closure

1.5.1	Added:	RVM Vouchers can overwrite record when old_expiration+VoucherSafeOverWriteDays<new_expiration
		Added:	Full CSC dump can generate file on the name of "FullCSC.dmp" as option 2

1.5.0	Change:	Logging overnight fuel sales to the beginning of the new trading day
				No fuel sales logged during the closed period of the store

1.4.0	Change:	Changed logging of 21099 (data entry) elements to the new version

1.3.3	Bugfix:	RVM status has been sved to the REG in V2 mode on the V1 named binvalue

1.3.2	Added:	Fuel: automatic station PoS window time can be parameterised, enabled/disabled

1.3.1	Bugfix:	Bad rounding for fuel unit price
		Bugfix:	Item substitution ruins the item code for item sales collection
		Bugfix:	100% processor usage in idle
		Bugfix:	RVM Autologoff date&time comparison is bad
		Adjust:	RVM Autologoff time is +1 seconds as the last transaction time
		BugFix:	Performance slot closure comparison is bad

1.3.0	Added:	Fuel item substitution
		Added:	Fuel station weight sales-> EAN type is 5 for EAN21-29
		Added:	RVM autologoff by the last transactions time details and HP. Causes: drawer reassignment, proper PerfSlotLogging
		BugFix:	RVM Periodic Performance data is positive and *100 the value
		BugFix:	support of 1 decimal digit in fuel unit price calculations

1.2.2	Bugfix:	RVM Log file truncation was not set by reg value
		Bugfix:	EOT element number is bad
		Bugfix:	Logging trunctation LF missing

1.2.1	Change: Autom. fuel station opening restriction changed to 6:00-18:00

1.2.0	Added:	Fuel station 24h support
		BugFix:	Bottle return: Voucher handled first, then the transaction

1.1.2	Bugfix:	FlexArray::AddFigures - bad memory handling

1.1.1	Bugfix:	GetProgress cannot void transaction in progress

1.1.0	Added:	Fuel station will retry when workfile size turns out to be zero

1.0.3	BugFix: Fuel station update file name missing a point - still was wrong

1.0.2	BugFix: Fuel station update file name missing a point

1.0.1	Product live release - with bugfixes

1.0.0	Initial (test) release

--------------- WriteIssLog Revision History ------------------------

1.9.0	Added/bugfix: Access violation if no payment record in file. 
			Preprocessor added to handle such sequencing errors.
  
1.8.2	Added/bugfix: Price update VAT rate conversion error handling had a bug

1.8.1	Added/bugfix: Zero quantity item sale is handled - because of exception

1.8.0	Added: Separate error log file containing problems only
		Added: ProcClose shows missing closure records

1.7.0	Bugfix: Adjusting transaction number if ISS reports different last txn number
		Bugfix: EOT having false End Date data
		Added: Automatic logon if a sales transaction comes in logged off state
			(governed by AutoActions & 0x00000001)
		Added: ProcSale gives warning when sales user different than the logged on
		Added: Automatic logoff/on if a sales transaction comes from a different user	
			(governed by AutoActions & 0x00000002)
		Added: ProcLogOff gives warning when logoff user different than the logged on
		Added: ProcLogOff refuses logoff of a different user than the one has been logged on
			(governed by RefusedActions & 0x00000001)
  
1.6.1	Bugfix: Bad display of parameters of some warning messages
		Added blocking by PoS number for LogOn and LogOff

1.6.0	Corrected bug of producing customer change file

1.5.0	Improved error handling for startup
		Clean closing in now possible

1.1.0	QuantityDigitLoss option added. (default: 2(cl->liter))

1.0.4	No performance record logging mask registry option added (default: cashier perf disabled)

		Defaultly 15 seconds of txn time applied when incoming txn shows 0 secs

1.0.3	Corrected bug of producing customer change file

1.0.2	PriceByCentiLiters parameter introduced

1.0.1	Put sold amount as quantity, not weight (AmountAsQuantity chged to 1 in registry)

1.0.0	Initial (test) release

*/

#include <windows.h>
#include <process.h>
#include <resapi.h>
#include <vector>
#ifdef TIXML_USE_STL
	#include <iostream>
	#include <sstream>
	using namespace std;
#else
	#include <stdio.h>
#endif
#if defined( WIN32 ) && defined( TUNE )
	#include <crtdbg.h>
	_CrtMemState startMemState;
	_CrtMemState endMemState;
#endif
#include "wininet.h"
#include "tinyxml.h"


#define NO_UINT32_DEFINE_IN_OSIFBASEH

#include "iss.h"
#include "isslog.h"
#include "isslgerr.h"
#include "gdecimal.h"
#include "MyGDec.h"
#include "MyArray.h"
#include "FlexArray.h"
#include "MyIssLog46.h"
#include "isshlog.h"
#include "LogElements.h"
#include "ReadIssDB.h"
#include "Reg.h"
#include "GenLog2File.h"
#include "math.h"
#include "time.h"
#include <stdio.h>
#define W32_LEAN_AND_MEAN
//#include <winsock2.h>

#include "psapi.h"


#include "Shlwapi.h"
//#include "CSmtp.h"
//#include <iostream>
#include "mysql.h"

#pragma comment(lib, "ws2_32.lib")


//#include "isshlog.h"

const char C_Ver[]="V 6.0.1.0";

const char C_RegKey[]="Software\\SupDev\\AuPosSim";
const char C_RegVNDirtyExit[]="_DirtyExit";
const char C_RegValNameGenPosData[]="Fuel.PoSData.#%d";
const char C_RegTenderTotals[]="Fuel.Counter.TenderTotalsArray";
const char C_RegCashierTotals[]="Fuel.Counter.CashierTotalsArray";
//const char C_RegItemTotals[]="Fuel.Counter.ItemTotalsArray";
const char C_RegMachineTotals[]="RVM.Counter.MachineTotalsArray";
const char C_RegTicketIssue[]="RVM.Counter.TicketIssueArray";
const char C_RegRItemTotals[]="RVM.Counter.ItemTotalsArray";
const char C_RegGItemTotals[]="Fuel.Counter.ItemTotalsArray";
const char C_RegPendingClosure[]="RVM.PendingClosure";
const char C_RegValLastTranHP[]="Fuel.LastTranHP";
const char C_RegValRLastTranHP[]="RVM.LastTranHP";
const char C_InitCSC[]="_ThrowFullCustomerList";
const C_TENDERS=30;

const char C_RegValNameRPosData[]="RVM.PoSData";
const char C_RegValNameRPosDataV2[]="RVM.PoSData.V2";

typedef struct _t_tender {
	long	id;
	long	cnt;
	long	val;
//	struct _t_tender *next;
} TTender;

typedef struct _t_slist {
	long	id;
	char	*p;
	struct _t_slist *n;
} TSList;

typedef struct _t_pos {
	short				sid;
	long				lasttran;
	char				user[4];
	ISS_T_INT_DATE		drwdate;
	ISS_T_INT_TIME		drwtime;
	ISS_T_INT_DATE		signondate;
	ISS_T_INT_TIME		signontime;
	SINT32				signontransno;
	long				items;
	long				regtime;
	long				tendertime;
	ISS_T_INT_DATE		p_startdate;
	ISS_T_INT_TIME		p_starttime;	//can be like 13:01:28
	long				p_regtime;
	long				p_tendertime;
	long				p_itemcnt;
	long				p_custcnt;
	long				p_salesval;
	ISS_T_INT_DATE		lasttrandate;
	ISS_T_INT_TIME		lasttrantime;
	long				tenders;
	TTender				tender[C_TENDERS];
} TPos;

typedef struct _t_items {
	short				sid;
	long				lasttran;
	char				user[4];
	ISS_T_INT_DATE		drwdate;
	ISS_T_INT_TIME		drwtime;
	ISS_T_INT_DATE		signondate;
	ISS_T_INT_TIME		signontime;
	SINT32				signontransno;
	long				items;
	long				regtime;
	long				tendertime;
	ISS_T_INT_DATE		p_startdate;
	ISS_T_INT_TIME		p_starttime;	//can be like 13:01:28
	long				p_regtime;
	long				p_tendertime;
	long				p_itemcnt;
	long				p_custcnt;
	long				p_salesval;
	long				lasttrandate;
	long				lasttrantime;
	long				lasttranlocdate;//Local system date when the last transaction was logged (ISS format)
	long				lasttranloctime;//Local system time when the last transaction was logged (ISS format)
	long				lasttranhp;	//Last HP value used to log any ISS transaction for RVM
} TRPos;

typedef struct _t_header {
	short				sid;
	long				transID;
	int					cashier;
	char				BTime[9];
	char				ETime[9];
	char				BDate[11];
	char				EDate[11];
	char				Lautoken[11];
	char				AP[10];
	char				CloseNR[6];
	char				ReceiptNR[9];
	short				custinqueue;
	char				custnr[21];
	char				cust_taxnr[16];
	char				custname[81];
	char				custaddr[81];
	char				custzip[7];
	char				custcity[46];
	char				zip[7];
	char				licence_plate[10];
	int					chg_amnt;
	bool				voided;
	bool				invoice;
	int					TicketNum;
	int					Total;
	char				Voided_AP[10];
	char				Voided_CloseNR[6];
	char				Voided_ReceiptNR[9];
	char				Voided_Type[2];
} THeader;

typedef struct _t_iupdate {
	char				Dept_Id[5];
	char				Description[40];
	char				Tax_id[2];
	char				Price[12];
	char				EAN[14];
	char				VTSZ[15];
	char				Unit[4];
	bool				isFuel;
	char				Auchan_code[8];
	char				Linked_ean[14];
} TIUpdate;

typedef struct _t_items_file {
	char				EAN[14];
	char				Unit[4];
	bool				isFuel;
} TIItems;


typedef struct _t_ACISConv {
	char				ean[14];
	char				Description[40];
	char				ItemID[4];
	char				VTSZ[10];
} ACISConvStruct;

typedef struct _t_TankVol {
	char				tank_id[2];
	char				fuel_product_id[16];
	char				FuelProductVolume[16];
	char				FuelProductTemperature[6];
	char				Ullage[16];
	char				Density[16];
	char				DensityTC[16];
	char				Mass[16];
	char				WaterVolume[11];
	char				ClosingTemperatureCorrectedVolume[16];
} TankVol;

typedef struct _t_Counters {
	char				tank_id[3];
	char				position_id[3];
	char				fuel_product_id[16];
	char				FuelNozzleID[16];
	char				FuelProductNonResettableAmountNumber[10];
	char				FuelProductNonResettableVolumeNumber[16];
} Counters;

typedef struct _t_GilbClose {
	char				ean[14];
	float				quantity;
	long				value;
	short				done;
} GilbClose;

typedef struct _t_CSC {
	char *custid;
	char *Name;
	char *Zip;
	char *City;
	char *Address;
	char *Licnr;
	char *TaxNR;
	bool create;
} CSCType;

typedef struct _t_tender_details {
	char data[220];
} TenderDetails_list;

typedef struct _t_fuel_prices {
	char ean[20];
	long disc_price;
	char date[20];
	long price;
} FuelPricesType;

typedef struct _t_loyalty_desc {
	int ext_id;
	int disc_value;
	char disc_name[30];
} LoyaltyDescription;

typedef struct _t_tomra_ean {
	char ean[13];
	char tomra[13];
} TomraEAN;


// ---------------------------
// G l o b a l
// ---------------------------

ISS_T_INT_DATE Date;//globally having current transaction's date
ISS_T_INT_DATE TomraExpiryDate;//globally having current transaction's date

char *FNItem=NULL;	//incoming item update file name (c:\AUFuelIF\UpdIn\fhaucp00)
long ProcessLoopDelay=5000; //Amount of waiting time [ms] between checking for new files
bool KeepLooping=true; //Stay in the main loop control
long ExitOnNoTran;	//Exit when no more txn or update to process
long StoreNum=0; //Store number gets extracted from SASSTAT
char *VatConvTable=NULL;	//VAT rate conversion table for item updates
char *smtpserv, *Email,*subject,*from;
long smtpport=25;
int disableEmptyChange=0; //0 if the empty change transaction go into the ISS, 1 if transaction go into VPOP.emptychange table only

bool TDNoGo=true;
ISS_T_INT_DATE TDStartDate;
ISS_T_INT_TIME TDStartTime;
ISS_T_INT_TIME TDRefreshTime=0;

// ---------------------------
// F U E L
// ---------------------------

long FuelFunction=0; //Fuel related processings will be done
char *DNIn=NULL;	//input directory name
char *DNIGASSIn=NULL;	//input directory name
long FirstDispenser=3;
char *ACISDNIn=NULL;	//ACIS input directory name
char *ACISDNOut=NULL;	//ACIS out directory name
char *NewACISDNOut=NULL; //New ACIS out directory name
char *ACISConvTable=NULL;	//ACIS input directory name
char *VPOPDataBase=NULL;	//VPOP adatb�zi el�r�s
char *VPOPServerIP=NULL;	//VPOP adatb�zi el�r�s
char *VPOPUser=NULL;	//VPOP adatb�zi el�r�s
char *VPOPPassw=NULL;	//VPOP adatb�zi el�r�s
char *R910DataBase=NULL;	//VPOP adatb�zi el�r�s
char *R910ServerIP=NULL;	//VPOP adatb�zi el�r�s
char *R910User=NULL;	//VPOP adatb�zi el�r�s
char *R910Passw=NULL;	//VPOP adatb�zi el�r�s

char *DNWork=NULL;	//working directory name
char *DNDone=NULL;	//place of finished files - directory name
char *DNBad=NULL;
char *DNDuplicate=NULL;	//place of duplicated files - directory name
char *DNHist=NULL;	//place of finished files for previous days (closures) - directory name
//char *DNUpdI=NULL;	//place of the incoming (fhaucp00 style) item update file - not used
char *DNUpdO=NULL;	//place to put the item and customer updates
char *IgassUpdO=NULL; //place to put item update file to IGASS
char *FNCSCI=NULL;	//csc input file name (d:\iss\work\csc.chg)
char *FNCSCW=NULL;	//csc workfile name (d:\iss\work\csc.wrk)
char *FNItemList=NULL; //NAMOS items updates
char *FNTomraEAN=NULL; //TOMRA code to EAN converter file

char *FuelPrices=NULL; //NAMOS fuel prices
char *ItemSubst=NULL;	//Item Substitution list
char *FuelUpdFilter=NULL;
char *ItemUpdFilter=NULL;	//Item update file filtering barcode list
long FuelUpdateTimeSill=10800;  //Az az id� ami el�tt 2 �r�t kell be�rni a NAMOS itemUpdate f�jlba
long FuelPlusTimeAfterSill=1800; //Ha a k�sz�b id�t elhagyjuk, akkor mennyi legyen a jelen id�h�z hozz�adva
long FuelPlusTimeBeforeSill=7200; //Ha a k�sz�b id� el�tt, mennyi legyen a jelen id�h�z hozz�adva
long NamosCurrentTime=0; //A NAMOS rendszert�l kapott f�jlban lev� id�
char *NITD=NULL; long NamosISSTimeDIfferent=0; //A NAmos �s az ISS k�z�tti id�elt�r�s
long PosCount=0;	//Number for fuel PoSes
long FuelClose=0;	//Arrived DC closure file
long PosSIDBase=-1;//SID of the first pos (#1)
long TraceLevel=0;	//Tracing level of the processings
long ForceLogOffAtClosure=1; //If closure txn comes, all cashiers will be forced signing off.
TPos *APos=NULL;	//Array of PoS details
GenLog2File L;		//Logging to file & screen mechanism
SINT32 ItemDept[9];	//Item department number (lowest to highest like in ILGREF)
long TaxRate=-1;	//Tax rate used for all items. -1 -> get it at first use
long PerfPeriod=1800; //# of seconds in a perofrmance slot period (30 mins)
long InitCSC=0;		//full CSC file will be generated when !=0; 1:standard naming  2:"FullCSC.dmp"
long LogNoTran=0;	//Put an entry to the log each loop when no file to process
long AmountAsQuantity=1; //Use quantity instead of weight for sales log records
long PriceByCentiLiters=0; //This switches between liter and centiliter based pricing
long QuantityDigitLoss=2; //This controls the quantity conversion from centiliter larger quantities (to liter)
long NoPerfLogMask=2; //The service will not log performance blocks to be sure that for cashiers the items/min measurements remain unchanged
long ZeroTimeTxnTime=15; //0 secs txn time will be replaced by this time
long AutoActions=3; //bit0:ProcSale.AutoLogOn bit1:ProcSale_AutoChgUser
long RefusedActions=1; //bit0:ProcLogOff.Refused logoff of a different user
long FuelZeroSizeRetries=0; //Number of retries to resolve zero size file problem
long FuelZeroSizeRetriesMax=60; //60 cycles=5 minutes
long LastTranHP=0;	//Last HP value used to log any ISS transaction - for fuel
long AutoOpenStartTime=21600;//6:00
long AutoOpenEndTime=64800;//18:00
long AutoOpenEnabled=1;//0=disabled
long CashRounding=0;//0=disabled, others: modulo w/ rounding to the nearest (2,5,10,20,50,100,...)
long GilBarco=1; //GILBARCO system, 1  = true
long namos=1; //NAMOS system, 1  = true
long igass=1; //IGASS system, 1  = true
char *NewACISDir=NULL; //New ACIS automata pumps files
long FirstACISPumpID=7; //The first automata pump totem
long ACISCounter=3; // The counter of new ACIS pump totemt
long NewACISDateCor=1; // The date correction after trading date
long AcisWOEJ;
Array TenderTotals(10);
Array CashierTotals(10);
Array ItemTotals(20);
int CscRefreshCounter=0;
ACISConvStruct AStruct[20];
CSCType csc_type[5001];
long start_counter=0;
int refresh=0;
char *FlexDnOut=NULL;	//Flexys directory out for update
char *FlexDnIn=NULL;	//Flexys directory in for transactions
long FLEXYS=0;
long ACIS_BBOX=0;
bool is_disc_price=false; //There is a new PostPaid discount price 
int disc_price=0,default_price=0;
// ------------------------------------
// R V M
// ------------------------------------

long VoucherSafeOverWriteDays=215;//Number of days in expiration difference to consider safe reusing of voucher ID
long TranTimeIdleLimit=900;//seconds to elapse since last transaction time to perform autologoff
long LocalTimeIdleLimit=1000;//seconds to elapse since last transaction time to perform autologoff
long VoucherPadChar='1'; //ASCII code of placeholder char before the check digit of EAN 13 / or 0 if not used
long VoucherLeadNumber=9903;//Numbers before data in the voucher's EAN 13 including store ID
char VoucherLeadStr[5];//VoucherLeadNumber as string
long RVMFunction=0; //RVM related processings will be done
long TomraFunction=0; //Tomra related processings will be done
char *RDNIn=NULL;	//input directory name
char *RDNWork=NULL;	//working directory name
char *RDNDone=NULL;	//place of finished files - directory name
char *RDNHist=NULL;	//place of finished files for previous days (closures) - directory name
//char *RDNUpdI=NULL;	//place of the incoming (fhaucp00 style) item update file - not used
char *RDNUpdO=NULL;	//place to put the item and customer updates
long RPosCount=0;	//Number for fuel PoSes
long RPosSIDBase=-1;//SID of simulating pos
long RTraceLevel=0;	//Tracing level of the processings
//long ForceLogOffAtClosure=1; //If closure txn comes, all cashiers will be forced signing off.
//long ProcessLoopDelay=5000; //Amount of waiting time [ms] between checking for new files
TRPos RPos;			//Return PoS details
GenLog2File RL;		//Logging to file & screen mechanism
SINT32 RItemDept[9];	//RVM Item department number (lowest to highest like in ILGREF)
SINT32 RItemDeptPET[9];	//PETLOG Item department number (lowest to highest like in ILGREF)
long RSendLinks=0;	//Item update will send linked item records
long RTaxRate=-1;	//Tax rate used for all items. -1 -> get it at first use
long PendingClosure=0; //Waiting for more closure files to come
long RPerfPeriod=1800; //# of seconds in a performance slot period (30 mins)
//long RLogNoTran;		//Put an entry to the log each loop when no file to process
//long AmountAsQuantity=1; //Use quantity instead of weight for sales log records
//long PriceByCentiLiters=0; //This switches between liter and centiliter based pricing
//long QuantityDigitLoss=2; //This controls the quantity conversion from centiliter larger quantities (to liter)
long RNoPerfLogMask=2; //The service will not log performance blocks to be sure that for cashiers the items/min measurements remain unchanged
long RVMTxnTimePerItem=5; //5 secs txn time will be added per item
//long RAutoActions=0; //bit0:ProcSale.AutoLogOn bit1:ProcSale_AutoChgUser
//long RefusedActions=0; //bit0:ProcLogOff.Refused logoff of a different user
long RUser=1111;	//Cashier code that is being used for all transactions
long RTenderId=11;	//Tender ID that is being used by the virtual cashier
long RTenderIdPET=13;	//Tender ID that is being used by the virtual cashier for PETLOG returns
char RUserS[10]={0,0,0,0,0,0,0,0,0,0};
char LastTranDateS[9]="00000000";
Array MachineTotals(10);//id=machineid, val=value
Array TicketIssue(1,1);//machineid,firstvoucher,lastvoucher (voucher counter only)
FlexArray RItemTotals(sizeof(long)+sizeof(ISS_GDECIMAL_6),2*sizeof(long),20);//machineid,itemid,count,value
FlexArray GItemTotals(sizeof(long)+sizeof(ISS_GDECIMAL_6),2*sizeof(long),2*sizeof(long));//machineid,itemid,count,value
int RoundVocher; //Round the voucher value 21->25, 24->25, 26->30, 29->30
int keepDays = 100;
// =============  TOMRA  ==============
bool is_tomra=false;
int TomraExpireDays=31;
FuelPricesType FuelPriceArray[10];


// ===========================================================

long ProcLogOn(char *s);
long ProcLogOff(char *s);
int RSaveStatus(void);
char* Select_MYSQL(char *s);
char* slice_string (char *string, char sep_char, int piece);
int NAMOS_ProcClose();
// ===========================================================


void SetActualFuelPrice(){
	char sql[1024],sql_ins[1024],tmp[40];
	char a[20],b[50];
	int xt=-1,i;
	SYSTEMTIME t;
	GetLocalTime(&t);
	char last_date[30];
	long csc_counter=0;
	MYSQL *hnd=NULL; // mysql connection handle
	const char *sinf=NULL; // mysql server information
	bool db_ok=false;
	hnd = mysql_init(NULL);
	//MYSQL_RES *res=NULL; // result of asking the database for a listing of its tables
	long res,partn,ins;
	MYSQL_ROW row; // one row from the result set
	MYSQL_RES *queryResult_ptr;
	char *dateTime=NULL;
	int x=0;
	Reg r(false, C_RegKey);
	disc_price=0; default_price=0;
	sprintf(a,"%.4d%.2d%.2d%.2d%.2d00",t.wYear,t.wMonth,t.wDay,t.wHour,t.wMinute);
	if(NULL == mysql_real_connect(hnd,R910ServerIP,R910User,R910Passw,"Petrol_card",0,NULL,0)){
		db_ok=false;
		for(int x=0;x<10;x++) {
			Sleep(10000);
			L.LogE("Problem encountered connecting to the Petrol_card database on %s with user %s. %d (%s)\n",R910ServerIP,R910User,GetLastError(),mysql_error(hnd));

			if(NULL != mysql_real_connect(hnd,R910ServerIP,R910User,R910Passw,"Petrol_card",0,NULL,0)){
				x=10;
				db_ok=true;
			}
		}
	}
	else {
		db_ok=true;
		L.Log("Succesfully connected to the Petrol_card database on %s.\n",R910ServerIP);	
	}
	if (db_ok) {
		sprintf(sql,"SET NAMES cp1250");
		mysql_query(hnd,sql);
		while (FuelPriceArray[x].price>0){
			sprintf(sql,"INSERT INTO `Petrol_card`.`PP_fuel_prices` (`ean`, `disc_price`, `default_price`,updated, `store_nr`) VALUES ('%s', '%d', '%d',now(), '%d')",FuelPriceArray[x].ean,FuelPriceArray[x].disc_price,FuelPriceArray[x].price,StoreNum);
			L.Log("SQL: %s\n",sql);
			res=mysql_query(hnd,sql);
			if (res)
				L.LogE("ERROR - Cannot read Petrol_card.PP_fuel_prices database!\n %s\n%s\n",sql,mysql_error(hnd));
			else {
				L.LogE("Set the actual fuel prices\n");
			}
			x++;
		}
		mysql_close(hnd);
	}
}


void set_fuel_price(){
	char tmp[500];
	int x=0;
	char buffer[20];
	Reg r(false, C_RegKey);

	tmp[0]=0;
	while (FuelPriceArray[x].price>0){
		strcat(tmp,FuelPriceArray[x].ean);
		strcat(tmp,";");
		itoa(FuelPriceArray[x].price,buffer,10);
		strcat(tmp,buffer);
		strcat(tmp,";");
		itoa(FuelPriceArray[x].disc_price,buffer,10);
		strcat(tmp,buffer);
		strcat(tmp,";");
		strcat(tmp,FuelPriceArray[x].date);
		strcat(tmp,"|");
		x++;
	}
	r.SetValueSZ("Fuel.NAMOS.Price",tmp);
	SetActualFuelPrice();
}

void get_fuel_prices(char *tmp){

 char seps[]   = "|\n";
 char *token;
 int x=0,counter=0;
	token = strtok( tmp, seps );
	for(int i=0;i<strlen(token);i++){
		if(token[i]==';') 
			counter++;
	}
	if(counter==3) //Ez m�r tartalmazza a kedvezm�nyes �rat is
		while( token != NULL )	{
			sprintf(FuelPriceArray[x].ean,"%s",slice_string(token,';',1));
			FuelPriceArray[x].ean[13]=0;
			FuelPriceArray[x].price=atoi(slice_string(token,';',2));
			FuelPriceArray[x].disc_price=atoi(slice_string(token,';',3));
			sprintf(FuelPriceArray[x].date,"%s",slice_string(token,';',4));
			token = strtok( NULL, seps );	
			x++;
		}
	if(counter==2) // M�g nem tartalmazza a kedvezm�nyes �rat
		while( token != NULL )	{
			sprintf(FuelPriceArray[x].ean,"%s",slice_string(token,';',1));
			FuelPriceArray[x].ean[13]=0;
			FuelPriceArray[x].price=atoi(slice_string(token,';',2));
			FuelPriceArray[x].disc_price=0;
			sprintf(FuelPriceArray[x].date,"%s",slice_string(token,';',3));
			token = strtok( NULL, seps );	
			x++;
		}

}


void send_message(char *message)
{
 /*   bool bError = false;

    try
    {
        CSmtp mail;

        mail.SetSMTPServer(smtpserv,smtpport);
        mail.SetSenderName(from);
        mail.SetSubject(subject);
        mail.AddRecipient(Email);
        mail.SetXPriority(XPRIORITY_NORMAL);
        mail.SetXMailer("The Bat! (v3.02) Professional");
        mail.AddMsgLine(message);
    
        mail.Send();
    }
    catch(ECSmtp e)
    {

        RL.LogE("Error: %s\n",e.GetErrorText().c_str());
        bError = true;
    }

    if(!bError)
    {
        RL.Log(  "Mail was send successfully.\n");
//        return 0;
    }
  //  else
    //    return 1;
*/		
}

bool isRunning(char* pName)
{
	char tmp[80];
	int count=0;
	unsigned long aProcesses[1024], cbNeeded, cProcesses;
	if(!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded)) {
		L.Log("Cannot get processes!\n");
		return true;
	}

	cProcesses = cbNeeded / sizeof(unsigned long);
	L.Log("Number of processes:%d\n",cProcesses);
	for(unsigned int i = 0; i < cProcesses; i++)
	{
		if(aProcesses[i] == 0)
			continue;

		HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, 0, aProcesses[i]);
		char buffer[50];
		GetModuleBaseName(hProcess, 0, buffer, 50);
		CloseHandle(hProcess);
		strcpy(tmp,buffer);
		//tmp.MakeUpper();
		L.Log("%d. Process name: %s\n",i,buffer);
		if(_stricmp(pName,tmp)==0) {
			L.Log("The %s program is running!\n",pName);
			count++;
		}
	}
	L.Log("Count: %d\n",count);
	if (count>1)
		return true;
	L.LogE("ERROR - The %s program is running! Cannot start %s in 2 copies!\n",pName,pName);
	return false;
}



bool IsNumber(const char* s, long len) //Check for 'len' characters to be '0'..'9'
{
	while (len>0) {
		len--;
		
		if ((s[len]<'0')||(s[len]>'9')) 
			if(s[len]!='.')
				if(s[len]!='-')
					return false;
	}
	return true;
}

void strcpyi(char* d, const char *s, int i) //Copy i characters to the d buffer from the s buffer
  {
   while (i--) *(d++)=*(s++);
  }

char* strmid(const char *s, int i) //Copy i characters to the d buffer from the s buffer
  {
   char ff[100];
   int x=0;
   
   while (i--){ 
	   ff[x]=s[x];
	   x++;
   }
   ff[x]=0;
   printf("strmid:%s\n",ff);
   return ff;
  }


char* KeepNumber(const char* s) //Check for 'len' characters to be '0'..'9'
{
	int len=0,i=0,x=0;
	char b[200];
	len=strlen(s);
	while (x<len) {
		if ((s[x]<'0')||(s[x]>'9')) 
			if ((s[x]<'A')||(s[x]>'Z'))
				if((s[x]<'a')||(s[x]>'z')){
			x++;
			continue;
		}
			b[i]=s[x];
			i++;
			x++;
	}
	b[i]=0;
//	strcpy(s,b);
	return b;
}

wchar_t *CodePageToUnicode(int codePage, const char *src)
    {
    if (!src) return 0;
    int srcLen = strlen(src);
    if (!srcLen)
	{
	wchar_t *w = new wchar_t[1];
	w[0] = 0;
	return w;
	}
	
    int requiredSize = MultiByteToWideChar(codePage,
        0,
        src,srcLen,0,0);
	
    if (!requiredSize)
        {
        return 0;
        }
	
    wchar_t *w = new wchar_t[requiredSize+1];
    w[requiredSize] = 0;
	
    int retval = MultiByteToWideChar(codePage,
        0,
        src,srcLen,w,requiredSize);
    if (!retval)
        {
        delete [] w;
        return 0;
        }
	
    return w;
    }

char *UnicodeToCodePage(int codePage, const wchar_t *src)
    {
    if (!src) return 0;
    int srcLen = wcslen(src);
    if (!srcLen)
	{
	char *x = new char[1];
	x[0] = '\0';
	return x;
	}
	
    int requiredSize = WideCharToMultiByte(codePage,
        0,
        src,srcLen,0,0,0,0);
	
    if (!requiredSize)
        {
        return 0;
        }
	
    char *x = new char[requiredSize+1];
    x[requiredSize] = 0;
	
    int retval = WideCharToMultiByte(codePage,
        0,
        src,srcLen,x,requiredSize,0,0);
    if (!retval)
        {
        delete [] x;
        return 0;
        }
	
    return x;
    }


void ConvSpecChar(unsigned char *i)
{
	while (*i) {
		switch (*i) {
			case 213: *i='O'; break;
			//case 211: *i='O'; break;
			case 245: *i='o'; break;
			//case 243: *i='o'; break;
			case 205: *i='I'; break;
			case 237: *i='i'; break;
			case 219: *i='U'; break;
			//case 218: *i='U'; break;
			//case 252: *i='u'; break;
			case 251: *i='u'; break;
			//case 250: *i='u'; break;
			case '?': *i=' '; break; 
			case '&': *i=' '; break;
			case ';': *i=' '; break;
			case '~': *i=' '; break;
			case '^': *i=' '; break;
			case '%': *i=' '; break;
			case '@': *i=' '; break;
			case '#': *i=' '; break;
		}	
		i++;
	}
}





void ConvChar852ToWin(unsigned char *i)
{
	while (*i) {
		switch (*i) {
			case 129: *i=252; break;
			case 160: *i=225; break;
			case 161: *i=237; break;
			case 162: *i=243; break;
			case 163: *i=250; break;
			case 148: *i=246; break; //�
			case 139: *i=246; break; //�
			case 130: *i='�'; break;
			case 251: *i='�'; break;
			case 181: *i=193; break;
			case 144: *i=201; break;
			case 214: *i=205; break;
			case 224: *i=211; break;
			case 153: *i=214; break;
			case 138: *i=214; break; //�
			case 233: *i=218; break;
			case 154: *i=220; break;
			case 235: *i='�'; break;
			//case 114: *i=' '; break;
			case 150: *i='�'; break; //u
			case 234: *i='�'; break; //U
			case 228: *i='�'; break; //o
			case 229: *i='�'; break; //O
			case 132: *i=228; break;  //�
			case 142: *i=196; break;  //�
		}	
		i++;
	}
}




void ConvCharWinTo852(unsigned char *i)
{
	while (*i) {
		switch (*i) {
			case 252: *i=129; break;
			case 225: *i=160; break;
			case 237: *i=161; break;
			case 243: *i=162; break;
			case '�': *i=163; break;
			case 246: *i=148; break;
			case 245: *i=139; break;
			case 233: *i=130; break;
			case 251: *i=251; break;
			case 193: *i=181; break;
			case 201: *i=144; break;
			case 205: *i=214; break;
			case 211: *i=224; break;
			case 214: *i=153; break;
			case 213: *i=138; break;
			case 218: *i=233; break;
			case 220: *i=154; break;
			case 219: *i=235; break;
		}	
		i++;
	}
}


bool StrSpaceExtend(char *s, int len) //Will return true if an actual extension happened
{
	bool b;
	for (b=false;len>0;len--) {
		if (*s==0) b=true;
		if (b) *s=' ';
		s++;
	}
	*s=0;
	return b;
}

char* StrCharExtend(char *s, int len, char e) //Will return true if an actual extension happened
{
	bool b=false;
	char d[200];
	char t[220];
	int i=0;
	
	for (int a=0;a<len;a++){
		d[a]=e;
		//if (a==0) strcpy();
		//else strcat(d,(char *)e);
		
	}
	i=strlen(s);
	d[a]=0;
	strcpy(t,d+i);
	strcat(t,s);
	s=t;
	//strcpy(s,t);
	
	return t;
}


int remove_lchar(char *p,char s[1]) 
{
	int l=0,t=1;

	l=strlen(p);
	while ((t>0)&&(l>1)){
		t=0;
		if(p[0]==s[0]) {
			for(t=1;t<l;t++)
				p[t-1]=p[t];
			l--;
		}
	}
	p[l]=0;
	return l;
}

int remove_rchar(char *p,char s[1]) 
{
	int l=0,t=1;

	l=strlen(p);
	while (t>0) {
		t=0;
		if(p[l-1]==s[0]) {
			l--;
			t=l;
		}
		//if(l==1) t=0;
	}
	if (l==0) l=1;
	p[l]=0;
	return l;
}

char* slice_string (char *string, char sep_char, int piece){
	char d[200];
	int counter=1,x=0;
	for(int i=0;i<strlen(string);i++){
		if(counter==piece){
			d[x]=string[i];
			x++;
		}
		if(string[i]==sep_char) 
			counter++;
	}
	if(x>0) 
		d[x]=0;
	else {
		d[0]=0;
		strcpy(d,"END");
		L.Log("Nincs m�r t�bb r�sz!\n");
	}
return d;
}

char* Chk4AuchanCode(char* string){
	char *s;
	char a[230];
	remove_lchar(string,"0");
	sprintf(a,"SELECT id,auchan_code FROM Infodesk2.Price_Update where ean ='%s' order by day desc,time desc limit 1",string);
	s=Select_MYSQL(a);
	char sep[]   = "|\n";
	strtok( s, sep );
	long id=atol(strtok( NULL, sep ));
	L.Log("The id is: %d\n",id);
	if(id>0) return strtok( NULL, sep );
	else return "0";
}

long GetNamosTranFromDB(char *begin_date,char *trans_id,int pos_number){
	char a[230];
	char* s;
	sprintf(a,"SELECT id FROM Infodesk2.namos_conv where orig_date='%s' and trans_id=%s and pos_no=%d",begin_date,trans_id,pos_number);
	s=Select_MYSQL(a);
	char sep[]   = "|\n";
	if(strcmp(s,"-1")!=0){
		strtok( s, sep );
		long id=atol(strtok( NULL, sep ));
		L.Log("The id is: %d\n",id);
		if(id>0) return id;
		else return -1;
	}
	return -1;
}

void RemoveLetter(char* s) // Numbers return only
{
	int len;
	len=strlen(s);
	while (len>0) {
		len--;
		if ((s[len]<'0')||(s[len]>'9')) 
			s[len]=' ';
	}
	remove_lchar(s," ");
	remove_rchar(s," ");
}

long RegCSC(char csc_id[22],char company_name[31],char company_name2[31],char addr1[31],char addr2[31],char town[31],char zip[11],char nip[16],char lp[15])//
{
	long expdate;
//	long dt;
	FILE* F;
	ISS_T_BOOLEAN test;
	bool hasprev=false;	//When a voucher with same ID has been found on file
	SYSTEMTIME st;
	GetLocalTime(&st);
	char *aa;
	char cs[22];
	char tmp[31];
	char fl[230];//,f2[230];
	
	expdate=DateYMD2Int(st.wYear,st.wMonth,st.wDay);

	
    ISS_T_REPLY        result=ISS_SUCCESS;
    ISS_T_REPLY        result2=ISS_SUCCESS;
    UINT16             buffer_id;
    ISS_T_DB_KEY       key[1];
    //ISS_GDECIMAL_6     item_code;

    // Get the item read from the database first, with exclusive lock
	strcpyi(cs,csc_id,21);
	aa=StrCharExtend(csc_id,21,'0');
	aa[21]=0;
	strcpy(cs,aa);
	sprintf(fl,"%s",aa+1);
    key[0].field_name          = "CUSTOMER_ID";
    key[0].field_length        = sizeof(cs);
    key[0].field_value         = cs;
    key[0].field_value_unknown = FALSE;

    result=ReadISS(&buffer_id,true,"CSC",1,key,ISS_C_DB_EQUAL,ISS_C_DB_EXCLUSIVE_LOCK
		,"FREQ_SHOPPER",4,&test,false);

	if (result==ISS_SUCCESS) {//found on file!
		
			ISS_T_BOOLEAN flag=FALSE;
			L.Log("Info: RegCSC: Customer has been found with same ID.\n");
			hasprev=true;
	
			

	}
    else {
		if (result!=ISS_E_SDBMS_NOREC) {
			L.LogE("ERROR: RegCSC: Unexpected error during checking existence: %d!\n",result);
			if ((result2=iss_db_end_su())!=ISS_SUCCESS)
				L.LogE("ERROR: RegCSC: Cannot end success unit! retval=%d\n",result2);
			return result;
		}
		//Begin success unit for writing the database
		//Gotta create buffer - voucher is not in the database yet
		if ((result=iss_db_create_buffer("CSC", &buffer_id))!=ISS_SUCCESS) {
			L.LogE("ERROR: RegCSC: Cannot create buffer! retval=%d\n",result);
			if ((result2=iss_db_end_su())!=ISS_SUCCESS)
				L.LogE("ERROR: RegCSC: Cannot end success unit! retval=%d\n",result2);
			return result;
		}
		if ((result=iss_db_update_field(buffer_id, "CUSTOMER_ID", sizeof(cs), 0
				,&cs, FALSE))!=ISS_SUCCESS) {
			L.LogE("ERROR: RegCSC: Cannot update field CUSTOMER_ID! retval=%d\n",result);
			if ((result=iss_db_free_buffer(&buffer_id))!=ISS_SUCCESS)
				L.LogE("ERROR: RegCSC: Cannot free buffer! retval=%d\n",result);
			if ((result=iss_db_end_su())!=ISS_SUCCESS)
				L.LogE("ERROR: RegCSC: Cannot end success unit! retval=%d\n",result);
			return result;
		}
		if ((result=iss_db_update_field(buffer_id, "LAST_PURCH_DATE", sizeof(expdate), 0
					,&expdate, FALSE))!=ISS_SUCCESS) {
				L.LogE("ERROR: RegCSC: Cannot update field LAST_PURCH_DATE! retval=%d\n",result);
				if ((result2=iss_db_end_su())!=ISS_SUCCESS)
					L.LogE("ERROR: RegCSC: Cannot end success unit! retval=%d\n",result2);
				return result;
		}
		if ((result=iss_db_update_field(buffer_id, "DATE_CREATION", sizeof(expdate), 0
					,&expdate, FALSE))!=ISS_SUCCESS) {
				L.LogE("ERROR: RegCSC: Cannot update field DATE_CREATION! retval=%d\n",result);
				if ((result2=iss_db_end_su())!=ISS_SUCCESS)
					L.LogE("ERROR: RegCSC: Cannot end success unit! retval=%d\n",result2);
				return result;
		}
    }
	//Common part of the update
		strcpyi(tmp,company_name,30);
		tmp[30]=0;
		strcpyi(aa,tmp,24);
		StrSpaceExtend(aa,24);
		sprintf(fl,"%s%s",fl,aa);
		ConvCharWinTo852((unsigned char *)tmp);
		StrSpaceExtend(tmp,30);
		if ((result=iss_db_update_field(buffer_id, "NOM", sizeof(tmp), 0
				,&tmp, FALSE))!=ISS_SUCCESS) {
			L.LogE("ERROR: RegCSC: Cannot update field NOM! retval=%d\n",result);
			if ((result2=iss_db_end_su())!=ISS_SUCCESS)
				L.LogE("ERROR: RegCSC: Cannot end success unit! retval=%d\n",result2);
			return result;
		}
		strcpyi(tmp,lp,14);
		tmp[14]=0;
		aa=KeepNumber(tmp);
		aa[6]=0;
		StrSpaceExtend(aa,6);
		sprintf(fl,"%s%s",fl,aa);
		ConvCharWinTo852((unsigned char *)tmp);
		StrSpaceExtend(tmp,14);
		if ((result=iss_db_update_field(buffer_id, "NO_SIRET", sizeof(tmp), 0
				,&tmp, FALSE))!=ISS_SUCCESS) {
			L.LogE("ERROR: RegCSC: Cannot update field NO_SIRET! retval=%d\n",result);
			if ((result2=iss_db_end_su())!=ISS_SUCCESS)
				L.LogE("ERROR: RegCSC: Cannot end success unit! retval=%d\n",result2);
			return result;
		}
		strcpyi(tmp,company_name2,30);
		tmp[30]=0;
		StrSpaceExtend(tmp,30);
		sprintf(fl,"%s%s",fl,tmp);
		ConvCharWinTo852((unsigned char *)tmp);
		if ((result=iss_db_update_field(buffer_id, "PRENOM", sizeof(tmp), 0
				,&tmp, FALSE))!=ISS_SUCCESS) {
			L.LogE("ERROR: RegCSC: Cannot update field PRENOM! retval=%d\n",result);
			if ((result2=iss_db_end_su())!=ISS_SUCCESS)
				L.LogE("ERROR: RegCSC: Cannot end success unit! retval=%d\n",result2);
			return result;
		}
		strcpyi(tmp,zip,10);
		tmp[10]=0;
		StrSpaceExtend(tmp,10);
		if ((result=iss_db_update_field(buffer_id, "CODE_POSTAL", sizeof(tmp), 0
				,&tmp, FALSE))!=ISS_SUCCESS) {
			L.LogE("ERROR: RegCSC: Cannot update field CODE_POSTAL! retval=%d\n",result);
			if ((result2=iss_db_end_su())!=ISS_SUCCESS)
				L.LogE("ERROR: RegCSC: Cannot end success unit! retval=%d\n",result2);
			return result;
		}
		sprintf(fl,"%s%s",fl,tmp);
		strcpyi(tmp,town,30);
		tmp[30]=0;
		StrSpaceExtend(tmp,30);
		sprintf(fl,"%s%s",fl,tmp);
		ConvCharWinTo852((unsigned char *)tmp);
		if ((result=iss_db_update_field(buffer_id, "VILLE", sizeof(tmp), 0
				,&tmp, FALSE))!=ISS_SUCCESS) {
			L.LogE("ERROR: RegCSC: Cannot update field VILLE! retval=%d\n",result);
			if ((result2=iss_db_end_su())!=ISS_SUCCESS)
				L.LogE("ERROR: RegCSC: Cannot end success unit! retval=%d\n",result2);
			return result;
		}
		strcpyi(tmp,addr1,30);
		//tmp[30]=0;
		strcpy(aa,tmp);
		StrSpaceExtend(aa,40);
		sprintf(fl,"%s%s",fl,aa);
		ConvCharWinTo852((unsigned char *)tmp);
		StrSpaceExtend(tmp,30);
		if ((result=iss_db_update_field(buffer_id, "ADRESSE", sizeof(tmp), 0
				,&tmp, FALSE))!=ISS_SUCCESS) {
			L.LogE("ERROR: RegCSC: Cannot update field ADRESSE_0! retval=%d\n",result);
			if ((result2=iss_db_end_su())!=ISS_SUCCESS)
				L.LogE("ERROR: RegCSC: Cannot end success unit! retval=%d\n",result2);
			return result;
		}
		strcpyi(tmp,addr2,30);
		tmp[30]=0;
		strcpy(aa,tmp);
		StrSpaceExtend(aa,40);
		sprintf(fl,"%s%s",fl,aa);
		ConvCharWinTo852((unsigned char *)tmp);
		StrSpaceExtend(tmp,30);
		if ((result=iss_db_update_field(buffer_id, "ADRESSE", sizeof(tmp), 1
				,&tmp, FALSE))!=ISS_SUCCESS) {
			L.LogE("ERROR: RegCSC: Cannot update field ADRESSE_1! retval=%d\n",result);
			if ((result2=iss_db_end_su())!=ISS_SUCCESS)
				L.LogE("ERROR: RegCSC: Cannot end success unit! retval=%d\n",result2);
			return result;
		}
		strcpyi(tmp,nip,15);
		tmp[15]=0;
		ConvCharWinTo852((unsigned char *)tmp);
		StrSpaceExtend(tmp,15);
		if ((result=iss_db_update_field(buffer_id, "NIP", sizeof(tmp), 0
				,&tmp, FALSE))!=ISS_SUCCESS) {
			L.LogE("ERROR: RegCSC: Cannot update field NIP! retval=%d\n",result);
			if ((result2=iss_db_end_su())!=ISS_SUCCESS)
				L.LogE("ERROR: RegCSC: Cannot end success unit! retval=%d\n",result2);
			return result;
		}
		if ((result=iss_db_update_field(buffer_id, "LAST_PURCH_DATE", sizeof(expdate), 0
				,&expdate, FALSE))!=ISS_SUCCESS) {
			L.LogE("ERROR: RegCSC: Cannot update field LAST_PURCH_DATE! retval=%d\n",result);
			if ((result2=iss_db_end_su())!=ISS_SUCCESS)
				L.LogE("ERROR: RegCSC: Cannot end success unit! retval=%d\n",result2);
			return result;
		}
		/*if ((F=fopen(FNCSCI,"a+"))!=NULL) {
			fprintf(F,"C%s%s\n",fl,tmp);		
		}
		else
			L.LogE("ERROR - Cannot open % file! \n",FNCSCI);
		fclose(F);*/
		//char csc_id[22],char company_name[31],char company_name2[31],char addr1[31],char addr2[31],char town[31],char zip[11],char nip[16],char lp[15]
		if ((F=fopen("d:\\iss\\work\\csc2db.chg","a+"))!=NULL) {
			fprintf(F,"%s             %s\n",fl,tmp);		
		}
		else
			L.LogE("ERROR - Cannot open D:\\iss\\work\\csc2db.chg file! \n");
		fclose(F);

    //Write buffer to DB
    if (result==ISS_SUCCESS) {
        if ((result=iss_db_write_buffer(&buffer_id))!=ISS_SUCCESS)
			L.LogE("ERROR: RegCSC: Cannot write buffer to DB! retval=%d\n",result);
    }
    //Finish transaction
    if (result==ISS_SUCCESS) {
        if ((result=iss_db_end_su())!=ISS_SUCCESS)
			L.LogE("ERROR: RegCSC: Cannot end success unit! retval=%d\n",result);
    }
    //Abort transaction upon error
    if (result!=ISS_SUCCESS) {
        if (iss_db_abort_su()!=ISS_SUCCESS) 
			L.LogE("ERROR: RegCSC: Cannot abort success unit! retval=%d\n",result);
    }
    return result;
}

char* remove_char(char *p,char s[1]) 
{
	int l=0,t=0,x;
	l=strlen(p);
	while (t<l){
		if(p[t]==s[0]) {
			for(x=t+1;x<l;x++)
				p[x-1]=p[x];
			l--;
		}
		t++;
	}
	p[l]=0;
	return p;
} 



void WriteRegister2RegOld(int colID,int nozzleID,char* count)
{
Reg r(false, C_RegKey);
char tmp[40],tmp1[450],tmp2[450];
int result=0;
char zero[15]="00000000000000";
char *Registers=NULL;
bool found=false;
float counter,regcounter=0;
char *pdest;

L.Log("ACIS Registers - Write colID: %.2d nozzleID: %d and Counter:%s to registry!\n",colID, nozzleID, count);
	if (!Registers) r.ValueSZ("Fuel.ACIS.Registers",&Registers,"");
	int lgth=strlen(Registers);
	counter=atof(count);
//remove_char(tss,".")
	int ch='.';
	pdest = strrchr( count, ch );
    result = pdest - count + 1;
    if( pdest != NULL ){
		result=result-strlen(count);
		if(result==-1) strcat(count,"0");
	}
	else
		strcat(count,"00");

	count=remove_char(count,".");
	zero[12-strlen(count)]=0;
	sprintf(tmp,"%.2d%d%s%s",colID,nozzleID,zero,count);
	tmp[15]=0;
	if (lgth==0){
		L.Log("ACIS Registers - There isn't any counter in the Registry! %s\n",tmp);
		r.SetValueSZ("Fuel.ACIS.Registers",tmp);
	}
	else {
		for (int i=0; i<=lgth;i=i+15) {
			if (strncmp(tmp,Registers+i,3)==0){
				found=true;
				strcpy(tmp2,Registers);
				strncpy(tmp2,Registers+i,15);
				if (strncmp(tmp2+i,tmp,15)<0) {
					strncpy(tmp2+i,tmp,15);
					L.Log("ACIS Registers - Found the counter in the Registry! %s\n",tmp);
					r.SetValueSZ("Fuel.ACIS.Registers",tmp2);
				}
				else 
					L.Log("WARRNING - ACIS Registers - Found the counter in the Registry but the new one is less ! %s\n",tmp);
			}
		}
		if (!found) {
			sprintf(tmp1,"%s%s",Registers,tmp);
			L.Log("ACIS Registers - Concatenate the counter to the Registry! %s\n",tmp);
			r.SetValueSZ("Fuel.ACIS.Registers",tmp1);
		}
	}
}

void WriteRegister2Reg(int colID,int nozzleID,char* count)
{
Reg r(false, C_RegKey);
char tmp[40],tmp1[450],tmp2[450];
long reg_counter[200];
for (int x=0;x<200;x++)
	reg_counter[x]=0;
int id=0;
int result=0;
char zero[15]="00000000000000";
char *Registers=NULL;
bool found=false;
float counter,regcounter=0;
char *pdest;

L.Log("ACIS Registers - Write colID: %.2d nozzleID: %d and Counter:%s to registry!\n",colID, nozzleID, count);
	if (!Registers) r.ValueSZ("Fuel.ACIS.Registers",&Registers,"");
	int lgth=strlen(Registers);
	if(lgth>0){
		for (int i=0; i<lgth;i=i+15) {
			strncpy(tmp2,Registers+i,3);
			tmp2[3]=0;
			id=atoi(tmp2);
			strncpy(tmp2,Registers+i+3,12);
			tmp2[12]=0;
			reg_counter[id]=atol(tmp2);
		}
	}
	counter=atof(count)*100;
	sprintf(tmp,"%d%d",colID,nozzleID);
	id=atoi(tmp);
	if(counter>reg_counter[id])
		reg_counter[id]=counter;
	else
		L.Log("WARRNING - ACIS Registers - Found the counter in the Registry but the new one is less ! %d %d old:%d new:%d\n",colID,nozzleID,reg_counter[id],counter);
	strcpy(tmp2,"");
	for(x=0;x<200;x++){
		if(reg_counter[x]!=0){
			sprintf(tmp1,"%.3d%.12d",x,reg_counter[x]);
			strcat(tmp2,tmp1);
			L.Log("ACIS Registers - The new register value: %.2d -  %.12d\n");
		}
	}
	L.Log("ACIS Registers - Concatenate the counter to the Registry! %s\n",tmp2);
	if(strlen(tmp2)>0 && (strlen(tmp2) % 15)==0);
		r.SetValueSZ("Fuel.ACIS.Registers",tmp2);
}


time_t TimeFromSystemTime(const SYSTEMTIME * pTime)
{
struct tm tm;
	memset(&tm, 0, sizeof(tm));
	tm.tm_year = pTime->wYear - 1900;
	tm.tm_mon = pTime->wMonth - 1;
	tm.tm_mday = pTime->wDay;

	tm.tm_hour = pTime->wHour;
	tm.tm_min = pTime->wMinute;
	tm.tm_sec = pTime->wSecond;

return mktime(&tm);
}





void WriteCSC2XML(char *cusid,char *Nam,char *Zp,char *Cty,char *Addres,char *Lcnr,char *TxNR,bool create, int number, int seq_num){
		long expdate;
//	long dt;
char custid[120];
char Name[120];
char Zip[20];
char City[120];
char Address[120];
char Licnr[20];
char TaxNR[20];
strcpy(custid,cusid);
strcpy(Name,Nam);
strcpy(Zip,Zp);
strcpy(City,Cty);
strcpy(Address,Addres);
strcpy(Licnr,Lcnr);
strcpy(TaxNR,TxNR);
	SYSTEMTIME st,t;
	GetLocalTime(&st);
	char tmp[125];
	char ft1 [5];
	int xt=0;
	expdate=DateYMD2Int(st.wYear,st.wMonth,st.wDay);
		/********************  GILBARCO **********************/
			char tmp1[100],csc[81];
			int leng=0,leng1=0;
		/************************  ACIS CSC file   *****************************/
			TiXmlDocument ACISdocwrite;  
			TiXmlElement* msg;
 			TiXmlDeclaration* ACISdecl = new TiXmlDeclaration( "1.0", "windows-1250", "" );  
			ACISdocwrite.LinkEndChild( ACISdecl ); 
			TiXmlElement * ACISwroot = new TiXmlElement( "GVRXML-MaintenanceRequest" );  
			ACISdocwrite.LinkEndChild( ACISwroot );
			ACISwroot->SetAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
			ACISwroot->SetAttribute("xmlns:xsd","http://www.w3.org/2001/XMLSchema");
			ACISwroot->SetAttribute("version","3.3");
			ACISwroot->SetAttribute("xmlns","http://www.gilbarco.com/POSBO/Vocabulary/2006-04-18");
			/******************* TransmissionHeader   ******************/
			TiXmlElement * ACISTransHead = new TiXmlElement( "TransmissionHeader" );  
			ACISwroot->LinkEndChild( ACISTransHead );
			ACISTransHead->SetAttribute("xmlns","http://www.naxml.org/POSBO/Vocabulary/2003-10-16");
			msg = new TiXmlElement( "StoreLocationID" );  
			msg->LinkEndChild( new TiXmlText( "HUNGARY" ));  
			ACISTransHead->LinkEndChild( msg );  
			msg = new TiXmlElement( "VendorName" );  
			msg->LinkEndChild( new TiXmlText( "HUNGARY" ));  
			ACISTransHead->LinkEndChild( msg ); 
			msg = new TiXmlElement( "VendorModelVersion" );  
			msg->LinkEndChild( new TiXmlText( "1.0" ));  
			ACISTransHead->LinkEndChild( msg ); 
			/******************* CustomerMaintenance   ******************/
			TiXmlElement * ACISCustMaint = new TiXmlElement( "CustomerMaintenance" );  
			ACISwroot->LinkEndChild( ACISCustMaint);
			TiXmlElement * ACISTableAct = new TiXmlElement( "TableAction" );  
			ACISCustMaint->LinkEndChild( ACISTableAct );
			ACISTableAct->SetAttribute("type","initialize");
			
			TiXmlElement * ACISRecAct = new TiXmlElement( "RecordAction" );  
			ACISCustMaint->LinkEndChild( ACISRecAct );
			ACISRecAct->SetAttribute("type","create");
//				ConvChar852ToWin((unsigned char *)a);
			L.Log("Create ACIS CSC file! xt:%d\n",number);
			for (xt=1;xt<=number;xt++) {
				if (strlen(cusid)==0){
					strcpy(custid,csc_type[xt].custid);
					L.Log("Writecsc - :Customer ID: %s\n",custid);
					ConvSpecChar((unsigned char *)csc_type[xt].Name);
					sprintf(Name,"%s",csc_type[xt].Name);
					L.Log("Writecsc - Customer name: %s\n",Name);
					sprintf(Zip,"%s",csc_type[xt].Zip);
					ConvSpecChar((unsigned char *)csc_type[xt].City);
					sprintf(City,"%s",csc_type[xt].City);
					ConvSpecChar((unsigned char *)csc_type[xt].Address);
					sprintf(Address,"%s",csc_type[xt].Address);
					sprintf(Licnr,"%s",csc_type[xt].Licnr);
					sprintf(TaxNR,"%s",csc_type[xt].TaxNR);
					create=csc_type[xt].create;
				}
				
				TiXmlElement * ACISCSTDetail = new TiXmlElement( "CSTDetail" );  
				ACISCustMaint->LinkEndChild( ACISCSTDetail );
				msg = new TiXmlElement( "RecordAction" );
				if (!create)
					msg->SetAttribute( "type","delete" );
				else
					msg->SetAttribute( "type","create" );
				ACISCSTDetail->LinkEndChild( msg ); 
				//ACIS csak 10 karakteren tudja fogadni, ha kisebb, akkor baj van
				char zero[16];
				strcpy(zero,"000000000000000");
				int h=6;
				int idlength=strlen(custid);
				if(!ACIS_BBOX) {
					if (idlength==13) {
						strncpy(tmp,custid,4);
						strncpy(tmp+4,custid+7,6);
						tmp[10]=0;
						strcpy(custid,tmp);
					}
					if (idlength<10){ 
						strcpy(tmp,zero);
						tmp[10-idlength]=0;
						strcat(tmp,custid);
						tmp[10]=0;
					}
				}
				else {
					/*if (idlength<13){ 
						strcpy(tmp,zero);
						tmp[13-idlength]=0;
						strcat(tmp,custid);
						tmp[13]=0;
					}
					else*/
						strcpy(tmp,custid);
				}
				msg = new TiXmlElement( "CustomerID" );  
				msg->LinkEndChild( new TiXmlText( tmp )); 
				ACISCSTDetail->LinkEndChild( msg );
				msg = new TiXmlElement( "CustomerName" );  
				memcpy(tmp,Name,48);
				if(strlen(Name)>48)
					tmp[48]=0;
				msg->LinkEndChild( new TiXmlText( tmp )); 
				ACISCSTDetail->LinkEndChild( msg );	
				leng=strlen(TaxNR);
				if (leng>1) {
					strcpy(tmp,TaxNR);
					msg = new TiXmlElement( "FiscalCode" );  
					msg->LinkEndChild( new TiXmlText( tmp )); 
					ACISCSTDetail->LinkEndChild( msg );
				}
				strcpy(tmp,Address);
				tmp[24]=0;
				msg = new TiXmlElement( "Address" );  
				msg->LinkEndChild( new TiXmlText( tmp )); 
				ACISCSTDetail->LinkEndChild( msg );						
				
				memcpy(tmp,Zip,6);
				tmp[6]=0;
				msg = new TiXmlElement( "ZIPCode" );  
				msg->LinkEndChild( new TiXmlText( tmp )); 
				ACISCSTDetail->LinkEndChild( msg );						
				
				memcpy(tmp,City,20);
				if(strlen(City)>20);
					tmp[20]=0;
				msg = new TiXmlElement( "City" );  
				msg->LinkEndChild( new TiXmlText( tmp )); 
				ACISCSTDetail->LinkEndChild( msg );						
				/*if (strlen(Licnr)>0) {
					TiXmlElement * Vehicle = new TiXmlElement( "Vehicle" );  
					ACISCSTDetail->LinkEndChild( Vehicle);
					msg = new TiXmlElement( "VehicleRegistrationNumber" );  
					msg->LinkEndChild( new TiXmlText( Licnr )); 
					Vehicle->LinkEndChild( msg );
				}*/
			}
			GetLocalTime(&t);
			
			char byear[20];
			sprintf(byear,"%d",t.wYear);
			//sprintf(byear,"%s",tmp+2);
			L.Log("Year: %s\n",byear+2);
			strcpy(ft1,"null");
			if (_strnicmp(ACISDNOut,ft1,4)!=0) { 
				L.Log("atnevezes\n");
				//PCATS_PCM_181011182830.XML
				sprintf(tmp,"%s\\PCATS_PCM_%02d%02d%s%02d%02d%02d.XML",ACISDNOut,t.wDay,t.wMonth,byear+2,t.wHour,t.wMinute,t.wSecond+seq_num);
				L.Log("%s\n",tmp);
				ACISdocwrite.SaveFile( "c:\\ACIScsc.xml" );
				if (CopyFile("c:\\ACIScsc.xml",tmp,true)!=0)
					L.Log("Info: Chk4Upd.Customers: A ACIS customer change file has been put to output directory. File/path: %s\n",tmp);
				else 
					L.LogE("ERROR: Chk4Upd.Customers: Cannot put ACIS customer change file to output directory. File/path: %s  Error code:%d\n",tmp,GetLastError());
			}			
			if ((_strnicmp(NewACISDNOut,ft1,4)!=0) && (ACIS_BBOX==0)) { 
				L.Log("atnevezes\n");
				sprintf(tmp,"%s\\PCATS_PCM_%02d%02d%s%02d%02d%02d.XML",NewACISDNOut,t.wDay,t.wMonth,byear+2,t.wHour,t.wMinute,t.wSecond+seq_num);
				L.Log("%s\n",tmp);
				//sprintf(tmp,"C:\\PCATS_PCM_%02d%02d%s%02d%02d%02d.XML",t.wDay,t.wMonth,tmp+2,t.wHour,t.wMinute,t.wSecond);
				L.Log("Move file\n");
				if (MoveFile("c:\\ACIScsc.xml",tmp)!=0)
					L.Log("Info: Chk4Upd.Customers: A new ACIS customer change file has been put to output directory. File/path: %s\n",tmp);
				else 
					L.LogE("ERROR: Chk4Upd.Customers: Cannot put new ACIS customer change file to output directory. File/path: %s  Error code:%d\n",tmp,GetLastError());
			}
/**********************  GILBARCO FILE  ***************************/
			TiXmlDocument docwrite;  
 			TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "windows-1250", "" );  
			docwrite.LinkEndChild( decl ); 
			TiXmlElement * wroot = new TiXmlElement( "GVRXML-MaintenanceRequest" );  
			docwrite.LinkEndChild( wroot );
			wroot->SetAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
			wroot->SetAttribute("xmlns:xsd","http://www.w3.org/2001/XMLSchema");
			wroot->SetAttribute("version","3.3");
			wroot->SetAttribute("xmlns","http://www.gilbarco.com/POSBO/Vocabulary/2006-04-18");
			/******************* TransmissionHeader   ******************/
			TiXmlElement * TransHead = new TiXmlElement( "TransmissionHeader" );  
			wroot->LinkEndChild( TransHead );
			TransHead->SetAttribute("xmlns","http://www.naxml.org/POSBO/Vocabulary/2003-10-16");
			msg = new TiXmlElement( "StoreLocationID" );  
			msg->LinkEndChild( new TiXmlText( "HUNGARY" ));  
			TransHead->LinkEndChild( msg );  
			msg = new TiXmlElement( "VendorName" );  
			msg->LinkEndChild( new TiXmlText( "HUNGARY" ));  
			TransHead->LinkEndChild( msg ); 
			msg = new TiXmlElement( "VendorModelVersion" );  
			msg->LinkEndChild( new TiXmlText( "1.0" ));  
			TransHead->LinkEndChild( msg ); 
			/******************* CustomerMaintenance   ******************/
			TiXmlElement * CustMaint = new TiXmlElement( "CustomerMaintenance" );  
			wroot->LinkEndChild( CustMaint);
			TiXmlElement * TableAct = new TiXmlElement( "TableAction" );  
			CustMaint->LinkEndChild( TableAct );
			TableAct->SetAttribute("type","initialize");
			
			TiXmlElement * RecAct = new TiXmlElement( "RecordAction" );  
			CustMaint->LinkEndChild( RecAct );
			RecAct->SetAttribute("type","create");
//				ConvChar852ToWin((unsigned char *)a);
			for (xt=1;xt<=number;xt++) {
				if (strlen(cusid)==0){
					strcpy(custid,csc_type[xt].custid);
					//L.Log("Customer ID: %s\n",custid);
					sprintf(Name,"%s",csc_type[xt].Name);
//					L.Log("Customer name: %s\n",Name);
					sprintf(Zip,"%s",csc_type[xt].Zip);
					sprintf(City,"%s",csc_type[xt].City);
					sprintf(Address,"%s",csc_type[xt].Address);
					sprintf(Licnr,"%s",csc_type[xt].Licnr);
					if(Licnr[0]=='(')
						strcpy(Licnr,"");
					sprintf(TaxNR,"%s",csc_type[xt].TaxNR);
					create=csc_type[xt].create;
				}

				TiXmlElement * CSTDetail = new TiXmlElement( "CSTDetail" );  
				CustMaint->LinkEndChild( CSTDetail );
				msg = new TiXmlElement( "RecordAction" );
				if (!create)
					msg->SetAttribute( "type","delete" );
				else
					msg->SetAttribute( "type","create" );
				CSTDetail->LinkEndChild( msg ); 
				msg = new TiXmlElement( "CustomerID" );  
				msg->LinkEndChild( new TiXmlText( custid )); 
				CSTDetail->LinkEndChild( msg );
				memcpy(tmp,Name,48);
				tmp[48]=0;
				msg = new TiXmlElement( "CustomerName" );  
				msg->LinkEndChild( new TiXmlText( tmp )); 
				CSTDetail->LinkEndChild( msg );	

				if (strlen(TaxNR)>1) {
					TaxNR[15]=0;
					msg = new TiXmlElement( "FiscalCode" );  
					msg->LinkEndChild( new TiXmlText( TaxNR )); 
					CSTDetail->LinkEndChild( msg );
				}
				//if (strlen(Address)>1) {
					Address[24]=0;
					msg = new TiXmlElement( "Address" );  
					msg->LinkEndChild( new TiXmlText( Address )); 
					CSTDetail->LinkEndChild( msg );						
				//}				
				//if (strlen(Zip)>1) {
					Zip[6]=0;
					msg = new TiXmlElement( "ZIPCode" );  
					msg->LinkEndChild( new TiXmlText( Zip )); 
					CSTDetail->LinkEndChild( msg );						
				//}
				City[20]=0;
				msg = new TiXmlElement( "City" );  
				msg->LinkEndChild( new TiXmlText( City )); 
				CSTDetail->LinkEndChild( msg );

				if (strlen(Licnr)>0) {
					TiXmlElement * Vehicle = new TiXmlElement( "Vehicle" );  
					CSTDetail->LinkEndChild( Vehicle);
					msg = new TiXmlElement( "VehicleRegistrationNumber" );  
					msg->LinkEndChild( new TiXmlText( Licnr )); 
					Vehicle->LinkEndChild( msg );
				}
			}	
			GetLocalTime(&t);
			sprintf(tmp,"%s\\PCATS_PCM_%.4d%.2d%.2d%.2d%.2d_%.2d.xml",DNUpdO,t.wYear,t.wMonth,t.wDay,t.wHour,t.wMinute,seq_num);
			//sprintf(tmp,"C:\\PCATS_PCM_%.4d%.2d%.2d%.2d%.2d.xml",t.wYear,t.wMonth,t.wDay,t.wHour,t.wMinute);
			docwrite.SaveFile( "c:\\GILBcsc.xml" );
			if (MoveFile("c:\\GILBcsc.xml",tmp)!=0)
				L.Log("Info: Chk4Upd.Customers: A new GILBARCO customer change file has been put to output directory. File/path: %s\n",tmp);
			else
				L.LogE("ERROR: Chk4Upd.Customers: Cannot put new GILBARCO customer change file to output directory. File/path: %s  Error code: %d\n",tmp,GetLastError());
			DeleteFile(FNCSCW);

		

}




void WriteCSC2DB(char *custid,char *Name,char *Zip,char *City,char *Address,char *Licnr,char *TaxNR, int seq_nr){
	char sql[1024];
	MYSQL *hnd=NULL; // mysql connection handle
	const char *sinf=NULL; // mysql server information
	bool db_ok=false;
	hnd = mysql_init(NULL);
	//MYSQL_RES *res=NULL; // result of asking the database for a listing of its tables
	long res,partn;
	MYSQL_ROW row; // one row from the result set
	MYSQL_RES *queryResult_ptr;
	if(NULL == mysql_real_connect(hnd,VPOPServerIP,VPOPUser,VPOPPassw,VPOPDataBase,0,NULL,0)){
		db_ok=false;
		for(int x=0;x<10;x++) {
			Sleep(10000);
			L.LogE("Problem encountered connecting to the %s database on %s with user %s. %d %s\n",VPOPDataBase,VPOPServerIP,VPOPUser,GetLastError(),mysql_error(hnd));
			if(NULL != mysql_real_connect(hnd,VPOPServerIP,VPOPUser,VPOPPassw,VPOPDataBase,0,NULL,0)){
				x=10;
				db_ok=true;
			}
		}
	}
	else {
		db_ok=true;
		L.Log("Succesfully connected to the %s database on %s.\n",VPOPDataBase,VPOPServerIP);	
	}
	if (db_ok) {
		L.Log("I find the card! %s\n",custid);
		mysql_query(hnd,"set names 'cp852';");
		sprintf(sql,"SELECT * FROM Auchan_card.Cards where nr='%s';",custid);
		res=mysql_query(hnd,sql);
		if (res)
			L.LogE("ERROR - Cannot read database!\n %s\n%s\n",sql,mysql_error(hnd));
		else {
			L.Log("I read the database!\n");
			queryResult_ptr = mysql_store_result(hnd);
			L.Log("mysql_num_rows %d\n",mysql_num_rows(queryResult_ptr));
			if (mysql_num_rows(queryResult_ptr)==0) {
			//if (queryResult_ptr==NULL) {
					L.Log("Try to insert csc to db!\n");
					sprintf(sql,"insert into Auchan_card.Partners (`cust_name`,`cust_zip`,`cust_city`,`cust_addr`,`cust_taxnr`,`cust_lic_nr`,`store_nr`) 	VALUES ('%s','%s','%s','%s','%s','%s',%d)",Name,Zip,City,Address,TaxNR,Licnr,StoreNum); 
					partn = mysql_query(hnd,sql);
					L.Log("%s\n partn:%d\n",sql,partn);
					if (!partn) {
						long partn_id = mysql_insert_id(hnd);
						sprintf(sql,"insert into Auchan_card.Cards (`nr`,`type`,`partner_id`,`valid_from`,`valid_to`,`del`,`last_mod`,`active`,`need_invoice`) VALUES ('%s',6,%d,now(),'2020-01-01',0,now(),1,1);",custid,partn_id);
						L.Log("%s\n",sql);
						partn = mysql_query(hnd,sql);
						L.Log("Writed the %s card into DB!\n", custid);
					}
					else 
						L.LogE("ERROR -- I cannot write csc to DB! Error code:%s \n SQL: %s\n",mysql_error(hnd),sql); 
			}
			mysql_free_result(queryResult_ptr);
			mysql_close(hnd);
		}
	}
	//WriteCSC2XML(custid,Name,Zip,City,Address,Licnr,TaxNR,1,1,seq_nr);

}

void GetPostCodeStatusFromDB(){
	char sql[1024],tmp[40];
	char a[20],b[50];
	int xt=-1,i;
	SYSTEMTIME t;
	GetLocalTime(&t);
	char last_date[30];
	long csc_counter=0;
	MYSQL *hnd=NULL; // mysql connection handle
	const char *sinf=NULL; // mysql server information
	bool db_ok=false;
	hnd = mysql_init(NULL);
	//MYSQL_RES *res=NULL; // result of asking the database for a listing of its tables
	long res,partn;
	MYSQL_ROW row; // one row from the result set
	MYSQL_RES *queryResult_ptr;
	char *dateTime=NULL;
	int seq_num=0;
	TiXmlElement * root_igas = new TiXmlElement( "Data" );
	TiXmlElement * Questions = new TiXmlElement( "Questions" );
	TiXmlElement * question = new TiXmlElement( "Question" );
	Reg r(false, C_RegKey);
	sprintf(a,"%.4d%.2d%.2d%.2d%.2d00",t.wYear,t.wMonth,t.wDay,t.wHour+2,t.wMinute);
	if(NULL == mysql_real_connect(hnd,R910ServerIP,R910User,R910Passw,"Infodesk2",0,NULL,0)){
		db_ok=false;
		for(int x=0;x<10;x++) {
			Sleep(10000);
			L.LogE("Problem encountered connecting to the Infodesk2 database on %s with user %s. %d (%s)\n",R910ServerIP,R910User,GetLastError(),mysql_error(hnd));

			if(NULL != mysql_real_connect(hnd,R910ServerIP,R910User,R910Passw,"Infodesk2",0,NULL,0)){
				x=10;
				db_ok=true;
			}
		}
	}
	else {
		db_ok=true;
		L.Log("Succesfully connected to the Infodesk2 database on %s.\n",R910ServerIP);	
	}
	if (db_ok) {
		sprintf(sql,"SET NAMES cp1250");
		mysql_query(hnd,sql);
		sprintf(sql,"SELECT id,mod_date,turn_On FROM Infodesk2.post_code_status where mod_date='%4d%.2d%.2d' and store_nr=%d ",t.wYear,t.wMonth,t.wDay,StoreNum);
		L.Log("SQL: %s\n",sql);
		res=mysql_query(hnd,sql);
		if (res)
			L.LogE("ERROR - Cannot read Infodesk2.post_code_status database!\n %s\n%s\n",sql,mysql_error(hnd));
		else {
			queryResult_ptr = mysql_store_result(hnd);
			if ((queryResult_ptr!=NULL) && (mysql_num_rows(queryResult_ptr)!=0)) {
				while (row = mysql_fetch_row(queryResult_ptr)) {
					xt++;
					TiXmlDocument docwrite_igas;  
					TiXmlElement* msg_igas;
 					TiXmlDeclaration* decl_igas = new TiXmlDeclaration( "1.0", "UTF-8", "" );  
					docwrite_igas.LinkEndChild( decl_igas ); 
					seq_num=r.ValueDW("Fuel.NextUpdateNr",0);
					seq_num++;
					/**********  POSTCODE UPDATE  ********************/
						root_igas->Clear();
						root_igas->SetAttribute("TxDev","SIM");
						root_igas->SetAttribute("TxDevNum","0");
						root_igas->SetAttribute("RxDev","NAM");
						root_igas->SetAttribute("RxDevNum","0");
						root_igas->SetAttribute("SerGroup","50");
						root_igas->SetAttribute("SerCode","0");
						root_igas->SetAttribute("Prio","1");
						root_igas->SetAttribute("SeqNum",seq_num);
						root_igas->SetAttribute("Time",a);
						root_igas->SetAttribute("Version","2,23");
						docwrite_igas.LinkEndChild( root_igas );
						Questions->Clear();
						question->Clear();
						Questions->SetAttribute("InitialLoad","0");
						question->SetAttribute("QuestNr","1");
						question->SetAttribute("QuestType","0");
						question->SetAttribute("LinkNr","0");
						question->SetAttribute("Question","Irányítószám?");
						question->SetAttribute("AnswerType","1");
						question->SetAttribute("MinLength","4");
						question->SetAttribute("MaxLength","4");
						question->SetAttribute("ReferenceID","0");
						question->SetAttribute("ReceiptPrint","0");
						question->SetAttribute("CustDisplay","1");
//DeleteFlag="0" QuestNr="1" QuestType="0" LinkNr="0" Question="Irányítószám?" AnswerType="1" MinLength="4" MaxLength="4" ReferenceID="0" ReceiptPrint="0" CustDisplay="1"
					if(atoi(row[2])==1) { //Bekapcsolom
						question->SetAttribute("DeleteFlag","0");
					}
					else {//Kikapcsolom a bek�r�st
						question->SetAttribute("DeleteFlag","1");
					}
					Questions->LinkEndChild( question );
					root_igas->LinkEndChild( Questions );
					sprintf(b,"C:\\DAT_000_%.6d.xml",seq_num);
					docwrite_igas.SaveFile( b );
					L.Log("NAMOS PostCode update file created! %s\n",a);
					sprintf(a,"%s\\DAT_000_%.6d.xml",DNUpdO,seq_num);	
					for(i=0;i<10;i++){
						if(!CopyFile(b,a,false)){
							L.LogE("ERROR -- Cannot move NAMOS POSTCODE update file %s! Error: %d\n",a,GetLastError());
							Sleep(2000);
						}
						else i=10;
					}
					r.SetValueDW("Fuel.NextUpdateNr",seq_num);
					sprintf(sql,"Update Infodesk2.post_code_status set send_date=now() where id=%s ",row[0]);
					L.Log("SQL: %s\n",sql);
					res=mysql_query(hnd,sql);
					if (res)
						L.LogE("ERROR - Cannot update Infodesk2.post_code_status database!\n %s\n%s\n",sql,mysql_error(hnd));

				}
			}
		}
		mysql_close(hnd);
		//L.Log("Last date: %s Counter %d  xt: %d\n",last_date,csc_counter,xt);
	}
}

void GetDiscountPrice( ){
	char sql[1024],sql_ins[1024],tmp[40];
	char a[20],b[50];
	int xt=-1,i,avg_counter=0;
	int avg_price=0;
	float asd=0;
	SYSTEMTIME t;
	GetLocalTime(&t);
	char last_date[30];
	long csc_counter=0;
	MYSQL *hnd=NULL; // mysql connection handle
	const char *sinf=NULL; // mysql server information
	bool db_ok=false;
	hnd = mysql_init(NULL);
	//MYSQL_RES *res=NULL; // result of asking the database for a listing of its tables
	long res,partn,ins;
	MYSQL_ROW row; // one row from the result set
	MYSQL_RES *queryResult_ptr;
	char *dateTime=NULL;
	int seq_num=0;
	Reg r(false, C_RegKey);
	sprintf(a,"%.4d%.2d%.2d%.2d%.2d00",t.wYear,t.wMonth,t.wDay,t.wHour,t.wMinute);
	if(NULL == mysql_real_connect(hnd,R910ServerIP,R910User,R910Passw,"Petrol_card",0,NULL,0)){
		db_ok=false;
		for(int x=0;x<10;x++) {
			Sleep(10000);
			L.LogE("Problem encountered connecting to the Petrol_card database on %s with user %s. %d (%s)\n",R910ServerIP,R910User,GetLastError(),mysql_error(hnd));

			if(NULL != mysql_real_connect(hnd,R910ServerIP,R910User,R910Passw,"Petrol_card",0,NULL,0)){
				x=10;
				db_ok=true;
			}
		}
	}
	else {
		db_ok=true;
		L.Log("Succesfully connected to the Petrol_card database on %s.\n",R910ServerIP);	
	}
	if (db_ok) {
		sprintf(sql,"SET NAMES cp1250");
		mysql_query(hnd,sql);
		for(int counter=0;counter<10;counter++){
			if(FuelPriceArray[counter].ean!="0"){
				sprintf(sql,"SELECT ifnull(price,0),count(*) FROM Petrol_card.PP_spec_prices where '%4d-%.2d-%.2d' between date_from and date_to and ean='%s' order by last_mod desc limit 1",t.wYear,t.wMonth,t.wDay,FuelPriceArray[counter].ean);
				L.Log("SQL: %s\n",sql);
				res=mysql_query(hnd,sql);
				if (res)
					L.LogE("ERROR - Cannot read Petrol_card.PP_spec_prices database!\n %s\n%s\n",sql,mysql_error(hnd));
				else {
					queryResult_ptr = mysql_store_result(hnd);
					if ((queryResult_ptr!=NULL) && (mysql_num_rows(queryResult_ptr)!=0)) {
						while (row = mysql_fetch_row(queryResult_ptr)) {
							if(FuelPriceArray[counter].disc_price!=atoi(row[0])){
								FuelPriceArray[counter].disc_price=atoi(row[0]);
								L.LogE("The discount price was changed ean: %s, price:%sFt\n",FuelPriceArray[counter].ean,row[0]);
								seq_num++;
							}
							
						}
					}
					
				}
				if(FuelPriceArray[counter].price!=0){
					avg_counter++;
					avg_price+=FuelPriceArray[counter].price;
					L.Log("Price:%d sum_price:%d counter:%d\n",FuelPriceArray[counter].price,avg_price,avg_counter);
				}
			}
		}
		if(StoreNum==2) { //Fel kell k�ldeni az FHAUCP00 f�jlban �rkez� �rat
			asd=(avg_price/avg_counter);
			L.LogE("The average fuel price:%f",asd);
			sprintf(sql_ins,"INSERT INTO `Petrol_card`.`Bors_atlagar` (`Datum`, `Atlagar`) VALUES ('%4d-%.2d-%.2d','%f') ",t.wYear,t.wMonth,t.wDay,asd);	
			ins=mysql_query(hnd,sql_ins);
			if (ins)
				L.LogE("ERROR - Cannot INSERT default price to database from Buda�rs!\n %s\n%s\n",sql_ins,mysql_error(hnd));
		}
		mysql_close(hnd);
		if(seq_num>0){
			is_disc_price=true;
			L.LogE("The discount price was changed\n");
		}
	}
}

void GetActualFuelPrice(char* ean,int price, char* bdate ){
	char sql[1024],sql_ins[1024],tmp[40];
	char a[20],b[50];
	int xt=-1,i;
	SYSTEMTIME t;
	GetLocalTime(&t);
	char last_date[30];
	long csc_counter=0;
	MYSQL *hnd=NULL; // mysql connection handle
	const char *sinf=NULL; // mysql server information
	bool db_ok=false;
	hnd = mysql_init(NULL);
	//MYSQL_RES *res=NULL; // result of asking the database for a listing of its tables
	long res,partn,ins;
	MYSQL_ROW row; // one row from the result set
	MYSQL_RES *queryResult_ptr;
	char *dateTime=NULL;
	int seq_num=0;
	Reg r(false, C_RegKey);
	disc_price=0; default_price=0;
	sprintf(a,"%.4d%.2d%.2d%.2d%.2d00",t.wYear,t.wMonth,t.wDay,t.wHour,t.wMinute);
	if(NULL == mysql_real_connect(hnd,R910ServerIP,R910User,R910Passw,"Petrol_card",0,NULL,0)){
		db_ok=false;
		for(int x=0;x<10;x++) {
			Sleep(10000);
			L.LogE("Problem encountered connecting to the Petrol_card database on %s with user %s. %d (%s)\n",R910ServerIP,R910User,GetLastError(),mysql_error(hnd));

			if(NULL != mysql_real_connect(hnd,R910ServerIP,R910User,R910Passw,"Petrol_card",0,NULL,0)){
				x=10;
				db_ok=true;
			}
		}
	}
	else {
		db_ok=true;
		L.Log("Succesfully connected to the Petrol_card database on %s.\n",R910ServerIP);	
	}
	if (db_ok) {
		sprintf(sql,"SET NAMES cp1250");
		mysql_query(hnd,sql);
		sprintf(sql,"SELECT disc_price,default_price,updated FROM Petrol_card.PP_fuel_prices where ean='%s' and store_nr=%d and updated<='%s235959' and (disc_price =%d or default_price=%d) order by updated desc limit 1",ean,StoreNum,bdate,price,price);
		L.Log("SQL: %s\n",sql);
		res=mysql_query(hnd,sql);
		if (res)
			L.LogE("ERROR - Cannot read Petrol_card.PP_fuel_prices database!\n %s\n%s\n",sql,mysql_error(hnd));
		else {
			queryResult_ptr = mysql_store_result(hnd);
			if ((queryResult_ptr!=NULL) && (mysql_num_rows(queryResult_ptr)!=0)) {
				row = mysql_fetch_row(queryResult_ptr);
				disc_price=atoi(row[0]);
				default_price=atoi(row[1]);
			}
		}
		mysql_close(hnd);
	}
}


void GetCSCFromDB(){
	char sql[1024],tmp[40];
	int xt=-1;
	char last_date[30];
	long csc_counter=0;
	MYSQL *hnd=NULL; // mysql connection handle
	const char *sinf=NULL; // mysql server information
	bool db_ok=false;
	hnd = mysql_init(NULL);
	//MYSQL_RES *res=NULL; // result of asking the database for a listing of its tables
	long res,partn;
	MYSQL_ROW row; // one row from the result set
	MYSQL_RES *queryResult_ptr;
	char *dateTime=NULL;
	Reg r(false, C_RegKey);
	r.ValueSZ("Fuel.LastCSCTime",&dateTime,"19700131");
	int seq_num=0;	

	if(NULL == mysql_real_connect(hnd,VPOPServerIP,VPOPUser,VPOPPassw,"Auchan_card",0,NULL,0)){
		db_ok=false;
		for(int x=0;x<10;x++) {
			Sleep(10000);
			L.LogE("Problem encountered connecting to the Auchan_card database on %s with user %s. %d (%s)\n",VPOPServerIP,VPOPUser,GetLastError(),mysql_error(hnd));

			if(NULL != mysql_real_connect(hnd,VPOPServerIP,VPOPUser,VPOPPassw,"Auchan_card",0,NULL,0)){
				x=10;
				db_ok=true;
			}
		}
	}
	else {
		db_ok=true;
		L.Log("Succesfully connected to the Auchan_card database on %s.\n",VPOPServerIP);	
	}
	if (db_ok) {
		sprintf(sql,"SET NAMES cp1250");
		mysql_query(hnd,sql);
		strcpy(last_date,dateTime);
		while(xt==-1) {
			xt=0;
			//					0		1		2			3		4			5		6			7			8		9
			//sprintf(sql,"SELECT nr,cust_name,cust_city,cust_zip,cust_addr,cus:t_taxnr,cust_lic_nr,IFNULL(last_mode,0),IFNULL(last_mod,0),c.active FROM Auchan_card.Cards c JOIN Auchan_card.Partners p on c.partner_id=p.id where type in (3,6) and (p.last_mode>'%s' or last_mod>'%s') limit %d,5000",dateTime,dateTime,csc_counter);
			sprintf(sql,"SELECT nr,cust_name,cust_city,cust_zip,cust_addr,cust_taxnr,cust_lic_nr,IFNULL(last_mode,0),IFNULL(last_mod,0),c.active FROM Auchan_card.Cards c JOIN Auchan_card.Partners p on c.partner_id=p.id where type in (3,7,9,13,17,23,25) and (p.last_mode>'%s' or last_mod>'%s') and c.active=1 and p.active=1 limit %d,5000",dateTime,dateTime,csc_counter);
			//sprintf(sql,"SELECT * FROM Auchan_card.Cards where nr='%s';",custid);
			L.Log("SQL: %s\n",sql);
			res=mysql_query(hnd,sql);
			if (res)
				L.LogE("ERROR - Cannot read Auchan_card database!\n %s\n%s\n",sql,mysql_error(hnd));
			else {
				queryResult_ptr = mysql_store_result(hnd);
				if ((queryResult_ptr!=NULL) && (mysql_num_rows(queryResult_ptr)!=0)) {
					while (row = mysql_fetch_row(queryResult_ptr)) {
						xt++;
						csc_counter++;
						csc_type[xt].custid=row[0];
						L.Log("Send custid %s\n",csc_type[xt].custid);
						csc_type[xt].Name =row[1];
						csc_type[xt].City =row[2];
						csc_type[xt].Zip =row[3];
						csc_type[xt].Address =row[4];
						csc_type[xt].TaxNR =row[5];
						csc_type[xt].Licnr =row[6];
						csc_type[xt].create =row[9];
						strcpy(tmp,row[7]);

						L.Log("datum jon %s ---- %s\n",row[7],row[8]);
						if(tmp[0]!='(')
							if (strcmp(last_date,row[7])<0) strcpy(last_date,row[7]);
						
						if (strcmp(last_date,row[8])<0) strcpy(last_date,row[8]);
					}
					L.Log("Send xt parameter! %d\n",xt);
					WriteCSC2XML("","","","","","","",true, xt,seq_num);

					if (xt==5000){
						seq_num++;
						if(seq_num>99) {
							seq_num=0;
							Sleep(61000);
						}
						L.Log("I'm waiting for 1 minute!\n");
						xt=-1;
						//Sleep(61000);
					}
				}
			}
		}	
		mysql_free_result(queryResult_ptr);
		sprintf(sql,"SET NAMES cp1250");
		mysql_query(hnd,sql);

		int lt=xt;
		xt=-1;csc_counter=0;
		seq_num++;
		while(xt==-1) {
			xt=0;
			//					0		1		2			3		4			5		6			7			8		9
			//sprintf(sql,"SELECT nr,cust_name,cust_city,cust_zip,cust_addr,cust_taxnr,cust_lic_nr,IFNULL(last_mode,0),IFNULL(last_mod,0),c.active FROM Auchan_card.Cards c JOIN Auchan_card.Partners p on c.partner_id=p.id where type in (3,6) and (p.last_mode>'%s' or last_mod>'%s') limit %d,5000",dateTime,dateTime,csc_counter);
			sprintf(sql,"SELECT nr,company_short_name,company_city,company_zip,company_address,company_tax_id,licence_plate_nr,IFNULL(c.last_mod,0),IFNULL(p.last_mod,0),c.active FROM Petrol_card.Cards c JOIN Petrol_card.Partners p on c.partner_id=p.id where (p.last_mod>'%s' or c.last_mod>'%s') and c.active=1 and p.active=1 limit %d,1;",dateTime,dateTime,csc_counter);
			//sprintf(sql,"SELECT * FROM Auchan_card.Cards where nr='%s';",custid);
			L.Log("SQL: %s\n",sql);
			res=mysql_query(hnd,sql);
			if (res)
				L.LogE("ERROR - Cannot read Petrol_card database!\n %s\n%s\n",sql,mysql_error(hnd));
			else {
				queryResult_ptr = mysql_store_result(hnd);
				if (mysql_num_rows(queryResult_ptr)!=0) {
					while (row = mysql_fetch_row(queryResult_ptr)) {
						xt++;
						csc_counter++;
						csc_type[xt].custid=row[0];
						L.Log("Send custid %s\n",csc_type[xt].custid);
						csc_type[xt].Name =row[1];
						csc_type[xt].City =row[2];
						csc_type[xt].Zip =row[3];
						csc_type[xt].Address =row[4];
						csc_type[xt].TaxNR =row[5];
						csc_type[xt].Licnr =row[6];
						csc_type[xt].create =row[9];
						strcpy(tmp,row[7]);

						L.Log("datum jon %s ---- %s\n",row[7],row[8]);
						/*if(tmp[0]!='(')
							if (strcmp(last_date,row[7])<0) strcpy(last_date,row[7]);
						
						if (strcmp(last_date,row[8])<0) strcpy(last_date,row[8]);
						*/
					}
					L.Log("Send xt parameter! %d\n",xt);
					WriteCSC2XML("","","","","","","",true,xt,seq_num);

					if (xt==1){
						
						seq_num++;
						if(seq_num>99) {
							L.Log("I'm waiting for 1 minute!\n");
							seq_num=0;
							Sleep(61000);
						}
						xt=-1;
						//Sleep(61000);
					}
				}
			}
		}	
		mysql_free_result(queryResult_ptr);




		mysql_close(hnd);
		L.Log("Last date: %s Counter %d  xt: %d\n",last_date,csc_counter,xt);
		if (lt!=-1) r.SetValueSZ("Fuel.LastCSCTime",last_date);

	}
}



void WriteEmptyChange2DB(char *EDate,char *ETime,char *AP,char *CloseNR,char *ReceiptNR,int cashier,float weight,long amnt,int pump_id,int Dispenser_id){
	char sql[1024];
	MYSQL *hnd=NULL; // mysql connection handle
	const char *sinf=NULL; // mysql server information
	bool db_ok=false;
	hnd = mysql_init(NULL);
	//MYSQL_RES *res=NULL; // result of asking the database for a listing of its tables
	long res;
	MYSQL_ROW row; // one row from the result set
	MYSQL_RES *queryResult_ptr;
	if(NULL == mysql_real_connect(hnd,VPOPServerIP,VPOPUser,VPOPPassw,VPOPDataBase,0,NULL,0)){
		db_ok=false;
		for(int x=0;x<10;x++) {
			Sleep(10000);
			L.LogE("Problem encountered connecting to the %s database on %s with user %s. %d\n",VPOPDataBase,VPOPServerIP,VPOPUser,GetLastError());
			if(NULL != mysql_real_connect(hnd,VPOPServerIP,VPOPUser,VPOPPassw,VPOPDataBase,0,NULL,0)){
				x=10;
				db_ok=true;
			}
		}
	}
	else {
		db_ok=true;
		L.Log("Succesfully connected to the %s database on %s.\n",VPOPDataBase,VPOPServerIP);	
	}
	if (db_ok) {
		sprintf(sql,"INSERT INTO `VPOP`.`emptychange` (`date`, `time`, `hostess`, `quantity`, `extprice`, `ap`, `closureNr`, `receiptNr`, `dispenser_id`, `pump_id`) VALUES ('%s', '%s', '%d', '%f', '%d', '%s', '%s', '%s', '%d', '%d');",EDate,ETime,cashier,weight,amnt,AP,CloseNR,ReceiptNR,Dispenser_id,pump_id);
		res=mysql_query(hnd,sql);
		if (res)
			L.LogE("ERROR - Cannot write emptychange int database!\n %s\n%s\n",sql,mysql_error(hnd));
		else {
			sprintf(sql,"INSERT INTO `VPOP`.`emptychange` (`date`:%s, `time`:%s, `hostess`:%d, `quantity`:%f, `extprice`:%d, `ap`:%s, `closureNr`:%s, `receiptNr`:%s, `dispenser_id`:%d, `pump_id`:%d",EDate,ETime,cashier,weight,amnt,AP,CloseNR,ReceiptNR,Dispenser_id,pump_id);
			L.LogE("Empty change:\n%s",sql); 
		}
	
	}
}


char* Select_MYSQL(char *s){
	char sql[1024];
	MYSQL *hnd=NULL; // mysql connection handle
	const char *sinf=NULL; // mysql server information
	bool db_ok=false;
	hnd = mysql_init(NULL);
	//MYSQL_RES *res=NULL; // result of asking the database for a listing of its tables
	long res;
	MYSQL_ROW row; // one row from the result set
	MYSQL_RES *queryResult_ptr;
	if(NULL == mysql_real_connect(hnd,VPOPServerIP,VPOPUser,VPOPPassw,VPOPDataBase,0,NULL,0)){
		db_ok=false;
		for(int x=0;x<10;x++) {
			Sleep(10000);
			L.LogE("Problem encountered connecting to the %s database on %s with user %s. %d %s\n",VPOPDataBase,VPOPServerIP,VPOPUser,GetLastError(),mysql_error(hnd));
			if(NULL != mysql_real_connect(hnd,VPOPServerIP,VPOPUser,VPOPPassw,VPOPDataBase,0,NULL,0)){
				x=10;
				db_ok=true;
			}
		}
	}
	else {
		L.Log("Succesfully connected to the %s database on %s.\n",VPOPDataBase,VPOPServerIP);	
		db_ok=true;
	}
	if (db_ok) {
		long row_id=-1;
		if (strlen(s)>20){
			sprintf(sql,"%s;",s);
			res=mysql_query(hnd,sql);
			queryResult_ptr = mysql_store_result(hnd);

			if ((queryResult_ptr!= NULL) && (mysql_num_rows(queryResult_ptr)>0)) {
				L.Log("Found record\n");
				row = mysql_fetch_row(queryResult_ptr);
				row_id = atol(row[0]);
				if (row_id >=0) {  // there are rows
					L.Log("The id is : %d\n",row_id);
					int numfield = mysql_num_fields(queryResult_ptr);
					sprintf(sql,"|0");
					for (int z=0;z<numfield;z++)
						sprintf(sql,"%s|%s",sql,row[z]);
					mysql_free_result(queryResult_ptr);
					mysql_close(hnd);
					L.Log("The return value : %s\n",sql);
					return sql;
				}
				else
					L.LogE("ERROR - Cannot read the id!");
			}
			else 
				L.Log("ERROR - I didn't find any record!\n");
			mysql_free_result(queryResult_ptr);
			mysql_close(hnd);
			return "-1";
		}
	}
	//mysql_close(hnd);
	return "-1";
}

long Insert_MYSQL(char *s){
	char sql[1024];
	MYSQL *hnd=NULL; // mysql connection handle
	const char *sinf=NULL; // mysql server information
	bool db_ok=false;
	hnd = mysql_init(NULL);
	MYSQL_ROW row; // one row from the result set
	MYSQL_RES *queryResult_ptr;

	//MYSQL_RES *res=NULL; // result of asking the database for a listing of its tables
	long res;
	if(NULL == mysql_real_connect(hnd,VPOPServerIP,VPOPUser,VPOPPassw,VPOPDataBase,0,NULL,0)){
		db_ok=false;
		for(int x=0;x<10;x++) {
			Sleep(10000);
			L.LogE("Problem encountered connecting to the %s database on %s with user %s. %d\n",VPOPDataBase,VPOPServerIP,VPOPUser,GetLastError());
			if(NULL != mysql_real_connect(hnd,VPOPServerIP,VPOPUser,VPOPPassw,VPOPDataBase,0,NULL,0)){
				x=10;
				db_ok=true;
			}
		}
	}
	else {
		L.Log("Succesfully connected to the %s database on %s.\n",VPOPDataBase,VPOPServerIP);	
		db_ok=true;
	}
	if (db_ok) {
		long row_id=-1;
		if (strlen(s)>20){
			sprintf(sql,"%s;",s);
			res=mysql_query(hnd,sql);
			if(res) {
				L.LogE("ERROR -- Cannot insert datas! Error:%s %s\n",mysql_error(hnd),sql);
				mysql_close(hnd);
				return -1;
			}
			else {
				mysql_close(hnd);
				return 0;
			}
		}
		mysql_close(hnd); 
		return 0;
	}
	else return 0;
}

long Insert_MYSQL910(char *s){
	char sql[1024];
	MYSQL *hnd=NULL; // mysql connection handle
	const char *sinf=NULL; // mysql server information
	bool db_ok=false;
	hnd = mysql_init(NULL);
	MYSQL_ROW row; // one row from the result set
	MYSQL_RES *queryResult_ptr;

	//MYSQL_RES *res=NULL; // result of asking the database for a listing of its tables
	long res;
	if(NULL == mysql_real_connect(hnd,R910ServerIP,R910User,R910Passw,"Petrol_card",0,NULL,0)){
		db_ok=false;
		for(int x=0;x<10;x++) {
			Sleep(10000);
			L.LogE("Problem encountered connecting to the %s database on %s with user %s. %d\n",VPOPDataBase,VPOPServerIP,VPOPUser,GetLastError());
			if(NULL != mysql_real_connect(hnd,R910ServerIP,R910User,R910Passw,"Petrol_card",0,NULL,0)){
				x=10;
				db_ok=true;
			}
		}
	}
	else {
		L.Log("Succesfully connected to the Petrol_card database on %s.\n",R910ServerIP);	
		db_ok=true;
	}
	if (db_ok) {
		long row_id=-1;
		if (strlen(s)>20){
			sprintf(sql,"%s;",s);
			res=mysql_query(hnd,sql);
			if(res) {
				L.LogE("ERROR -- Cannot insert datas! Error:%s %s\n",mysql_error(hnd),sql);
				mysql_close(hnd);
				return -1;
			}
			else {
				mysql_close(hnd);
				return 0;
			}
		}
		mysql_close(hnd); 
		return 0;
	}
	else return 0;
}


long Insert_BBON_MYSQL(char *s,long value,char *exp_date){
	char sql[1024];
	char sendmess[2024];
	MYSQL *hnd=NULL; // mysql connection handle
	const char *sinf=NULL; // mysql server information
	bool db_ok=true;
	hnd = mysql_init(NULL);
	//MYSQL_RES *res=NULL; // result of asking the database for a listing of its tables
	long res;
	MYSQL_ROW row; // one row from the result set
	MYSQL_RES *queryResult_ptr;

	if(NULL == mysql_real_connect(hnd,VPOPServerIP,VPOPUser,VPOPPassw,"Infodesk2",0,NULL,0)){
		db_ok=false;
		for(int x=0;x<10;x++) {
			Sleep(10000);
			L.LogE("Problem encountered connecting to the Infodesk2 database on %s with user %s. %d\n",VPOPServerIP,VPOPUser,GetLastError());
			if(NULL != mysql_real_connect(hnd,VPOPServerIP,VPOPUser,VPOPPassw,"Infodesk2",0,NULL,0)){
				x=10;
				db_ok=true;
			}
		}
	}
	else
		L.Log("Succesfully connected to the Infodesk2 database on %s.\n",VPOPServerIP);	
	if (db_ok) {
		long row_id=-1;
		if (strlen(s)>12){
//			sprintf(sql,"SELECT id,expiry_date,pay_date,bon_val,trans_store,store_nr FROM `Infodesk2`.`Bottle` where bon_id='%s' and (pay_date>'20000101' OR expiry_date<date_sub(curdate(),INTERVAL 100 DAY));",s);
			sprintf(sql,"SELECT id FROM `Infodesk2`.`Bottle` where bon_id='%s';",s);
			res=mysql_query(hnd,sql);
			if(res) {
				RL.LogE("ERROR - Cannot read voucher informat from Bottle database! %s\n%s\n",mysql_error(hnd),sql);
				mysql_close(hnd);
				return -1;
			}
			else {
				queryResult_ptr = mysql_store_result(hnd);
				row = mysql_fetch_row(queryResult_ptr);
				if (mysql_num_rows(queryResult_ptr)>0) {
					sprintf(sql,"SELECT id FROM `Infodesk2`.`Bottle` where bon_id='%s' and (pay_date>'20000101' OR expiry_date<date_sub(curdate(),INTERVAL %d DAY));",s,keepDays);
					res=mysql_query(hnd,sql);
					queryResult_ptr = mysql_store_result(hnd);
					row = mysql_fetch_row(queryResult_ptr);
					if (mysql_num_rows(queryResult_ptr)==0) { //�rv�nyes a b�n
						sprintf(sendmess,"ERROR - Az �veg b�n m�r megtal�lhat� az Infodesk2.Bottle adatb�zisban �s m�g nem haszn�lt�k fel!\n%s\n",sql);
						mysql_free_result(queryResult_ptr);
						RL.LogE(sendmess);
						send_message(sendmess);
						return 0;
					}
				}
				//else {
					sprintf(sql,"SELECT id FROM `Infodesk2`.`Bottle` where bon_id='%s';",s);
					res=mysql_query(hnd,sql);
					if(res) {
						RL.LogE("ERROR - Cannot query the Bottle table! \n %s\n",sql);
						mysql_close(hnd);
						return -1;
					}
					else {
						queryResult_ptr = mysql_store_result(hnd);
						row = mysql_fetch_row(queryResult_ptr);
						if (mysql_num_rows(queryResult_ptr)>0) {
							sprintf(sql,"UPDATE `Infodesk2`.`Bottle` SET `expiry_date`='%s', pay_date ='19700131',`bon_val`='%d', `trans_store`='0', `till`='0', `trans_nr`='0', `store_nr`='%d',last_mode=now() WHERE `id`='%s';",exp_date,value,StoreNum,row[0]  );
							RL.Log("%s\n",sql);
							res=mysql_query(hnd,sql);
							if(res) {
								RL.LogE("ERROR - Cannot update the Bottle table! \n %s\n",sql);
								sprintf(sendmess,"ERROR - Nem siker�lt fel�l�rni az �veg b�nt az Infodesk2.Bottle adatb�zisban!\n%s\n",sql);
								RL.LogE(sendmess);
								mysql_free_result(queryResult_ptr);
								send_message(sendmess);
								mysql_close(hnd);
								return 0;
							}
						}
						else {
							sprintf(sql,"INSERT INTO `Infodesk2`.`Bottle` (`bon_id`, `expiry_date`, `pay_date`, `bon_val`, `trans_store`, `till`, `trans_nr`, `store_nr`,last_mode) VALUES ('%s', '%s', '19700131', %d, '0', '0', '0', '%d',now());",s,exp_date,value,StoreNum  );
							res=mysql_query(hnd,sql);
							if(res) {
								RL.LogE("ERROR - Cannot query the Bottle table! \n %s\n",sql);
								sprintf(sendmess,"ERROR - Az �veg b�nt nem siker�lt be�rni az Infodesk2.Bottle adatb�zisba!\n%s\n%s\n",sql,mysql_error(hnd));
								send_message(sendmess);
								mysql_free_result(queryResult_ptr);
								mysql_close(hnd);
								return 0;
							}
							else
								RL.Log("Succesfully write Bottle bon to database! %s - %d\n",s,value);
						}
						mysql_free_result(queryResult_ptr);
						mysql_close(hnd);
						return 0;
					}
				//}
				mysql_free_result(queryResult_ptr);
				mysql_close(hnd);
				return 0;
			}
		}
		else {
			RL.LogE("The voucheh number is too short! %s\n",s);
			return 0;
			
			
			
		}
	}
	RL.LogE("I couldn't connect to MYSQL server!\n");
	return 0;
}




int LoadVar(void)
{
	long l;
	int res=0,counter=0;
	FILE *F;
	char buf1[200];
	char seps[]   = "|\n";
	char *token;
	

	for(counter=0;counter<10;counter++){
		FuelPriceArray[counter].price=0;
		FuelPriceArray[counter].disc_price=0;
		sprintf(FuelPriceArray[counter].ean,"%s","0");
		sprintf(FuelPriceArray[counter].date,"%s","0");
	}
	//GLOBAL
	Reg r(false, C_RegKey);
	l=r.ValueDW("_LogFileTruncationLength",0x00A00000);
	FuelClose=r.ValueDW("Fuel.System.Close",0);
	L.SetTruncation(l);
	RL.SetTruncation(l);
	//NAMOS TIME syncron
	FuelUpdateTimeSill=r.ValueDW("Fuel.FuelUpdateTimeSill",FuelUpdateTimeSill);
	FuelPlusTimeAfterSill=r.ValueDW("Fuel.FuelPlusTimeAfterSill",FuelPlusTimeAfterSill);
	FuelPlusTimeBeforeSill=r.ValueDW("Fuel.FuelPlusTimeBeforeSill",FuelPlusTimeBeforeSill);
	NamosCurrentTime=r.ValueDW("Fuel.NamosCurrentTime",NamosCurrentTime);
	if (!NITD) r.ValueSZ("Fuel.NamosISSTimeDIfferent",&NITD,"0");
	NamosISSTimeDIfferent=atol(NITD);
	ExitOnNoTran=r.ValueDW("_ExitOnNoTransaction",0);
	LogNoTran=r.ValueDW("_LogForNoTransactionToProcess",0);
	ProcessLoopDelay=r.ValueDW("_ProcessLoopDelayInMilliSecs",ProcessLoopDelay);
	if (!FNCSCI) r.ValueSZ("_FileName.CustomerDataChangeIn",&FNCSCI,"D:\\ISS\\WORK\\CSC.Chg");
	if (!FNItemList) r.ValueSZ("_FileName.Items4UpdateFile",&FNItemList,"c:\\AufuelIf\\Items4Update.txt");
	if (!FNTomraEAN) r.ValueSZ("_FileName.TomraCode2EAN",&FNTomraEAN,"F:\\Tomra\\Tomra2EAN.txt");

	if (!FNCSCW) r.ValueSZ("_FileName.CustomerDataWork",&FNCSCW,"D:\\ISS\\WORK\\CSC.Wrk");
	if (!FNItem) r.ValueSZ("_FileName.ItemUpdateIn",&FNItem,"C:\\ItemUpdIn\\fhaucp00");
	if (!VatConvTable) r.ValueSZ("_VATRateConversionTable",&VatConvTable,"4210310210");
	//FUEL
	TenderTotals.ReadFromRegistry(false,(char *)C_RegKey,(char *)C_RegTenderTotals);
	CashierTotals.ReadFromRegistry(false,(char *)C_RegKey,(char *)C_RegCashierTotals);
	//ItemTotals.ReadFromRegistry(false,(char *)C_RegKey,(char *)C_RegItemTotals);
	GItemTotals.ReadFromRegistry(false,(char *)C_RegKey,(char *)C_RegGItemTotals);
	char *LogFN=NULL;
	r.ValueSZ("Fuel.LogFileName",&LogFN,"C:\\AUFuelIf\\Log.txt");
	L.SetLogFileName(LogFN);
	delete [] LogFN;
	LogFN=NULL;
	r.ValueSZ("Fuel.LogFileName.ErrorsOnly",&LogFN,"C:\\AUFuelIf\\ErrorLog.txt");
	L.SetErrorLogFileName(LogFN);
	delete [] LogFN;
	
	if (!DNIn) r.ValueSZ("Fuel.Directory.Input",&DNIn,"C:\\AUFuelIf\\In");
	if (!DNIGASSIn) r.ValueSZ("Fuel.IGASS.Directory.Input",&DNIGASSIn,"C:\\AUFuelIf\\In");
	if (!ACISDNIn) r.ValueSZ("Fuel.ACIS.Directory.Input",&ACISDNIn,"C:\\AUFuelIf\\ACIS\\In");
	if (!NewACISDir) r.ValueSZ("Fuel.NewACISInputDir",&NewACISDir,"C:\\AUFuelIf\\ACIS\\NewIn");
	if (!VPOPDataBase) r.ValueSZ("Fuel.VPOP.DataBase",&VPOPDataBase,"Mydb");
	if (!VPOPServerIP) r.ValueSZ("Fuel.VPOP.DBServerIP",&VPOPServerIP,"192.168.1.125");
	if (!VPOPPassw) r.ValueSZ("Fuel.VPOP.DBPassw",&VPOPPassw,"");
	if (!VPOPUser) r.ValueSZ("Fuel.VPOP.DBUser",&VPOPUser,"root");
	if (!R910DataBase) r.ValueSZ("Fuel.R910.DataBase",&R910DataBase,"Petrol_card");
	if (!R910ServerIP) r.ValueSZ("Fuel.R910.DBServerIP",&R910ServerIP,"135.200.32.101");
	if (!R910Passw) r.ValueSZ("Fuel.R910.DBPassw",&R910Passw,"Asdfqwer12345");
	if (!R910User) r.ValueSZ("Fuel.R910.DBUser",&R910User,"info23");
	if (!ACISDNOut) r.ValueSZ("Fuel.ACIS.Directory.Output",&ACISDNOut,"C:\\AUFuelIf\\ACIS\\Out");
	if (!NewACISDNOut) r.ValueSZ("Fuel.NewACIS.Directory.Output",&NewACISDNOut,"NULL");
	if (!ACISConvTable) r.ValueSZ("Fuel.ACIS.Conversation.Table",&ACISConvTable,"C:\\AUFuelIf\\ACIS\\ACISConvTable.txt");
	if (!DNWork) r.ValueSZ("Fuel.Directory.Working",&DNWork,"C:\\AUFuelIf\\Work");
	if (!DNDone) r.ValueSZ("Fuel.Directory.DoneToday",&DNDone,"C:\\AUFuelIf\\Done");
	if (!DNDuplicate) r.ValueSZ("Fuel.Directory.Duplicate",&DNDuplicate,"C:\\AUFuelIf\\Duplicate");
	if (!DNBad) r.ValueSZ("Fuel.Directory.Bad",&DNBad,"C:\\AUFuelIf\\Bad");
	
	if (!DNHist) r.ValueSZ("Fuel.Directory.History",&DNHist,"C:\\AUFuelIf\\History");
	if (!DNUpdO) r.ValueSZ("Fuel.Directory.UpdatesOut",&DNUpdO,"C:\\AUFuelIf\\Out");
	if (!IgassUpdO) r.ValueSZ("Fuel.Directory.IGASSUpdatesOut",&IgassUpdO,"C:\\AUFuelIf\\Out");
	
	if (!ItemSubst) r.ValueSZ("Fuel.ItemSubstitutions",&ItemSubst,"");
	if (!FuelPrices) r.ValueSZ("Fuel.NAMOS.Price",&FuelPrices,"");
	token=NULL;
	r.ValueSZ("Fuel.IGASS.FirstDispenser-4",&token,"-3"); //A val�s sz�mb�l le kell vonni 4-t �s azt kell ide�rni
	FirstDispenser= atol(token);
	if (!FlexDnIn) r.ValueSZ("Fuel.Flexys_Directory.Input",&FlexDnIn,"C:\\AUFuelIf\\In");
	if (!FlexDnOut) r.ValueSZ("Fuel.Flexys_Directory.Output",&FlexDnOut,"C:\\AUFuelIf\\Out");
	FLEXYS=r.ValueDW("Fuel.FLEXYS_System",0);
	ACIS_BBOX=r.ValueDW("Fuel.NEW_ACIS_AE",0); //Ha az �j BBOX-os rendszer fut AE-vel, akkor 1
	FirstACISPumpID=r.ValueDW("Fuel.ACIS.FirstStationNumber",7);
	ACISCounter=r.ValueDW("Fuel.ACIS.NumberOfStation",3);
	NewACISDateCor=r.ValueDW("Fuel.ACIS.DateCorrection",1);
	r.ValueSZ("SMTP_Server",&smtpserv,"135.200.32.67");
	smtpport=r.ValueDW("SMTP_Port",25);
	r.ValueSZ("Email",&Email,"auchanhiba@laurel.hu");
	r.ValueSZ("Subject",&subject,"Auchan HIBA !!!!");
	r.ValueSZ("From",&from,"AuchanTorokbalint");
	AcisWOEJ=r.ValueDW("Fuel.ACISWithoutEJ",AcisWOEJ);
	TomraExpireDays =r.ValueDW("TomraExpireDays",TomraExpireDays);
	
	
	long count=0;
	l=0;
	char *p;
	char *s;
	char *pp;

	int db=0;
	for (db=0;db<20;db++)
		sprintf(AStruct[db].ean,"0");
	if ((F=fopen(ACISConvTable,"rt"))!=NULL) {
		while (fgets(buf1,199,F)) {//loop to read conversion table
			 token = strtok( buf1, seps );
			counter=0;
			while( token != NULL )	{
			    counter++;
				switch (counter) {
					case 2:sprintf(AStruct[db].Description,"%s",token);
						break;
					case 1:db=atoi(token);sprintf(AStruct[db].ItemID,"%s",token);
						break;
					case 3:sprintf(AStruct[db].ean,"%s",token);
						break;
					case 4:sprintf(AStruct[db].VTSZ,"%s",token);
						break;
				}
				token = strtok( NULL, seps );	
			}
		}
		fclose(F);
	}
	else
		L.LogE("ERROR - Cannot find ACIS conversion file! %s\n",ACISConvTable);

	if (*ItemSubst) {//Reformatting
		count=0;
		l=strlen(ItemSubst);
		p=ItemSubst;
		s=new char[l+1];
		pp=s;
		while((*p)&&((*p<'0')||(*p>'9'))) p++;
		while(*p) {
			count++;
			while((*p>='0')&&(*p<='9')) *(pp++)=*(p++);
			*(pp++)=0;
			while((*p)&&((*p<'0')||(*p>'9'))) p++;
		}
		if (count%2==1) count--;
		if (!count) {//Zero substitutions on failure
			*ItemSubst=0;
		}
		else {
			long c=0;
			delete[] ItemSubst;
			ItemSubst=new char[count*13+2];
			memset(ItemSubst,'0',count*13);
			ItemSubst[count*13]=0;
			p=s;
			while (c<count) {
				long l=strlen(p);
				if (l>13) {p=p+(l-13);l=13;}
				memcpy(ItemSubst+c*13+(13-l),p,l);
				c++;
				p=p+l+1;
			}
		}
		delete[] s;
	}
	if (!FuelUpdFilter) r.ValueSZ("Fuel.FuelUpdateAllowedBarcodes",&FuelUpdFilter,"2155210000000,2155220000007,2155230000004,2190720000003");
	if (*FuelUpdFilter) {//Reformatting
		count=0;
		l=strlen(FuelUpdFilter);
		p=FuelUpdFilter;
		s=new char[l+1];
		pp=s;
		while((*p)&&((*p<'0')||(*p>'9'))) p++;
		while(*p) {
			count++;
			while((*p>='0')&&(*p<='9')) *(pp++)=*(p++);
			*(pp++)=0;
			while((*p)&&((*p<'0')||(*p>'9'))) p++;
		}
		if (!count) {//Zero substitutions on failure
			*FuelUpdFilter=0;
		}
		else {
			long c=0;
			delete[] FuelUpdFilter;
			FuelUpdFilter=new char[count*13+2];
			memset(FuelUpdFilter,'0',count*13);
			FuelUpdFilter[count*13]=0;
			p=s;
			while (c<count) {
				long l=strlen(p);
				if (l>13) {p=p+(l-13);l=13;}
				memcpy(FuelUpdFilter+c*13+(13-l),p,l);
				c++;
				p=p+l+1;
			}
		}
		delete[] s;
	}
	if (!ItemUpdFilter) r.ValueSZ("Fuel.ItemUpdateAllowedBarcodes",&ItemUpdFilter,"5999506630017,5999506630024,5999506630116,5999506630123,5999506630124");
	if (*ItemUpdFilter) {//Reformatting
		count=0;
		l=strlen(ItemUpdFilter);
		p=ItemUpdFilter;
		s=new char[l+1];
		pp=s;
		while((*p)&&((*p<'0')||(*p>'9'))) p++;
		while(*p) {
			count++;
			while((*p>='0')&&(*p<='9')) *(pp++)=*(p++);
			*(pp++)=0;
			while((*p)&&((*p<'0')||(*p>'9'))) p++;
		}
		if (!count) {//Zero substitutions on failure
			*ItemUpdFilter=0;
		}
		else {
			long c=0;
			delete[] ItemUpdFilter;
			ItemUpdFilter=new char[count*13+2];
			memset(ItemUpdFilter,'0',count*13);
			ItemUpdFilter[count*13]=0;
			p=s;
			while (c<count) {
				long l=strlen(p);
				if (l>13) {p=p+(l-13);l=13;}
				memcpy(ItemUpdFilter+c*13+(13-l),p,l);
				c++;
				p=p+l+1;
			}
		}
		delete[] s;
	}
	FuelFunction=r.ValueDW("Fuel.Function",FuelFunction);
	PosCount=r.ValueDW("Fuel.PoSCount",PosCount);
	PosSIDBase=r.ValueDW("Fuel.PoSSIDBase",PosSIDBase);
	InitCSC=r.ValueDW(C_InitCSC,InitCSC);
	ForceLogOffAtClosure=r.ValueDW("Fuel.ForceLogOffAtClosure",ForceLogOffAtClosure);
	AmountAsQuantity=r.ValueDW("Fuel.AmountAsQuantity",AmountAsQuantity);
	PriceByCentiLiters=r.ValueDW("Fuel.PriceByCentiLiters",PriceByCentiLiters);
	QuantityDigitLoss=r.ValueDW("Fuel.QuantityDigitLoss",QuantityDigitLoss);
	NoPerfLogMask=r.ValueDW("Fuel.NotLoggedPerformanceDetailsMask",NoPerfLogMask);
	ZeroTimeTxnTime=r.ValueDW("Fuel.ZeroSecondsTransactionTimeSubstituteValue",ZeroTimeTxnTime);
	AutoOpenStartTime=r.ValueDW("Fuel.AutoOpenStartTime",AutoOpenStartTime);
	AutoOpenEndTime=r.ValueDW("Fuel.AutoOpenEndTime",AutoOpenEndTime);
	disableEmptyChange=r.ValueDW("Fuel.DisableEmptyChangeTrans",0);
	AutoOpenEnabled=r.ValueDW("Fuel.AutoOpenEnabled",AutoOpenEnabled);
	GilBarco=r.ValueDW("Fuel.Gilbarco",GilBarco);
	namos=r.ValueDW("Fuel.Namos",namos);
	igass=r.ValueDW("Fuel.IGASS",igass);
	AutoActions=r.ValueDW("Fuel.Recovery.AutomaticActionsMask",AutoActions); //bit0:ProcSale.AutoLogOn bit1:ProcSale_AutoChgUser
	RefusedActions=r.ValueDW("Fuel.Recovery.RefusedActions",RefusedActions); //bit0:ProcLogOff.Refused logoff of a different user
	FuelZeroSizeRetriesMax=r.ValueDW("Fuel.ZeroSizeFileMaxRetries",FuelZeroSizeRetriesMax);
	CashRounding=r.ValueDW("Fuel.CashRounding",CashRounding);
	LastTranHP=r.ValueDW(C_RegValLastTranHP,LastTranHP);
//	r.ValueSZ("LaunchedProgram",&launch,"c:\\winnt\\system32\\cmd.exe");
//	r.ValueSZ("LaunchedProgParameters",&params,"/c \"c:\\AUFuelIF\\History.cmd\" >>stdout.txt 2>>stderr.txt");
	if (PosSIDBase>=0) {
		L.Log("The first POS number is: %d\n",PosSIDBase/2);
		if (APos=new TPos[PosCount+1]) {
			long siz;
			char name[32];
			char *p;
			long goodsiz;
			memset(&(APos[0]),0,sizeof (TPos)*(PosCount));
			for (l=0; l<PosCount; l++) {
				APos[l].sid=(short) (PosSIDBase+2*l);
				sprintf(name,C_RegValNameGenPosData,l);
				siz=sizeof(TPos)-(C_TENDERS-APos[l].tenders)*sizeof(TTender);
				p=NULL;
				r.ValueBin(name,&p,&siz,(char *) &(APos[l]));
				memcpy(&(APos[l]),p,(siz<sizeof(TPos)) ? siz : sizeof(TPos));
				goodsiz=sizeof(TPos)-(C_TENDERS-APos[l].tenders)*sizeof(TTender);
				if (siz<goodsiz) {
					memset(&(APos[l]),0,sizeof (TPos));
					APos[l].sid=(short) (PosSIDBase+2*l);
					L.LogE("ERROR: LoadVar.Fuel: PoS Saved data #%d is corrupt (short in size) in the registry! Zero values will be used!.\n"
						,l);
				}
				else if (siz>goodsiz) {
					L.LogE("Warning: LoadVar.Fuel: PoS Saved data #%d may be corrupt (too long) in the registry.\n"
						,l);
				}
				delete [] p;
			}
		}
	}
	else {
		L.LogE("ERROR: LoadVar.Fuel: PoS SID Base (%d) is invalid (Hasn't been set up?) - exiting.\n",PosSIDBase);
		res=-1000;//Exit
	}
	ItemDept[0]=r.ValueDW("Fuel.ItemDepartmentID",0);

	ISS_T_REPLY    rc;
    ISS_T_BOOLEAN  unknown;
	UINT16         precision;
    UINT16         buf;
	ISS_T_DB_KEY key[1];
    key[0].field_name           = "LOOKUP_GROUP";
    key[0].field_length         = 4;
    key[0].field_value          = &ItemDept[0];
    key[0].field_value_unknown  = FALSE;
    if ( (rc = ReadISS(&buf,FALSE,"ILGREF",1,key,ISS_C_DB_EQUAL,ISS_C_DB_NO_LOCK
		,"DEFAULT_GROUP",4,&ItemDept[1],FALSE)) == ISS_SUCCESS) {
		if (ItemDept[1]!=ItemDept[0]) {
			L.LogE("ERROR: LoadVar.Fuel.GetItemGroups: Default group ID doesn't match (%d<>%d)!\n"
				,ItemDept[0],ItemDept[1]);
			rc=-1;
		}
		if (rc==ISS_SUCCESS) {
			for(long i=0; i<8; i++) {
				rc=iss_db_extract_field(buf,"LOGGING_GROUPS",4,i,&ItemDept[i+1],&precision,&unknown);
				if (rc!=ISS_SUCCESS) {
					L.LogE("ERROR: LoadVar.Fuel.GetItemGroups: Extract LOGGING_GROUPS field Extent #%d failed with error code %d!\n"
						,i,rc);
				}
			}
		}
		if ((iss_db_free_buffer(&buf))!=ISS_SUCCESS) {
			L.LogE("ERROR: LoadVar.Fuel.GetItemGroups: Freeing buffer failed!\n");
		}
		if (rc!=ISS_SUCCESS) return rc;
	}
	else {
		L.LogE("ERROR: LoadVar.Fuel.GetItemGroups: Failed to get item grouping record! DB error=%d\n",rc);
		return -1;
	}

	
	// RVM	
	MachineTotals.ReadFromRegistry(false,(char *)C_RegKey,(char *)C_RegMachineTotals);
	TicketIssue.ReadFromRegistry(false,(char *)C_RegKey,(char *)C_RegTicketIssue);
	RItemTotals.ReadFromRegistry(false,(char *)C_RegKey,(char *)C_RegRItemTotals);
	//GItemTotals.ReadFromRegistry(false,(char *)C_RegKey,(char *)C_RegGItemTotals);
	LogFN=NULL;
	r.ValueSZ("RVM.LogFileName",&LogFN,"C:\\AURVMIf\\Log.txt");
	RL.SetLogFileName(LogFN);
	delete [] LogFN;
	LogFN=NULL;
	r.ValueSZ("RVM.LogFileName.ErrorsOnly",&LogFN,"C:\\AURVMIf\\ErrorLog.txt");
	RL.SetErrorLogFileName(LogFN);
	delete [] LogFN;
	if (!RDNIn) r.ValueSZ("RVM.Directory.Input",&RDNIn,"C:\\AURVMIf\\In");
	if (!RDNWork) r.ValueSZ("RVM.Directory.Working",&RDNWork,"C:\\AURVMIf\\Work");
	if (!RDNDone) r.ValueSZ("RVM.Directory.DoneToday",&RDNDone,"C:\\AURVMIf\\Done");
	if (!RDNHist) r.ValueSZ("RVM.Directory.History",&RDNHist,"C:\\AURVMIf\\History");
	if (!RDNUpdO) r.ValueSZ("RVM.Directory.UpdatesOut",&RDNUpdO,"C:\\AURVMIf\\Out");
	RPosCount=r.ValueDW("RVM.PoSCount",RPosCount);
	RPosSIDBase=r.ValueDW("RVM.PoSSID",RPosSIDBase);
	VoucherSafeOverWriteDays=r.ValueDW("RVM.VoucherSafeOverWriteDays",VoucherSafeOverWriteDays);
	RNoPerfLogMask=r.ValueDW("RVM.NotLoggedPerformanceDetailsMask",RNoPerfLogMask);
	RVMTxnTimePerItem=r.ValueDW("RVM.TransactionTimePerItem",RVMTxnTimePerItem);
	RVMFunction=r.ValueDW("RVM.Function",RVMFunction);
	TomraFunction=r.ValueDW("Tomra.Function",0);
	RoundVocher=r.ValueDW("RVM.RoundVocher",1);
	keepDays=r.ValueDW("RVM.KeepDays",100);
	RUser=r.ValueDW("RVM.TransactionUser",RUser);
	sprintf(RUserS,"%d",RUser);
	RTenderId=r.ValueDW("RVM.TenderId",RTenderId);
	RTenderIdPET=r.ValueDW("RVM.TenderIdPETLOG",RTenderIdPET);
	TranTimeIdleLimit=r.ValueDW("RVM.LastTransactionIdleTimeLimit",TranTimeIdleLimit);
	LocalTimeIdleLimit=r.ValueDW("RVM.LocalTimeIdleTimeLimit",LocalTimeIdleLimit);
	PendingClosure=r.ValueDW(C_RegPendingClosure,PendingClosure);
	VoucherPadChar=r.ValueDW("RVM.VoucherPaddingCharacter",VoucherPadChar);
	RSendLinks=r.ValueDW("RVM.UpdateSendItemLinks",RSendLinks);
	is_tomra=r.ValueDW("RVM.TomraSystem",0);
	if ((VoucherPadChar<0)||(VoucherPadChar>255)) {
		RL.LogE("Warning: LoadVar.RVM: Bad voucher padding character (%c) changed to '1'!\n"
			,VoucherPadChar);
		VoucherPadChar='1';
	}
	VoucherLeadNumber=r.ValueDW("RVM.VoucherLeadingNumber",VoucherLeadNumber);
	if ((VoucherLeadNumber<0)||(VoucherLeadNumber>9999)) {
		RL.LogE("Warning: LoadVar.RVM: Bad voucher leading number (%04d) changed to 9903!\n"
			,VoucherLeadNumber);
		VoucherLeadNumber=9903;
	}
	sprintf(VoucherLeadStr,"%d",VoucherLeadNumber);

	if (RPosSIDBase>=0) {
		long siz;
		char *p=NULL;
		char *p2=NULL;
		long goodsiz;
		memset(&RPos,0,sizeof (TRPos));
		RPos.sid=(short) RPosSIDBase;
		siz=sizeof(TRPos);
		p=NULL;
		if (!(r.ValueBin(C_RegValNameRPosDataV2,&p,&siz,NULL))) {//No V2 posdata yet
			if (p) {
				delete [] p;
				p=NULL;
			}
			siz=sizeof(TPos)-C_TENDERS*sizeof(TTender);
			if (r.ValueBin(C_RegValNameRPosData,&p,&siz,NULL)) {//Has V1 posdata
				memcpy(&RPos,p,(siz<sizeof(TRPos)) ? siz : sizeof(TRPos));
				goodsiz=sizeof(TPos)-C_TENDERS*sizeof(TTender);
				if (siz<goodsiz) {
					memset(&RPos,0,sizeof (TRPos));
					RPos.sid=(short) RPosSIDBase;
					RL.LogE("ERROR: LoadVar.RVM: RVM PoS V1 Saved data is corrupt (short in size) in the registry! Zero values will be used!.\n");
					//RSaveStatus();
				}
				else if (siz>goodsiz) {
					RL.LogE("Warning: LoadVar.RVM: RVM PoS V1 Saved data may be corrupt (too long) in the registry.\n");
				}
			}
			if (p) {
				delete [] p;
				p=NULL;
			}
			siz=sizeof(TRPos);
			r.ValueBin(C_RegValNameRPosDataV2,&p,&siz,(char *) &(RPos));
			RL.Log("Info: LoadVar.RVM: RVM PoS V1 Saved data has been upgraded to V2.\n");
		}
		else {//Has V2 posdata
		//r.ValueBin(C_RegValNameRPosData,&p,&siz,(char *) &(RPos));
			memcpy(&RPos,p,(siz<sizeof(TRPos)) ? siz : sizeof(TRPos));
			goodsiz=sizeof(TRPos);
			if (siz<goodsiz) {
				memset(&RPos,0,sizeof (TRPos));
				RPos.sid=(short) RPosSIDBase;
				RL.LogE("ERROR: LoadVar.RVM: RVM PoS Saved data is corrupt (short in size) in the registry! Zero values will be used!.\n");
				//RSaveStatus();
			}
			else if (siz>goodsiz) {
				RL.LogE("Warning: LoadVar.RVM: RVM PoS Saved data may be corrupt (too long) in the registry.\n");
			}
		}
		delete [] p;
	}
	else {
		RL.LogE("ERROR: LoadVar.RVM: RVM PoS SID (%d) is invalid (Hasn't been set up?) - exiting.\n",RPosSIDBase);
		res=-1000;//Exit
	}
	
	RItemDept[0]=r.ValueDW("RVM.ItemDepartmentID",0);

    key[0].field_name           = "LOOKUP_GROUP";
    key[0].field_length         = 4;
    key[0].field_value          = &RItemDept[0];
    key[0].field_value_unknown  = FALSE;
    if ( (rc = ReadISS(&buf,FALSE,"ILGREF",1,key,ISS_C_DB_EQUAL,ISS_C_DB_NO_LOCK
		,"DEFAULT_GROUP",4,&RItemDept[1],FALSE)) == ISS_SUCCESS) {
		if (RItemDept[1]!=RItemDept[0]) {
			RL.LogE("ERROR: LoadVar.RVM.GetItemGroups: Default group ID doesn't match (%d<>%d)!\n"
				,RItemDept[0],RItemDept[1]);
			rc=-1;
		}
		if (rc==ISS_SUCCESS) {
			for(long i=0; i<8; i++) {
				rc=iss_db_extract_field(buf,"LOGGING_GROUPS",4,i,&RItemDept[i+1],&precision,&unknown);
				if (rc!=ISS_SUCCESS) {
					RL.LogE("ERROR: LoadVar.RVM.GetItemGroups: Extract LOGGING_GROUPS field Extent #%d failed with error code %d!\n"
						,i,rc);
				}
			}
		}
		if ((iss_db_free_buffer(&buf))!=ISS_SUCCESS) {
			RL.LogE("ERROR: LoadVar.RVM.GetItemGroups: Freeing buffer failed!\n");
		}
		if (rc!=ISS_SUCCESS) return rc;
	}
	else {
		RL.LogE("ERROR: LoadVar.RVM.GetItemGroups: Failed to get item grouping record! DB error=%d\n",rc);
		return -1;
	}
	RItemDeptPET[0]=r.ValueDW("RVM.PETItemDepartmentID",0);

    key[0].field_name           = "LOOKUP_GROUP";
    key[0].field_length         = 4;
    key[0].field_value          = &RItemDeptPET[0];
    key[0].field_value_unknown  = FALSE;
    if ( (rc = ReadISS(&buf,FALSE,"ILGREF",1,key,ISS_C_DB_EQUAL,ISS_C_DB_NO_LOCK
		,"DEFAULT_GROUP",4,&RItemDeptPET[1],FALSE)) == ISS_SUCCESS) {
		if (RItemDeptPET[1]!=RItemDeptPET[0]) {
			RL.LogE("ERROR: LoadVar.PET.GetItemGroups: Default group ID doesn't match (%d<>%d)!\n"
				,RItemDeptPET[0],RItemDeptPET[1]);
			rc=-1;
		}
		if (rc==ISS_SUCCESS) {
			for(long i=0; i<8; i++) {
				rc=iss_db_extract_field(buf,"LOGGING_GROUPS",4,i,&RItemDeptPET[i+1],&precision,&unknown);
				if (rc!=ISS_SUCCESS) {
					RL.LogE("ERROR: LoadVar.PET.GetItemGroups: Extract LOGGING_GROUPS field Extent #%d failed with error code %d!\n"
						,i,rc);
				}
			}
		}
		if ((iss_db_free_buffer(&buf))!=ISS_SUCCESS) {
			RL.LogE("ERROR: LoadVar.PET.GetItemGroups: Freeing buffer failed!\n");
		}
		if (rc!=ISS_SUCCESS) return rc;
	}
	else {
		RL.LogE("ERROR: LoadVar.PET.GetItemGroups: Failed to get item grouping record! DB error=%d\n",rc);
		return -1;
	}
	return res;
}

int PreFetchData(void)
{
    ISS_T_REPLY    result;
    UINT16         buffer_id;

	//Read Store Number
	result=ReadISS(&buffer_id, false, "SASSTAT", 0, NULL, ISS_C_DB_FIRST, ISS_C_DB_NO_LOCK,
		"STOREID", sizeof(StoreNum), &StoreNum, true);
	if (result != ISS_SUCCESS) {
		if (result == ISS_E_SDBMS_NOREC)
			L.LogE("ERROR: Cannot find store number detail!");
		else L.LogE("ERROR: Cannot retrieve store number! - DataBase offline?");
		return -1;
	}
	L.Log("Info: Store number: %d.\n",StoreNum);
	return 0;
}

char GenCD(const char * txt)
{
	char text[14]="0000000000000";
	const char resp[]="0987654321";
	int x=0;
	strcpy(text+(13-strlen(txt)),txt);
	for (int i=0; i<12; i+=2) {
		x = x + (text[i]-'0') + 3 * (text[i+1]-'0');
	}
	return(resp[x%10]);
}

bool DoItemSubst(char *s,bool reverse)
{
	char *p=ItemSubst;
	while (*p) {
		if (memcmp(s,reverse?p+13:p,13)==0) {
			memcpy(s,reverse?p:p+13,13);
			return true;
		}
		p+=26;
	}
	return false;
}

bool ItemOnList(char *s)
{
	char *p=ItemUpdFilter;
	while (*p) {
		if (memcmp(s,p,13)==0) return true;
		p+=13;
	}
	return false;
}

bool FuelOnList(char *s)
{
	char *p=FuelUpdFilter;
	while (*p) {
		if (memcmp(s,p,13)==0) return true;
		p+=13;
	}
	return false;
}

void SignalCleanExit2False(void)
{
	//Log("Marking exit as dirty - in the registry.");
	Reg r(false, C_RegKey);
	r.SetValueDW(C_RegVNDirtyExit,1);
}

int SaveStatus(bool exit=true, long pos=-1)
{
	long l;
	char name[24];
	long goodsiz;
	Reg r(false, C_RegKey);
	for ((pos==-1) ? l=0 : l=pos; l<((pos!=-1) ? pos+1 : PosCount); l++) {
		sprintf(name,C_RegValNameGenPosData,l);
		goodsiz=sizeof(TPos)-(C_TENDERS-APos[l].tenders)*sizeof(TTender);
		r.SetValueBin(name,(char *) &(APos[l]),goodsiz);
		L.Log("Info: PoS #%d status has been saved to the registry.\n",l);
	}
	if (exit) {
		r.SetValueDW(C_RegVNDirtyExit,0);
		L.Log("Info: Overall status has been saved, unmarked dirty-exit in the registry.\n");
	}
	return 0;
}

int RSaveStatus(void)
{
	long goodsiz;
	Reg r(false, C_RegKey);
	goodsiz=sizeof(TRPos);
	r.SetValueBin(C_RegValNameRPosDataV2,(char *) &(RPos),goodsiz);
	RL.Log("Info: RVM PoS status has been saved to the registry.\n");
	return 0;
}

int GetProgressStatus(void)
{
	//To check
	SINT32 tranno;
	SINT16 elemno;
	ISS_T_BOOLEAN inprogress;
	ISS_T_REPLY retval;
	int subretval=0;
	//To resolve
	ISS_T_LOG_BLOCK_HANDLE handle;
	ISS_T_LOG_ELEM_ID element;
	char transuser[9];
	ISS_T_INT_TIME elem_time;
	ISS_T_INT_DATE Date;
	SINT16 hp;
	SYSTEMTIME t;
	while (true) {
		if ((retval=iss46_hlog_get_current_hp(&hp))==ISS_SUCCESS) break;
		L.LogE("ERROR: GetProgress.Fuel.get_current_hp: %d, retval=%d - will retry in 30secs!\n",hp,retval);
		Sleep(30000);
	}
	L.Log("Info: GetProgress.get_current_hp: %d (successful).\n",hp);
	if (FuelFunction) {
		for (long i=0; i<PosCount; i++) {
			while (true) {
				if ((retval=iss46_hlog_get_curr_trans_num(APos[i].sid,&tranno,&inprogress,&elemno))==ISS_SUCCESS) {
					//successfully read sid #n's status
					if (!inprogress) {
						L.Log("Info: GetProgress.Fuel: No transaction in progress. SID=%d Tran=%d Elem=%d\n"
							,APos[i].sid,tranno,elemno);
						break;
					}
					else {
						L.LogE("Warning: GetProgress.Fuel: Transaction in progress! SID=%d Tran=%d Elem=%d\n"
							,APos[i].sid,tranno,elemno);

						GetLocalTime(&t);
						elem_time=t.wHour*3600+t.wMinute*60+t.wSecond;

						Date=DateYMD2Int(t.wYear,t.wMonth,t.wDay);

						element.element_num=elemno;
						element.element_time=elem_time;
						memcpy(transuser,RUserS,9);

						if ((retval=iss46_hlog_get_current_hp(&hp))!=ISS_SUCCESS) {
							L.LogE("ERROR: GetProgress.Fuel.get_current_hp: %d, posn=%d retval=%d\n",hp,i+1,retval);
						}
						else {
							L.Log("Info: GetProgress.Fuel.get_current_hp: %d\n",hp);

							retval=iss46_hlog_start_block(APos[i].sid,hp,tranno,Date,elem_time
								,0,0x00018000,element,9,transuser,&handle);
							if (retval!=ISS_SUCCESS) {
								L.LogE("ERROR: GetProgress.Fuel.start_new_block: Bad return value! (%d) posn=%d\n",retval,i+1);
							}
							else {
								L.Log("Info: GetProgress.Fuel: start_new_block: successful.\n");

								if ((retval=iss46_hlog_void_trans(handle,Date,elemno))!=ISS_SUCCESS) {
									L.LogE("ERROR: GetProgress.Fuel: void_trans returned: %d! posn=%d\n",retval,i+1);
								}
								else L.Log("Info: GetProgress.Fuel: void_trans successful.\n");
							}
						}
						Sleep(5000);
					}
				}
				else {
					L.LogE("ERROR: GetProgress.Fuel: Unable to get current transaction! posn=%d\n",i+1);
					subretval=3;
				}
			}
		}
	}
	if (RVMFunction) {
		while (true) {
			if ((retval=iss46_hlog_get_curr_trans_num(RPos.sid,&tranno,&inprogress,&elemno))==ISS_SUCCESS) {
				//successfully read sid #n's status
				if (!inprogress) {
					RL.Log("Info: GetProgress.RVM: No transaction in progress. SID=%d Tran=%d Elem=%d\n"
						,RPos.sid,tranno,elemno);
					break;
				}
				else {
					RL.LogE("Warning: GetProgress.RVM: Transaction in progress! SID=%d Tran=%d Elem=%d\n"
						,RPos.sid,tranno,elemno);
					
					GetLocalTime(&t);
					elem_time=t.wHour*3600+t.wMinute*60+t.wSecond;

					Date=DateYMD2Int(t.wYear,t.wMonth,t.wDay);

					element.element_num=elemno;
					element.element_time=elem_time;
					memcpy(transuser,RUserS,9);

					if ((retval=iss46_hlog_get_current_hp(&hp))!=ISS_SUCCESS) {
						RL.LogE("ERROR: GetProgress.RVM.get_current_hp: %d, retval=%d\n",hp,retval);
					}
					else {
						RL.Log("Info: GetProgress.RVM.get_current_hp: %d\n",hp);

						retval=iss46_hlog_start_block(RPos.sid,hp,tranno,Date,elem_time
							,0,0x00018000,element,9,transuser,&handle);
						if (retval!=ISS_SUCCESS) {
							RL.LogE("ERROR: GetProgress.RVM.start_new_block: Bad return value! (%d)\n",retval);
						}
						else {
							RL.Log("Info: GetProgress.RVM: start_new_block: successful.\n");

							if ((retval=iss46_hlog_void_trans(handle,Date,elemno))!=ISS_SUCCESS) {
								RL.LogE("ERROR: GetProgress.RVM: void_trans returned: %d!\n",retval);
							}
							else RL.Log("Info: GetProgress.RVM: void_trans successful.\n");
						}
					}
					Sleep(5000);
				}
			}
			else {
				RL.LogE("ERROR: GetProgress.RVM: Unable to get current transaction!\n");
				subretval=3;
			}
		}
		if (RPos.lasttranhp!=hp) *RPos.user=0; //user should be invalid since hp has been changed
	}
	return subretval;
}

inline void FreeArr(void *p)
{
	if (p) {delete [] p; p=NULL;}
}

void CleanUp(void)
{
	FreeArr(FNItem);
	FreeArr(APos);
	FreeArr(VatConvTable);
	FreeArr(DNIn);
	FreeArr(ACISDNIn);
	FreeArr(ACISDNOut);
	FreeArr(ACISConvTable);
	FreeArr(VPOPDataBase);
	FreeArr(VPOPServerIP);
	FreeArr(VPOPUser);
	FreeArr(VPOPPassw);
	FreeArr(DNWork);
	FreeArr(DNDone);
	FreeArr(DNHist);
	FreeArr(FNCSCI);
	FreeArr(FNCSCW);
	FreeArr(DNUpdO);
	FreeArr(RDNIn);
	FreeArr(RDNWork);
	FreeArr(RDNDone);
	FreeArr(RDNHist);
	FreeArr(RDNUpdO);
	FreeArr(ItemSubst);
	FreeArr(ItemUpdFilter);
	FreeArr(FNItemList);
	FreeArr(FNTomraEAN);
}

bool FileNameOk(char *n,bool blockautopos)
{
	char *p;
	long l;
	if (strlen(n)<11) return false;
	if (((*n|32)=='z')||((*n|32)=='i')) {
		p=n+1;
		l=12;
		if ((*p|32)=='i') p++;
		if ((*n|32)=='z') l=6;
		else { //check for blockautopos
			//file names after i or ii are DDHHMMSS.TTTP
			if (blockautopos) //check for 4<=P<=6
				if ((p[12]>='4')||((p[12]<='6'))) return false;
		}
		while (l) {
			l--;
			if (l==8) {//point?
				if (p[l]!='.') return false;
			}
			else {//number?
				if ((p[l]<'0')||(p[l]>'9')) return false;
			}
		}
		return true;
	}
	return false;
}

bool RVMFileNameOk(char *n)
{
	long l=19;
	if (strlen(n)<19) return false;
	while (l) {
		l--;
		if (l==14) {//point?
			if (n[l]!='.') return false;
		}
		else {//number?
			if ((n[l]<'0')||(n[l]>'9')) return false;
		}
	}
	return true;
}

bool TomraFileNameOk(char *n)
{
	//YYMMDDhhmmss_fffffffarrrrrrpppppp.ler
	long l=31;
	char str[10];
	if (strlen(n)<31) return false;
	while (l) {
		l--;
		if (l==27) {//point?
			if (n[l]!='.') return false;
		}
		else {//number?
			if(l<27)
				if ((n[l]<'0')||(n[l]>'9'))
					if(n[l]!='_'){
						RL.Log("TOMRA -- The filename contains an incorrect character (%s)\n",n);
						return false;
					}
		}
	}
	strcpyi(str,n+13,7);
	str[7]=0;
	if(VoucherLeadNumber-9900!=atol(str)) {
		RL.Log("TOMRA -- store number is not equal (%d != %d)\n",(VoucherLeadNumber-9900),atol(str));
		return false;
	}
	return true;
}

void SysTimeFromLong(SYSTEMTIME *t,long s)
{
	t->wHour=floor(s/3600);
	t->wMinute=floor((s-t->wHour*3600)/60);
	t->wSecond=s-t->wHour*3600-t->wMinute*60;
}


void SysTimeFromS14(SYSTEMTIME *t,char *s)
{
	t->wYear=(s[0]-'0')*1000+(s[1]-'0')*100+(s[2]-'0')*10+(s[3]-'0');
	t->wMonth=(s[4]-'0')*10+(s[5]-'0');
	t->wDay=(s[6]-'0')*10+(s[7]-'0');
	t->wHour=(s[8]-'0')*10+(s[9]-'0');
	t->wMinute=(s[10]-'0')*10+(s[11]-'0');
	t->wSecond=(s[12]-'0')*10+(s[13]-'0');
}

void SysTimeFromS18(SYSTEMTIME *t,char *s)
{
	t->wYear=(s[0]-'0')*1000+(s[1]-'0')*100+(s[2]-'0')*10+(s[3]-'0');
	t->wMonth=(s[5]-'0')*10+(s[6]-'0');
	t->wDay=(s[8]-'0')*10+(s[9]-'0');
	t->wHour=(s[10]-'0')*10+(s[11]-'0');
	t->wMinute=(s[13]-'0')*10+(s[14]-'0');
	t->wSecond=(s[16]-'0')*10+(s[17]-'0');
}


bool CleanUserID(char *s) //true=valid name
{
	if (s[3]==' ') {//kill trailing spaces
		s[3]=0;
		if (s[2]==' ') {
			s[2]=0;
			if (s[1]==' ') {
				s[1]=0;
				if (*s==' ') *s=0;
			}
		}
	}
	while ((*s==' ')||(*s=='0')) {//kill leading zeroes or spaces
		memmove(s,s+1,3);
		s[3]=0;
	}
	if (*s) return (*s);
	//At least one zero must remain
	*s='0';
	s[1]=0;
	return (*s);
}

void LongVal2Cobol4(unsigned char s[4], long l)
{
	s[0]=(((char *)&l)[3])^0x80;
	s[1]=((char *)&l)[2];
	s[2]=((char *)&l)[1];
	s[3]=((char *)&l)[0];
}

void LongVal2Cobol6My(unsigned char s[6], long l)
{
	long d=0,f=0;
//	char *d;
	s[0]=0x80;
//	d=((char *)&l);
	s[1]=0;
	d=l/1677216;
	s[2]= d; //*16777216
	f=d*1677216;
	d=(l-f)/65536;
	s[3]=d; //*65536
	f=f+(d*65536);
	d=(l-f)/256;
	s[4]=d; //*256
	f=f+(d*256);
	d=l-f;
	s[5]=f; //*1
}

void LongVal2Cobol6(unsigned char s[6], long l)
{
//	char *d;
	s[0]=0x80;
//	d=((char *)&l);
	s[1]=0;
	s[2]=((char *)&l)[3];
	s[3]=((char *)&l)[2];
	s[4]=((char *)&l)[1];
	s[5]=((char *)&l)[0];
}

void LongVal2Cobol10(unsigned char s[10], long l)
{
	s[0]=0x80;
	s[1]=0;
	s[2]=0;
	s[3]=0;
	s[4]=0;
	s[5]=0;
	s[6]=((char *)&l)[3];
	s[7]=((char *)&l)[2];
	s[8]=((char *)&l)[1];
	s[9]=((char *)&l)[0];
}

void GDec102Cobol10(unsigned char s[6], ISS_GDECIMAL_10 l)
{
	s[0]=(*(((char *)l)+11))^0x80;
	s[1]=*(((char *)l)+10);
	s[2]=*(((char *)l)+9);
	s[3]=*(((char *)l)+8);
	s[4]=*(((char *)l)+7);
	s[5]=*(((char *)l)+6);
	s[6]=*(((char *)l)+5);
	s[7]=*(((char *)l)+4);
	s[8]=*(((char *)l)+3);
	s[9]=*(((char *)l)+2);
}

void GDec62Cobol6(unsigned char s[6], ISS_GDECIMAL_6 l)
{
	s[0]=(*(((char *)l)+7))^0x80;
	s[1]=*(((char *)l)+6);
	s[2]=*(((char *)l)+5);
	s[3]=*(((char *)l)+4);
	s[4]=*(((char *)l)+3);
	s[5]=*(((char *)l)+2);
}

void FillPerfElement(long pos, TElem21097 &e, ISS_T_INT_DATE date, ISS_T_INT_TIME time)
//By the given period closing date and time, the intermediate period data will be put into e
//Then all periodic data will be made zero to restart counting for the next perf period
{
	PreFill(&e,21097);

	e.PerfSlotStartDate=APos[pos].p_startdate;
	e.PerfSlotStartTime=APos[pos].p_starttime;
	e.LocSignOnTtl=(date-e.PerfSlotStartDate)*86400+(time-e.PerfSlotStartTime);
	e.LocRegTimeTtl=APos[pos].p_regtime;
	e.LocTenderTimeTtl=APos[pos].p_tendertime;
	e.LocItemKeyCodeTtl=APos[pos].p_itemcnt;
	e.PerCntSales=APos[pos].p_custcnt;
	LongVal2Cobol6(e.PerValSales,APos[pos].p_salesval*100);
	e.PerCntItem=APos[pos].p_itemcnt;
	LongVal2Cobol6(e.PerValItem,APos[pos].p_salesval*100);
	e.PerCntReturn=0;
	LongVal2Cobol6(e.PerValReturn,0);
	e.PerCntItemReturn=0;
	LongVal2Cobol6(e.PerValItemReturn,0);
	LongVal2Cobol6(e.PerValItemNeg, 0);
	
	//e.NumberOfCustomers=APos[pos].p_custcnt;

	APos[pos].p_startdate=date;
	APos[pos].p_starttime=time;
	APos[pos].p_custcnt=0;
	APos[pos].p_itemcnt=0;
	APos[pos].p_regtime=0;
	APos[pos].p_salesval=0;
	APos[pos].p_tendertime=0;
}

bool Check4NewPerfSlot(long pos, ISS_T_INT_DATE date, ISS_T_INT_TIME time) //true: moved to another slot
//Checking whether needed to advance to the next performance slot.
//If needed one transaction will be made, and perf slot rolled to the next period.
//So gotta be called with while to write more than one intermediate period!
{
	ISS_T_INT_DATE dt=APos[pos].p_startdate;
	ISS_T_INT_DATE tm=APos[pos].p_starttime;
	tm+=PerfPeriod;
	tm-=tm%PerfPeriod;
	if (tm>86400) {//next day comes
		tm-=86400;
		dt++;
	}
	if (dt>date) return false; //next slot is on the next day compared to tran -> no advance
	if ((dt==date)&&(tm>time)) return false; //next slot is later than tran -> no advance
	TElem21097 E;
	if (dt+5<date) {
		L.LogE("Warning: Check4NewPerfSlot: Date difference is more than 5 days. last:%d.%d this:%d.%d.\n",dt,tm,date,time);
		L.LogE("==> Check4NewPerfSlot only started a brand new slot.\n");
		FillPerfElement(pos,E,date,time);
		SaveStatus(false,pos);
		return false;
	}
	L.Log("Info: Check4NewPerfSlot: Have to close a previous performance slot. End date=%d time=%d\n"
		,dt,tm);

	FillPerfElement(pos,E,dt,tm);

	if ((NoPerfLogMask&0x0000001)==0) {
		SINT16 hp;
		ISS_T_LOG_BLOCK_HANDLE  handle;
		ISS_T_REPLY retval=0;
		if ((retval=iss46_hlog_get_current_hp(&hp))!=ISS_SUCCESS) {
			L.LogE("ERROR: Check4NewPerfSlot.get_current_hp: %d, retval=%d\n",hp,retval);
			L.LogE("==> Check4NewPerfSlot will be aborted or retried!\n");
			return retval;
		}
		L.Log("Info: Check4NewPerfSlot.get_current_hp: %d\n",hp);

		//Checks done before opening a new transaction
		SINT32        transno;
		ISS_T_BOOLEAN inprogress;
		SINT16        element_num;

		retval=iss46_hlog_get_curr_trans_num(APos[pos].sid,&transno,&inprogress,&element_num);
		L.Log("Info: Check4NewPerfSlot.check_before_start.get_curr_trans: tr=%d, open=%d, elem=%d\n"
			,transno,inprogress,element_num);
		if (retval!=ISS_SUCCESS) {
			L.LogE("ERROR: Check4NewPerfSlot.check_before_start.get_curr_trans: Bad return value! (%d)\n",retval);
			return false;
		}
		if (inprogress) {
			L.LogE("ERROR: Check4NewPerfSlot.check_before_start: Transaction must not be open at this point!\n");
			return false;
		}
		APos[pos].lasttran++;//Advance to the new transaction number
		if (APos[pos].lasttran!=transno) {
			L.LogE("Warning: Check4NewPerfSlot.check_before_start: Transaction numbers doesn't match!\n");
			L.LogE("==> APos[%d].lasttran=%d ISSLOG.transno=%d SID=%d\n"
				,pos,APos[pos].lasttran,transno,APos[pos].sid);
		}

		//Opening a transaction
		char transuser[9]={0,0,0,0,0,0,0,0,0};
		memcpy(transuser,APos[pos].user,4);
		
		ISS_T_LOG_ELEM_ID element;
		element.element_num=1;
		element.element_time=time;


		if ((retval=iss46_hlog_start_block(APos[pos].sid,hp,APos[pos].lasttran,dt,tm
			,0,0x00018000,element,9,transuser,&handle))!=ISS_SUCCESS) {
			L.LogE("ERROR: Check4NewPerfSlot.start_tran returned: %d -> retrying!\n",retval);
			return(retval);
		}
		else L.Log("Check4NewPerfSlot.start_tran: Successful.\n");

		retval=iss46_hlog_send_element(handle,21097,tm,0x0000,sizeof(TElem21097),(char*)&E);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcLogoff.21097 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,dt,2))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcLogoff.21097: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcLogoff.21097: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("ProcLogoff.21097: successful.\n");

		retval=iss46_hlog_end_trans(handle,dt,2);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: Check4NewPerfSlot.end_tran returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,dt,2))!=ISS_SUCCESS) {
				L.LogE("ERROR: Check4NewPerfSlot.end_tran: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: Check4NewPerfSlot.end_tran: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("Check4NewPerfSlot.end_tran: successful.\n");

//		retval=iss46_hlog_send_block(handle);
//		L.Log("Check4NewPerfSlot.sendb: %d\n",retval);

	}
	else {
		L.Log("Check4NewPerfSlot: Making performance log records has been disabled.(L1)\n");
	}

	SaveStatus(false,pos);

	return true;
}

void GetTaxRate(long * rate,ISS_GDECIMAL_6 ean)
{
	ISS_T_REPLY    rc;
    UINT16         buf;
	ISS_T_DB_KEY key[1];
    key[0].field_name           = "ITEM_CODE";
    key[0].field_length         = sizeof(ean);
    key[0].field_value          = ean;
    key[0].field_value_unknown  = FALSE;
	if ( (rc = ReadISS(&buf,FALSE,"ILIITEM",1,key,ISS_C_DB_EQUAL,ISS_C_DB_NO_LOCK
		,"TAX_RATES",4,rate,TRUE)) != ISS_SUCCESS) {
		L.LogE("ERROR: GetTaxRate failed with error code %d!\n",rc);
		*rate=0;
	}
}

long GetItemData(ISS_GDECIMAL_4 uprice, long * rate, long * reportg, long * eantype
				 ,ISS_GDECIMAL_6 ean)
{
	ISS_T_REPLY    rc;
	ISS_T_REPLY    result;
    UINT16         buf;
    ISS_T_BOOLEAN unknown;
    UINT16        precision;
	ISS_T_DB_KEY key[1];
    key[0].field_name           = "ITEM_CODE";
    key[0].field_length         = sizeof(ean);
    key[0].field_value          = ean;
    key[0].field_value_unknown  = FALSE;
	if ( (rc = ReadISS(&buf,FALSE,"ILIITEM",1,key,ISS_C_DB_EQUAL,ISS_C_DB_NO_LOCK
		,"PRICE",4,uprice,FALSE)) == ISS_SUCCESS) {

		rc=iss_db_extract_field(buf,"REPORT_GROUP",4,0,reportg,&precision,&unknown);
		if (rc!=ISS_SUCCESS) {
			RL.LogE("ERROR: GetItemData: Extract REPORT_GROUP field failed with error code %d!\n",rc);
			*reportg=0;
		}

		rc=iss_db_extract_field(buf,"TAX_RATES",4,0,rate,&precision,&unknown);
		if (rc!=ISS_SUCCESS) {
			RL.LogE("ERROR: GetItemData: Extract TAX_RATES field extent 0 failed with error code %d!\n",rc);
			*rate=2;
		}
	
		rc=iss_db_extract_field(buf,"EAN_TYPE",4,0,eantype,&precision,&unknown);
		if (rc!=ISS_SUCCESS) {
			RL.LogE("ERROR: GetItemData: Extract EAN_TYPE field failed with error code %d!\n",rc);
			*eantype=1;
		}
	
	}
	else {
		RL.LogE("ERROR: GetItemData: ReadISS failed with error code %d!\n",rc);
		*uprice=1;
	}
    if ( (result = iss_db_free_buffer (&buf) ) != ISS_SUCCESS) {
		RL.LogE("ERROR: GetItemData: Free buffer failed with error code %d!\n",result);
	}

	return rc;
}

long GetItemDataDept(ISS_GDECIMAL_4 uprice, long * rate, long * reportg, long * eantype, long * deptid ,ISS_GDECIMAL_6 ean)
{
	ISS_T_REPLY    rc;
	ISS_T_REPLY    result;
    UINT16         buf;
    ISS_T_BOOLEAN unknown;
    UINT16        precision;
	ISS_T_DB_KEY key[1];
    key[0].field_name           = "ITEM_CODE";
    key[0].field_length         = sizeof(ean);
    key[0].field_value          = ean;
    key[0].field_value_unknown  = FALSE;
	if ( (rc = ReadISS(&buf,FALSE,"ILIITEM",1,key,ISS_C_DB_EQUAL,ISS_C_DB_NO_LOCK
		,"PRICE",4,uprice,FALSE)) == ISS_SUCCESS) {

		rc=iss_db_extract_field(buf,"LOOKUP_GROUP",4,0,deptid,&precision,&unknown);
		if (rc!=ISS_SUCCESS) {
			RL.LogE("ERROR: GetItemData: Extract LOOKUP_GROUP field failed with error code %d!\n",rc);
			*deptid=0;
		}
		
		rc=iss_db_extract_field(buf,"REPORT_GROUP",4,0,reportg,&precision,&unknown);
		if (rc!=ISS_SUCCESS) {
			RL.LogE("ERROR: GetItemData: Extract REPORT_GROUP field failed with error code %d!\n",rc);
			*reportg=0;
		}

		rc=iss_db_extract_field(buf,"TAX_RATES",4,0,rate,&precision,&unknown);
		if (rc!=ISS_SUCCESS) {
			RL.LogE("ERROR: GetItemData: Extract TAX_RATES field extent 0 failed with error code %d!\n",rc);
			*rate=2;
		}
	
		rc=iss_db_extract_field(buf,"EAN_TYPE",4,0,eantype,&precision,&unknown);
		if (rc!=ISS_SUCCESS) {
			RL.LogE("ERROR: GetItemData: Extract EAN_TYPE field failed with error code %d!\n",rc);
			*eantype=1;
		}
	
	}
	else {
		RL.LogE("ERROR: GetItemData: ReadISS failed with error code %d!\n",rc);
		*uprice=1;
	}
    if ( (result = iss_db_free_buffer (&buf) ) != ISS_SUCCESS) {
		RL.LogE("ERROR: GetItemData: Free buffer failed with error code %d!\n",result);
	}

	return rc;
}

long GetItemDataFromReportGroup(ISS_GDECIMAL_4 uprice, long * rate, long * reportg, long * eantype, long * deptid ,ISS_GDECIMAL_6 ean)
{
	ISS_T_REPLY    rc;
	ISS_T_REPLY    result;
    UINT16         buf;
    ISS_T_BOOLEAN unknown;
    UINT16        precision;
	ISS_T_DB_KEY key[1];
    key[0].field_name           = "ITEM_CODE";
    key[0].field_length         = sizeof(ean);
    key[0].field_value          = ean;
    key[0].field_value_unknown  = FALSE;
	if ( (rc = ReadISS(&buf,FALSE,"ILIITEM",1,key,ISS_C_DB_EQUAL,ISS_C_DB_NO_LOCK
		,"PRICE",4,uprice,FALSE)) == ISS_SUCCESS) {

		rc=iss_db_extract_field(buf,"LOOKUP_GROUP",4,0,deptid,&precision,&unknown);
		if (rc!=ISS_SUCCESS) {
			RL.LogE("ERROR: GetItemData: Extract LOOKUP_GROUP field failed with error code %d!\n",rc);
			*deptid=0;
		}
		
		rc=iss_db_extract_field(buf,"REPORT_GROUP",4,0,reportg,&precision,&unknown);
		if (rc!=ISS_SUCCESS) {
			RL.LogE("ERROR: GetItemData: Extract REPORT_GROUP field failed with error code %d!\n",rc);
			*reportg=0;
		}

		rc=iss_db_extract_field(buf,"TAX_RATES",4,0,rate,&precision,&unknown);
		if (rc!=ISS_SUCCESS) {
			RL.LogE("ERROR: GetItemData: Extract TAX_RATES field extent 0 failed with error code %d!\n",rc);
			*rate=2;
		}
	
		rc=iss_db_extract_field(buf,"EAN_TYPE",4,0,eantype,&precision,&unknown);
		if (rc!=ISS_SUCCESS) {
			RL.LogE("ERROR: GetItemData: Extract EAN_TYPE field failed with error code %d!\n",rc);
			*eantype=1;
		}
	
	}
	else {
		RL.LogE("ERROR: GetItemData: ReadISS failed with error code %d!\n",rc);
		*uprice=1;
	}
    if ( (result = iss_db_free_buffer (&buf) ) != ISS_SUCCESS) {
		RL.LogE("ERROR: GetItemData: Free buffer failed with error code %d!\n",result);
	}

	return rc;
}


void AddTendering(long posn, long id, long val)
{
	for (long l=0; l<APos[posn].tenders;l++) {
		if (APos[posn].tender[l].id==id) break;
	}
	if (l==APos[posn].tenders) {
		if (APos[posn].tenders==C_TENDERS) {
			L.LogE("ERROR: AddTendering: Cannot register tender #%d! No more tenders can fit! Drawers will be corrupted!\n",id);
			return;
		}
		APos[posn].tenders++;
		APos[posn].tender[l].id=id;
		APos[posn].tender[l].cnt=(val>0) ? 1 : 0;
		APos[posn].tender[l].val=val;
		L.Log("Info: AddTendering: %d is a new tender in array loc %d. First value=%d.\n"
			,id,l,val);
	}
	else {
		APos[posn].tender[l].cnt+=(val>0) ? 1 : 0;
		APos[posn].tender[l].val+=val;
		L.Log("Info: AddTendering: %d is tender found in array loc %d. Added value=%d.\n"
			,id,l,val);
	}
}

Xv ISS_T_REPLY Xw ISSCALL SafeSendHLogElement(
						long					 posn,
                        ISS_T_LOG_BLOCK_HANDLE * phandle,
                        SINT16                   element_type,
                        ISS_T_INT_TIME           elem_time,
                        UINT16                   elem_flags,
                        ISS_T_BYTE               user_data_size,
                        char ISSPTR              user_data)
{
	ISS_T_REPLY retval;
	retval=iss46_hlog_send_element(*phandle,element_type,elem_time,elem_flags,user_data_size
		,user_data);
	if (retval!=ISS_E_HLOG_META_ELEM_FULL) return retval;
	L.Log("Info: SafeSendHLogElement: Current block cannot take current element, new block needed.\n");
	retval=iss46_hlog_send_block(*phandle);
	if (retval!=ISS_SUCCESS) {
		L.LogE("ERROR: SafeSendHLogElement: Sending complete block was not successful! (%d)\n"
			,retval);
		return retval;
	}
	L.Log("Info: SafeSendHLogElement: Sending complete block was successful.\n");
  
    SINT32        transno;
    ISS_T_BOOLEAN inprogress;
    SINT16        element_num;
	SINT16		  hp;

    retval=iss46_hlog_get_recent_hp(&hp);
	if (retval!=ISS_SUCCESS) {
		L.LogE("ERROR: SafeSendHLogElement: get_recent_hp returned bad status: hp=%d, retval=%d\n",hp,retval);
		return retval;
	}
	L.Log("Info: SafeSendHLogElement: get_recent_hp: hp=%d - successful\n",hp);

	retval=iss46_hlog_get_curr_trans_num(APos[posn].sid,&transno,&inprogress,&element_num);
	L.Log("Info: SafeSendHLogElement: get_curr_trans: tr=%d, open=%d, elem=%d\n"
		,transno,inprogress,element_num);
	if (retval!=ISS_SUCCESS) {
		L.LogE("ERROR: SafeSendHLogElement.get_curr_trans: Bad return value! (%d)\n",retval);
		return retval;
	}
	if (!inprogress) {
		L.LogE("ERROR: SafeSendHLogElement.get_curr_trans: Transaction must be open at this point!\n");
		return(-1);
	}

/*
Xv ISS_T_REPLY Xw ISSCALL iss46_hlog_get_curr_trans_num(
                UINT16               sid,
                P_SINT32             curr_trans_num,
                ISS_T_BOOLEAN ISSPTR trans_in_progress,
                P_SINT16             element_num);
*/

	ISS_T_LOG_ELEM_ID element;
	char transuser[9]={0,0,0,0,0,0,0,0,0};
	element.element_num=element_num;
	element.element_time=elem_time;

	memcpy(transuser,APos[posn].user,4);

	retval=iss46_hlog_start_block(APos[posn].sid,hp,transno,Date,elem_time
		,0,0x00018000,element,9,transuser,phandle);
	if (retval!=ISS_SUCCESS) {
		L.LogE("ERROR: SafeSendHLogElement.start_new_block: Bad return value! (%d)\n",retval);
		return retval;
	}
	L.Log("Info: SafeSendHLogElement: start_new_block: successful.\n");


	retval=iss46_hlog_send_element(*phandle,element_type,elem_time,elem_flags,user_data_size
		,user_data);
	if (retval!=ISS_SUCCESS) {
		L.LogE("ERROR: SafeSendHLogElement: Sending element into the new block was not successful! (%d)\n"
			,retval);
	}
	return retval;
}

Xv ISS_T_REPLY Xw ISSCALL RSafeSendHLogElement(
                        ISS_T_LOG_BLOCK_HANDLE * phandle,
                        SINT16                   element_type,
                        ISS_T_INT_TIME           elem_time,
                        UINT16                   elem_flags,
                        ISS_T_BYTE               user_data_size,
                        char ISSPTR              user_data)
{
	ISS_T_REPLY retval;
	retval=iss46_hlog_send_element(*phandle,element_type,elem_time,elem_flags,user_data_size
		,user_data);
	if (retval!=ISS_E_HLOG_META_ELEM_FULL) return retval;
	RL.Log("Info: RSafeSendHLogElement: Current block cannot take current element, new block needed.\n");
	retval=iss46_hlog_send_block(*phandle);
	if (retval!=ISS_SUCCESS) {
		RL.LogE("ERROR: RSafeSendHLogElement: Sending complete block was not successful! (%d)\n"
			,retval);
		return retval;
	}
	RL.Log("Info: RSafeSendHLogElement: Sending complete block was successful.\n");
  
    SINT32        transno;
    ISS_T_BOOLEAN inprogress;
    SINT16        element_num;
	SINT16		  hp;

    retval=iss46_hlog_get_recent_hp(&hp);
	if (retval!=ISS_SUCCESS) {
		RL.LogE("ERROR: RSafeSendHLogElement: get_recent_hp returned bad status: hp=%d, retval=%d\n",hp,retval);
		return retval;
	}
	RL.Log("Info: RSafeSendHLogElement: get_recent_hp: hp=%d - successful\n",hp);

	retval=iss46_hlog_get_curr_trans_num(RPos.sid,&transno,&inprogress,&element_num);
	RL.Log("Info: RSafeSendHLogElement: get_curr_trans: tr=%d, open=%d, elem=%d\n"
		,transno,inprogress,element_num);
	if (retval!=ISS_SUCCESS) {
		RL.LogE("ERROR: RSafeSendHLogElement.get_curr_trans: Bad return value! (%d)\n",retval);
		return retval;
	}
	if (!inprogress) {
		RL.LogE("ERROR: RSafeSendHLogElement.get_curr_trans: Transaction must be open at this point!\n");
		return(-1);
	}

/*
Xv ISS_T_REPLY Xw ISSCALL iss46_hlog_get_curr_trans_num(
                UINT16               sid,
                P_SINT32             curr_trans_num,
                ISS_T_BOOLEAN ISSPTR trans_in_progress,
                P_SINT16             element_num);
*/

	ISS_T_LOG_ELEM_ID element;
	char transuser[9];
	element.element_num=element_num;
	element.element_time=elem_time;

	memcpy(transuser,RUserS,9);

	retval=iss46_hlog_start_block(RPos.sid,hp,transno,Date,elem_time
		,0,0x00018000,element,9,transuser,phandle);
	if (retval!=ISS_SUCCESS) {
		RL.LogE("ERROR: RSafeSendHLogElement.start_new_block: Bad return value! (%d)\n",retval);
		return retval;
	}
	RL.Log("Info: RSafeSendHLogElement: start_new_block: successful.\n");


	retval=iss46_hlog_send_element(*phandle,element_type,elem_time,elem_flags,user_data_size
		,user_data);
	if (retval!=ISS_SUCCESS) {
		RL.LogE("ERROR: RSafeSendHLogElement: Sending element into the new block was not successful! (%d)\n"
			,retval);
	}
	return retval;
}

void CheckTranDateTime(ISS_T_INT_DATE * pdate,ISS_T_INT_TIME * ptime,long posn)
{
	ISS_T_INT_DATE date=*pdate;
	ISS_T_INT_TIME time=*ptime;
	//Cannot put before the last transaction
	if (APos[posn].lasttrandate>date) {
		date=APos[posn].lasttrandate;
		time=APos[posn].lasttrantime+1;
		if (time>=86400) time=86399;
	}
	else {
		if ((APos[posn].lasttrandate==date)&&(APos[posn].lasttrantime>time)) {
			time=APos[posn].lasttrantime+1;
			if (time>=86400) time=86399;
		}
	}
	//cannot put before the last performance slot
	if (APos[posn].p_startdate>date) {
		date=APos[posn].p_startdate;
		time=APos[posn].p_starttime;
		if (time>=86400) time=86399;
	}
	else {
		if ((APos[posn].p_startdate==date)&&(APos[posn].p_starttime>time)) {
			time=APos[posn].p_starttime;
			if (time>=86400) time=86399;
		}
	}
	//Cannot put before the opening of the trading day
	if (TDStartDate>date) {
		date=TDStartDate;
		time=TDStartTime+1;
		if (time>=86400) time=86399;
	}
	else {
		if ((TDStartDate==date)&&(TDStartTime>time)) {
			time=TDStartTime+1;
			if (time>=86400) time=86399;
		}
	}
	if ((date!=(*pdate))||(time!=(*ptime))) {
		L.Log("Info: Transaction date and time have been adjusted: %d->%d, %d->%d.\n",*pdate,date,*ptime,time);
		*pdate=date;
		*ptime=time;
	}
}

/**********************************************   FLEXSYS   ***********************************************************************/
long FLEXSYS_ProcSale(char* F) //Return value: 0=no_retries others=retries
{
	Reg r(false, C_RegKey);
	THeader header;
	TElem1021	E1021; //Return mode
	TElem5100	E5100;
	TElem5101	E5101;
	TElem21001	E21001[100];
	float ext_price[100];
	float itemweight[100];
	TElem2000	E2000[20];
	long	tendval[20],ttt=0;
	TElem2060	E2060;
	TElem21099V2	E21099[50];
	TElem21020	E21020;
	TElem3023	E3023;
	TElem21024	E21024;
	TElem21499	E21499[50];
	TElem21499	E2000_21499[50];
	PreFill(&E5100,	5100);
	PreFill(&E5101,	5101);
	PreFill(&E2060,	2060);
	PreFill(&E21020,	21020);
	PreFill(&E3023,	3023);
	PreFill(&E21024,21024);
	char OrigEAN[14][100];
	int c_21099=0;
	int c_21499=0;
	int c_2000=0;
	int c_21001=0;
	int i,sale_line=0,chg_rate=1;
	long dept_id_sum[10001];
	char tmp[400];
	SINT16 hp;
	ISS_T_LOG_BLOCK_HANDLE  handle;
	SYSTEMTIME st,et;
	ISS_T_REPLY retval=0;
	if ((retval=iss46_hlog_get_current_hp(&hp))!=ISS_SUCCESS) {
		L.LogE("ERROR: FLEXSYS_ProcSale.get_current_hp: %d, retval=%d\n",hp,retval);
		L.LogE("==> ProcSale will be aborted or retried!\n");
		return retval;
	}
	L.Log("Info: FLEXSYS_ProcSale.get_current_hp: %d\n",hp);
	bool open=false; //transaction opened
	long tenders=0;	//payment records logged already (to reject sales too)
	short round_amnt=0; //rounding amount
	long sold;		//total sales
	long total=0;		//current balance
	long rounded;	//payment value rounded (if cash)
	long totrounddiff=0; //Total rounding difference collected by all payment lines
	long posn;		//pos number
	long trantime;	//transaction total time
//	long availtime; //available time - time left from total transaction time
	long elements;	//counting logged elements
	long items=0;		//count of item records
	long weight;
	long quantity;
//	long prevquantity;
	ISS_GDECIMAL_4 unitprice;
	long extprice;
	bool ean2x[100];	//Will be true for EAN21-29
	char s[30];
//	unsigned char custno[12];
	bool gotcust;
	char transuser[9]={0,0,0,0,0,0,0,0,0};
	ISS_T_LOG_ELEM_ID element;
//	ISS_T_INT_DATE date;
	ISS_T_INT_TIME time;
	ISS_GDECIMAL_6 ean, eano[100];
//	ISS_GDECIMAL_6 eannosubst;
	char logon[20]; //Buser--date--=time=p# from Hp#user--date--=time=...
	char tranuser[5];
	char progress=0;
	char seps[]   = ":";
	int counter,seq_nr=0;
	char atoken[200];
	char *token;
	long amnt=1, quant=1;
	long qq,vv;
	char ss[21];
	char itemcollector[sizeof(ISS_GDECIMAL_6)+3*sizeof(long)];
	ISS_GDECIMAL_6 ean1;
	int last_tender4_id=-1;
	int card_nr=0;
	for (i=1;i<10001;i++)
		dept_id_sum[i]=0;

		TiXmlNode* fejlec = 0;
		TiXmlNode* root = 0;
		TiXmlElement* todoElement= 0;
		TiXmlElement* XMLelement= 0;
		TiXmlElement* CardElement;
		TiXmlHandle hRoot(0);

		TiXmlDocument doc( F );
		
		if ( !doc.LoadFile() )	{
			L.LogE("ERROR: FLEXSYS_ProcSale.PreProc: Could not load %s file.!  Error='%s'. Exiting.\n",F, doc.ErrorDesc());
			return 2;
		}
		GetLocalTime(&et);
		sprintf(header.custnr,"%s","0");
		sprintf(header.zip,"%","000000");
		header.custinqueue=0;
		header.invoice=false;
		header.voided=false;
		sprintf(header.Lautoken,"%s","0");
		sprintf(header.AP,"%s","000000000");
		sprintf(header.CloseNR,"%","1");
		sprintf(header.ReceiptNR,"%","1");
		header.transID=1; header.chg_amnt=0;
		root = doc.FirstChild( "szamla" );
		fejlec = root->FirstChildElement( "fejlec" );
		TiXmlElement* vevo = fejlec->FirstChildElement( "vevo" );
		TiXmlElement* szlainfo = fejlec->FirstChildElement( "szamlainfo" );
/************ Szamlainfo ***********************/
		//sorszam
		XMLelement = szlainfo->FirstChildElement( "sorszam" );
		strncpy(header.AP,strmid(XMLelement->GetText()+4,8),8);
		printf("%s\n",header.AP);
		header.AP[8]=0;
		strncpy(header.CloseNR,strmid(XMLelement->GetText()+13,4),5);
		strncpy(header.ReceiptNR,strmid(XMLelement->GetText()+18,5),6);
		if(strcmp(strmid(XMLelement->GetText(),2),"SZ")==0)
			header.invoice=true;
		if(strcmp(strmid(XMLelement->GetText(),2),"ST")==0){
			E21020.ReturnMode=TRUE;
			PreFill(&E1021,	1021);
		}
		else
			E21020.ReturnMode=FALSE;
		/********************* Log ON/OFF   ***********************/
		XMLelement = szlainfo->FirstChildElement( "arfolyam" );
		if (XMLelement)
			chg_rate=atoi(XMLelement->GetText());
		else 
			chg_rate=1;
		
		XMLelement = szlainfo->FirstChildElement( "egyebadat" );
		printf("Egyebadat %s \n",XMLelement->GetText());
		strcpy(tmp,XMLelement->GetText());
		char pdest[100];
		int count=1, z=0;
		char zero[37]="                                    ", asd[10];
		char ch[50],tstring[40];
		bool go=false;
		//pdest=strchr(tmp,ch);
		strcpy(pdest,slice_string(tmp,';',count));
		if(strlen(pdest)>6) go=true;
		while (go){
			//z=pdest - (tmp + 1+count);
			strncpy(asd,pdest,6);
			asd[6]=0;
			printf("%s - %s\n",pdest,asd);
			if(strcmp( asd," fizke" )==0) {
				if(E21020.ReturnMode==TRUE)
					header.chg_amnt=-1*atoi(strmid(pdest+8,4));
				else
					header.chg_amnt=atoi(strmid(pdest+8,4));
				printf("ChangeAmount %d \n",header.chg_amnt);
			}
			if(strcmp(asd," kezda" )==0) {
				strcpy(header.BDate,strmid(pdest+8,10));
				printf("BeginDate %s \n",header.BDate);
			}
			if(strcmp( asd," vegda" )==0) {
				strcpy(header.EDate,strmid(pdest+8,10));
				printf("EndDate %s \n",header.EDate);
			}
			if(strcmp( asd," kezid" )==0) {
				strcpy(header.BTime,strmid(pdest+8,8));
				printf("BeginTime %s \n",header.BTime);
			}
			if(strcmp( asd," vegid" )==0) {
				strcpy(header.ETime,strmid(pdest+8,8));
				printf("EndTime %s \n",header.ETime);
			}
			
			if(strcmp( asd,"kezelo" )==0) {
				strcpy(tstring,strmid(pdest+7,(strlen(pdest)-8)));
				printf("%s %d User: %s\n",pdest+7,strlen(pdest),tstring);
				header.cashier=atoi(strmid(pdest+7,(strlen(pdest)-8)));
				if (header.cashier>999) {
					L.LogE("ERROR: FLEXSYS_ProcSale: Cashier id more than tree digits! (%d) -> File rejected!\n",header.cashier);
					return ISS_SUCCESS;
				}
				printf("CashierID %d \n",header.cashier);
			}
			if(strcmp( asd," kassz" )==0) {
				strcpy(tstring,strmid(pdest+8,2));
				printf("POS: %s\n",tstring);
				posn=atoi(tstring)-1;
				printf("POSn: %d\n",posn);
				header.sid=posn;
				//printf("POSn: %d\n",posn);
				if (header.sid>2) {
					L.LogE("ERROR: FLEXSYS_ProcSale: Till id more than 2! (%d) -> File rejected!\n",header.sid);
					return ISS_SUCCESS;
				}
				printf("TillID %d \n",header.sid);
			}
			if(strcmp( asd," irany" )==0) {
				strcpy(header.zip,strmid(pdest+14,strlen(pdest)-15));	
				printf("ZipCode %s \n",header.zip);
			}
			if(strcmp( asd," kphel" )==0) {
				c_2000++;
				PreFill(&E2000[c_2000],	2000);
				sprintf(s,"%s%s",header.BDate,header.BTime);
				L.Log("Begin time: %s - %s\n",header.BTime,s);
				SysTimeFromS18(&st,s);
				sprintf(s,"%s%s",header.EDate,header.ETime);
				L.Log("End time: %s - %s\n",header.BTime,s);
				SysTimeFromS18(&et,s);
				E2000[c_2000].DrwIDDate=DateYMD2Int(et.wYear,et.wMonth,et.wDay);
				E2000[c_2000].DrwIDTime=et.wHour*3600+et.wMinute*60+et.wSecond;
				E2000[c_2000].DrwIDLocID=header.sid;

				sprintf(seps,",");
				sprintf(atoken,"%s",pdest);
				token = strtok( atoken, seps );
				counter=0;
				
				//strcpy(token,slice_string(atoken,';',1));
				while( token != NULL )	{
					counter++;
					sprintf(atoken,"%s",token);
					if(counter==3 && strlen(token)<4) counter++;
					switch (counter) {
						case 1: sprintf(ch,"%s",token+8); break;
						case 2: tendval[c_2000]=abs(atol(token));
								tenders=tenders+((chg_rate*100*tendval[c_2000])/100);
								LongVal2Cobol6(E2000[c_2000].TenderVal,100*tendval[c_2000]);
								break;
						case 3:card_nr=atoi(token); break;
						case 4:token[strlen(token)-1]=0;E2000[c_2000].TenderID=atoi(token); break;
					}
					printf("%s\n",token);
					//strcpy(token,slice_string(atoken,';',counter));
					token = strtok( NULL, seps );
				}
				if (E2000[c_2000].TenderID==4 && last_tender4_id!=-1){
					/*if(!header.voided){
						L.LogE("ERROR - The second_tender_id=4 and it is forbidden! File name:%s\n",total,tenders-header.chg_amnt);
						L.LogE("	==>Void this transaction!! File name: %s\n",F);
						return false;
					}*/
					tendval[last_tender4_id]+=tendval[c_2000];
					/*LongVal2Cobol6(E2000[last_tender4_id].TenderVal,100*tendval[last_tender4_id]);*/
					c_2000--;
					//count--;
				}
				if (E2000[c_2000].TenderID==4 && last_tender4_id==-1){
					/*PreFill(&E2000_21499[2*c_2000],21499);
					PreFill(&E2000_21499[2*c_2000-1],21499);
					E2000_21499[2*c_2000].Record_Type=-1;
					E2000_21499[2*c_2000-1].Record_Type=-1;*/
					last_tender4_id=c_2000;
					/*memset( E2000_21499[2*c_2000-1].Data, ' ', 180 );
					memset( E2000_21499[2*c_2000].Data, ' ', 36 );
					E2000_21499[2*c_2000-1].Record_Type=6;
					zero[34-strlen(ch)]=0;
					sprintf(E2000_21499[2*c_2000-1].Data,"D:************%.4d                  E:                                  K:%s%sN:                                  O:%s%s",card_nr,ch,zero,remove_char(header.EDate,"-"),remove_char(header.ETime,":"));
					L.Log("FLEXYS - Misc Tender : %s\n",E2000_21499[2*c_2000-1].Data);
					E2000_21499[2*c_2000].Record_Type=6;
					sprintf(E2000_21499[2*c_2000].Data,"Y:");*/
				}
				
			}
			count++;

			strcpy(pdest,slice_string(tmp,';',count));
			if(strcmp( pdest,"END" )==0) go=false;
			//strcpy(pdest,slice_string(tmp,';',9));
		}
		if(last_tender4_id>-1){
			c_2000=last_tender4_id;
			PreFill(&E2000_21499[2*c_2000],21499);
			PreFill(&E2000_21499[2*c_2000-1],21499);
			E2000_21499[2*c_2000].Record_Type=-1;
			E2000_21499[2*c_2000-1].Record_Type=-1;
			memset( E2000_21499[2*c_2000-1].Data, ' ', 180 );
			memset( E2000_21499[2*c_2000].Data, ' ', 36 );
			E2000_21499[2*c_2000-1].Record_Type=6;
			zero[34-strlen(ch)]=0;
			sprintf(E2000_21499[2*c_2000-1].Data,"D:************%.4d                  E:                                  K:%s%sN:                                  O:%s%s",card_nr,ch,zero,remove_char(header.EDate,"-"),remove_char(header.ETime,":"));
			L.Log("FLEXYS - Misc Tender : %s\n",E2000_21499[2*c_2000-1].Data);
			E2000_21499[2*c_2000].Record_Type=6;
			sprintf(E2000_21499[2*c_2000].Data,"Y:");
			LongVal2Cobol6(E2000[last_tender4_id].TenderVal,100*tendval[last_tender4_id]);
		}
		tenders=tenders+header.chg_amnt;
		/*if(NewACISDateCor) {
			GetLocalTime(&st);
			Date=DateYMD2Int(st.wYear,st.wMonth,st.wDay);
			time=st.wHour*3600+st.wMinute*60+st.wSecond;
			sprintf(header.BDate,"%04d-%02d-%02d",st.wYear,st.wMonth,st.wDay);
			sprintf(header.BTime,"%02d:%02d:00",st.wHour,st.wMinute);//,st.wSecond
			sprintf(header.EDate,"%04d-%02d-%02d",st.wYear,st.wMonth,st.wDay);
			sprintf(header.ETime,"%02d:%02d:28",st.wHour,st.wMinute);//,st.wSecond
			CheckTranDateTime(&Date,&time,posn);
		}*/
		header.chg_amnt=0;
		if (!(*(APos[posn].user))) {//noone is currently logged on
				sprintf(logon,"%.4d%s%s%.2d",header.cashier,remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid+1);
				if((retval=ProcLogOn(logon))!=0) {
						L.LogE("ERROR: ProcSale.AutoLogOn: ProcLogOn returned %d!\n",retval);
				}
		}
		else { // Ha m�s van bel�pve, akkor ki kell l�ptetni, majd bel�ptetni
			sprintf(tranuser,"%.4d",header.cashier);
			//tranuser[5]=0;
			CleanUserID(tranuser);
			if ((strcmp(APos[posn].user,tranuser))!=0) {
				char u[5];
				memcpy(u,APos[posn].user,4);
				u[4]=0;
				tranuser[4]=0;
				sprintf(logon,"%.4d%s%s%.2d",atoi(APos[posn].user),remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid+1);
				logon[20]=0;
				if ((retval=ProcLogOff(logon))!=0) {
					L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOff returned %d!\n",retval);
					//return retval;//retry allowed
				}
				if ((retval=ProcLogOn(logon))!=0) {
					L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOn returned %d!\n",retval);
					//return retval;//retry allowed
				}
			}
		}
		//header.chg_amnt=0;
		sprintf(header.Lautoken,"0");
		header.custinqueue=0;
		header.transID=0;
/*************************   VEV�    **************************/
	wchar_t csctmp[80];
	wchar_t *wText;
	char *ansiText;
	int room;
	counter=0;
		XMLelement = vevo->FirstChildElement( "nev" );
		if((XMLelement) && (XMLelement->GetText()!=NULL) && (strcmp(XMLelement->GetText(),"(null)")!=0)){
				counter=1;
				sprintf(header.custnr,"9899999999999");
				L.Log("Customer Name: %s \n",XMLelement->GetText());
				wText = CodePageToUnicode(65001,XMLelement->GetText());
				ansiText = UnicodeToCodePage(852,wText);	
				strcpy(header.custname,ansiText);
				L.Log("Name: %s \n",ansiText);							
				header.invoice=true;
		}
		XMLelement = vevo->FirstChildElement( "cim" )->FirstChildElement( "irszam" );
		if((XMLelement) && (counter==1) && (XMLelement->GetText()!=NULL)){
				printf("Customer zip %s \n",XMLelement->GetText());
				strcpy(header.custzip,XMLelement->GetText());
		}
		XMLelement = vevo->FirstChildElement( "cim" )->FirstChildElement( "telepules" );
		if((XMLelement) && (counter==1)  && (XMLelement->GetText()!=NULL)){
			if (strlen(XMLelement->GetText())>0) {
				L.Log("Customer city: %s \n",XMLelement->GetText());
				wText = CodePageToUnicode(65001,XMLelement->GetText());
				ansiText = UnicodeToCodePage(852,wText);	
				strcpy(header.custcity,ansiText);
				L.Log("city: %s \n",ansiText);
			}
		}
		XMLelement = vevo->FirstChildElement( "cim" )->FirstChildElement( "kozternev" );
		if((XMLelement) && (counter==1)  && (XMLelement->GetText()!=NULL)){
			if (strlen(XMLelement->GetText())>0) {
				L.Log("Customer address: %s \n",XMLelement->GetText());
				wText = CodePageToUnicode(65001,XMLelement->GetText());
				ansiText = UnicodeToCodePage(852,wText);	
				strcpy(header.custaddr,ansiText);
				L.Log("address: %s \n",ansiText);
			}
		}

/*************************   T�TELEK    **************************/
		TiXmlElement* tetelek= root->FirstChildElement( "tetelek" );
		TiXmlElement* tetel= tetelek->FirstChildElement( "tetel" ); 
		int fuel=0;
		for( tetel; tetel; tetel=tetel->NextSiblingElement()){
			c_21001++;
			PreFill(&E21001[c_21001],21001);
			gdec_init_z(ean,6,0,iss_gdec_round_normal);
			gdec_init_z(eano[c_21001],6,0,iss_gdec_round_normal);
			XMLelement = tetel->FirstChildElement( "megjegyzes" );
			if(XMLelement) {
				sprintf(seps, ";");
				sprintf(atoken,"%s",XMLelement->GetText());
				token = strtok( atoken, seps );
				counter=0;
				int card_nr=0;
				while( token != NULL )	{
					counter++;
					sprintf(atoken,"%s",token);
					if(strncmp("cikkszam",token,8)==0){
						sprintf(s,"%s",token+9);
						
						sprintf(OrigEAN[c_21001],"%s",s);
						DoItemSubst(s,false);
						L.Log("The original EAN %s and the new : %s\n",token+9,s);
						gdec_from_ascii(ean,s);
						gdec_from_ascii(eano[c_21001],s);
									
						if (GetItemDataDept(unitprice,&E21001[c_21001].TaxID,&E21001[c_21001].ReportGroup,&E21001[c_21001].EANType,&E21001[c_21001].DeptId,ean)==ISS_E_SDBMS_NOREC) {
							L.LogE("ERROR: FLEXYS.Item: No record in ISS400 for EAN %s! Needed values will be guessed.\n",s);
							E21001[c_21001].ReportGroup=1;
							E21001[c_21001].TaxID=2;
							E21001[c_21001].EANType=5;
							E21001[c_21001].DeptId=3075;
						}
						ean2x[c_21001]=((s[0]=='2')&&(s[1]>'0')&&(s[1]<='9'));
						GDec62Cobol6(E21001[c_21001].ItemCode,ean);
						if(E21020.ReturnMode==TRUE){
							GDec62Cobol6(E1021.ItemCode,ean);//itemcode ok
							E1021.DeptId=E21001[c_21001].DeptId;
							E1021.ReturnMode=false;
							E1021.SubtractedItem=true;
							E1021.ReturnType=14;
							
							
						}
					}
					token = strtok( NULL, seps );
				}
			}
			XMLelement = tetel->FirstChildElement( "mennyegys" );				
			if(XMLelement) {
				if (strncmp(XMLelement->GetText(),"L",1)==0) fuel=1;
			}
			XMLelement = tetel->FirstChildElement( "menny" );				
			if(XMLelement) {
				if (fuel){
					E21001[c_21001].Quantity=1;
					if(E21020.ReturnMode==TRUE){
						weight=-1000*atof(XMLelement->GetText());
						E1021.Quantity=1;

					}
					else
						weight=1000*atof(XMLelement->GetText());
					itemweight[c_21001]=weight;
					LongVal2Cobol4(E21001[c_21001].Weight,weight);
				}
				else {
					if(E21020.ReturnMode==TRUE){
						E21001[c_21001].Quantity=-1*atof(XMLelement->GetText());
						E1021.Quantity=-1*atof(XMLelement->GetText());
					}
					else
						E21001[c_21001].Quantity=-1*atof(XMLelement->GetText());
					itemweight[c_21001]=0;
					LongVal2Cobol4(E21001[c_21001].Weight,0);
				
				}
				printf("Quantity : %s",XMLelement->GetText());
			}
			XMLelement = tetel->FirstChildElement( "bruttoar" );				
			if(XMLelement) {
				printf("Ext Price: %s",XMLelement->GetText());
				if(E21020.ReturnMode==TRUE){
							ext_price[c_21001]=-1*atol(XMLelement->GetText());
							LongVal2Cobol6(E1021.ExtPrice,100*ext_price[c_21001]);//Extprice ok
				}
				else
							ext_price[c_21001]=atol(XMLelement->GetText());
				LongVal2Cobol6(E21001[c_21001].ExtPrice,100*ext_price[c_21001]);//Extprice ok
				dept_id_sum[E21001[c_21001].DeptId]+=ext_price[c_21001];
				total=total+ext_price[c_21001];
				/*ext_price[c_21001]=atof(XMLelement->GetText());
				LongVal2Cobol6(E21001[c_21001].ExtPrice,(100*ext_price[c_21001]));//Extprice ok
				dept_id_sum[E21001[c_21001].DeptId]+=atol(XMLelement->GetText());
				total=total+atoi(XMLelement->GetText());*/
			}
			XMLelement = tetel->FirstChildElement( "bruttoegysegar" );				
			if(XMLelement) {
				rounded=100*atol(XMLelement->GetText());
				LongVal2Cobol6(E21001[c_21001].UnitPrice,rounded);//Unitprice ok
				LongVal2Cobol6(E21001[c_21001].PriceFromPlu,rounded);

				printf("Unit Price :%s",XMLelement->GetText());
			}
			E21001[c_21001].InputDevice=12;
			if (E21020.ReturnMode==TRUE)
				E21001[c_21001].ReturnMode=TRUE;
			else
				E21001[c_21001].ReturnMode=FALSE;
			E21001[c_21001].NegativeItem=FALSE;

		}


/****************************************************/		
	time_t t1,t2;
	t1=TimeFromSystemTime(&st);
	t2=TimeFromSystemTime(&et);
	trantime=difftime(t2,t1);  //Fizet�s kezdete �s v�ge k�z�tt eltelt id�
	round_amnt=total-(tenders-header.chg_amnt);
	
	if (abs(round_amnt)>2) {//Fizet�eszk�z�k �rt�ke nem egyezik meg a cikkek �sszeg�vel
		if(!header.voided){
			L.LogE("ERROR - The total: %d and the tender total: %d doesn't match!\n",total,tenders-header.chg_amnt);
			L.LogE("	==>Void this transaction!! File name: %s\n",F);
			return false;
		}
	}
	short y,m,d;
	DateInt2YMD(APos[posn].signondate,&y,&m,&d);
	L.Log("Sign on date: %d.%d.%d\n",y,m,d);
	L.Log("Check the user! Apos.User: %s , F�jl user: %d\n",APos[posn].user,header.cashier);
	if(header.cashier>999) {
		L.LogE("ERROR - Bad user %d!\n",header.cashier);
		return 0;
	}

	if (!(*(APos[posn].user))) {//noone is currently logged on
				if ((posn>2)||((AutoActions&0x00000001)!=0)) {
					if (posn<=2) L.LogE("Warning: ProcSale: Sequence error - noone has been logged on (PoS #%d)!\n",posn+1);
					
					if (posn<=2) L.LogE("==> Now performing autologon by sales transaction header: %s\n",s);
					else L.Log("Performing autologon by sales transaction header (pos #%d): %s\n",posn+1,s);
					sprintf(logon,"%.4d%s%s%.2d",header.cashier,remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid+1);
					if ((retval=ProcLogOn(logon))!=0) {
						L.LogE("ERROR: ProcSale.AutoLogOn: ProcLogOn returned %d!\n",retval);
						return 0;//no retries
					}
					sprintf(APos[posn].user,"%d",header.cashier);
				}
				else {
					L.LogE("ERROR: ProcSale: Transaction sequence error - Noone has been logged on (PoS #%d)!\n",posn+1);
					return 0;//no retries
				}
	}
	else {
				
				char tranuser[5];
				sprintf(tranuser,"%.4d",header.cashier);
				//tranuser[5]=0;
				CleanUserID(tranuser);
				L.Log("Tranuser: %s APos.User: %s \n",tranuser,APos[posn].user);
				if ((strcmp(APos[posn].user,tranuser))!=0) {
					char u[5];
					memcpy(u,APos[posn].user,4);
					u[4]=0;
					tranuser[4]=0;
					if ((AutoActions&0x00000002)!=0) {
						char logon[20]; //user--date--=time=p# from Hp#user--date--=time=...
						long retval;
//						long len;
						L.LogE("Warning: ProcSale: Not the logged on user does sales! (Selling:%s Logged_on:%d))! (PoS #%d)\n"
							,tranuser,header.cashier,posn+1);
						L.LogE("==> Now performing automatic logoff/logon by sales transaction ID: %d\n",header.transID);
						sprintf(logon,"%.4d%s%s%.2d",header.cashier,remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid+1);
						logon[20]=0;
						if ((retval=ProcLogOff(logon))!=0) {
							L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOff returned %d!\n",retval);
							return retval;//retry allowed
						}
						if ((retval=ProcLogOn(logon))!=0) {
							L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOn returned %d!\n",retval);
							return retval;//retry allowed
						}
					}
					else {
						L.LogE("ERROR: ProcSale: Not the logged on user does sales! (Selling:%s Logged_on:%s))! (PoS #%d)\n"
							,tranuser,u,posn+1);
						return 0;//no retries
					}
				}
	}
	if(E21020.ReturnMode==TRUE) sprintf(E1021.UserID,"%d",header.cashier);
	//Advancing performance period(s) if needed
	int g;
	sprintf(tmp,"%s%s",remove_char(header.BDate,"-"),remove_char(header.BTime,":"));
	tmp[14]=0;
	SysTimeFromS14(&st,tmp);
	Date=DateYMD2Int(st.wYear,st.wMonth,st.wDay);
	c_21099++;
	//Lautoken
	sprintf(header.Lautoken,"0");
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=81;
	E21099[c_21099].TransStore=StoreNum;
	g=21-strlen(header.Lautoken);
	memset(E21099[c_21099].DataEnter,'0',g);
	E21099[c_21099].DataEnter[2]='1';
	strcpy(E21099[c_21099].DataEnter+g,header.Lautoken);
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	c_21099++;
	//Nyugta sz�mla
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=81;
	E21099[c_21099].TransStore=StoreNum;
	memset(E21099[c_21099].DataEnter,' ',21);
	if(!header.invoice) //Nyugta
		strcpy(E21099[c_21099].DataEnter,"006NY");
	else   //Sz�mla
		strcpy(E21099[c_21099].DataEnter,"006SZ");
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	
	c_21099++;
	//AP
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=81;
	E21099[c_21099].TransStore=StoreNum;
	g=21-strlen(header.AP);
	memset(E21099[c_21099].DataEnter,'0',g);
	E21099[c_21099].DataEnter[2]='4';
	strcpy(E21099[c_21099].DataEnter+g,header.AP);
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	//Z�r�ssz�m
	c_21099++;
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=81;
	E21099[c_21099].TransStore=StoreNum;
	g=21-strlen(header.CloseNR);
	memset(E21099[c_21099].DataEnter,'0',g);
	E21099[c_21099].DataEnter[2]='5';
	strcpy(E21099[c_21099].DataEnter+g,header.CloseNR);
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	int cust_cnt=0;
	if (strcmp(header.custnr,"0")!=0) {
		cust_cnt=4;
		gotcust=true;
		c_21099++;
		
		//V�s�rl� azonos�t�
		PreFill(&E21099[c_21099],21099);
		E21099[c_21099].CurrDate=Date;
		E21099[c_21099].TransLoc=APos[posn].sid/2;
		E21099[c_21099].DataType=17;
		E21099[c_21099].TransStore=StoreNum;
		memset(E21099[c_21099].DataEnter,' ',21);
		g=13-strlen(header.custnr);
		memset(E21099[c_21099].DataEnter,'0',g);
		strcpy(E21099[c_21099].DataEnter+g,header.custnr);
		E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
		c_21499++;
		//V�s�rl�i adatok
		PreFill(&E21499[c_21499],21499);
		E21499[c_21499].Record_Type=1;
		memset(E21499[c_21499].Data,' ',184);
		g=18-strlen(header.custnr);
		memset(E21499[c_21499].Data,'0',g);
		strcpy(E21499[c_21499].Data+g,header.custnr);
		//n�v
		strcpy(E21499[c_21499].Data+18,header.custname);
		E21499[c_21499].Data[18+strlen(header.custname)]=' ';
		//irsz�m
		strcpy(E21499[c_21499].Data+63,header.custzip);
		E21499[c_21499].Data[63+strlen(header.custzip)]=' ';
		//v�ros
		strcpy(E21499[c_21499].Data+68,header.custcity);
		E21499[c_21499].Data[68+strlen(header.custcity)]=' ';
		//c�m
		strcpy(E21499[c_21499].Data+108,header.custaddr);
		E21499[c_21499].Data[108+strlen(header.custaddr)]=' ';
		E21499[c_21499].Data[184]=0;
		c_21499++;
		//V�s�rl�i adatok
		PreFill(&E21499[c_21499],21499);
		E21499[c_21499].Record_Type=2;
		memset(E21499[c_21499].Data,' ',45);
		strcpy(E21499[c_21499].Data,header.cust_taxnr);
		E21499[c_21499].Data[strlen(header.cust_taxnr)]=' ';
		E21499[c_21499].Data[45]='1';

		c_21099++;
		//V�s�rl� azonos�t�
		PreFill(&E21099[c_21099],21099);
		E21099[c_21099].CurrDate=Date;
		E21099[c_21099].TransLoc=APos[posn].sid/2;
		E21099[c_21099].DataType=81;
		E21099[c_21099].TransStore=StoreNum;
		memset(E21099[c_21099].DataEnter,' ',21);
		memset(E21099[c_21099].DataEnter,'0',2);
		E21099[c_21099].DataEnter[2]='2';
		strcpy(E21099[c_21099].DataEnter+3,header.custnr);
		E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	}
	c_21099++;
	//D�tum
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=51;
	E21099[c_21099].TransStore=StoreNum;
	time=st.wHour*3600+st.wMinute*60+st.wSecond;
	//CheckTranDateTime(&Date,&time,posn);
	sprintf(E21099[c_21099].DataEnter,"%04dA%02dA%02d",st.wYear,st.wMonth,st.wDay);	//Fiscal date gets stored
	memset(E21099[c_21099].DataEnter+10,' ',11);
	c_21099++;
	//Napi nyugta sz�ml�l� ISS
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=16;
	E21099[c_21099].TransStore=StoreNum;
	//CheckTranDateTime(&Date,&time,posn);
	memset(E21099[c_21099].DataEnter,' ',21);
	sprintf(E21099[c_21099].DataEnter,"%d",atol(header.ReceiptNR));
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	//Fiscalis bejegyz�s
	c_21099++;
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=92;
	E21099[c_21099].TransStore=StoreNum;
	//CheckTranDateTime(&Date,&time,posn);
	memset(E21099[c_21099].DataEnter,' ',21);
	sprintf(E21099[c_21099].DataEnter,"%s/%.4d/%.5d",header.AP,atoi(header.CloseNR),atol(header.ReceiptNR));
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	//Ir�ny�t�sz�m bejegyz�s
	c_21099++;
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=7;
	E21099[c_21099].TransStore=StoreNum;
	//CheckTranDateTime(&Date,&time,posn);
	memset(E21099[c_21099].DataEnter,' ',7);
	sprintf(E21099[c_21099].DataEnter,"%s",header.zip);
	E21099[c_21099].DataEnter[strlen(header.zip)]=' ';

	while (Check4NewPerfSlot(posn,Date,time)); //Advancing performance slots until it's necessary

	//Checks done before opening a new transaction
	SINT32        transno;
	ISS_T_BOOLEAN inprogress;
	SINT16        element_num;

	retval=iss46_hlog_get_curr_trans_num(APos[posn].sid,&transno,&inprogress,&element_num);
	L.Log("Info: ProcSale.check_before_start.get_curr_trans: tr=%d, open=%d, elem=%d\n"
		,transno,inprogress,element_num);
	if (retval!=ISS_SUCCESS) {
		L.LogE("ERROR: ProcSale.check_before_start.get_curr_trans: Bad return value! (%d)\n",retval);
		return retval;
	}
	if (inprogress) {
		L.LogE("ERROR: ProcSale.check_before_start: Transaction must not be open at this point!\n");
		return(-1);
	}
	APos[posn].lasttran++;//Preparing new transaction's number
	if (APos[posn].lasttran!=transno) {//checks whether ISS thinks the same
		L.LogE("Warning: ProcSale.check_before_start: Transaction numbers doesn't match!\n");
		L.LogE("==> APos[%d].lasttran=%d ISSLOG.transno=%d SID=%d\n"
			,posn,APos[posn].lasttran,transno,APos[posn].sid);
		APos[posn].lasttran=transno; //Correction by ISS
	}

	//Preparing opening a new transaction
	element.element_num=1;
	element.element_time=time;
	memcpy(transuser,APos[posn].user,4);

	if ((retval=iss46_hlog_start_block(APos[posn].sid,hp,APos[posn].lasttran,Date,time
		,0,0x00018000,element,9,transuser,&handle))!=ISS_SUCCESS) {
		L.LogE("ERROR: ProcSale.start returned: %d -> retrying!\n",retval);
		return(retval);
	}
	else L.Log("ProcSale.start: Successful.\n");
	

	elements=1;
	L.Log("FLEXYS - Return mode: %d\n",E21020.ReturnMode);
	if(E21020.ReturnMode==TRUE){
		//E5101.LocTrading=false;
		//E5101.SignedOn=false;
		E5101.ReturnType=13;
		if ((retval=iss46_hlog_send_element(handle,5101,time,0x0000,sizeof(E5101),(char*)&E5101))
			!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcSale.5101 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.5101: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcSale.5101: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("Info: ProcSale.5101: Successful.\n");
	}
	else {
		if ((retval=iss46_hlog_send_element(handle,5100,time,0x0000,sizeof(E5100),(char*)&E5100))
			!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcSale.5100 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.5100: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcSale.5100: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("Info: ProcSale.5100: Successful.\n");
	}
	open=true;
	elements++;
	sold=0;
	APos[posn].p_custcnt++;
//cust_cnt=0;
	/*********************   21099   *******************/
	for(i=1;i<=2+cust_cnt;i++) {
				retval=SafeSendHLogElement(posn,&handle,21099,time,0x0000,sizeof(E21099[i])
					,(char*)&E21099[i]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21099.fiscal_date returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21099.fiscal_date: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21099.fiscal_date: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21099.fiscal_date: successful.\n");
				elements++;
	}
	/*********************   21499   *******************/
	if (cust_cnt>0)
		for(i=1;i<=2;i++) {
					retval=SafeSendHLogElement(posn,&handle,21499,time,0x0000,sizeof(E21499[i])
						,(char*)&E21499[i]);
					if (retval!=ISS_SUCCESS) {
						ISS_T_REPLY ret2;
						L.LogE("ERROR: ProcSale.21499.fiscal_date returned: %d -> abort tran & retry!\n",retval);
						if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
							L.LogE("ERROR: ProcSale.21499.fiscal_date: AbortTran: %d!\n",ret2);
						}
						else L.Log("Info: ProcSale.21499.fiscal_date: AbortTran successful.\n");
						return(retval);
					}
					else L.Log("Info: ProcSale.21499.fiscal_date: successful.\n");
					elements++;
		}
	/*********************   21001   *******************/
	for(i=1;i<=c_21001;i++) {
		sprintf(ss,"0000000%s",OrigEAN[i]);
		ss[21]=0;
		L.Log("EAN code: %s\n",ss);
		gdec_init_z(ean1,6,0,iss_gdec_round_normal);
		gdec_from_ascii(ean1,ss);
		//gdec_init_z(ean,6,0,iss_gdec_round_normal);
		memcpy(itemcollector,&posn,sizeof(long));
		memcpy(itemcollector+sizeof(long),ean1,sizeof(ISS_GDECIMAL_6));
		if (GItemTotals.GetData(itemcollector,NULL,false)) {
			L.Log("The addad Value=%d Quantity=%d\n",ext_price[i],itemweight[i]);
			L.Log("This is the first sale of this ITEM %s at this till %d.\n",OrigEAN[i],header.sid+PosSIDBase/2);
			qq=0;vv=0;
		}
		else {
			qq=*((long*)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)));
			vv=*((long*)(itemcollector+sizeof(long)*2+sizeof(ISS_GDECIMAL_6)));
			L.Log("The addad Value=%d Quantity=%d\n",ext_price[i],itemweight[i]);
			L.Log("Total sales for item %s on this till %d! ValueMy=%d QuantityMy=%d\n"
					,OrigEAN[i],header.sid+PosSIDBase/2,vv,qq);
		}
		extprice=ext_price[i];quantity=itemweight[i];
		memcpy(itemcollector,&posn,sizeof(long));
		memcpy(itemcollector+sizeof(long),ean1,sizeof(ISS_GDECIMAL_6));
		memcpy(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6),&quantity,sizeof(long));
		memcpy(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6),&extprice,sizeof(long));
		GItemTotals.AddFigures(itemcollector);
//E21001[1].OfferID
		retval=SafeSendHLogElement(posn,&handle,21001,time,0x0000,sizeof(E21001[i]),(char*)&E21001[i]);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcSale.21001 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.21001: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcSale.21001: AbortTran successful.\n");
			return (ISS_SUCCESS);
		}
		else L.Log("Info: ProcSale.21001: successful.\n");
		element.element_time+=(trantime/c_21001);
		time+=(trantime/c_21001);
		elements++;
		items+=E21001[c_21001].Quantity;

		if (!header.voided) {
			APos[posn].p_regtime+=(trantime/c_21001);
			APos[posn].regtime+=(trantime/c_21001);
			APos[posn].items+=E21001[c_21001].Quantity;
			APos[posn].p_itemcnt+=E21001[c_21001].Quantity;
		}
	}
	if (!header.voided) 
		APos[posn].p_salesval+=total;
	sold=total;

if (!header.voided) {
/*******************  RETURN MODE  **************************/
	if(E21020.ReturnMode==TRUE) {
		retval=RSafeSendHLogElement(&handle,1021,time,0x0000,sizeof(E1021)
				,(char*)&E1021);
			if (retval!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				L.LogE("ERROR: FLEXYS.ProcSale.1021 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: FLEXYS.ProcSale.1021: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: FLEXYS.ProcSale: AbortTran successful.\n");
				return (ISS_SUCCESS);
			}
			else L.Log("FLEXYS.ProcSale.1021: successful.\n");
	}

/********************    2000    ************************/
char truser[9];
//long q1,v1;
	for(i=1;i<=c_2000;i++) {
		//memcpy(E2000[i].DrwIDUserID,transuser,9);
		strcpy(truser,transuser);
		truser[strlen(transuser)]=0;
		strcpy(E2000[i].DrwIDUserID,truser);
		memcpy(E2060.DrwIDUserID,transuser,9);
		E2000[i].DrwIDDate=Date;//APos[posn].drwdate;
		E2000[i].DrwIDTime=APos[posn].drwtime;
		E2060.DrwIDDate=Date;//APos[posn].drwdate;
		E2060.DrwIDTime=APos[posn].drwtime;
		if(E21020.ReturnMode) E2000[i].ReturnMode=true;
		rounded=total;//Initially not rounded
		if ((CashRounding)&&(E2000[i].TenderID==1)) {//Do rounding
			rounded=((total+CashRounding/2)/CashRounding)*CashRounding;
			totrounddiff+=rounded-total;
			if(E21020.ReturnMode==TRUE) {
				
			}
			LongVal2Cobol6(E2000[i].TenderVal,(100*tendval[i]+100*totrounddiff));
		}
		
		//TenderTotals.AddFigures(E2000[i].TenderID,tendval[i],1);
		//AddTendering(posn,E2000.TenderID,extprice);
		AddTendering(posn,(long) E2000[i].TenderID,tendval[i]);
		retval=SafeSendHLogElement(posn,&handle,2000,time,0x0000,sizeof(E2000[i]),(char*)&E2000[i]);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcSale.2000 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.2000: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcSale.2000: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("Info: ProcSale.2000: successful.\n");
		elements++;
		if(E2000_21499[2*i-1].Record_Type!=-1) {
				retval=SafeSendHLogElement(posn,&handle,21499,time,0x0000,sizeof(E2000_21499[2*i-1])
						,(char*)&E2000_21499[2*i-1]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21499 Tender details returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21499.Tender details: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21499.Tender details: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21499.Tender details: successful.\n");
				elements++;
				retval=SafeSendHLogElement(posn,&handle,21499,time,0x0000,sizeof(E2000_21499[2*i])
						,(char*)&E2000_21499[2*i]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21499 Tender details Y returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21499.Tender details Y: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21499.Tender details Y: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21499.Tender details Y: successful.\n");
				elements++;
		}
	}
/*********************    2060      ***************************/
	if (header.chg_amnt>0) {//change was given (even after rounding applied)
		LongVal2Cobol6(E2060.ChangeGiven,header.chg_amnt*100);
		retval=SafeSendHLogElement(posn,&handle,2060,time,0x0000,sizeof(E2060)
			,(char*)&E2060);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcSale.2600 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.2600: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcSale.2600: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("Info: ProcSale.2600: successful.\n");
		elements++;
		//AddTendering(posn,1,-rounded); //Subtract rounded cash given back
		AddTendering(posn,E2060.TenderID,-1*header.chg_amnt); //Subtract cash given back
	}
} //voided transakci�
else {
	TElem4000	E4000;
	PreFill(&E4000,	4000);
	sprintf(E4000.AuthUser,"%d",header.cashier);
	E4000.AuthRefused=FALSE;
	sprintf(E4000.Function,"%s","TAWW-AUTHVOID");
	retval=SafeSendHLogElement(posn,&handle,4000,time,0x0000,sizeof(E4000)
		,(char*)&E4000);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcSale.4000 returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcSale.4000: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcSale.4000: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("Info: ProcSale.4000: successful.\n");
	elements++;
}
	/*********************   21099   *******************/
	for(i=3+cust_cnt;i<=c_21099;i++) {
				retval=SafeSendHLogElement(posn,&handle,21099,time,0x0000,sizeof(E21099[i])
					,(char*)&E21099[i]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21099.fiscal_date returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21099.fiscal_date: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21099.fiscal_date: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21099.fiscal_date: successful.\n");
				elements++;
	}
	if (cust_cnt>0) cust_cnt=3;

/**********************    21020     ***********************************/	
	//Sale perf record
	LongVal2Cobol6(E21020.SalesTotal,sold*100);
	LongVal2Cobol6(E21020.ItemSoldTtl,sold*100);
	if (CashRounding) {//Rounding active
		LongVal2Cobol6(E21020.TenderTotal,(tenders)*100);
		LongVal2Cobol6(E21020.RoundingTtl,0);
	}
	else LongVal2Cobol6(E21020.TenderTotal,(tenders)*100);
	E21020.ItemCnt=c_21001;
	//E21020.ReturnMode=TRUE;
	E21020.ItemSoldCnt=c_21001;
	E21020.TenderCnt=c_2000;
	retval=SafeSendHLogElement(posn,&handle,21020,time,0x0000,sizeof(E21020)
		,(char*)&E21020);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcSale.21020 returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcSale.21020: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcSale.21020: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("Info: ProcSale.21020: successful.\n");
	elements++;
	if (header.voided) {  //Voided tranzakci�
		ISS_T_REPLY ret2;
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.4000: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.4000: AbortTran successful.\n");
				return ISS_SUCCESS;
	}
/***********************   3023     ******************/
if(E21020.ReturnMode==FALSE){
	//dept perf records
	E3023.CountSales=items;
	E3023.ItemKey;
	for (i=1;i<10001;i++) {
		if (dept_id_sum[i]!=0) {
			E3023.DeptNo=i;
			LongVal2Cobol6(E3023.ValueSales,dept_id_sum[i]*100);	
			retval=SafeSendHLogElement(posn,&handle,3023,time,0x0000,sizeof(E3023)
				,(char*)&E3023);
			if (retval!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				L.LogE("ERROR: ProcSale.3023 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.3023: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.3023: AbortTran successful.\n");
				return(retval);
			}
			else L.Log("Info: ProcSale.3023: successful.\n");
			elements++;
		}
	}
	LongVal2Cobol6(E3023.ValueSales,total*100);

	for(extprice=1;extprice<8;extprice++) {
		if (ItemDept[extprice]!=0) {
			E3023.DeptNo=ItemDept[extprice];
			retval=SafeSendHLogElement(posn,&handle,3023,time,0x0000,sizeof(E3023)
				,(char*)&E3023);
			if (retval!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				L.LogE("ERROR: ProcSale.3023 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.3023: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.3023: AbortTran successful.\n");
				return(retval);
			}
			else L.Log("Info: ProcSale.3023: successful.\n");
			elements++;
		}
	}
}
else {
	//dept perf records
	E21024.ReturnMode=TRUE;
	E21024.SalesCnt=items;
	E21024.ItemScanCnt=items;
	LongVal2Cobol6(E21024.SalesVal,total*100);

	for (i=1;i<10001;i++) {
		if (dept_id_sum[i]!=0) {
			E21024.Dept=i;
			LongVal2Cobol6(E21024.SalesVal,dept_id_sum[i]*100);	
			retval=SafeSendHLogElement(posn,&handle,21024,time,0x0000,sizeof(E21024)
				,(char*)&E21024);
			if (retval!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				L.LogE("ERROR: ProcSale.21024 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.21024: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.21024: AbortTran successful.\n");
				return(retval);
			}
			else L.Log("Info: ProcSale.21024: successful.\n");
			elements++;
		}
	}
	LongVal2Cobol6(E21024.SalesVal,total*100);

	for(extprice=1;extprice<8;extprice++) {
		if (ItemDept[extprice]!=0) {
			E21024.Dept=ItemDept[extprice];
			retval=SafeSendHLogElement(posn,&handle,21024,time,0x0000,sizeof(E21024)
				,(char*)&E21024);
			if (retval!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				L.LogE("ERROR: ProcSale.21024 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.21024: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.21024: AbortTran successful.\n");
				return(retval);
			}
			else L.Log("Info: ProcSale.21024: successful.\n");
			elements++;
		}
	}

}
	//close tran
	element.element_time=time+trantime;
	element.element_num=elements;
	retval=iss46_hlog_end_trans(handle,Date,(unsigned short)elements);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcSale.EndTran returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcSale.EndTran: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcSale.EndTran: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("Info: ProcSale.EndTran: successful.\n");
	APos[posn].lasttrandate=Date;
	APos[posn].lasttrantime=time+trantime;

	GItemTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegGItemTotals);
	SaveStatus(false,posn);
	memcpy(itemcollector,&posn,sizeof(long));
	GItemTotals.InitCursor();
	int index;
	for (index=0;index<GItemTotals.GetCount();index++) {
		GItemTotals.GetDataByIndex(index,itemcollector);
		gdec2s((unsigned char *)(itemcollector+sizeof(long)),6,s);
		L.Log("Sales for item %s on till %d! Quantity=%d Value=%d.\n"
			,s,*((long *)(itemcollector)),*((long *)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)))
			,*((long *)(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6))));
	}

	r.SetValueDW("Fuel.System.Close",0); //Engedem a benzink�t f�jlok feldolgoz�s�t	
	open=false;
	return ISS_SUCCESS;
}

/****************************************************************************************************************************************/





/**********************************************   GILBARCO0   ***********************************************************************/
long GILBARCO_ProcSale(char* F) //Return value: 0=no_retries others=retries
{
	Reg r(false, C_RegKey);
	THeader header;
	TElem5100	E5100;
	TElem21001	E21001[100];
	float ext_price[100];
	float itemweight[100];
	TElem2000	E2000[20];
	long	tendval[20];
	TElem2060	E2060;
	TElem21099V2	E21099[50];
	TElem21020	E21020;
	TElem3023	E3023;
	TElem21499	E21499[50];
	TElem21499	E2000_21499[50];
	PreFill(&E5100,	5100);
	PreFill(&E2060,	2060);
	PreFill(&E21020,	21020);
	PreFill(&E3023,	3023);
	char OrigEAN[14][100];
	int c_21099=0;
	int c_21499=0;
	int c_2000=0;
	int c_21001=0;
	int i,sale_line=0,chg_rate=1;
	long dept_id_sum[10001];
	SINT16 hp;
	ISS_T_LOG_BLOCK_HANDLE  handle;
	SYSTEMTIME st,et;
	ISS_T_REPLY retval=0;
	if ((retval=iss46_hlog_get_current_hp(&hp))!=ISS_SUCCESS) {
		L.LogE("ERROR: GILBARCO_ProcSale.get_current_hp: %d, retval=%d\n",hp,retval);
		L.LogE("==> ProcSale will be aborted or retried!\n");
		return retval;
	}
	L.Log("Info: GILBARCO_ProcSale.get_current_hp: %d\n",hp);
	bool open=false; //transaction opened
	long tenders=0;	//payment records logged already (to reject sales too)
	short round_amnt=0; //rounding amount
	long sold;		//total sales
	long total=0;		//current balance
	long rounded;	//payment value rounded (if cash)
	long totrounddiff=0; //Total rounding difference collected by all payment lines
	long posn;		//pos number
	long trantime;	//transaction total time
//	long availtime; //available time - time left from total transaction time
	long elements;	//counting logged elements
	long items=0;		//count of item records
	long weight;
	long quantity;
//	long prevquantity;
	ISS_GDECIMAL_4 unitprice;
	long extprice;
	bool ean2x[100];	//Will be true for EAN21-29
	char s[30];
//	unsigned char custno[12];
	bool gotcust;
	char transuser[9]={0,0,0,0,0,0,0,0,0};
	ISS_T_LOG_ELEM_ID element;
//	ISS_T_INT_DATE date;
	ISS_T_INT_TIME time;
	ISS_GDECIMAL_6 ean, eano[100];
//	ISS_GDECIMAL_6 eannosubst;
	char logon[20]; //Buser--date--=time=p# from Hp#user--date--=time=...
	char tranuser[5];
	char progress=0;
	char seps[]   = ":";
	int counter,seq_nr=0;
	char atoken[200];
	char *token;
	long amnt=1, quant=1;
	long qq,vv;
	char ss[21];
	char itemcollector[sizeof(ISS_GDECIMAL_6)+3*sizeof(long)];
	ISS_GDECIMAL_6 ean1;

	for (i=1;i<10001;i++)
		dept_id_sum[i]=0;

		TiXmlNode* node = 0;
		TiXmlNode* root = 0;
		TiXmlElement* todoElement= 0;
		TiXmlElement* XMLelement= 0;
		TiXmlElement* CardElement;
		TiXmlHandle hRoot(0);
		TiXmlDocument doc( F );
		
		if ( !doc.LoadFile() )	{
			L.LogE("ERROR: GILBARCO_ProcSale.PreProc: Could not load %s file.!  Error='%s'. Exiting.\n",F, doc.ErrorDesc());
			return 2;
		}
		sprintf(header.custnr,"%s","0");
		sprintf(header.zip,"%","000000");
		header.custinqueue=0;
		header.invoice=false;
		sprintf(header.Lautoken,"%s","0");
		sprintf(header.AP,"%s","000000000");
		sprintf(header.CloseNR,"%","1");
		sprintf(header.ReceiptNR,"%","1");
		header.transID=1; header.chg_amnt=0;
		root = doc.FirstChild( "NAXML-POSJournal" );
		node = root->FirstChildElement( "TransmissionHeader" );
		TiXmlNode* JournalReport = root->FirstChildElement( "JournalReport" );
		TiXmlElement* Journal = JournalReport->FirstChildElement( "JournalHeader" );
		printf("TRANSACTION HEADER\n");
		//Transaction Date
		XMLelement = Journal->FirstChildElement( "BeginDate" );
		//XMLelement = Journal->ToElement();
		printf("BeginDate %s \n",XMLelement->GetText());
		strcpy(header.BDate,XMLelement->GetText());
		//Transaction START time
		XMLelement = Journal->FirstChildElement( "BeginTime" );
		printf("BeginTime %s \n",XMLelement->GetText());
		strcpy(header.BTime,XMLelement->GetText());
		//Transaction END Date
		XMLelement = Journal->FirstChildElement( "EndDate" );
		//XMLelement = node->ToElement();
		printf("EndDate %s \n",XMLelement->GetText());
		strcpy(header.EDate,XMLelement->GetText());
		
		//Transaction END time
		XMLelement = Journal->FirstChildElement( "EndTime" );
		//XMLelement = node->ToElement();
		printf("EndTime %s \n",XMLelement->GetText());
		strcpy(header.ETime,XMLelement->GetText());

		sprintf(s,"%s%s",header.BDate,header.BTime);
		SysTimeFromS18(&st,s);
		sprintf(s,"%s%s",header.EDate,header.ETime);
		SysTimeFromS18(&et,s);

		/********************* Log ON/OFF   ***********************/
		TiXmlElement* Other = JournalReport->FirstChildElement( "OtherEvent" );
		//CashierID
		if(Other) {
			XMLelement = Other->FirstChildElement( "gvr:OperatorID" );
			if (IsNumber(XMLelement->GetText(),strlen(XMLelement->GetText()))) {
				printf("CashierID %s \n",XMLelement->GetText());
				header.cashier=atoi(XMLelement->GetText());
				if (header.cashier>999) {
					L.LogE("ERROR: ProcSale: Cashier id more than tree digits! (%s) -> File rejected!\n",XMLelement->GetText());
					return ISS_SUCCESS;
				}
			}
			else { 
				L.LogE("ERROR: ProcSale: Cashier isn't a number! (%s) -> File rejected!\n",XMLelement->GetText());
				return ISS_SUCCESS;
			}
			XMLelement = Other->FirstChildElement( "RegisterID" );
			if (IsNumber(XMLelement->GetText(),strlen(XMLelement->GetText()))) {
				printf("RegisterID %s \n",XMLelement->GetText());
				seq_nr=atoi(XMLelement->GetText());
				posn=atoi(XMLelement->GetText())-2;
				header.sid=atoi(XMLelement->GetText())-2;
			}
			else { 
				L.LogE("ERROR: ProcSale: RegisterID isn't a number! (%s) -> File rejected!\n",XMLelement->GetText());
				return ISS_SUCCESS;
			}
			XMLelement = Other->FirstChildElement( "CashierDetail" );
			if (XMLelement){
				char onoff[20];
				bool LoggedOn=true;
				sprintf(onoff,XMLelement->Attribute( "detailType"));
				if((onoff[0]=='o')|| (onoff[0]=='O'))
					LoggedOn=false;
				sprintf(logon,"%.4d%s%s%.2d",header.cashier,remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid+1);
				if (LoggedOn){
					if ((retval=ProcLogOff(logon))!=0) {
							L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOff returned %d!\n",retval);
							return retval;//retry allowed
					}
					L.Log("Succesfully logged out!\n");
					return 0;
				}
				
				if (!(*(APos[posn].user))) {//noone is currently logged on
						sprintf(logon,"%.4d%s%s%.2d",header.cashier,remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid+1);
						if (!LoggedOn)
							if((retval=ProcLogOn(logon))!=0) {
								L.LogE("ERROR: ProcSale.AutoLogOn: ProcLogOn returned %d!\n",retval);
								return 1;//no retries
							}
				}
				else { // Ha m�s van bel�pve, akkor ki kell l�ptetni, majd bel�ptetni
					sprintf(tranuser,"%.4d",header.cashier);
					//tranuser[5]=0;
					CleanUserID(tranuser);
					if ((strcmp(APos[posn].user,tranuser))!=0) {
						char u[5];
						memcpy(u,APos[posn].user,4);
						u[4]=0;
						tranuser[4]=0;
						sprintf(logon,"%.4d%s%s%.2d",atoi(APos[posn].user),remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid+1);
						logon[20]=0;
						if ((retval=ProcLogOff(logon))!=0) {
							L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOff returned %d!\n",retval);
							return retval;//retry allowed
						}
						if ((retval=ProcLogOn(logon))!=0) {
							L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOn returned %d!\n",retval);
							return retval;//retry allowed
						}
					}
				}
				return 0;
			}
			XMLelement = Other->FirstChildElement( "ShiftDetail" ); //M�szak z�r�s/nyit�s
			if (XMLelement){
				return 0;
			}
			XMLelement = Other->FirstChildElement( "DayDetail" ); //Nap z�r�s/nyit�s
			if (XMLelement){
				return 0;
			}
		}

		/********************* Elhajt�s/ �resv�lt�s   ***********************/
		TiXmlElement* Financial = JournalReport->FirstChildElement( "FinancialEvent" );
		//CashierID
		if(Financial) {
			sprintf(header.Lautoken,"%s","0");
			XMLelement = Financial->FirstChildElement( "gvr:ExternalTransactionID" );
			if(XMLelement){
				printf("LauToken %s \n",XMLelement->GetText());
				strcpy(header.Lautoken,XMLelement->GetText());
			}
			//AP
			XMLelement = Financial->FirstChildElement( "gvr:FiscalIDPrinter" );
			if(XMLelement) {
				printf("AP %s \n",XMLelement->GetText());
				strcpy(header.AP,XMLelement->GetText());
			}
			//CloseNR
			XMLelement = Financial->FirstChildElement( "gvr:FiscalDayNumber" );
			if(XMLelement) {
				printf("CloseNR %s \n",XMLelement->GetText());
				sprintf(header.CloseNR,"%s",XMLelement->GetText());
	//			header.CloseNR[strlen(XMLelement->GetText())]=0;
				//strcpy(header.CloseNR,XMLelement->GetText());
			}
			//ReceiptNR
			XMLelement = Financial->FirstChildElement( "gvr:FiscalReceiptNumber" );
			if(XMLelement) {
				printf("FiscalReceipt %s \n",XMLelement->GetText());
				sprintf(header.ReceiptNR,"%s",XMLelement->GetText());
			}
			
			//Print Invoice
			XMLelement = Financial->FirstChildElement( "gvr:FiscalInvoiceNumber" );
			if(XMLelement) {
				printf("Print Invoice %s \n",XMLelement->GetText());
				header.invoice=true;
			}
			//CustInQueue
			XMLelement = Financial->FirstChildElement( "CustInQueue" );
			if(XMLelement) {
				printf("CustInQueue %s \n",XMLelement->GetText());
				header.custinqueue=atoi(XMLelement->GetText());
			}
			//TransactionID
			XMLelement = Financial->FirstChildElement( "TransactionID" );
			if(XMLelement) {
				printf("TransactionID %s \n",XMLelement->GetText());
				header.transID=atol(XMLelement->GetText());
			}
			//Customer ZIP code
			XMLelement = Financial->FirstChildElement( "gvr:ZipCode" );
			if(XMLelement) {
				printf("Zip code %s \n",XMLelement->GetText());
				sprintf(header.zip,"%s",XMLelement->GetText());
			}

			c_2000++;
			PreFill(&E2000[c_2000],	2000);
			XMLelement = Financial->FirstChildElement( "gvr:OperatorID" );
			if (IsNumber(XMLelement->GetText(),strlen(XMLelement->GetText()))) {
				printf("CashierID %s \n",XMLelement->GetText());
				header.cashier=atoi(XMLelement->GetText());
				if (header.cashier>999) {
					L.LogE("ERROR: ProcSale: Cashier id more than tree digits! (%s) -> File rejected!\n",XMLelement->GetText());
					return ISS_SUCCESS;
				}
			}
			else { 
				L.LogE("ERROR: ProcSale: Cashier isn't a number! (%s) -> File rejected!\n",XMLelement->GetText());
				return ISS_SUCCESS;
			}
			XMLelement = Financial->FirstChildElement( "RegisterID" );
			if (IsNumber(XMLelement->GetText(),strlen(XMLelement->GetText()))) {
				printf("RegisterID %s \n",XMLelement->GetText());
				posn=atoi(XMLelement->GetText())-2;
				header.sid=atoi(XMLelement->GetText())-2;
			}
			else { 
				L.LogE("ERROR: ProcSale: RegisterID isn't a number! (%s) -> File rejected!\n",XMLelement->GetText());
				return ISS_SUCCESS;
			}
			//TransactionID
			XMLelement = Financial->FirstChildElement( "TransactionID" );
			if(XMLelement) {
				printf("TransactionID %s \n",XMLelement->GetText());
				header.transID=atol(XMLelement->GetText());
			}
			TiXmlElement* PayIn = Financial->FirstChildElement( "FinancialEventDetail" )->FirstChildElement("PayInDetail");
			if (PayIn) return ISS_SUCCESS;
			TiXmlElement* PayOut = Financial->FirstChildElement( "FinancialEventDetail" )->FirstChildElement("PayOutDetail");
			if (PayOut) return ISS_SUCCESS;
			TiXmlElement* Event = Financial->FirstChildElement( "FinancialEventDetail" )->FirstChildElement("PumpTestDetail");
			if (Event){
				int Dispenser_id=0,pump_id=0;
				XMLelement = Event->FirstChildElement( "FuelPositionID" );
				if(XMLelement){
					printf("Dispenser_id %s \n",XMLelement->GetText());
					Dispenser_id=atoi(XMLelement->GetText());
				}
				XMLelement = Event->FirstChildElement( "gvr:FuelNozzleID" );
				if(XMLelement){
					printf("pump_id %s \n",XMLelement->GetText());
					pump_id=atoi(XMLelement->GetText());
				}
				XMLelement = Event->FirstChildElement( "FuelGradeID" );
				if(XMLelement) {
						c_21001++;
						PreFill(&E21001[c_21001],	21001);
						gdec_init_z(ean,6,0,iss_gdec_round_normal);
						gdec_init_z(eano[c_21001],6,0,iss_gdec_round_normal);
						sprintf(s,"%s",XMLelement->GetText());
						sprintf(OrigEAN[c_21001],"%s",s);
						DoItemSubst(s,false);
						L.Log("The original EAN %s and the new : %s\n",XMLelement->GetText(),s);
						
						gdec_from_ascii(ean,s);
						gdec_from_ascii(eano[c_21001],s);
						if (GetItemDataDept(unitprice,&E21001[c_21001].TaxID,&E21001[c_21001].ReportGroup,&E21001[c_21001].EANType,&E21001[c_21001].DeptId,ean)==ISS_E_SDBMS_NOREC) {
							L.LogE("ERROR: GILBARCO.Item: No record in ISS400 for EAN %s! Needed values will be guessed.\n"
								,s);
							E21001[c_21001].ReportGroup=1;
							E21001[c_21001].TaxID=2;
							E21001[c_21001].EANType=5;
							E21001[c_21001].DeptId=3075;
						}
						ean2x[c_21001]=((s[0]=='2')&&(s[1]>'0')&&(s[1]<='9'));
						//printf("ItemCode %s \n",XMLelement->GetText());
						printf("POSCode %s \n",s);
						GDec62Cobol6(E21001[c_21001].ItemCode,ean);
						
				}
				E2000[c_2000].TenderID=90;
				E21001[c_21001].InputDevice=12;
				E21001[c_21001].ReturnMode=FALSE;
				E21001[c_21001].NegativeItem=FALSE;
				E21001[c_21001].SplitQuantity=1;
				float ecquantity=0;
				XMLelement = Event->FirstChildElement( "PumpTestVolume" );
				if(XMLelement){
					printf("SalesQuantity %s \n",XMLelement->GetText());
					E21001[c_21001].Quantity=1;
					ecquantity=atof(XMLelement->GetText());
					weight=1000*ecquantity;
					itemweight[c_21001]=weight;
					LongVal2Cobol4(E21001[c_21001].Weight,weight);
					//items=items+E21001[c_21001].Quantity;
				}
				XMLelement = Event->FirstChildElement( "PumpTestAmount" );
				if(XMLelement) {
					printf("SalesAmount %s \n",XMLelement->GetText());
					ext_price[c_21001]=atof(XMLelement->GetText());
					LongVal2Cobol6(E21001[c_21001].ExtPrice,100*ext_price[c_21001]);//Extprice ok
					dept_id_sum[E21001[c_21001].DeptId]+=atol(XMLelement->GetText());
					total=total+atol(XMLelement->GetText());
					amnt=100*atol(XMLelement->GetText());
				}
				tenders+=(amnt/100);
				rounded=((((1000*amnt)/weight)+5)/10)*10; //�r kisz�mol�sa
				LongVal2Cobol6(E21001[c_21001].UnitPrice,rounded);//Unitprice ok
				LongVal2Cobol6(E21001[c_21001].PriceFromPlu,rounded);
				
				if(disableEmptyChange){
					WriteEmptyChange2DB(header.EDate,header.ETime,header.AP,header.CloseNR,header.ReceiptNR,header.cashier,ecquantity,amnt/100,pump_id,Dispenser_id);
					return ISS_SUCCESS;
				}
			}
			TiXmlElement* DriveOff = Financial->FirstChildElement( "FinancialEventDetail" )->FirstChildElement("DriveOffDetail");
			if (DriveOff){
				c_21001++;
				PreFill(&E21001[c_21001],	21001);
				XMLelement = DriveOff->FirstChildElement( "FuelGradeID" );
				if(XMLelement) {
						gdec_init_z(ean,6,0,iss_gdec_round_normal);
						gdec_init_z(eano[c_21001],6,0,iss_gdec_round_normal);
						sprintf(s,"%s",XMLelement->GetText());
						DoItemSubst(s,false);
						sprintf(OrigEAN[c_21001],"%s",s);
						gdec_from_ascii(ean,s);
						if (GetItemDataDept(unitprice,&E21001[c_21001].TaxID,&E21001[c_21001].ReportGroup,&E21001[c_21001].EANType,&E21001[c_21001].DeptId,ean)==ISS_E_SDBMS_NOREC) {
							L.LogE("ERROR: GILBARCO.Item: No record in ISS400 for EAN %s! Needed values will be guessed.\n"
								,s);
							E21001[c_21001].ReportGroup=1;
							E21001[c_21001].TaxID=2;
							E21001[c_21001].EANType=5;
							E21001[c_21001].DeptId=3075;
						}
						ean2x[c_21001]=((s[0]=='2')&&(s[1]>'0')&&(s[1]<='9'));
						//printf("ItemCode %s \n",XMLelement->GetText());
						printf("POSCode %s \n",s);
						GDec62Cobol6(E21001[c_21001].ItemCode,ean);
				}
				E2000[c_2000].TenderID=91;
				E21001[c_21001].InputDevice=12;
				E21001[c_21001].ReturnMode=FALSE;
				E21001[c_21001].NegativeItem=FALSE;
				E21001[c_21001].SplitQuantity=1;
				long amnt=0, quant=0;
				XMLelement = DriveOff->FirstChildElement( "FuelGradeSalesVolume" );
				if(XMLelement){
					printf("SalesQuantity %s \n",XMLelement->GetText());
					E21001[c_21001].Quantity=1;
					weight=1000*atof(XMLelement->GetText());
					itemweight[c_21001]=weight;
					LongVal2Cobol4(E21001[c_21001].Weight,weight);
					//items=items+E21001[c_21001].Quantity;
				}
				XMLelement = DriveOff->FirstChildElement( "FuelGradeSalesAmount" );
				if(XMLelement) {
					printf("SalesAmount %s \n",XMLelement->GetText());
					ext_price[c_21001]=atof(XMLelement->GetText());
					LongVal2Cobol6(E21001[c_21001].ExtPrice,100*ext_price[c_21001]);//Extprice ok
					dept_id_sum[E21001[c_21001].DeptId]+=atol(XMLelement->GetText());
					total=total+atol(XMLelement->GetText());
					amnt=100*atol(XMLelement->GetText());
				}
				tenders+=(amnt/100);
				rounded=((((1000*amnt)/weight)+5)/10)*10; //�r kisz�mol�sa
				LongVal2Cobol6(E21001[c_21001].UnitPrice,rounded);//Unitprice ok
				LongVal2Cobol6(E21001[c_21001].PriceFromPlu,rounded);
			}
			header.chg_amnt=0;
			tendval[c_2000]=tenders;
			LongVal2Cobol6(E2000[c_2000].TenderVal,100*tenders);
			E2000[c_2000].DrwIDDate=DateYMD2Int(et.wYear,et.wMonth,et.wDay);
			E2000[c_2000].DrwIDTime=et.wHour*3600+et.wMinute*60+et.wSecond;
			E2000[c_2000].DrwIDLocID=APos[posn].sid/2;
			PreFill(&E2000_21499[2*c_2000-1],	21499);
			PreFill(&E2000_21499[2*c_2000],	21499);
			E2000_21499[2*c_2000].Record_Type=-1;
			E2000_21499[2*c_2000-1].Record_Type=-1;
		}

		
		/*********************  SALE *****************************/
		header.voided=false; 
		TiXmlElement* Sale = JournalReport->FirstChildElement( "VoidEvent" );
		if (Sale)
			header.voided=true;
		else 
			Sale = JournalReport->FirstChildElement( "SaleEvent" );
		//CashierID
		if(Sale) {
			XMLelement = Sale->FirstChildElement( "gvr:OperatorID" );
			if (IsNumber(XMLelement->GetText(),strlen(XMLelement->GetText()))) {
				L.Log("CashierID %s \n",XMLelement->GetText());
				header.cashier=atoi(XMLelement->GetText());
				if (header.cashier>999) {
					L.LogE("ERROR: ProcSale: Cashier id more than tree digits! (%s) -> File rejected!\n",XMLelement->GetText());
					return ISS_SUCCESS;
				}
			}
			else { 
				L.LogE("ERROR: ProcSale: Cashier isn't a number! (%s) -> File rejected!\n",XMLelement->GetText());
				return ISS_SUCCESS;
			}
		
			//Till
			XMLelement = Sale->FirstChildElement( "RegisterID" );
			if (IsNumber(XMLelement->GetText(),strlen(XMLelement->GetText()))) {
				printf("RegisterID %s \n",XMLelement->GetText());
				posn=atoi(XMLelement->GetText())-2;
				header.sid=atoi(XMLelement->GetText())-2;
			}
			else { 
				L.LogE("ERROR: ProcSale: RegisterID isn't a number! (%s) -> File rejected!\n",XMLelement->GetText());
				return ISS_SUCCESS;
			}		
			XMLelement = Sale->FirstChildElement( "gvr:ExternalTransactionID" );
			if(XMLelement){
				printf("LauToken %s \n",XMLelement->GetText());
				strcpy(header.Lautoken,XMLelement->GetText());
			}
			//AP
			XMLelement = Sale->FirstChildElement( "gvr:FiscalIDPrinter" );
			if(XMLelement) {
				printf("AP %s \n",XMLelement->GetText());
				strcpy(header.AP,XMLelement->GetText());
			}
			//CloseNR
			XMLelement = Sale->FirstChildElement( "gvr:FiscalDayNumber" );
			if(XMLelement) {
				printf("CloseNR %s \n",XMLelement->GetText());
				sprintf(header.CloseNR,"%s",XMLelement->GetText());
	//			header.CloseNR[strlen(XMLelement->GetText())]=0;
				//strcpy(header.CloseNR,XMLelement->GetText());
			}
			//ReceiptNR
			XMLelement = Sale->FirstChildElement( "gvr:FiscalReceiptNumber" );
			if(XMLelement) {
				printf("FiscalReceipt %s \n",XMLelement->GetText());
				sprintf(header.ReceiptNR,"%s",XMLelement->GetText());
			}
			
			//Print Invoice
			XMLelement = Sale->FirstChildElement( "gvr:FiscalInvoiceNumber" );
			if(XMLelement) {
				printf("Print Invoice %s \n",XMLelement->GetText());
				header.invoice=true;
			}
			//CustInQueue
			XMLelement = Sale->FirstChildElement( "CustInQueue" );
			if(XMLelement) {
				printf("CustInQueue %s \n",XMLelement->GetText());
				header.custinqueue=atoi(XMLelement->GetText());
			}
			//TransactionID
			XMLelement = Sale->FirstChildElement( "TransactionID" );
			if(XMLelement) {
				printf("TransactionID %s \n",XMLelement->GetText());
				header.transID=atol(XMLelement->GetText());
			}
			//Customer ZIP code
			XMLelement = Sale->FirstChildElement( "gvr:ZipCode" );
			if(XMLelement) {
				printf("Zip code %s \n",XMLelement->GetText());
				sprintf(header.zip,"%s",XMLelement->GetText());
			}
			
	/************************** TRANSACTION ********************************/
			//Transaction detail group
			TiXmlElement* TenderID=0;
			TiXmlElement* TaxID=0;
			TiXmlElement* Tender=0;
			TiXmlElement* TransLine =0;
			TiXmlElement* FuelLine=0;
			TiXmlElement* ItemLine=0;
			TiXmlElement* Customer=0;
			TiXmlElement* TransDetail = Sale->FirstChildElement( "TransactionDetailGroup" );
			//TiXmlElement* TransDet = JournalReport->FirstChildElement( "TransactionDetailGroup" )->ToElement();
			TransLine = TransDetail->FirstChildElement( "TransactionLine" );
			//TiXmlElement* TransLine = TransDet->FirstChildElement( "TransactionLine" );


			for( TransLine; TransLine; TransLine=TransLine->NextSiblingElement())
			{
				FuelLine = TransLine->FirstChildElement( "FuelLine" );
				if(FuelLine) {
					printf("******** FUEL Transaction **********\n");
					c_21001++;
					PreFill(&E21001[c_21001],	21001);
					XMLelement = FuelLine->FirstChildElement( "FuelGradeID" );
					if(XMLelement) {
						gdec_init_z(ean,6,0,iss_gdec_round_normal);
						gdec_init_z(eano[c_21001],6,0,iss_gdec_round_normal);
						sprintf(s,"%s",XMLelement->GetText());
						sprintf(OrigEAN[c_21001],"%s",s);
						DoItemSubst(s,false);
						L.Log("The original EAN %s and the new : %s\n",XMLelement->GetText(),s);
						gdec_from_ascii(ean,s);
						gdec_from_ascii(eano[c_21001],s);
						if (GetItemDataDept(unitprice,&E21001[c_21001].TaxID,&E21001[c_21001].ReportGroup,&E21001[c_21001].EANType,&E21001[c_21001].DeptId,ean)==ISS_E_SDBMS_NOREC) {
							L.LogE("ERROR: GILBARCO.Item: No record in ISS400 for EAN %s! Needed values will be guessed.\n"
								,s);
							E21001[c_21001].ReportGroup=1;
							E21001[c_21001].TaxID=2;
							E21001[c_21001].EANType=5;
							E21001[c_21001].DeptId=1;
						}
						ean2x[c_21001]=((s[0]=='2')&&(s[1]>'0')&&(s[1]<='9'));
						//printf("ItemCode %s \n",XMLelement->GetText());
						L.Log("FuelCode %s \n",s);
						GDec62Cobol6(E21001[c_21001].ItemCode,ean);
						
					}
				/*	XMLelement = FuelLine->FirstChildElement( "MerchandiseCode" );
					if(XMLelement) {
						printf("MerchandiseCode %s \n",XMLelement->GetText());
						E21001[c_21001].DeptId=atoi(XMLelement->GetText());					
					}*/
					XMLelement = FuelLine->FirstChildElement( "InputType" );
					E21001[c_21001].InputDevice=12;
					if(XMLelement) {
						printf("InputType %s \n",XMLelement->GetText());
						E21001[c_21001].InputDevice=atoi(XMLelement->GetText());					
					}
					XMLelement = FuelLine->FirstChildElement( "ReturnMode" );
					E21001[c_21001].ReturnMode=FALSE;
					if(XMLelement){
						printf("ReturnMode %s \n",XMLelement->GetText());
						if (XMLelement->GetText()=="1")
							E21001[c_21001].ReturnMode=TRUE;
					}
					XMLelement = FuelLine->FirstChildElement( "ReportGroup" );
					if(XMLelement){
						printf("ReportGroup %s \n",XMLelement->GetText());
						E21001[c_21001].ReportGroup=atoi(XMLelement->GetText());
					}
					XMLelement = FuelLine->FirstChildElement( "NegativItem" );
					E21001[c_21001].NegativeItem=FALSE;
					if(XMLelement) {
						printf("NegativItem %s \n",XMLelement->GetText());
						if (XMLelement->GetText()=="1")
							E21001[c_21001].NegativeItem=TRUE;
					}
					XMLelement = FuelLine->FirstChildElement( "ActualSalesPrice" );
					if(XMLelement) {
						printf("ActualSalesPrice %s \n",XMLelement->GetText());
						LongVal2Cobol6(E21001[c_21001].UnitPrice,100*atof(XMLelement->GetText()));//Unitprice ok
						LongVal2Cobol6(E21001[c_21001].PriceFromPlu,100*atof(XMLelement->GetText()));
						
						//gdec2cobol((char *)E21001[c_21001].UnitPrice,atol(XMLelement->GetText()),4);//Unitprice ok-ish
					}
					XMLelement = FuelLine->FirstChildElement( "SellingUnits" );
					if(XMLelement){
						printf("SellingUnits %s \n",XMLelement->GetText());
						E21001[c_21001].SplitQuantity=atoi(XMLelement->GetText());
					}
					XMLelement = FuelLine->FirstChildElement( "SalesQuantity" );
					if(XMLelement){
						printf("SalesQuantity %s \n",XMLelement->GetText());
						E21001[c_21001].Quantity=1;
						weight=1000*atof(XMLelement->GetText());
						itemweight[c_21001]=weight;
						LongVal2Cobol4(E21001[c_21001].Weight,weight);
						//items=items+E21001[c_21001].Quantity;
					}
					XMLelement = FuelLine->FirstChildElement( "SalesAmount" );
					if(XMLelement) {
						printf("SalesAmount %s \n",XMLelement->GetText());
						ext_price[c_21001]=atof(XMLelement->GetText());
						LongVal2Cobol6(E21001[c_21001].ExtPrice,100*ext_price[c_21001]);//Extprice ok
						dept_id_sum[E21001[c_21001].DeptId]+=atol(XMLelement->GetText());
						total=total+atol(XMLelement->GetText());
					}
			/******************** ItemTAX  ********************************/		
					printf("TAX\n");
					XMLelement = FuelLine->FirstChildElement( "ItemTax" )->FirstChildElement( "TaxLevelID" );
					if(XMLelement) {
						printf("TaxLevelID %s \n",XMLelement->GetText());
						E21001[c_21001].TaxID=atoi(XMLelement->GetText())-1;
					}
				}
				ItemLine = TransLine->FirstChildElement( "ItemLine" );
				if(ItemLine) {
					printf("******** ITEM Transaction **********\n");
					XMLelement = ItemLine->FirstChildElement( "ItemCode" );
					c_21001++;
					PreFill(&E21001[c_21001],	21001);
					if(XMLelement) {
						gdec_init_z(ean,6,0,iss_gdec_round_normal);
						gdec_init_z(eano[c_21001],6,0,iss_gdec_round_normal);
						//gdec_from_ascii(ean,XMLelement->FirstChildElement( "POSCode" )->GetText());
						sprintf(s,"%s",XMLelement->FirstChildElement( "POSCode" )->GetText());
						sprintf(OrigEAN[c_21001],"%s",s);
						DoItemSubst(s,false);
						L.Log("The original EAN %s and the new : %s\n",XMLelement->FirstChildElement( "POSCode" )->GetText(),s);
						//sprintf(OrigEAN[c_21001],"%s",s);
						gdec_from_ascii(ean,s);
						gdec_from_ascii(eano[c_21001],s);
						if (GetItemData(unitprice,&E21001[c_21001].TaxID,&E21001[c_21001].ReportGroup,&E21001[c_21001].EANType,ean)==ISS_E_SDBMS_NOREC) {
							L.LogE("ERROR: GILBARCO.Item: No record in ISS400 for EAN %s! Needed values will be guessed.\n"
								,s);
							E21001[c_21001].ReportGroup=1;
							E21001[c_21001].TaxID=2;
							E21001[c_21001].EANType=5;
						}
						ean2x[c_21001]=((s[0]=='2')&&(s[1]>'0')&&(s[1]<='9'));
						//printf("ItemCode %s \n",XMLelement->GetText());
						L.Log("POSCode %s \n",XMLelement->FirstChildElement( "POSCode" )->GetText());
						GDec62Cobol6(E21001[c_21001].ItemCode,ean);
						//eano[c_21001]=ean;		
					}
					
					E21001[c_21001].InputDevice=12;
					E21001[c_21001].ReturnMode=FALSE;
					E21001[c_21001].NegativeItem=FALSE;

					XMLelement = ItemLine->FirstChildElement( "MerchandiseCode" );
					if(XMLelement){
						L.Log("ReportGroup %s \n",XMLelement->GetText());
						E21001[c_21001].DeptId=atoi(XMLelement->GetText());					
					}
					XMLelement = ItemLine->FirstChildElement( "RegularSellPrice" );
					if(XMLelement) {
						L.Log("ActualSalesPrice %s \n",XMLelement->GetText());
						LongVal2Cobol6(E21001[c_21001].UnitPrice,100*atof(XMLelement->GetText()));//Unitprice ok
						LongVal2Cobol6(E21001[c_21001].PriceFromPlu,100*atof(XMLelement->GetText()));
						//gdec2cobol((char *)E21001[c_21001].UnitPrice,atol(XMLelement->GetText()),4);//Unitprice ok-ish
					}
					XMLelement = ItemLine->FirstChildElement( "SellingUnits" );
					if(XMLelement) {
						L.Log("SellingUnits %s \n",XMLelement->GetText());
						E21001[c_21001].SplitQuantity=atoi(XMLelement->GetText());
					}
					XMLelement = ItemLine->FirstChildElement( "SalesQuantity" );
					if(XMLelement){
						L.Log("SalesQuantity %s \n",XMLelement->GetText());
						E21001[c_21001].Quantity=atoi(XMLelement->GetText());
						weight=0;
						//atoi(XMLelement->GetText());
						itemweight[c_21001]=weight;
						LongVal2Cobol4(E21001[c_21001].Weight,weight);
						//items=items+E21001[c_21001].Quantity;
					}
					XMLelement = ItemLine->FirstChildElement( "SalesAmount" );
					if(XMLelement) {
						L.Log("SalesAmount %s \n",XMLelement->GetText());
						ext_price[c_21001]=atof(XMLelement->GetText());
						LongVal2Cobol6(E21001[c_21001].ExtPrice,100*ext_price[c_21001]);//Extprice ok
						dept_id_sum[E21001[c_21001].DeptId]+=atol(XMLelement->GetText());
						total=total+atoi(XMLelement->GetText());
					}
			/******************** ItemTAX  ********************************/		
					L.Log("TAX\n");
					XMLelement = ItemLine->FirstChildElement( "ItemTax" )->FirstChildElement( "TaxLevelID" );
					if(XMLelement) {
						L.Log("TaxLevelID %s \n",XMLelement->GetText());
						E21001[c_21001].TaxID=atoi(XMLelement->GetText())-1;
					}
				}
				/****************  CUSTOMER   *********************************/
				Customer = TransLine->FirstChildElement( "CustomerID" );
				if(Customer) {
					wchar_t csctmp[80];
					wchar_t *wText;
					char *ansiText;
					int room;
						L.Log("************ CUSTOMER ***********\n");
						XMLelement = Customer->FirstChildElement( "PersonalID" );
						if(XMLelement) {
							L.Log("CustID %s \n",XMLelement->GetText());
							sprintf(header.custnr,"%s",XMLelement->GetText());
							header.custnr[13]=0;
						}
						strcpy(header.custname," ");
						XMLelement = Customer->FirstChildElement( "gvr:CustomerName" );
						if((XMLelement) && (XMLelement->GetText()!=NULL) && (strcmp(XMLelement->GetText(),"(null)")!=0)) {
							L.Log("Name: %s \n",XMLelement->GetText());
							wText = CodePageToUnicode(65001,XMLelement->GetText());
							ansiText = UnicodeToCodePage(852,wText);	
							strcpy(header.custname,ansiText);
							//strcpy(header.custname,XMLelement->GetText());
							L.Log("Name: %s \n",ansiText);
							//wcstombs(header.custname, csctmp, room);
							/*room = MultiByteToWideChar(CP_THREAD_ACP, MB_ERR_INVALID_CHARS, XMLelement->GetText(), strlen(XMLelement->GetText()), NULL, 0);
							MultiByteToWideChar(852, 0, XMLelement->GetText(), strlen(XMLelement->GetText()), csctmp, 80);
							wcstombs(header.custname, csctmp, room);*/

						}
						strcpy(header.custaddr," ");
						XMLelement = Customer->FirstChildElement( "gvr:CustomerAddress" );
						if((XMLelement) && (XMLelement->GetText()!=NULL) && (strcmp(XMLelement->GetText(),"(null)")!=0)) {
							L.Log("Addr: %s \n",XMLelement->GetText());
							wText = CodePageToUnicode(65001,XMLelement->GetText());
							ansiText = UnicodeToCodePage(852,wText);	
							strcpy(header.custaddr,ansiText);
							//strcpy(header.custaddr,XMLelement->GetText());
							L.Log("CustAddr %s \n",header.custaddr);
						}
						strcpy(header.custcity," ");
						XMLelement = Customer->FirstChildElement( "gvr:CustomerCity" );
						if((XMLelement) && (XMLelement->GetText()!=NULL) && (strcmp(XMLelement->GetText(),"(null)")!=0)) {
							L.Log("City: %s \n",XMLelement->GetText());
							wText = CodePageToUnicode(65001,XMLelement->GetText());
							ansiText = UnicodeToCodePage(852,wText);	
							strcpy(header.custcity,ansiText);
							//strcpy(header.custcity,XMLelement->GetText());
							L.Log("CustCity %s \n",header.custcity);
						}
						strcpy(header.custzip," ");
						XMLelement = Customer->FirstChildElement( "gvr:CustomerZipCode" );
						if((XMLelement) && (XMLelement->GetText()!=NULL) && (strcmp(XMLelement->GetText(),"(null)")!=0)) {
							L.Log("CustZip %s \n",XMLelement->GetText());
							sprintf(header.custzip,"%s",XMLelement->GetText());
						}
						strcpy(header.cust_taxnr," ");
						XMLelement = Customer->FirstChildElement( "gvr:CustomerFiscalCode" );
						if((XMLelement) && (XMLelement->GetText()!=NULL) && (strcmp(XMLelement->GetText(),"(null)")!=0)) {
							L.Log("Cust TaxNr %s \n",XMLelement->GetText());
							sprintf(header.cust_taxnr,"%s",XMLelement->GetText());
						}
						strcpy(header.licence_plate," ");
						XMLelement = Customer->FirstChildElement( "gvr:CustomerVIN" );
						if((XMLelement) && (XMLelement->GetText()!=NULL) && (strcmp(XMLelement->GetText(),"(null)")!=0)) {
							L.Log("Licence plate %s \n",XMLelement->GetText());
							char lp1[100];
							sprintf(lp1,"%s",XMLelement->GetText());
							lp1[9]=0;
							sprintf(header.licence_plate,"%s",lp1);
							if(strcmp(header.licence_plate,"(null)")==0) strcpy(header.licence_plate," ");
						}
						char n1[30],n2[30];
						n1[0]=0;n2[0]=0;
						int cl=strlen(header.custname);
						if (cl>30) {
							strcpyi(n2,header.custname,30);
						
							strcpyi(n1,header.custname+30,cl-30);
							n2[30]=0;n1[cl-30]=0;
						}
						else {
							strcpyi(n2,header.custname,cl);
							n2[cl]=0;
						}
						char addr1[30],addr2[30];
						addr1[0]=0;addr2[0]=0;
						cl=strlen(header.custaddr);
						if (cl>30) {
							strcpyi(addr1,header.custaddr,30);
							strcpyi(addr2,header.custaddr+30,cl-30);
							addr1[30]=0;addr2[cl-30]=0;
						}
						else {
							strcpyi(addr1,header.custaddr,cl);
							addr1[cl]=0;
						}
						if((strlen(header.custnr)==13) && (strncmp(header.custnr,"980000",2)==0)) {
							L.Log("Write customer to DB! %s\n",header.custnr);
							//RegCSC(header.custnr,n1,n2,addr1,addr2,header.custcity,header.custzip,header.cust_taxnr,header.licence_plate);
							//WriteCSC2DB(header.custnr,header.custname,header.custzip,header.custcity,header.custaddr,header.licence_plate,header.cust_taxnr,seq_nr);
							
						}
						else {
							L.Log("It's not a new customer card! %s\n",header.custnr);
						}
						
				}
			
			/****************  TENDERS   *********************************/
				Tender = TransLine->FirstChildElement( "TenderInfo" );
				bool go=true;
				bool goCard=true;
				while((Tender) && (go)) {
					//for(Tender->FirstChildElement();Tender;Tender=Tender->NextSiblingElement()) {
						printf("************ TENDERS ***********\n");
						c_2000++;
						PreFill(&E2000_21499[2*c_2000-1],	21499);
						PreFill(&E2000_21499[2*c_2000],	21499);
						PreFill(&E2000[c_2000],	2000);
						memset( E2000_21499[2*c_2000-1].Data, ' ', 180 );
						memset( E2000_21499[2*c_2000].Data, ' ', 36 );

						E2000_21499[2*c_2000].Record_Type=-1;
						E2000_21499[2*c_2000-1].Record_Type=-1;
						
						TenderID = Tender->FirstChildElement( "Tender" );
						//printf("%s \n",XMLelement->GetText());
						XMLelement = TenderID->FirstChildElement( "TenderSubCode" );
						if(XMLelement) {
							L.Log("TenderID %s \n",XMLelement->GetText());
							if(IsNumber(XMLelement->GetText(),strlen(XMLelement->GetText())))
								E2000[c_2000].TenderID=atoi(XMLelement->GetText());
						}
						else 
							E2000[c_2000].TenderID=1;
						chg_rate=100;
						XMLelement = Tender->FirstChildElement( "gvr:ChangeRate" );
						if(XMLelement) {
							L.Log("Change Rate %s \n",XMLelement->GetText());
							if(IsNumber(XMLelement->GetText(),strlen(XMLelement->GetText())))
								chg_rate=100*atof(XMLelement->GetText());
						}
						XMLelement = Tender->FirstChildElement( "gvr:ChangeAmount" );
						if(XMLelement) {
							L.Log("Change amount %s \n",XMLelement->GetText());
							if(IsNumber(XMLelement->GetText(),strlen(XMLelement->GetText())))
								header.chg_amnt+=atof(XMLelement->GetText());
						}
						XMLelement = Tender->FirstChildElement( "TenderAmount" );
						L.Log("TenderAmount %s \n",XMLelement->GetText());
						if(IsNumber(XMLelement->GetText(),strlen(XMLelement->GetText()))){
							tendval[c_2000]=atol(XMLelement->GetText());
							LongVal2Cobol6(E2000[c_2000].TenderVal,100*tendval[c_2000]);
								//E2000[c_2000].TenderVal=atoi(XMLelement->GetText());
							
							tenders=tenders+((chg_rate*100*atof(XMLelement->GetText())+5001)/10000);
						}
						 
						E2000[c_2000].DrwIDDate=DateYMD2Int(et.wYear,et.wMonth,et.wDay);
						E2000[c_2000].DrwIDTime=et.wHour*3600+et.wMinute*60+et.wSecond;
						E2000[c_2000].DrwIDLocID=header.sid;
						CardElement = Tender->FirstChildElement( "gvr:PrivateData" );
						goCard=true;
						while((CardElement) && (goCard)) {
							printf("%s\n",CardElement->GetText());
//							for(int g=0;g<2;g++) {
							if (atoi(CardElement->Attribute("line"))==1) {  //(E2000[c_2000].TenderID!=21) && (
									
								if(E2000[c_2000].TenderID==18) { //Aj�nd�kk�rtya
										memset(E2000_21499[2*c_2000-1].Data,' ',184);
										memset(E2000_21499[2*c_2000].Data,' ',180);
										E2000_21499[2*c_2000-1].Record_Type=4;
										E2000_21499[2*c_2000].Record_Type=6;
										sprintf(atoken,"%s",CardElement->GetText());
										if (strlen(atoken)>50) { //Egy sorban van minden
											token = strtok( atoken, seps );
											counter=0;
											while( token != NULL )	{
												if((counter>0)&&(counter<6))  {
													sprintf(atoken,"%s",token);
													E2000_21499[2*c_2000].Record_Type=6;
													if (counter!=5) 
														atoken[strlen(atoken)-2]=0;
													
													strcpy(E2000_21499[2*c_2000].Data+(counter-1)*36+2,atoken);
													E2000_21499[2*c_2000].Data[(counter-1)*36+1]=':';
													E2000_21499[2*c_2000].Data[(counter-1)*36+strlen(atoken)+2]=' ';
													switch (counter) {
														case 1:E2000_21499[2*c_2000].Data[(counter-1)*36]='D';
															break;
														case 2:E2000_21499[2*c_2000].Data[(counter-1)*36]='T';
															break;
														case 3:E2000_21499[2*c_2000].Data[(counter-1)*36]='A';
															break;
														case 4:E2000_21499[2*c_2000].Data[(counter-1)*36]='V';
															break;
														case 5:E2000_21499[2*c_2000].Data[(counter-1)*36]='B';
															break;
													}
													token = strtok( NULL, seps );
												}
												if(counter==0) {
													token = strtok( NULL, seps );
													E2000_21499[2*c_2000-1].Record_Type=4;
													memset(E2000_21499[2*c_2000-1].Data,' ',180);
													if (token!=NULL){
														sprintf(atoken,"%s",token);
														atoken[strlen(atoken)-2]=0;
													}
													else
														sprintf(atoken,"%s","  ");
													//atoken[strlen(atoken)]=0;
													
													strcpy(E2000_21499[2*c_2000-1].Data,atoken);
													E2000_21499[2*c_2000-1].Data[strlen(atoken)]=' ';
												}
												counter++;
											}
										}
										else {
											strcpy(E2000_21499[2*c_2000-1].Data,atoken+2);
											E2000_21499[2*c_2000-1].Data[strlen(atoken)-2]=' ';
											sprintf(E2000_21499[2*c_2000].Data,"%s",atoken);
											E2000_21499[2*c_2000].Data[strlen(atoken)]=' ';
										}
									}
									if((E2000[c_2000].TenderID==3)||(E2000[c_2000].TenderID==4)) {  //Hitelk�rtya
										sprintf(atoken,"%s",CardElement->GetText());
										E2000_21499[2*c_2000-1].Record_Type=6;
										E2000_21499[2*c_2000].Record_Type=6;
										memset(E2000_21499[2*c_2000-1].Data,' ',180);
										memset(E2000_21499[2*c_2000].Data,' ',36);
										if (strlen(atoken)>50) { //Egy sorban van minden
											token = strtok( atoken, seps );
											counter=0;
											while( token != NULL )	{
												token = strtok( NULL, seps );
												if(counter<5) {
													sprintf(atoken,"%s",token);
													atoken[strlen(atoken)-2]=0;
													strcpy(E2000_21499[2*c_2000-1].Data+(counter)*36+2,atoken);
													E2000_21499[2*c_2000-1].Data[(counter)*36+1]=':';
													E2000_21499[2*c_2000-1].Data[(counter)*36+strlen(atoken)+2]=' ';
													switch (counter) {
														case 0:E2000_21499[2*c_2000-1].Data[(counter)*36]='D';
															break;
														case 1:E2000_21499[2*c_2000-1].Data[(counter)*36]='E';
															break;
														case 2:E2000_21499[2*c_2000-1].Data[(counter)*36]='K';
															break;
														case 3:E2000_21499[2*c_2000-1].Data[(counter)*36]='N';
															break;
														case 4:E2000_21499[2*c_2000-1].Data[(counter)*36]='O';
															break;
													}
												}
												if(counter==5) {
													E2000_21499[2*c_2000].Record_Type=6;
													strcpy(E2000_21499[2*c_2000].Data,"Y:");
													if (token!=NULL)
														sprintf(atoken,"%s",token);
													else
														sprintf(atoken,"%s","  ");
													atoken[strlen(atoken)]=0;
													
													strcpy(E2000_21499[2*c_2000].Data+2,atoken);
													E2000_21499[2*c_2000].Data[strlen(atoken)+2]=' ';
												}
												counter++;
											}
										}
										else {
											strcpy(E2000_21499[2*c_2000-1].Data,atoken);
											E2000_21499[2*c_2000-1].Data[strlen(atoken)]=' ';
										}
									}
								}
//							}
							if (atoi(CardElement->Attribute("line"))==2) {
								sprintf(atoken,"%s",CardElement->GetText());	
								if(E2000[c_2000].TenderID==18) { //Aj�nd�kk�rtya
										strcpy(E2000_21499[2*c_2000].Data+(1*36),atoken);
										E2000_21499[2*c_2000].Data[1*36+strlen(atoken)]=' ';
									}
								if((E2000[c_2000].TenderID==3)||(E2000[c_2000].TenderID==4)) { //Bankk�rtya
										strcpy(E2000_21499[2*c_2000-1].Data+(1*36),atoken);
										E2000_21499[2*c_2000-1].Data[1*36+strlen(atoken)]=' ';
									}

							}
							if (atoi(CardElement->Attribute("line"))==3) {
									sprintf(atoken,"%s",CardElement->GetText());
									if(E2000[c_2000].TenderID==18) { //Aj�nd�kk�rtya
										strcpy(E2000_21499[2*c_2000].Data+(2*36),atoken);
										E2000_21499[2*c_2000].Data[2*36+strlen(atoken)]=' ';
									}
								if((E2000[c_2000].TenderID==3)||(E2000[c_2000].TenderID==4)) { //Bankk�rtya
										strcpy(E2000_21499[2*c_2000-1].Data+(2*36),atoken);
										E2000_21499[2*c_2000-1].Data[2*36+strlen(atoken)]=' ';
									}
							}
							if (atoi(CardElement->Attribute("line"))==4) {
								sprintf(atoken,"%s",CardElement->GetText());
								if(E2000[c_2000].TenderID==18) { //Aj�nd�kk�rtya
										strcpy(E2000_21499[2*c_2000].Data+(3*36),atoken);
										E2000_21499[2*c_2000].Data[3*36+strlen(atoken)]=' ';
									}
								if((E2000[c_2000].TenderID==3)||(E2000[c_2000].TenderID==4)) { //Bankk�rtya
										strcpy(E2000_21499[2*c_2000-1].Data+(3*36),atoken);
										E2000_21499[2*c_2000-1].Data[3*36+strlen(atoken)]=' ';
									}
							}
							if (atoi(CardElement->Attribute("line"))==5) {
									sprintf(atoken,"%s",CardElement->GetText());
									if(E2000[c_2000].TenderID==18) { //Aj�nd�kk�rtya
										strcpy(E2000_21499[2*c_2000].Data+(4*36),atoken);
										E2000_21499[2*c_2000].Data[4*36+strlen(atoken)]=' ';
									}
								if((E2000[c_2000].TenderID==3)||(E2000[c_2000].TenderID==4)) { //Bankk�rtya
										strcpy(E2000_21499[2*c_2000-1].Data+(4*36),atoken);
										E2000_21499[2*c_2000-1].Data[4*36+strlen(atoken)]=' ';
									}
							}
							if (atoi(CardElement->Attribute("line"))==6) {
								sprintf(atoken,"%s",CardElement->GetText());
								if((E2000[c_2000].TenderID==3)||(E2000[c_2000].TenderID==4)) { //Bankk�rtya
										strcpy(E2000_21499[2*c_2000].Data,atoken);
										E2000_21499[2*c_2000].Data[strlen(atoken)]=' ';
								}
							}
							CardElement=CardElement->NextSiblingElement();
							if((!CardElement)||(!CardElement->GetText())) 
								goCard=false;
						}
					//}
					Tender=Tender->NextSiblingElement();
					if(Tender) {
						TenderID = Tender->FirstChildElement( "Tender" );
						if(!TenderID)
							go=false;
					}
					else 	
						go=false;
				}
		}
	}

	time_t t1,t2;
	t1=TimeFromSystemTime(&st);
	t2=TimeFromSystemTime(&et);
	trantime=difftime(t2,t1);  //Fizet�s kezdete �s v�ge k�z�tt eltelt id�
	round_amnt=total-(tenders-header.chg_amnt);
	
	if (abs(round_amnt)>2) {//Fizet�eszk�z�k �rt�ke nem egyezik meg a cikkek �sszeg�vel
		if(!header.voided){
			L.LogE("ERROR - The total: %d and the tender total: %d doesn't match!\n",total,tenders-header.chg_amnt);
			L.LogE("	==>Void this transaction!! File name: %s\n",F);
			return false;
		}
	}
	short y,m,d;
	DateInt2YMD(APos[posn].signondate,&y,&m,&d);
	L.Log("Sign on date: %d.%d.%d\n",y,m,d);
	L.Log("Check the user! Apos.User: %s , F�jl user: %d\n",APos[posn].user,header.cashier);
	if(header.cashier>999) {
		L.LogE("ERROR - Bad user %d!\n",header.cashier);
		return 0;
	}

	if (!(*(APos[posn].user))) {//noone is currently logged on
				if ((posn>2)||((AutoActions&0x00000001)!=0)) {
					if (posn<=2) L.LogE("Warning: ProcSale: Sequence error - noone has been logged on (PoS #%d)!\n",posn+1);
					
					if (posn<=2) L.LogE("==> Now performing autologon by sales transaction header: %s\n",s);
					else L.Log("Performing autologon by sales transaction header (pos #%d): %s\n",posn+1,s);
					sprintf(logon,"%.4d%s%s%.2d",header.cashier,remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid+1);
					if ((retval=ProcLogOn(logon))!=0) {
						L.LogE("ERROR: ProcSale.AutoLogOn: ProcLogOn returned %d!\n",retval);
						return 0;//no retries
					}
					sprintf(APos[posn].user,"%d",header.cashier);
				}
				else {
					L.LogE("ERROR: ProcSale: Transaction sequence error - Noone has been logged on (PoS #%d)!\n",posn+1);
					return 0;//no retries
				}
	}
	else {
				
				char tranuser[5];
				sprintf(tranuser,"%.4d",header.cashier);
				//tranuser[5]=0;
				CleanUserID(tranuser);
				L.Log("Tranuser: %s APos.User: %s \n",tranuser,APos[posn].user);
				if ((strcmp(APos[posn].user,tranuser))!=0) {
					char u[5];
					memcpy(u,APos[posn].user,4);
					u[4]=0;
					tranuser[4]=0;
					if ((AutoActions&0x00000002)!=0) {
						char logon[20]; //user--date--=time=p# from Hp#user--date--=time=...
						long retval;
//						long len;
						L.LogE("Warning: ProcSale: Not the logged on user does sales! (Selling:%s Logged_on:%d))! (PoS #%d)\n"
							,tranuser,header.cashier,posn+1);
						L.LogE("==> Now performing automatic logoff/logon by sales transaction ID: %d\n",header.transID);
						sprintf(logon,"%.4d%s%s%.2d",header.cashier,remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid+1);
						logon[20]=0;
						if ((retval=ProcLogOff(logon))!=0) {
							L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOff returned %d!\n",retval);
							return retval;//retry allowed
						}
						if ((retval=ProcLogOn(logon))!=0) {
							L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOn returned %d!\n",retval);
							return retval;//retry allowed
						}
					}
					else {
						L.LogE("ERROR: ProcSale: Not the logged on user does sales! (Selling:%s Logged_on:%s))! (PoS #%d)\n"
							,tranuser,u,posn+1);
						return 0;//no retries
					}
				}
	}
	//Advancing performance period(s) if needed
	char tmp[50];
	int g;
	sprintf(tmp,"%s%s",remove_char(header.BDate,"-"),remove_char(header.BTime,":"));
	tmp[14]=0;
	SysTimeFromS14(&st,tmp);
	Date=DateYMD2Int(st.wYear,st.wMonth,st.wDay);
	c_21099++;
	//Lautoken
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=81;
	E21099[c_21099].TransStore=StoreNum;
	g=21-strlen(header.Lautoken);
	memset(E21099[c_21099].DataEnter,'0',g);
	E21099[c_21099].DataEnter[2]='1';
	strcpy(E21099[c_21099].DataEnter+g,header.Lautoken);
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	c_21099++;
	//Nyugta sz�mla
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=81;
	E21099[c_21099].TransStore=StoreNum;
	memset(E21099[c_21099].DataEnter,' ',21);
	if(!header.invoice) //Nyugta
		strcpy(E21099[c_21099].DataEnter,"006NY");
	else   //Sz�mla
		strcpy(E21099[c_21099].DataEnter,"006SZ");
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	
	c_21099++;
	//AP
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=81;
	E21099[c_21099].TransStore=StoreNum;
	g=21-strlen(header.AP);
	memset(E21099[c_21099].DataEnter,'0',g);
	E21099[c_21099].DataEnter[2]='4';
	strcpy(E21099[c_21099].DataEnter+g,header.AP);
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	//Z�r�ssz�m
	c_21099++;
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=81;
	E21099[c_21099].TransStore=StoreNum;
	g=21-strlen(header.CloseNR);
	memset(E21099[c_21099].DataEnter,'0',g);
	E21099[c_21099].DataEnter[2]='5';
	strcpy(E21099[c_21099].DataEnter+g,header.CloseNR);
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	int cust_cnt=0;
	if (strcmp(header.custnr,"0")!=0) {
		cust_cnt=4;
		gotcust=true;
		c_21099++;
		
		//V�s�rl� azonos�t�
		PreFill(&E21099[c_21099],21099);
		E21099[c_21099].CurrDate=Date;
		E21099[c_21099].TransLoc=APos[posn].sid/2;
		E21099[c_21099].DataType=17;
		E21099[c_21099].TransStore=StoreNum;
		memset(E21099[c_21099].DataEnter,' ',21);
		g=13-strlen(header.custnr);
		memset(E21099[c_21099].DataEnter,'0',g);
		strcpy(E21099[c_21099].DataEnter+g,header.custnr);
		E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
		c_21499++;
		//V�s�rl�i adatok
		PreFill(&E21499[c_21499],21499);
		E21499[c_21499].Record_Type=1;
		memset(E21499[c_21499].Data,' ',184);
		g=18-strlen(header.custnr);
		memset(E21499[c_21499].Data,'0',g);
		strcpy(E21499[c_21499].Data+g,header.custnr);
		//n�v
		strcpy(E21499[c_21499].Data+18,header.custname);
		E21499[c_21499].Data[18+strlen(header.custname)]=' ';
		//irsz�m
		strcpy(E21499[c_21499].Data+63,header.custzip);
		E21499[c_21499].Data[63+strlen(header.custzip)]=' ';
		//v�ros
		strcpy(E21499[c_21499].Data+68,header.custcity);
		E21499[c_21499].Data[68+strlen(header.custcity)]=' ';
		//c�m
		strcpy(E21499[c_21499].Data+108,header.custaddr);
		E21499[c_21499].Data[108+strlen(header.custaddr)]=' ';
		E21499[c_21499].Data[184]=0;
		c_21499++;
		//V�s�rl�i adatok
		PreFill(&E21499[c_21499],21499);
		E21499[c_21499].Record_Type=2;
		memset(E21499[c_21499].Data,' ',45);
		strcpy(E21499[c_21499].Data,header.cust_taxnr);
		E21499[c_21499].Data[strlen(header.cust_taxnr)]=' ';
		E21499[c_21499].Data[45]='1';

		c_21099++;
		//V�s�rl� azonos�t�
		PreFill(&E21099[c_21099],21099);
		E21099[c_21099].CurrDate=Date;
		E21099[c_21099].TransLoc=APos[posn].sid/2;
		E21099[c_21099].DataType=81;
		E21099[c_21099].TransStore=StoreNum;
		memset(E21099[c_21099].DataEnter,' ',21);
		memset(E21099[c_21099].DataEnter,'0',2);
		E21099[c_21099].DataEnter[2]='2';
		strcpy(E21099[c_21099].DataEnter+3,header.custnr);
		E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	}
	c_21099++;
	//D�tum
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=51;
	E21099[c_21099].TransStore=StoreNum;
	time=st.wHour*3600+st.wMinute*60+st.wSecond;
	//CheckTranDateTime(&Date,&time,posn);
	sprintf(E21099[c_21099].DataEnter,"%04dA%02dA%02d",st.wYear,st.wMonth,st.wDay);	//Fiscal date gets stored
	memset(E21099[c_21099].DataEnter+10,' ',11);
	c_21099++;
	//Napi nyugta sz�ml�l� ISS
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=16;
	E21099[c_21099].TransStore=StoreNum;
	//CheckTranDateTime(&Date,&time,posn);
	memset(E21099[c_21099].DataEnter,' ',21);
	sprintf(E21099[c_21099].DataEnter,"%d",atol(header.ReceiptNR));
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	//Fiscalis bejegyz�s
	c_21099++;
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=92;
	E21099[c_21099].TransStore=StoreNum;
	//CheckTranDateTime(&Date,&time,posn);
	memset(E21099[c_21099].DataEnter,' ',21);
	sprintf(E21099[c_21099].DataEnter,"%s/%.4d/%.5d",header.AP,atoi(header.CloseNR),atol(header.ReceiptNR));
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	//Ir�ny�t�sz�m bejegyz�s
	c_21099++;
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=7;
	E21099[c_21099].TransStore=StoreNum;
	//CheckTranDateTime(&Date,&time,posn);
	memset(E21099[c_21099].DataEnter,' ',7);
	sprintf(E21099[c_21099].DataEnter,"%s",header.zip);
	E21099[c_21099].DataEnter[strlen(header.zip)]=' ';

	while (Check4NewPerfSlot(posn,Date,time)); //Advancing performance slots until it's necessary

	//Checks done before opening a new transaction
	SINT32        transno;
	ISS_T_BOOLEAN inprogress;
	SINT16        element_num;

	retval=iss46_hlog_get_curr_trans_num(APos[posn].sid,&transno,&inprogress,&element_num);
	L.Log("Info: ProcSale.check_before_start.get_curr_trans: tr=%d, open=%d, elem=%d\n"
		,transno,inprogress,element_num);
	if (retval!=ISS_SUCCESS) {
		L.LogE("ERROR: ProcSale.check_before_start.get_curr_trans: Bad return value! (%d)\n",retval);
		return retval;
	}
	if (inprogress) {
		L.LogE("ERROR: ProcSale.check_before_start: Transaction must not be open at this point!\n");
		return(-1);
	}
	APos[posn].lasttran++;//Preparing new transaction's number
	if (APos[posn].lasttran!=transno) {//checks whether ISS thinks the same
		L.LogE("Warning: ProcSale.check_before_start: Transaction numbers doesn't match!\n");
		L.LogE("==> APos[%d].lasttran=%d ISSLOG.transno=%d SID=%d\n"
			,posn,APos[posn].lasttran,transno,APos[posn].sid);
		APos[posn].lasttran=transno; //Correction by ISS
	}

	//Preparing opening a new transaction
	element.element_num=1;
	element.element_time=time;
	memcpy(transuser,APos[posn].user,4);

	if ((retval=iss46_hlog_start_block(APos[posn].sid,hp,APos[posn].lasttran,Date,time
		,0,0x00018000,element,9,transuser,&handle))!=ISS_SUCCESS) {
		L.LogE("ERROR: ProcSale.start returned: %d -> retrying!\n",retval);
		return(retval);
	}
	else L.Log("ProcSale.start: Successful.\n");
	

	elements=1;
	if ((retval=iss46_hlog_send_element(handle,5100,time,0x0000,sizeof(E5100),(char*)&E5100))
		!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcSale.5100 returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcSale.5100: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcSale.5100: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("Info: ProcSale.5100: Successful.\n");
	open=true;
	elements++;
	sold=0;
	APos[posn].p_custcnt++;
//cust_cnt=0;
	/*********************   21099   *******************/
	for(i=1;i<=2+cust_cnt;i++) {
				retval=SafeSendHLogElement(posn,&handle,21099,time,0x0000,sizeof(E21099[i])
					,(char*)&E21099[i]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21099.fiscal_date returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21099.fiscal_date: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21099.fiscal_date: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21099.fiscal_date: successful.\n");
				elements++;
	}
	/*********************   21499   *******************/
	if (cust_cnt>0)
		for(i=1;i<=2;i++) {
					retval=SafeSendHLogElement(posn,&handle,21499,time,0x0000,sizeof(E21499[i])
						,(char*)&E21499[i]);
					if (retval!=ISS_SUCCESS) {
						ISS_T_REPLY ret2;
						L.LogE("ERROR: ProcSale.21499.fiscal_date returned: %d -> abort tran & retry!\n",retval);
						if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
							L.LogE("ERROR: ProcSale.21499.fiscal_date: AbortTran: %d!\n",ret2);
						}
						else L.Log("Info: ProcSale.21499.fiscal_date: AbortTran successful.\n");
						return(retval);
					}
					else L.Log("Info: ProcSale.21499.fiscal_date: successful.\n");
					elements++;
		}
	/*********************   21001   *******************/
	for(i=1;i<=c_21001;i++) {
		sprintf(ss,"0000000%s",OrigEAN[i]);
		ss[21]=0;
		L.Log("EAN code: %s\n",ss);
		gdec_init_z(ean1,6,0,iss_gdec_round_normal);
		gdec_from_ascii(ean1,ss);
		//gdec_init_z(ean,6,0,iss_gdec_round_normal);
		memcpy(itemcollector,&posn,sizeof(long));
		memcpy(itemcollector+sizeof(long),ean1,sizeof(ISS_GDECIMAL_6));
		if (GItemTotals.GetData(itemcollector,NULL,false)) {
			L.Log("The addad Value=%d Quantity=%d\n",ext_price[i],itemweight[i]);
			L.Log("This is the first sale of this ITEM %s at this till %d.\n",OrigEAN[i],header.sid+PosSIDBase/2);
			qq=0;vv=0;
		}
		else {
			qq=*((long*)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)));
			vv=*((long*)(itemcollector+sizeof(long)*2+sizeof(ISS_GDECIMAL_6)));
			L.Log("The addad Value=%d Quantity=%d\n",ext_price[i],itemweight[i]);
			L.Log("Total sales for item %s on this till %d! ValueMy=%d QuantityMy=%d\n"
					,OrigEAN[i],header.sid+PosSIDBase/2,vv,qq);
		}
		extprice=ext_price[i];quantity=itemweight[i];
		memcpy(itemcollector,&posn,sizeof(long));
		memcpy(itemcollector+sizeof(long),ean1,sizeof(ISS_GDECIMAL_6));
		memcpy(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6),&quantity,sizeof(long));
		memcpy(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6),&extprice,sizeof(long));
		GItemTotals.AddFigures(itemcollector);

		retval=SafeSendHLogElement(posn,&handle,21001,time,0x0000,sizeof(E21001[i])
			,(char*)&E21001[i]);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcSale.21001 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.21001: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcSale.21001: AbortTran successful.\n");
			return (ISS_SUCCESS);
		}
		else L.Log("Info: ProcSale.21001: successful.\n");
		element.element_time+=(trantime/c_21001);
		time+=(trantime/c_21001);
		elements++;
		items+=E21001[c_21001].Quantity;

		if (!header.voided) {
			APos[posn].p_regtime+=(trantime/c_21001);
			APos[posn].regtime+=(trantime/c_21001);
			APos[posn].items+=E21001[c_21001].Quantity;
			APos[posn].p_itemcnt+=E21001[c_21001].Quantity;
		}
	}
	if (!header.voided) 
		APos[posn].p_salesval+=total;
	sold=total;

if (!header.voided) {
/********************    2000    ************************/
char truser[9];
//long q1,v1;
	for(i=1;i<=c_2000;i++) {
		//memcpy(E2000[i].DrwIDUserID,transuser,9);
		strcpy(truser,transuser);
		truser[strlen(transuser)]=0;
		strcpy(E2000[i].DrwIDUserID,truser);
		memcpy(E2060.DrwIDUserID,transuser,9);
		E2000[i].DrwIDDate=APos[posn].drwdate;
		E2000[i].DrwIDTime=APos[posn].drwtime;
		E2060.DrwIDDate=APos[posn].drwdate;
		E2060.DrwIDTime=APos[posn].drwtime;

		rounded=total;//Initially not rounded
		if ((CashRounding)&&(E2000[i].TenderID==1)) {//Do rounding
			rounded=((total+CashRounding/2)/CashRounding)*CashRounding;
			totrounddiff+=rounded-total;
		}
		
		//TenderTotals.AddFigures(E2000[i].TenderID,tendval[i],1);
		//AddTendering(posn,E2000.TenderID,extprice);
		AddTendering(posn,(long) E2000[i].TenderID,tendval[i]);
		retval=SafeSendHLogElement(posn,&handle,2000,time,0x0000,sizeof(E2000[i]),(char*)&E2000[i]);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcSale.2000 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.2000: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcSale.2000: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("Info: ProcSale.2000: successful.\n");
		elements++;
		if(E2000_21499[2*i-1].Record_Type!=-1) {
				retval=SafeSendHLogElement(posn,&handle,21499,time,0x0000,sizeof(E2000_21499[2*i-1])
						,(char*)&E2000_21499[2*i-1]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21499 Tender details returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21499.Tender details: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21499.Tender details: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21499.Tender details: successful.\n");
				elements++;
				retval=SafeSendHLogElement(posn,&handle,21499,time,0x0000,sizeof(E2000_21499[2*i])
						,(char*)&E2000_21499[2*i]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21499 Tender details Y returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21499.Tender details Y: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21499.Tender details Y: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21499.Tender details Y: successful.\n");
				elements++;
		}
	}
/*********************    2060      ***************************/
	if (header.chg_amnt>0) {//change was given (even after rounding applied)
		LongVal2Cobol6(E2060.ChangeGiven,header.chg_amnt*100);
		retval=SafeSendHLogElement(posn,&handle,2060,time,0x0000,sizeof(E2060)
			,(char*)&E2060);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcSale.2600 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.2600: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcSale.2600: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("Info: ProcSale.2600: successful.\n");
		elements++;
		//AddTendering(posn,1,-rounded); //Subtract rounded cash given back
		AddTendering(posn,E2060.TenderID,-1*header.chg_amnt); //Subtract cash given back
	}
} //voided transakci�
else {
	TElem4000	E4000;
	PreFill(&E4000,	4000);
	sprintf(E4000.AuthUser,"%d",header.cashier);
	E4000.AuthRefused=FALSE;
	sprintf(E4000.Function,"%s","TAWW-AUTHVOID");
	retval=SafeSendHLogElement(posn,&handle,4000,time,0x0000,sizeof(E4000)
		,(char*)&E4000);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcSale.4000 returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcSale.4000: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcSale.4000: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("Info: ProcSale.4000: successful.\n");
	elements++;
}
	/*********************   21099   *******************/
	for(i=3+cust_cnt;i<=c_21099;i++) {
				retval=SafeSendHLogElement(posn,&handle,21099,time,0x0000,sizeof(E21099[i])
					,(char*)&E21099[i]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21099.fiscal_date returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21099.fiscal_date: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21099.fiscal_date: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21099.fiscal_date: successful.\n");
				elements++;
	}
	if (cust_cnt>0) cust_cnt=3;

/**********************    21020     ***********************************/	
	//Sale perf record
	LongVal2Cobol6(E21020.SalesTotal,sold*100);
	LongVal2Cobol6(E21020.ItemSoldTtl,sold*100);
	if (CashRounding) {//Rounding active
		LongVal2Cobol6(E21020.TenderTotal,(tenders)*100);
		LongVal2Cobol6(E21020.RoundingTtl,0);
	}
	else LongVal2Cobol6(E21020.TenderTotal,(tenders)*100);
	E21020.ItemCnt=c_21001;
	E21020.ItemSoldCnt=c_21001;
	E21020.TenderCnt=c_2000;
	retval=SafeSendHLogElement(posn,&handle,21020,time,0x0000,sizeof(E21020)
		,(char*)&E21020);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcSale.21020 returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcSale.21020: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcSale.21020: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("Info: ProcSale.21020: successful.\n");
	elements++;
	if (header.voided) {  //Voided tranzakci�
		ISS_T_REPLY ret2;
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.4000: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.4000: AbortTran successful.\n");
				return ISS_SUCCESS;
	}
/***********************   3023     ******************/
	//dept perf records
	E3023.CountSales=items;
	E3023.ItemKey;
	for (i=1;i<10001;i++) {
		if (dept_id_sum[i]!=0) {
			E3023.DeptNo=i;
			LongVal2Cobol6(E3023.ValueSales,dept_id_sum[i]*100);	
			retval=SafeSendHLogElement(posn,&handle,3023,time,0x0000,sizeof(E3023)
				,(char*)&E3023);
			if (retval!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				L.LogE("ERROR: ProcSale.3023 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.3023: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.3023: AbortTran successful.\n");
				return(retval);
			}
			else L.Log("Info: ProcSale.3023: successful.\n");
			elements++;
		}
	}
	LongVal2Cobol6(E3023.ValueSales,total*100);

	for(extprice=1;extprice<8;extprice++) {
		if (ItemDept[extprice]!=0) {
			E3023.DeptNo=ItemDept[extprice];
			retval=SafeSendHLogElement(posn,&handle,3023,time,0x0000,sizeof(E3023)
				,(char*)&E3023);
			if (retval!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				L.LogE("ERROR: ProcSale.3023 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.3023: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.3023: AbortTran successful.\n");
				return(retval);
			}
			else L.Log("Info: ProcSale.3023: successful.\n");
			elements++;
		}
	}
	//close tran
	element.element_time=time+trantime;
	element.element_num=elements;
	retval=iss46_hlog_end_trans(handle,Date,(unsigned short)elements);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcSale.EndTran returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcSale.EndTran: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcSale.EndTran: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("Info: ProcSale.EndTran: successful.\n");
	APos[posn].lasttrandate=Date;
	APos[posn].lasttrantime=time+trantime;

	GItemTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegGItemTotals);
	SaveStatus(false,posn);
	memcpy(itemcollector,&posn,sizeof(long));
	GItemTotals.InitCursor();
	int index;
	for (index=0;index<GItemTotals.GetCount();index++) {
		GItemTotals.GetDataByIndex(index,itemcollector);
		gdec2s((unsigned char *)(itemcollector+sizeof(long)),6,s);
		L.Log("Sales for item %s on till %d! Quantity=%d Value=%d.\n"
			,s,*((long *)(itemcollector)),*((long *)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)))
			,*((long *)(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6))));
	}

	r.SetValueDW("Fuel.System.Close",0); //Engedem a benzink�t f�jlok feldolgoz�s�t	
	open=false;
	return ISS_SUCCESS;
}

/****************************************************************************************************************************************/

/**********************************************   NEW ACIS   ***********************************************************************/
long NewACIS_ProcSale(char* F) //Return value: 0=no_retries others=retries
{
	Reg r(false, C_RegKey);
	THeader header;
	TElem5100	E5100;
	TElem21001	E21001[100];
	float ext_price[100];
	float itemweight[100];
	TElem2000	E2000[20];
	long	tendval[20];
	TElem2060	E2060;
	TElem21099V2	E21099[50];
	TElem21020	E21020;
	TElem3023	E3023;
	TElem21499	E21499[50];
	TElem21499	E2000_21499[50];
	PreFill(&E5100,	5100);
	PreFill(&E2060,	2060);
	PreFill(&E21020,	21020);
	PreFill(&E3023,	3023);
	char OrigEAN[14][100];
	int c_21099=0;
	int c_21499=0;
	int c_2000=0;
	int c_21001=0;
	int c_21002=0,c_21002_21499=0;
	int i,sale_line=0,chg_rate=1;
	long dept_id_sum[10001];
	SINT16 hp;
	ISS_T_LOG_BLOCK_HANDLE  handle;
	SYSTEMTIME st,et;
	char *ansiText;
	wchar_t *wText;
	ISS_T_REPLY retval=0;
	L.Log("Info: NewACIS_ProcSale.get_current_hp: %d\n",hp);
	if ((retval=iss46_hlog_get_current_hp(&hp))!=ISS_SUCCESS) {
		L.LogE("ERROR: NewACIS_ProcSale.get_current_hp: %d, retval=%d\n",hp,retval);
		L.LogE("==> ProcSale will be aborted or retried!\n");
		return retval;
	}
	
	bool open=false; //transaction opened
	long tenders=0;	//payment records logged already (to reject sales too)
	short round_amnt=0; //rounding amount
	long sold;		//total sales
	long total=0;		//current balance
	long rounded;	//payment value rounded (if cash)
	long totrounddiff=0; //Total rounding difference collected by all payment lines
	long posn;		//pos number
	long trantime;	//transaction total time
//	long availtime; //available time - time left from total transaction time
	long elements;	//counting logged elements
	long items=0;		//count of item records
	long weight;
	long quantity;
//	long prevquantity;
	ISS_GDECIMAL_4 unitprice;
	long extprice;
	bool ean2x[100];	//Will be true for EAN21-29
	char s[30];
//	unsigned char custno[12];
	bool gotcust;
	char transuser[9]={0,0,0,0,0,0,0,0,0};
	ISS_T_LOG_ELEM_ID element;
//	ISS_T_INT_DATE date;
	ISS_T_INT_TIME time;
	ISS_GDECIMAL_6 ean, eano[100];
//	ISS_GDECIMAL_6 eannosubst;
	char logon[20]; //Buser--date--=time=p# from Hp#user--date--=time=...
	char tranuser[5];
	char progress=0;
	char seps[]   = ":";
//	int counter;
//	char atoken[200];
//	char *token;
	long amnt=1, quant=1;
	long qq,vv;
	char ss[21],endDate[12],endTime[10];
	char itemcollector[sizeof(ISS_GDECIMAL_6)+3*sizeof(long)];
	ISS_GDECIMAL_6 ean1;
	header.voided=0;
	long cash=0; 
	float ecquantity=0;
	for (i=1;i<10001;i++)
		dept_id_sum[i]=0;

	TiXmlNode* node = 0;
	TiXmlNode* root = 0;
	TiXmlElement* todoElement= 0;
	TiXmlElement* XMLelement= 0;
//	TiXmlElement* CardElement;
	TiXmlHandle hRoot(0);
	TiXmlDocument doc( F );
	
	if ( !doc.LoadFile() )	{
		L.LogE("ERROR: NewACIS_ProcSale.PreProc: Could not load %s file.!  Error='%s'. Exiting.\n",F, doc.ErrorDesc());
		return 2;
	}
	sprintf(header.custnr,"%s","0");
	sprintf(header.zip,"%","000000");
	header.custinqueue=0;
	header.invoice=false;
	sprintf(header.Lautoken,"%s","0");
	sprintf(header.AP,"%s","000000000");
	sprintf(header.CloseNR,"%","1");
	sprintf(header.ReceiptNR,"%","1");
	header.transID=1; header.chg_amnt=0;
	if (ACIS_BBOX)
		root = doc.FirstChild( "NAMXML-POSJournal" );
	else
		root = doc.FirstChild( "NAXML-POSJournal" );
	node = root->FirstChildElement( "TransmissionHeader" );
	TiXmlNode* JournalReport = root->FirstChildElement( "JournalReport" );
	TiXmlElement* Journal = JournalReport->FirstChildElement( "SaleEvent" );
	printf("TRANSACTION HEADER\n");
	//Transaction Date
	XMLelement = Journal->FirstChildElement( "ReceiptDate" );
	//XMLelement = Journal->ToElement();
	printf("BeginDate %s \n",XMLelement->GetText());
	strcpy(header.BDate,XMLelement->GetText());
	//Transaction START time
	XMLelement = Journal->FirstChildElement( "ReceiptTime" );
	printf("BeginTime %s \n",XMLelement->GetText());
	strcpy(header.BTime,XMLelement->GetText());
	//Transaction END Date
	XMLelement = Journal->FirstChildElement( "EventEndDate" );
	//XMLelement = node->ToElement();
	printf("EndDate %s \n",XMLelement->GetText());
	strcpy(header.EDate,XMLelement->GetText());
	strcpy(endDate,XMLelement->GetText());
	
	//Transaction END time
	XMLelement = Journal->FirstChildElement( "EventEndTime" );
	//XMLelement = node->ToElement();
	printf("EndTime %s \n",XMLelement->GetText());
	strcpy(header.ETime,XMLelement->GetText());
	strcpy(endTime,XMLelement->GetText());	
	sprintf(s,"%s%s",header.BDate,header.BTime);
	SysTimeFromS18(&st,s);
	sprintf(s,"%s%s",header.EDate,header.ETime);
	SysTimeFromS18(&et,s);
	XMLelement = Journal->FirstChildElement( "TransactionFiscalID" );
	if(XMLelement) {

		printf("AP %s \n",XMLelement->GetText());
		if (AcisWOEJ) {
			char ap[21];//,ss[1];
			strcpy(ap,XMLelement->GetText());
			strncpy(header.AP,ap,8);
			for (int j=9;j<strlen(ap);j++)
				if(ap[j]=='/') {
					strncpy(header.CloseNR,ap+9,j-9);
					header.CloseNR[j-9]=0;
					strncpy(header.ReceiptNR,ap+j+1,strlen(ap)-j-1);
					header.ReceiptNR[strlen(ap)-j-1]=0;
					j=strlen(ap);
				}
			header.AP[8]=0;
		}else {
			char ap[21];//,ss[1];
			strcpy(ap,XMLelement->GetText());
			strncpy(header.AP,ap,9);
			strncpy(header.CloseNR,ap+10,4);
			header.CloseNR[4]=0;
			if (ACIS_BBOX==1){
				strncpy(header.ReceiptNR,ap+15,6);
				header.ReceiptNR[6]=0;
			}
			else {
				strncpy(header.ReceiptNR,ap+15,5);
				header.ReceiptNR[5]=0;
			}
		}
	}
	header.invoice=false;
	//Print Invoice
	XMLelement = Journal->FirstChildElement( "InvoiceFlag" );
	if(XMLelement) {
		if(_stricmp(XMLelement->Attribute( "value"),"yes")==0)
			header.invoice=true;
	}
	//TransactionID
	XMLelement = Journal->FirstChildElement( "TransactionID" );
	if(XMLelement) {
		printf("TransactionID %s \n",XMLelement->GetText());
		header.transID=atol(XMLelement->GetText());
	}
	//Fizet�s
	XMLelement = Journal->FirstChildElement( "PaymentId" );
	if(XMLelement) {
		printf("PaymentID %s \n",XMLelement->GetText());
		c_2000++;
		PreFill(&E2000[c_2000],	2000);
		E2000[c_2000].TenderID=4;
		if (atoi(XMLelement->GetText())==1)
			E2000[c_2000].TenderID=1;
		if (atoi(XMLelement->GetText())==120)
			E2000[c_2000].TenderID=90;
		if (atoi(XMLelement->GetText())==280)
			E2000[c_2000].TenderID=90;
	}
	int colID=0;
	TiXmlElement* FuelLine = Journal->FirstChildElement( "TransactionDetailGroup" )->FirstChildElement( "TransactionLine" )->FirstChildElement( "FuelLine" );
	XMLelement = FuelLine->FirstChildElement( "FuelPumpID" );
	if(XMLelement) {
		printf("CashierID %s \n",XMLelement->GetText());
		colID=atoi(XMLelement->GetText());
		L.Log("INFO - NewACIS_Process The CASHIER number is : %d \n",colID+4-FirstACISPumpID);
		header.cashier=colID+4-FirstACISPumpID;
		posn=header.cashier-1;
		header.sid=header.cashier;
	}
	if(NewACISDateCor) {
			GetLocalTime(&st);
			Date=DateYMD2Int(st.wYear,st.wMonth,st.wDay);
			time=st.wHour*3600+st.wMinute*60+st.wSecond;
			sprintf(header.BDate,"%04d-%02d-%02d",st.wYear,st.wMonth,st.wDay);
			sprintf(header.BTime,"%02d:%02d:%02d",st.wHour,st.wMinute,st.wSecond);
			sprintf(header.EDate,"%04d-%02d-%02d",st.wYear,st.wMonth,st.wDay);
			sprintf(header.ETime,"%02d:%02d:%02d",st.wHour,st.wMinute,st.wSecond);
			if(posn > 0){
				CheckTranDateTime(&Date,&time,posn);
			}
	}

	XMLelement = FuelLine->FirstChildElement( "FuelGradeID" );
	if(XMLelement) {
		printf("FuelGradeID %s \n",XMLelement->GetText());
		if (XMLelement->GetText()==NULL){
						header.voided=true;
			L.LogE("ERROR - NewAcis_procsale: Empty EAN code in the file! %s\n",F); 
			if (E2000[c_2000].TenderID==1) {
				TiXmlElement* Authorization	= Journal->FirstChildElement( "TransactionDetailGroup" )->FirstChildElement( "TransactionLine" )->FirstChildElement( "TenderInfo" )->FirstChildElement( "Authorization" );
				if (Authorization) {
					XMLelement = Authorization->FirstChildElement( "AuthorizedChargeAmount" );
					if(XMLelement) {
						printf("AuthorizedChargeAmount %s \n",XMLelement->GetText());
						if (XMLelement->GetText())
							cash=atoi(XMLelement->GetText());
					}

				}
				/********************* Write CASH to database  *****************************/
				L.Log("The cash value:%d",cash);
				if (cash!=0) {
					char stmp[1500];
					char sep[]   = "|\n";
					sprintf(stmp,"SELECT id FROM GL_interface.Accounts Where auto_insert=89");
					char *rt =Select_MYSQL(stmp);
					strtok( rt, sep );
					long acct_id=atol(strtok( NULL, sep ));
					L.Log("The id is: %d\n",acct_id);
					sprintf(stmp,"SELECT id,amount FROM GL_interface.Tills Where acct_id=%d and day=curdate()",acct_id);
					L.Log("%s\n",stmp);
					rt =Select_MYSQL(stmp);
					long till_id=-1;
					strtok( rt, sep );
					if(rt!="-1")
						till_id=atol(strtok( NULL, sep ));
					if(till_id>=0) 
						sprintf(stmp,"UPDATE `GL_interface`.`Tills` SET `amount`='%d' WHERE `id`='%d'",(atol(strtok( NULL, sep ))+cash),till_id);
					else
						sprintf(stmp,"INSERT INTO `GL_interface`.`Tills` (`day`, `acct_id`, `amount`) VALUES (curdate(), '%d', '%d')",acct_id,(cash));
					long ret_ins =Insert_MYSQL(stmp);
					if(!ret_ins)
						L.LogE("ERROR - Cannot write csah (%d) to Tills table!\n",cash);
					else
						L.LogE("Succesfully write cash (%d) to Tills table!\n",cash);
				}
			}
			return false;
		}
		if(strlen(AStruct[atoi(XMLelement->GetText())].ean)<10){
			header.voided=true;
			L.LogE("ERROR - NewAcis_procsale: Wrong EAN code in the file! %s\n",F); 
			if (E2000[c_2000].TenderID==1) {
				TiXmlElement* Authorization	= Journal->FirstChildElement( "TransactionDetailGroup" )->FirstChildElement( "TransactionLine" )->FirstChildElement( "TenderInfo" )->FirstChildElement( "Authorization" );
				if (Authorization) {
					XMLelement = Authorization->FirstChildElement( "AuthorizedChargeAmount" );
					if(XMLelement) {
						printf("AuthorizedChargeAmount %s \n",XMLelement->GetText());
						if (XMLelement->GetText())
							cash=atoi(XMLelement->GetText());
					}

				}
				/********************* Write CASH to database  *****************************/
				L.Log("The cash value:%d",cash);
				if (cash!=0) {
					char stmp[1500];
					char sep[]   = "|\n";
					sprintf(stmp,"SELECT id FROM GL_interface.Accounts Where auto_insert=89");
					char *rt =Select_MYSQL(stmp);
					strtok( rt, sep );
					long acct_id=atol(strtok( NULL, sep ));
					L.Log("The id is: %d\n",acct_id);
					sprintf(stmp,"SELECT id,amount FROM GL_interface.Tills Where acct_id=%d and day=curdate()",acct_id);
					L.Log("%s\n",stmp);
					rt =Select_MYSQL(stmp);
					long till_id=-1;
					strtok( rt, sep );
					if(rt!="-1")
						till_id=atol(strtok( NULL, sep ));
					if(till_id>=0) 
						sprintf(stmp,"UPDATE `GL_interface`.`Tills` SET `amount`='%d' WHERE `id`='%d'",(atol(strtok( NULL, sep ))+cash),till_id);
					else
						sprintf(stmp,"INSERT INTO `GL_interface`.`Tills` (`day`, `acct_id`, `amount`) VALUES (curdate(), '%d', '%d')",acct_id,(cash));
					long ret_ins =Insert_MYSQL(stmp);
					if(!ret_ins)
						L.LogE("ERROR - Cannot write csah (%d) to Tills table!\n",cash);
					else
						L.LogE("Succesfully write cash (%d) to Tills table!\n",cash);
				}
			}

			return false;
		}
		sprintf(s,"%s",AStruct[atoi(XMLelement->GetText())].ean);
	}
	c_21001++;
	PreFill(&E21001[c_21001],	21001);
	gdec_init_z(ean,6,0,iss_gdec_round_normal);
	gdec_init_z(eano[c_21001],6,0,iss_gdec_round_normal);
	sprintf(OrigEAN[c_21001],"%s",s);
	DoItemSubst(s,false);
	//L.Log("The original EAN %s and the new : %s\n",XMLelement->GetText(),s);
	
	gdec_from_ascii(ean,s);
	gdec_from_ascii(eano[c_21001],s);
	if (GetItemDataDept(unitprice,&E21001[c_21001].TaxID,&E21001[c_21001].ReportGroup,&E21001[c_21001].EANType,&E21001[c_21001].DeptId,ean)==ISS_E_SDBMS_NOREC) {
		L.LogE("ERROR: NewACIS.Item: No record in ISS400 for EAN %s! Needed values will be guessed.\n",s);
		E21001[c_21001].ReportGroup=1;
		E21001[c_21001].TaxID=2;
		E21001[c_21001].EANType=5;
		E21001[c_21001].DeptId=3075;
	}
	ean2x[c_21001]=((s[0]=='2')&&(s[1]>'0')&&(s[1]<='9'));
	GDec62Cobol6(E21001[c_21001].ItemCode,ean);
	E21001[c_21001].InputDevice=12;
	E21001[c_21001].ReturnMode=FALSE;
	E21001[c_21001].NegativeItem=FALSE;
	E21001[c_21001].SplitQuantity=1;

	/********************* Log ON/OFF   ***********************/
	if (!(*(APos[posn].user))) {//noone is currently logged on
			sprintf(logon,"%.4d%s%s%.2d",header.cashier,remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid);
			if((retval=ProcLogOn(logon))!=0) {
				L.LogE("ERROR: ProcSale.AutoLogOn: ProcLogOn returned %d!\n",retval);
				return 1;//no retries
			}
	}
	else { // Ha m�s van bel�pve, akkor ki kell l�ptetni, majd bel�ptetni
		sprintf(tranuser,"%.4d",header.cashier);
		//tranuser[5]=0;
		CleanUserID(tranuser);
		if ((strcmp(APos[posn].user,tranuser))!=0) {
			char u[5];
			memcpy(u,APos[posn].user,4);
			u[4]=0;
			tranuser[4]=0;
			sprintf(logon,"%.4d%s%s%.2d",atoi(APos[posn].user),remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid);
			logon[20]=0;
			if ((retval=ProcLogOff(logon))!=0) {
				L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOff returned %d!\n",retval);
				return retval;//retry allowed
			}
			if ((retval=ProcLogOn(logon))!=0) {
				L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOn returned %d!\n",retval);
				return retval;//retry allowed
			}
		}
	}
	XMLelement = FuelLine->FirstChildElement( "SalesQuantity" );
	if(XMLelement){
		printf("SalesQuantity %s \n",XMLelement->GetText());
		if (atof(XMLelement->GetText())==0){
			L.LogE("ERROR - NewACIS_Procsale - This is a zero transaction! %s\n",F); 
			header.voided=true;
			if (E2000[c_2000].TenderID==1) {
				TiXmlElement* Authorization	= Journal->FirstChildElement( "TransactionDetailGroup" )->FirstChildElement( "TransactionLine" )->FirstChildElement( "TenderInfo" )->FirstChildElement( "Authorization" );
				if (Authorization) {
					XMLelement = Authorization->FirstChildElement( "AuthorizedChargeAmount" );
					if(XMLelement) {
						printf("AuthorizedChargeAmount %s \n",XMLelement->GetText());
						if (XMLelement->GetText())
							cash=atoi(XMLelement->GetText());
					}

				}
				/********************* Write CASH to database  *****************************/
				L.Log("The cash value:%d",cash);
				if (cash!=0) {
					char stmp[1500];
					char sep[]   = "|\n";
					sprintf(stmp,"SELECT id FROM GL_interface.Accounts Where auto_insert=89");
					char *rt =Select_MYSQL(stmp);
					strtok( rt, sep );
					long acct_id=atol(strtok( NULL, sep ));
					L.Log("The id is: %d\n",acct_id);
					sprintf(stmp,"SELECT id,amount FROM GL_interface.Tills Where acct_id=%d and day=curdate()",acct_id);
					L.Log("%s\n",stmp);
					rt =Select_MYSQL(stmp);
					long till_id=-1;
					strtok( rt, sep );
					if(rt!="-1")
						till_id=atol(strtok( NULL, sep ));
					if(till_id>=0) 
						sprintf(stmp,"UPDATE `GL_interface`.`Tills` SET `amount`='%d' WHERE `id`='%d'",(atol(strtok( NULL, sep ))+cash),till_id);
					else
						sprintf(stmp,"INSERT INTO `GL_interface`.`Tills` (`day`, `acct_id`, `amount`) VALUES (curdate(), '%d', '%d')",acct_id,(cash));
					long ret_ins =Insert_MYSQL(stmp);
					if(!ret_ins)
						L.LogE("ERROR - Cannot write csah (%d) to Tills table!\n",cash);
					else
						L.LogE("Succesfully write cash (%d) to Tills table!\n",cash);
				}
			}
			return false;
		}
		E21001[c_21001].Quantity=1;
		
		ecquantity=atof(XMLelement->GetText());
		weight=1000*ecquantity;
		itemweight[c_21001]=weight;
		LongVal2Cobol4(E21001[c_21001].Weight,weight);
	}
	XMLelement = FuelLine->FirstChildElement( "SalesAmount" );
	if(XMLelement) {
		printf("SalesAmount %s \n",XMLelement->GetText());
		ext_price[c_21001]=atof(XMLelement->GetText());
		LongVal2Cobol6(E21001[c_21001].ExtPrice,100*ext_price[c_21001]);//Extprice ok
		dept_id_sum[E21001[c_21001].DeptId]+=atol(XMLelement->GetText());
		total=total+atol(XMLelement->GetText());
		amnt=100*atol(XMLelement->GetText());
	}
	int nozzleID=0;
	XMLelement = FuelLine->FirstChildElement( "FuelNozzleID" );
	if(XMLelement) {
		printf("FuelNozzleID %s \n",XMLelement->GetText());
		if (XMLelement->GetText()==NULL){
			L.LogE("ERROR - NewACIS_Procsale The FuelNozzleID is empty! %s \n",F);
			header.voided=true;
			if (E2000[c_2000].TenderID==1) {
				TiXmlElement* Authorization	= Journal->FirstChildElement( "TransactionDetailGroup" )->FirstChildElement( "TransactionLine" )->FirstChildElement( "TenderInfo" )->FirstChildElement( "Authorization" );
				if (Authorization) {
					XMLelement = Authorization->FirstChildElement( "AuthorizedChargeAmount" );
					if(XMLelement) {
						printf("AuthorizedChargeAmount %s \n",XMLelement->GetText());
						if (XMLelement->GetText())
							cash=atoi(XMLelement->GetText());
					}
				}
				/********************* Write CASH to database  *****************************/
				L.Log("The cash value:%d",cash);
				if (cash!=0) {
					char stmp[1500];
					char sep[]   = "|\n";
					sprintf(stmp,"SELECT id FROM GL_interface.Accounts Where auto_insert=89");
					char *rt =Select_MYSQL(stmp);
					strtok( rt, sep );
					long acct_id=atol(strtok( NULL, sep ));
					L.Log("The id is: %d\n",acct_id);
					sprintf(stmp,"SELECT id,amount FROM GL_interface.Tills Where acct_id=%d and day=curdate()",acct_id);
					L.Log("%s\n",stmp);
					rt =Select_MYSQL(stmp);
					long till_id=-1;
					strtok( rt, sep );
					if(rt!="-1")
						till_id=atol(strtok( NULL, sep ));
					if(till_id>=0) 
						sprintf(stmp,"UPDATE `GL_interface`.`Tills` SET `amount`='%d' WHERE `id`='%d'",(atol(strtok( NULL, sep ))+cash),till_id);
					else
						sprintf(stmp,"INSERT INTO `GL_interface`.`Tills` (`day`, `acct_id`, `amount`) VALUES (curdate(), '%d', '%d')",acct_id,(cash));
					long ret_ins =Insert_MYSQL(stmp);
					if(!ret_ins)
						L.LogE("ERROR - Cannot write csah (%d) to Tills table!\n",cash);
					else
						L.LogE("Succesfully write cash (%d) to Tills table!\n",cash);
				}
			}
			return false;
		}
		if (atoi(XMLelement->GetText())==0){
			L.LogE("WARRNING - NewACIS_Procsale The FuelNozzleID is zero! %s \n",F);
		}
		nozzleID=atoi(XMLelement->GetText());
		
	}
	XMLelement = FuelLine->FirstChildElement( "PumpMeterQuantity" );
	if(XMLelement) {
		printf("PumpMeterQuantity %s \n",XMLelement->GetText());
		if (XMLelement->GetText()==NULL){
			L.LogE("ERROR - NewACIS_Procsale The pumpmeter is empty! %s \n",F);
			header.voided=true;
			if (E2000[c_2000].TenderID==1) {
				TiXmlElement* Authorization	= Journal->FirstChildElement( "TransactionDetailGroup" )->FirstChildElement( "TransactionLine" )->FirstChildElement( "TenderInfo" )->FirstChildElement( "Authorization" );
				if (Authorization) {
					XMLelement = Authorization->FirstChildElement( "AuthorizedChargeAmount" );
					if(XMLelement) {
						printf("AuthorizedChargeAmount %s \n",XMLelement->GetText());
						if (XMLelement->GetText())
							cash=atoi(XMLelement->GetText());
					}
				}
				/********************* Write CASH to database  *****************************/
				L.Log("The cash value:%d",cash);
				if (cash!=0) {
					char stmp[1500];
					char sep[]   = "|\n";
					sprintf(stmp,"SELECT id FROM GL_interface.Accounts Where auto_insert=89");
					char *rt =Select_MYSQL(stmp);
					strtok( rt, sep );
					long acct_id=atol(strtok( NULL, sep ));
					L.Log("The id is: %d\n",acct_id);
					sprintf(stmp,"SELECT id,amount FROM GL_interface.Tills Where acct_id=%d and day=curdate()",acct_id);
					L.Log("%s\n",stmp);
					rt =Select_MYSQL(stmp);
					long till_id=-1;
					strtok( rt, sep );
					if(rt!="-1")
						till_id=atol(strtok( NULL, sep ));
					if(till_id>=0) 
						sprintf(stmp,"UPDATE `GL_interface`.`Tills` SET `amount`='%d' WHERE `id`='%d'",(atol(strtok( NULL, sep ))+cash),till_id);
					else
						sprintf(stmp,"INSERT INTO `GL_interface`.`Tills` (`day`, `acct_id`, `amount`) VALUES (curdate(), '%d', '%d')",acct_id,(cash));
					long ret_ins =Insert_MYSQL(stmp);
					if(!ret_ins)
						L.LogE("ERROR - Cannot write csah (%d) to Tills table!\n",cash);
					else
						L.LogE("Succesfully write cash (%d) to Tills table!\n",cash);
				}
			}
			return false;
		}
		if (atoi(XMLelement->GetText())==0){
			L.LogE("WARRNING - NewACIS_Procsale The pumpmeter is zero! %s \n",F);
		}
		char tss[20];
		strcpy(tss,XMLelement->GetText());
		L.Log("ACIS col:%d nozzle: %d PumpMeterQuantity:%s \n",colID,nozzleID,tss);
		WriteRegister2Reg(colID,nozzleID,tss);
	}
		tenders+=(amnt/100);
		rounded=((((1000*amnt)/weight)+5)/10)*10; //�r kisz�mol�sa
		LongVal2Cobol6(E21001[c_21001].UnitPrice,rounded);//Unitprice ok
		LongVal2Cobol6(E21001[c_21001].PriceFromPlu,rounded);
		header.chg_amnt=0;	
	
/************************** AUTHORIZATION ********************************/
	long tamount=0,vouch_val=0;
	if (E2000[c_2000].TenderID==4){
		tendval[c_2000]=tenders;
		LongVal2Cobol6(E2000[c_2000].TenderVal,100*tenders);
		E2000[c_2000].DrwIDDate=DateYMD2Int(et.wYear,et.wMonth,et.wDay);
		E2000[c_2000].DrwIDTime=et.wHour*3600+et.wMinute*60+et.wSecond;
		E2000[c_2000].DrwIDLocID=APos[posn].sid/2;
		PreFill(&E2000_21499[2*c_2000-1],	21499);
		PreFill(&E2000_21499[2*c_2000],	21499);
		TiXmlElement* Tinfo = Journal->FirstChildElement( "TransactionDetailGroup" )->FirstChildElement( "TransactionLine" )->FirstChildElement( "TenderInfo" );
		TiXmlElement* Authorization	= Tinfo->FirstChildElement( "Authorization" );
		if (Authorization) {
			//PreFill(&E2000_21499[2*c_2000-1],	21499);
			//PreFill(&E2000_21499[2*c_2000],	21499);
			//PreFill(&E2000[c_2000],	2000);
			memset( E2000_21499[2*c_2000-1].Data, ' ', 180 );
			memset( E2000_21499[2*c_2000].Data, ' ', 36 );

			E2000_21499[2*c_2000].Record_Type=6;
			E2000_21499[2*c_2000-1].Record_Type=6;
			for (int counter=0;counter<5;counter++){
				switch (counter) {
					case 0:E2000_21499[2*c_2000-1].Data[(counter)*36]='D';
					break;
					case 1:E2000_21499[2*c_2000-1].Data[(counter)*36]='E';
					break;
					case 2:E2000_21499[2*c_2000-1].Data[(counter)*36]='K';
					break;
					case 3:E2000_21499[2*c_2000-1].Data[(counter)*36]='N';
					break;
					case 4:E2000_21499[2*c_2000-1].Data[(counter)*36]='O';
					break;
				}
				E2000_21499[2*c_2000-1].Data[(counter)*36+1]=':';
			}
			E2000_21499[2*c_2000].Record_Type=6;
			strcpy(E2000_21499[2*c_2000].Data,"Y:");
			XMLelement = Authorization->FirstChildElement( "ReferenceNumber" );
			//if(XMLelement){
			if((XMLelement) && (XMLelement->GetText()!=NULL) && (strcmp(XMLelement->GetText(),"(null)")!=0)) {
				printf("ReferenceNumber %s \n",XMLelement->GetText());
				if (XMLelement->GetText())
					strcpy(E2000_21499[2*c_2000-1].Data+1*36+2,XMLelement->GetText());
				else 
					strcpy(E2000_21499[2*c_2000-1].Data+1*36+2,"  ");
				E2000_21499[2*c_2000-1].Data[strlen(XMLelement->GetText())+1*36+2]=' ';
			}
			XMLelement = Authorization->FirstChildElement( "AuthorizingTerminalID" );
			//if(XMLelement){
			if((XMLelement) && (XMLelement->GetText()!=NULL) && (strcmp(XMLelement->GetText(),"(null)")!=0)) {
				printf("AuthorizingTerminalID %s \n",XMLelement->GetText());
				if (XMLelement->GetText())
					strcpy(E2000_21499[2*c_2000-1].Data+3*36+2,XMLelement->GetText());
				else
					strcpy(E2000_21499[2*c_2000-1].Data+3*36+2," ");
				int strl=0;
				if(XMLelement->GetText()!=NULL) 
				strl=strlen(XMLelement->GetText());
				E2000_21499[2*c_2000-1].Data[strl+3*36+2]=' ';

			}
			XMLelement = Authorization->FirstChildElement( "AuthorizationDate" );
			//if(XMLelement){
			if((XMLelement) && (XMLelement->GetText()!=NULL) && (strcmp(XMLelement->GetText(),"(null)")!=0)) {
				printf("AuthorizationDate %s \n",XMLelement->GetText());
				if (XMLelement->GetText()) {
					strncpy(E2000_21499[2*c_2000-1].Data+4*36+2,XMLelement->GetText(),4);
					strncpy(E2000_21499[2*c_2000-1].Data+4*36+6,XMLelement->GetText()+5,2);
					strncpy(E2000_21499[2*c_2000-1].Data+4*36+8,XMLelement->GetText()+8,2);
				}
				else
					strcpy(E2000_21499[2*c_2000-1].Data+4*36+2,"        ");

			}
			XMLelement = Authorization->FirstChildElement( "AuthorizationTime" );
			//if(XMLelement){
			if((XMLelement) && (XMLelement->GetText()!=NULL) && (strcmp(XMLelement->GetText(),"(null)")!=0)) {
				printf("AuthorizationTime %s \n",XMLelement->GetText());
				if (XMLelement->GetText()) {
					strncpy(E2000_21499[2*c_2000-1].Data+4*36+10,XMLelement->GetText(),2);
					strncpy(E2000_21499[2*c_2000-1].Data+4*36+12,XMLelement->GetText()+3,2);
					strncpy(E2000_21499[2*c_2000-1].Data+4*36+14,XMLelement->GetText()+6,2);
				}
				else
					strcpy(E2000_21499[2*c_2000-1].Data+4*36+10,"      ");
			}

		}
	}
	//***************  �RESV�LT�S   **************************//
	if (E2000[c_2000].TenderID==90){
		tendval[c_2000]=tenders;
		LongVal2Cobol6(E2000[c_2000].TenderVal,100*tenders);
		E2000[c_2000].DrwIDDate=DateYMD2Int(et.wYear,et.wMonth,et.wDay);
		E2000[c_2000].DrwIDTime=et.wHour*3600+et.wMinute*60+et.wSecond;
		E2000[c_2000].DrwIDLocID=APos[posn].sid/2;
		PreFill(&E2000_21499[2*c_2000-1],	21499);
		PreFill(&E2000_21499[2*c_2000],	21499);
		E2000_21499[2*c_2000].Record_Type=-1;
		E2000_21499[2*c_2000-1].Record_Type=-1;
		if(disableEmptyChange){
					WriteEmptyChange2DB(endDate,endTime,header.AP,header.CloseNR,header.ReceiptNR,header.cashier,ecquantity,total,nozzleID,header.cashier-3);
					return ISS_SUCCESS;
		}
	}
	//**************** K�szp�nz vagy utalv�ny ******************//
	if (E2000[c_2000].TenderID==1){
		E2000_21499[2*c_2000].Record_Type=-1;
		E2000_21499[2*c_2000-1].Record_Type=-1;
		TiXmlElement* Tinfo = Journal->FirstChildElement( "TransactionDetailGroup" )->FirstChildElement( "TransactionLine" )->FirstChildElement( "TenderInfo" );
		TiXmlElement* TenderAmount	= Tinfo->FirstChildElement( "TenderAmount" );
		if (TenderAmount) {
			tamount=atol(TenderAmount->GetText());
		}
		TiXmlElement* RefAmount	= Tinfo->FirstChildElement( "RefundCodeAmountConsumed" );
		if (RefAmount) {
			vouch_val=atol(RefAmount->GetText());
		}
		
		TiXmlElement* Authorization	= Tinfo->FirstChildElement( "Authorization" );
		if (Authorization) {
			XMLelement = Authorization->FirstChildElement( "AuthorizedChargeAmount" );
			//if(XMLelement) {
			if((XMLelement) && (XMLelement->GetText()!=NULL) && (strcmp(XMLelement->GetText(),"(null)")!=0)) {
				printf("AuthorizedChargeAmount %s \n",XMLelement->GetText());
				if (XMLelement->GetText())
					cash=atoi(XMLelement->GetText());
			}

		}
		//if(!ACIS_BBOX){
			if (cash>=tamount){
				//tenders=cash;
				tendval[c_2000]=tenders;
				LongVal2Cobol6(E2000[c_2000].TenderVal,100*tenders);
				E2000[c_2000].DrwIDDate=DateYMD2Int(et.wYear,et.wMonth,et.wDay);
				E2000[c_2000].DrwIDTime=et.wHour*3600+et.wMinute*60+et.wSecond;
				E2000[c_2000].DrwIDLocID=APos[posn].sid/2;
			}
			if (cash==0) {
				E2000[c_2000].TenderID=5;
				tenders=tamount;
				tendval[c_2000]=tenders;
				LongVal2Cobol6(E2000[c_2000].TenderVal,100*tenders);
				E2000[c_2000].DrwIDDate=DateYMD2Int(et.wYear,et.wMonth,et.wDay);
				E2000[c_2000].DrwIDTime=et.wHour*3600+et.wMinute*60+et.wSecond;
				E2000[c_2000].DrwIDLocID=APos[posn].sid/2;
			}
			if (cash>0 && tamount>cash) {
				PreFill(&E2000[c_2000],	2000);
				E2000[c_2000].TenderID=1;
				//tenders+=(tamount-cash);
				tendval[c_2000]=cash;
				LongVal2Cobol6(E2000[c_2000].TenderVal,100*(cash));
				E2000[c_2000].DrwIDDate=DateYMD2Int(et.wYear,et.wMonth,et.wDay);
				E2000[c_2000].DrwIDTime=et.wHour*3600+et.wMinute*60+et.wSecond;
				E2000[c_2000].DrwIDLocID=APos[posn].sid/2;
				c_2000++;
				PreFill(&E2000[c_2000],	2000);
				E2000[c_2000].TenderID=5;
				//tenders+=(tamount-cash);
				tendval[c_2000]=tamount-cash;
				LongVal2Cobol6(E2000[c_2000].TenderVal,100*(tamount-cash));
				E2000[c_2000].DrwIDDate=DateYMD2Int(et.wYear,et.wMonth,et.wDay);
				E2000[c_2000].DrwIDTime=et.wHour*3600+et.wMinute*60+et.wSecond;
				E2000[c_2000].DrwIDLocID=APos[posn].sid/2;
			}
			// Ha a tankol�s �sszege 0 akkor sztorn�
			if (tamount==0)
				header.voided=true;
			if (tamount>(cash+vouch_val))
				L.LogE("ERROR - The AMOUNT is bigger than CASH!! Difference:%d\n",tamount-(cash+vouch_val));
/*		}
		else {

			if(vouch_val>0){ // Ha voucherrel is fizettek
				if(cash-vouch_val>0){ // Ha van voucher �s KP is
					PreFill(&E2000[c_2000],	2000);
					E2000[c_2000].TenderID=1;
					//tenders+=(tamount-cash);
					tendval[c_2000]=cash-vouch_val;
					LongVal2Cobol6(E2000[c_2000].TenderVal,100*(cash-vouch_val));
					E2000[c_2000].DrwIDDate=DateYMD2Int(et.wYear,et.wMonth,et.wDay);
					E2000[c_2000].DrwIDTime=et.wHour*3600+et.wMinute*60+et.wSecond;
					E2000[c_2000].DrwIDLocID=APos[posn].sid/2;
					cash=cash-vouch_val;
					c_2000++;
					PreFill(&E2000[c_2000],	2000);
					E2000[c_2000].TenderID=5;
					//tenders+=(tamount-cash);
					tendval[c_2000]=vouch_val;
					LongVal2Cobol6(E2000[c_2000].TenderVal,100*(vouch_val));
					E2000[c_2000].DrwIDDate=DateYMD2Int(et.wYear,et.wMonth,et.wDay);
					E2000[c_2000].DrwIDTime=et.wHour*3600+et.wMinute*60+et.wSecond;
					E2000[c_2000].DrwIDLocID=APos[posn].sid/2;
				}
				else { //csak voucherrel fizettek
					PreFill(&E2000[c_2000],	2000);
					E2000[c_2000].TenderID=5;
					//tenders+=(tamount-cash);
					cash=0;
					tendval[c_2000]=vouch_val;
					LongVal2Cobol6(E2000[c_2000].TenderVal,100*(vouch_val));
					E2000[c_2000].DrwIDDate=DateYMD2Int(et.wYear,et.wMonth,et.wDay);
					E2000[c_2000].DrwIDTime=et.wHour*3600+et.wMinute*60+et.wSecond;
					E2000[c_2000].DrwIDLocID=APos[posn].sid/2;
				}
			}
			else { //Nem volt voucher a fizet�skor
				PreFill(&E2000[c_2000],	2000);
				E2000[c_2000].TenderID=1;
				//tenders+=(tcash);
				tendval[c_2000]=cash;
				LongVal2Cobol6(E2000[c_2000].TenderVal,100*(cash));
				E2000[c_2000].DrwIDDate=DateYMD2Int(et.wYear,et.wMonth,et.wDay);
				E2000[c_2000].DrwIDTime=et.wHour*3600+et.wMinute*60+et.wSecond;
				E2000[c_2000].DrwIDLocID=APos[posn].sid/2;
			
			
			}
		
		
		}
*/
	}
	/****************  CUSTOMER   *********************************/
	TiXmlElement* Customer = Journal->FirstChildElement( "TransactionDetailGroup" )->FirstChildElement( "TransactionLine" )->FirstChildElement( "CustomerID" );
	if (Customer) {
		wchar_t csctmp[80];
		int room;
		sprintf(header.cust_taxnr,"               ");
		XMLelement = Customer->FirstChildElement( "CustomerName" );
		if((XMLelement) && (XMLelement->GetText()!=NULL) && (strcmp(XMLelement->GetText(),"(null)")!=0)) {
			printf("CustomerName %s \n",XMLelement->GetText());
			wText = CodePageToUnicode(65001,XMLelement->GetText());
							ansiText = UnicodeToCodePage(1250,wText);	
							strcpy(header.custname,ansiText);
/*
			room = MultiByteToWideChar(CP_UTF8, 0, XMLelement->GetText(), -1, NULL, 0);
			MultiByteToWideChar(CP_UTF8, 0, XMLelement->GetText(), -1, csctmp, 80);
			wcstombs(header.custname, csctmp, room);
			*/
		}
		XMLelement = Customer->FirstChildElement( "Address" );
		if((XMLelement) && (XMLelement->GetText()!=NULL) && (strcmp(XMLelement->GetText(),"(null)")!=0)) {
			printf("Address %s \n",XMLelement->GetText());
			wText = CodePageToUnicode(65001,XMLelement->GetText());
							ansiText = UnicodeToCodePage(1250,wText);	
							strcpy(header.custaddr,ansiText);
			/*room = MultiByteToWideChar(CP_UTF8, 0, XMLelement->GetText(), -1, NULL, 0);
			MultiByteToWideChar(CP_UTF8, 0, XMLelement->GetText(), -1, csctmp, 80);
			wcstombs(header.custaddr, csctmp, room);
			*/
		}
		XMLelement = Customer->FirstChildElement( "City" );
		if((XMLelement) && (XMLelement->GetText()!=NULL) && (strcmp(XMLelement->GetText(),"(null)")!=0)) {
			printf("City %s \n",XMLelement->GetText());
			wText = CodePageToUnicode(65001,XMLelement->GetText());
							ansiText = UnicodeToCodePage(1250,wText);	
							strcpy(header.custcity,ansiText);
			/*room = MultiByteToWideChar(CP_UTF8, 0, XMLelement->GetText(), -1, NULL, 0);
			MultiByteToWideChar(CP_UTF8, 0, XMLelement->GetText(), -1, csctmp, 80);
			wcstombs(header.custcity, csctmp, room);
			*/
		}
		XMLelement = Customer->FirstChildElement( "ZIPCode" );
		if((XMLelement) && (XMLelement->GetText()!=NULL) && (strcmp(XMLelement->GetText(),"(null)")!=0)) {
			printf("ZIPCode %s \n",XMLelement->GetText());
			sprintf(header.custzip,"%s",XMLelement->GetText());
		}
		if(ACIS_BBOX){
			XMLelement = Customer->FirstChildElement( "PersonalID" );
			if(XMLelement) {
				L.Log("CustID %s \n",XMLelement->GetText());
				sprintf(header.custnr,"%s",XMLelement->GetText());
			}
		}
	}
			

	time_t t1,t2;
	t1=TimeFromSystemTime(&st);
	t2=TimeFromSystemTime(&et);
	trantime=100;  //Fizet�s kezdete �s v�ge k�z�tt eltelt id�
	round_amnt=total-(tenders-header.chg_amnt);
	
	if (abs(round_amnt)>2) {//Fizet�eszk�z�k �rt�ke nem egyezik meg a cikkek �sszeg�vel
		if(!header.voided){
			L.LogE("ERROR - The total: %d and the tender total: %d doesn't match!\n",total,tenders-header.chg_amnt);
			L.LogE("	==>Void this transaction!! File name: %s\n",F);
			if (E2000[c_2000].TenderID==1) {
				TiXmlElement* Authorization	= Journal->FirstChildElement( "TransactionDetailGroup" )->FirstChildElement( "TransactionLine" )->FirstChildElement( "TenderInfo" )->FirstChildElement( "Authorization" );
				if (Authorization) {
					XMLelement = Authorization->FirstChildElement( "AuthorizedChargeAmount" );
					if(XMLelement) {
						printf("AuthorizedChargeAmount %s \n",XMLelement->GetText());
						if (XMLelement->GetText())
							cash=atoi(XMLelement->GetText());
					}

				}
				/********************* Write CASH to database  *****************************/
				L.Log("The cash value:%d",cash);
				if (cash>0) L.LogE("Cash:| %d",cash);
				if (cash!=0) {
					char stmp[1500];
					char sep[]   = "|\n";
					sprintf(stmp,"SELECT id FROM GL_interface.Accounts Where auto_insert=89");
					char *rt =Select_MYSQL(stmp);
					strtok( rt, sep );
					long acct_id=atol(strtok( NULL, sep ));
					L.Log("The id is: %d\n",acct_id);
					sprintf(stmp,"SELECT id,amount FROM GL_interface.Tills Where acct_id=%d and day=curdate()",acct_id);
					L.Log("%s\n",stmp);
					rt =Select_MYSQL(stmp);
					long till_id=-1;
					strtok( rt, sep );
					if(rt!="-1")
						till_id=atol(strtok( NULL, sep ));
					if(till_id>=0) 
						sprintf(stmp,"UPDATE `GL_interface`.`Tills` SET `amount`='%d' WHERE `id`='%d'",(atol(strtok( NULL, sep ))+cash),till_id);
					else
						sprintf(stmp,"INSERT INTO `GL_interface`.`Tills` (`day`, `acct_id`, `amount`) VALUES (curdate(), '%d', '%d')",acct_id,(cash));
					long ret_ins =Insert_MYSQL(stmp);
					if(!ret_ins)
						L.LogE("ERROR - Cannot write csah (%d) to Tills table!\n",cash);
					else
						L.LogE("Succesfully write cash (%d) to Tills table!\n",cash);
				}
			}
			return false;
		}
	}
	short y,m,d;
	DateInt2YMD(APos[posn].signondate,&y,&m,&d);
	L.Log("Sign on date: %d.%d.%d\n",y,m,d);
	L.Log("Check the user! Apos.User: %s , F�jl user: %d\n",APos[posn].user,header.cashier);
	if(header.cashier>99) {
		L.LogE("ERROR - Bad user %d!\n",header.cashier);
		return 0;
	}

	if (!(*(APos[posn].user))) {//noone is currently logged on
				if ((posn>2)||((AutoActions&0x00000001)!=0)) {
					if (posn<=2) L.LogE("Warning: ProcSale: Sequence error - noone has been logged on (PoS #%d)!\n",posn+1);
					
					if (posn<=2) L.LogE("==> Now performing autologon by sales transaction header: %s\n",s);
					else L.Log("Performing autologon by sales transaction header (pos #%d): %s\n",posn+1,s);
					sprintf(logon,"%.4d%s%s%.2d",header.cashier,remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid);
					if ((retval=ProcLogOn(logon))!=0) {
						L.LogE("ERROR: ProcSale.AutoLogOn: ProcLogOn returned %d!\n",retval);
						return 0;//no retries
					}
					sprintf(APos[posn].user,"%d",header.cashier);
				}
				else {
					L.LogE("ERROR: ProcSale: Transaction sequence error - Noone has been logged on (PoS #%d)!\n",posn+1);
					return 0;//no retries
				}
	}
	else {
				
				char tranuser[5];
				sprintf(tranuser,"%.4d",header.cashier);
				//tranuser[5]=0;
				CleanUserID(tranuser);
				L.Log("Tranuser: %s APos.User: %s \n",tranuser,APos[posn].user);
				if ((strcmp(APos[posn].user,tranuser))!=0) {
					char u[5];
					memcpy(u,APos[posn].user,4);
					u[4]=0;
					tranuser[4]=0;
					if ((AutoActions&0x00000002)!=0) {
						char logon[20]; //user--date--=time=p# from Hp#user--date--=time=...
						long retval;
//						long len;
						L.LogE("Warning: ProcSale: Not the logged on user does sales! (Selling:%s Logged_on:%d))! (PoS #%d)\n"
							,tranuser,header.cashier,posn+1);
						L.LogE("==> Now performing automatic logoff/logon by sales transaction ID: %d\n",header.transID);
						sprintf(logon,"%.4d%s%s%.2d",header.cashier,remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid);
						logon[20]=0;
						if ((retval=ProcLogOff(logon))!=0) {
							L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOff returned %d!\n",retval);
							return retval;//retry allowed
						}
						if ((retval=ProcLogOn(logon))!=0) {
							L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOn returned %d!\n",retval);
							return retval;//retry allowed
						}
					}
					else {
						L.LogE("ERROR: ProcSale: Not the logged on user does sales! (Selling:%s Logged_on:%s))! (PoS #%d)\n"
							,tranuser,u,posn+1);
						return 0;//no retries
					}
				}
	}
	//Advancing performance period(s) if needed
	char tmp[50];
	int g;
	sprintf(tmp,"%s%s",remove_char(header.BDate,"-"),remove_char(header.BTime,":"));
	tmp[14]=0;
	SysTimeFromS14(&st,tmp);
	Date=DateYMD2Int(st.wYear,st.wMonth,st.wDay);
	c_21099++;
	//Lautoken
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=81;
	E21099[c_21099].TransStore=StoreNum;
	g=21-strlen(header.Lautoken);
	memset(E21099[c_21099].DataEnter,'0',g);
	E21099[c_21099].DataEnter[2]='1';
	strcpy(E21099[c_21099].DataEnter+g,header.Lautoken);
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	c_21099++;
	//Nyugta sz�mla
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=81;
	E21099[c_21099].TransStore=StoreNum;
	memset(E21099[c_21099].DataEnter,' ',21);
	if(!header.invoice) //Nyugta
		strcpy(E21099[c_21099].DataEnter,"006NY");
	else   //Sz�mla
		strcpy(E21099[c_21099].DataEnter,"006SZ");
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	
	c_21099++;
	//AP
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=81;
	E21099[c_21099].TransStore=StoreNum;
	g=21-strlen(header.AP);
	memset(E21099[c_21099].DataEnter,'0',g);
	E21099[c_21099].DataEnter[2]='4';
	strcpy(E21099[c_21099].DataEnter+g,header.AP);
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	//Z�r�ssz�m
	c_21099++;
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=81;
	E21099[c_21099].TransStore=StoreNum;
	g=21-strlen(header.CloseNR);
	memset(E21099[c_21099].DataEnter,'0',g);
	E21099[c_21099].DataEnter[2]='5';
	strcpy(E21099[c_21099].DataEnter+g,header.CloseNR);
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	int cust_cnt=0;
	if (header.invoice) {
		cust_cnt=4;
		gotcust=true;
		c_21099++;
		
		//V�s�rl� azonos�t�
		PreFill(&E21099[c_21099],21099);
		E21099[c_21099].CurrDate=Date;
		E21099[c_21099].TransLoc=APos[posn].sid/2;
		E21099[c_21099].DataType=17;
		E21099[c_21099].TransStore=StoreNum;
		memset(E21099[c_21099].DataEnter,' ',21);
		g=13-strlen(header.custnr);
		memset(E21099[c_21099].DataEnter,'0',g);
		strcpy(E21099[c_21099].DataEnter+g,header.custnr);
		E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
		E21499[c_21499].Data[184]=0;
		c_21499++;
		//V�s�rl�i adatok
		PreFill(&E21499[c_21499],21499);
		E21499[c_21499].Record_Type=1;
		memset(E21499[c_21499].Data,' ',184);
		g=18-strlen(header.custnr);
		memset(E21499[c_21499].Data,'0',g);
		strcpy(E21499[c_21499].Data+g,header.custnr);
		//n�v
		strcpy(E21499[c_21499].Data+18,header.custname);
		E21499[c_21499].Data[18+strlen(header.custname)]=' ';
		//irsz�m
		strcpy(E21499[c_21499].Data+63,header.custzip);
		E21499[c_21499].Data[63+strlen(header.custzip)]=' ';
		//v�ros
		strcpy(E21499[c_21499].Data+68,header.custcity);
		E21499[c_21499].Data[68+strlen(header.custcity)]=' ';
		//c�m
		strcpy(E21499[c_21499].Data+108,header.custaddr);
		E21499[c_21499].Data[108+strlen(header.custaddr)]=' ';
		E21499[c_21499].Data[184]=0;
		c_21499++;
		//V�s�rl�i adatok
		PreFill(&E21499[c_21499],21499);
		E21499[c_21499].Record_Type=2;
		memset(E21499[c_21499].Data,' ',45);
		strcpy(E21499[c_21499].Data,header.cust_taxnr);
		E21499[c_21499].Data[strlen(header.cust_taxnr)]=' ';
		E21499[c_21499].Data[45]='1';

		c_21099++;
		//V�s�rl� azonos�t�
		PreFill(&E21099[c_21099],21099);
		E21099[c_21099].CurrDate=Date;
		E21099[c_21099].TransLoc=APos[posn].sid/2;
		E21099[c_21099].DataType=81;
		E21099[c_21099].TransStore=StoreNum;
		memset(E21099[c_21099].DataEnter,' ',21);
		memset(E21099[c_21099].DataEnter,'0',2);
		E21099[c_21099].DataEnter[2]='2';
		strcpy(E21099[c_21099].DataEnter+3,header.custnr);
		E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	}
	c_21099++;
	//D�tum
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=51;
	E21099[c_21099].TransStore=StoreNum;
	time=st.wHour*3600+st.wMinute*60+st.wSecond;
	//CheckTranDateTime(&Date,&time,posn);
	sprintf(E21099[c_21099].DataEnter,"%04dA%02dA%02d",st.wYear,st.wMonth,st.wDay);	//Fiscal date gets stored
	memset(E21099[c_21099].DataEnter+10,' ',11);
	c_21099++;
	//Napi nyugta sz�ml�l� ISS
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=16;
	E21099[c_21099].TransStore=StoreNum;
	//CheckTranDateTime(&Date,&time,posn);
	memset(E21099[c_21099].DataEnter,' ',21);
	sprintf(E21099[c_21099].DataEnter,"%d",atol(header.ReceiptNR));
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	//Fiscalis bejegyz�s
	c_21099++;
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=92;
	E21099[c_21099].TransStore=StoreNum;
	//CheckTranDateTime(&Date,&time,posn);
	memset(E21099[c_21099].DataEnter,' ',21);
	sprintf(E21099[c_21099].DataEnter,"%s/%.4d/%.5d",header.AP,atoi(header.CloseNR),atol(header.ReceiptNR));
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
/*	//Ir�ny�t�sz�m bejegyz�s
	c_21099++;
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=7;
	E21099[c_21099].TransStore=StoreNum;
	//CheckTranDateTime(&Date,&time,posn);
	memset(E21099[c_21099].DataEnter,' ',7);
	sprintf(E21099[c_21099].DataEnter,"%s",header.zip);
	E21099[c_21099].DataEnter[strlen(header.zip)]=' ';
*/
	while (Check4NewPerfSlot(posn,Date,time)); //Advancing performance slots until it's necessary

	//Checks done before opening a new transaction
	SINT32        transno;
	ISS_T_BOOLEAN inprogress;
	SINT16        element_num;

	retval=iss46_hlog_get_curr_trans_num(APos[posn].sid,&transno,&inprogress,&element_num);
	L.Log("Info: ProcSale.check_before_start.get_curr_trans: tr=%d, open=%d, elem=%d\n"
		,transno,inprogress,element_num);
	if (retval!=ISS_SUCCESS) {
		L.LogE("ERROR: ProcSale.check_before_start.get_curr_trans: Bad return value! (%d)\n",retval);
		return retval;
	}
	if (inprogress) {
		L.LogE("ERROR: ProcSale.check_before_start: Transaction must not be open at this point!\n");
		return(-1);
	}
	APos[posn].lasttran++;//Preparing new transaction's number
	if (APos[posn].lasttran!=transno) {//checks whether ISS thinks the same
		L.LogE("Warning: ProcSale.check_before_start: Transaction numbers doesn't match!\n");
		L.LogE("==> APos[%d].lasttran=%d ISSLOG.transno=%d SID=%d\n"
			,posn,APos[posn].lasttran,transno,APos[posn].sid);
		APos[posn].lasttran=transno; //Correction by ISS
	}

	//Preparing opening a new transaction
	element.element_num=1;
	element.element_time=time;
	memcpy(transuser,APos[posn].user,4);

	if ((retval=iss46_hlog_start_block(APos[posn].sid,hp,APos[posn].lasttran,Date,time
		,0,0x00018000,element,9,transuser,&handle))!=ISS_SUCCESS) {
		L.LogE("ERROR: ProcSale.start returned: %d -> retrying!\n",retval);
		return(retval);
	}
	else L.Log("ProcSale.start: Successful.\n");
	

	elements=1;
	if ((retval=iss46_hlog_send_element(handle,5100,time,0x0000,sizeof(E5100),(char*)&E5100))
		!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcSale.5100 returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcSale.5100: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcSale.5100: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("Info: ProcSale.5100: Successful.\n");
	open=true;
	elements++;
	sold=0;
	APos[posn].p_custcnt++;
//cust_cnt=0;
	/*********************   21099   *******************/
	for(i=1;i<=2+cust_cnt;i++) {
				retval=SafeSendHLogElement(posn,&handle,21099,time,0x0000,sizeof(E21099[i])
					,(char*)&E21099[i]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21099.fiscal_date returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21099.fiscal_date: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21099.fiscal_date: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21099.fiscal_date: successful.\n");
				elements++;
	}
	/*********************   21499   *******************/
	if (cust_cnt>0)
		for(i=1;i<=2;i++) {
					retval=SafeSendHLogElement(posn,&handle,21499,time,0x0000,sizeof(E21499[i])
						,(char*)&E21499[i]);
					if (retval!=ISS_SUCCESS) {
						ISS_T_REPLY ret2;
						L.LogE("ERROR: ProcSale.21499.fiscal_date returned: %d -> abort tran & retry!\n",retval);
						if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
							L.LogE("ERROR: ProcSale.21499.fiscal_date: AbortTran: %d!\n",ret2);
						}
						else L.Log("Info: ProcSale.21499.fiscal_date: AbortTran successful.\n");
						return(retval);
					}
					else L.Log("Info: ProcSale.21499.fiscal_date: successful.\n");
					elements++;
		}
	/*********************   21001   *******************/
	for(i=1;i<=c_21001;i++) {
		sprintf(ss,"0000000%s",OrigEAN[i]);
		ss[21]=0;
		L.Log("EAN code: %s\n",ss);
		gdec_init_z(ean1,6,0,iss_gdec_round_normal);
		gdec_from_ascii(ean1,ss);
		//gdec_init_z(ean,6,0,iss_gdec_round_normal);
		memcpy(itemcollector,&posn,sizeof(long));
		memcpy(itemcollector+sizeof(long),ean1,sizeof(ISS_GDECIMAL_6));
		if (GItemTotals.GetData(itemcollector,NULL,false)) {
			L.Log("The addad Value=%d Quantity=%d\n",ext_price[i],itemweight[i]);
			L.Log("This is the first sale of this ITEM %s at this till %d.\n",OrigEAN[i],header.sid+PosSIDBase/2);
			qq=0;vv=0;
		}
		else {
			qq=*((long*)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)));
			vv=*((long*)(itemcollector+sizeof(long)*2+sizeof(ISS_GDECIMAL_6)));
			L.Log("The addad Value=%d Quantity=%d\n",ext_price[i],itemweight[i]);
			L.Log("Total sales for item %s on this till %d! ValueMy=%d QuantityMy=%d\n"
					,OrigEAN[i],header.sid+PosSIDBase/2,vv,qq);
		}
		extprice=ext_price[i];quantity=itemweight[i];
		memcpy(itemcollector,&posn,sizeof(long));
		memcpy(itemcollector+sizeof(long),ean1,sizeof(ISS_GDECIMAL_6));
		memcpy(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6),&quantity,sizeof(long));
		memcpy(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6),&extprice,sizeof(long));
		GItemTotals.AddFigures(itemcollector);

		retval=SafeSendHLogElement(posn,&handle,21001,time,0x0000,sizeof(E21001[i])
			,(char*)&E21001[i]);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcSale.21001 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.21001: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcSale.21001: AbortTran successful.\n");
			return (ISS_SUCCESS);
		}
		else L.Log("Info: ProcSale.21001: successful.\n");
		element.element_time+=(trantime/c_21001);
		time+=(trantime/c_21001);
		elements++;
		items+=E21001[c_21001].Quantity;

		if (!header.voided) {
			APos[posn].p_regtime+=(trantime/c_21001);
			APos[posn].regtime+=(trantime/c_21001);
			APos[posn].items+=E21001[c_21001].Quantity;
			APos[posn].p_itemcnt+=E21001[c_21001].Quantity;
		}
	}
	if (!header.voided) 
		APos[posn].p_salesval+=total;
	sold=total;

if (!header.voided) {
/********************    2000    ************************/
char truser[9];
//long q1,v1;
	for(i=1;i<=c_2000;i++) {
		//memcpy(E2000[i].DrwIDUserID,transuser,9);
		strcpy(truser,transuser);
		truser[strlen(transuser)]=0;
		strcpy(E2000[i].DrwIDUserID,truser);
		memcpy(E2060.DrwIDUserID,transuser,9);
		E2000[i].DrwIDDate=APos[posn].drwdate;
		E2000[i].DrwIDTime=APos[posn].drwtime;
		E2060.DrwIDDate=APos[posn].drwdate;
		E2060.DrwIDTime=APos[posn].drwtime;

		rounded=total;//Initially not rounded
		if ((CashRounding)&&(E2000[i].TenderID==1)) {//Do rounding
			rounded=((total+CashRounding/2)/CashRounding)*CashRounding;
			totrounddiff+=rounded-total;
		}
		
		//TenderTotals.AddFigures(E2000[i].TenderID,tendval[i],1);
		//AddTendering(posn,E2000.TenderID,extprice);
		AddTendering(posn,(long) E2000[i].TenderID,tendval[i]);
		//L.Log("The tender_id: %s ; Total: %s\n",E2000[i].TenderID,E2000[i].TenderVal);
		retval=SafeSendHLogElement(posn,&handle,2000,time,0x0000,sizeof(E2000[i]),(char*)&E2000[i]);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcSale.2000 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.2000: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcSale.2000: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("Info: ProcSale.2000: successful.\n");
		elements++;
		if(E2000_21499[2*i-1].Record_Type!=-1) {
				retval=SafeSendHLogElement(posn,&handle,21499,time,0x0000,sizeof(E2000_21499[2*i-1])
						,(char*)&E2000_21499[2*i-1]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21499 Tender details returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21499.Tender details: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21499.Tender details: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21499.Tender details: successful.\n");
				elements++;
				retval=SafeSendHLogElement(posn,&handle,21499,time,0x0000,sizeof(E2000_21499[2*i])
						,(char*)&E2000_21499[2*i]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21499 Tender details Y returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21499.Tender details Y: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21499.Tender details Y: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21499.Tender details Y: successful.\n");
				elements++;
		}
	}
/*********************    2060      ***************************/
	if (header.chg_amnt>0) {//change was given (even after rounding applied)
		LongVal2Cobol6(E2060.ChangeGiven,header.chg_amnt*100);
		retval=SafeSendHLogElement(posn,&handle,2060,time,0x0000,sizeof(E2060)
			,(char*)&E2060);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcSale.2600 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.2600: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcSale.2600: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("Info: ProcSale.2600: successful.\n");
		elements++;
		//AddTendering(posn,1,-rounded); //Subtract rounded cash given back
		AddTendering(posn,E2060.TenderID,-1*header.chg_amnt); //Subtract cash given back
	}
} //voided transakci�
else {
	TElem4000	E4000;
	PreFill(&E4000,	4000);
	sprintf(E4000.AuthUser,"%d",header.cashier);
	E4000.AuthRefused=FALSE;
	sprintf(E4000.Function,"%s","TAWW-AUTHVOID");
	retval=SafeSendHLogElement(posn,&handle,4000,time,0x0000,sizeof(E4000)
		,(char*)&E4000);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcSale.4000 returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcSale.4000: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcSale.4000: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("Info: ProcSale.4000: successful.\n");
	elements++;
}
	/*********************   21099   *******************/
	for(i=3+cust_cnt;i<=c_21099;i++) {
				retval=SafeSendHLogElement(posn,&handle,21099,time,0x0000,sizeof(E21099[i])
					,(char*)&E21099[i]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21099.fiscal_date returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21099.fiscal_date: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21099.fiscal_date: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21099.fiscal_date: successful.\n");
				elements++;
	}
	if (cust_cnt>0) cust_cnt=3;

/**********************    21020     ***********************************/	
	//Sale perf record
	LongVal2Cobol6(E21020.SalesTotal,sold*100);
	LongVal2Cobol6(E21020.ItemSoldTtl,sold*100);
	if (CashRounding) {//Rounding active
		LongVal2Cobol6(E21020.TenderTotal,(tenders)*100);
		LongVal2Cobol6(E21020.RoundingTtl,0);
	}
	else LongVal2Cobol6(E21020.TenderTotal,(tenders)*100);
	E21020.ItemCnt=c_21001;
	E21020.ItemSoldCnt=c_21001;
	E21020.TenderCnt=c_2000;
	retval=SafeSendHLogElement(posn,&handle,21020,time,0x0000,sizeof(E21020)
		,(char*)&E21020);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcSale.21020 returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcSale.21020: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcSale.21020: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("Info: ProcSale.21020: successful.\n");
	elements++;
	if (header.voided) {  //Voided tranzakci�
		ISS_T_REPLY ret2;
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.4000: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.4000: AbortTran successful.\n");
				return ISS_SUCCESS;
	}
/***********************   3023     ******************/
	//dept perf records
	E3023.CountSales=items;
	E3023.ItemKey;
	for (i=1;i<10001;i++) {
		if (dept_id_sum[i]!=0) {
			E3023.DeptNo=i;
			LongVal2Cobol6(E3023.ValueSales,dept_id_sum[i]*100);	
			retval=SafeSendHLogElement(posn,&handle,3023,time,0x0000,sizeof(E3023)
				,(char*)&E3023);
			if (retval!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				L.LogE("ERROR: ProcSale.3023 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.3023: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.3023: AbortTran successful.\n");
				return(retval);
			}
			else L.Log("Info: ProcSale.3023: successful.\n");
			elements++;
		}
	}
	LongVal2Cobol6(E3023.ValueSales,total*100);

	for(extprice=1;extprice<8;extprice++) {
		if (ItemDept[extprice]!=0) {
			E3023.DeptNo=ItemDept[extprice];
			retval=SafeSendHLogElement(posn,&handle,3023,time,0x0000,sizeof(E3023)
				,(char*)&E3023);
			if (retval!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				L.LogE("ERROR: ProcSale.3023 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.3023: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.3023: AbortTran successful.\n");
				return(retval);
			}
			else L.Log("Info: ProcSale.3023: successful.\n");
			elements++;
		}
	}
	//close tran
	element.element_time=time+trantime;
	element.element_num=elements;
	retval=iss46_hlog_end_trans(handle,Date,(unsigned short)elements);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcSale.EndTran returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcSale.EndTran: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcSale.EndTran: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("Info: ProcSale.EndTran: successful.\n");
	APos[posn].lasttrandate=Date;
	APos[posn].lasttrantime=time+trantime;

	GItemTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegGItemTotals);
	SaveStatus(false,posn);
	memcpy(itemcollector,&posn,sizeof(long));
	GItemTotals.InitCursor();
	int index;
	
	for (index=0;index<GItemTotals.GetCount();index++) {
		GItemTotals.GetDataByIndex(index,itemcollector);
		gdec2s((unsigned char *)(itemcollector+sizeof(long)),6,s);
		L.Log("Sales for item %s on till %d! Quantity=%d Value=%d.\n"
			,s,*((long *)(itemcollector)),*((long *)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)))
			,*((long *)(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6))));
	}

	//r.SetValueDW("Fuel.System.Close",0); //Engedem a benzink�t f�jlok feldolgoz�s�t	
	//open=false;
	/********************* Write CASH to database  *****************************/
	L.Log("The cash value:%d\n",cash);
	if (cash!=0) {
		char stmp[1500];
		char sep[]   = "|\n";
		sprintf(stmp,"SELECT id FROM GL_interface.Accounts Where auto_insert=89");
		char *rt =Select_MYSQL(stmp);
		strtok( rt, sep );
		long acct_id=atol(strtok( NULL, sep ));
		L.Log("The id is: %d\n",acct_id);
		sprintf(stmp,"SELECT id,amount FROM GL_interface.Tills Where acct_id=%d and day=curdate()",acct_id);
		L.Log("%s\n",stmp);
		rt =Select_MYSQL(stmp);
		long till_id=-1;
		strtok( rt, sep );
		if(rt!="-1")
			till_id=atol(strtok( NULL, sep ));
		if(till_id>=0) 
			sprintf(stmp,"UPDATE `GL_interface`.`Tills` SET `amount`='%d' WHERE `id`='%d'",(atol(strtok( NULL, sep ))+cash),till_id);
		else
			sprintf(stmp,"INSERT INTO `GL_interface`.`Tills` (`day`, `acct_id`, `amount`) VALUES (curdate(), '%d', '%d')",acct_id,(cash));
		long ret_ins =Insert_MYSQL(stmp);
		if(!ret_ins)
			L.LogE("ERROR - Cannot write csah (%d) to Tills table!\n",cash);
		else
			L.LogE("Succesfully write cash (%d) to Tills table!\n",cash);
	}
	
	return ISS_SUCCESS;
	
}
	
/****************************************************************************************************************************************/

/**********************************************   NAMOS-WINCOR   ***********************************************************************/
long NAMOS_ProcSale(char* F, char *short_F) //Return value: 0=no_retries others=retries
{
	Reg r(false, C_RegKey);
	THeader header;
	bool postpaid=false;
	TElem5100	E5100;
	TElem21001	E21001[100];
	TElem1021	E1021[100];
	TElem21002	E21002[100];
	TElem21024	E21024;
	float ext_price[100];
	float itemweight[100];
	TElem2000	E2000[20];
	int tender_2000[100];
	long	tendval[20];
	long r_21003[100];
	long r_21003_promo_no[100];
	long r_21002[100];
//	long r_21002_21499[100];
	LoyaltyDescription LoyaltyDesc[100];
	TElem2060	E2060;
	TElem21099V2	E21099[50];
	TElem21020	E21020;
	TElem3023	E3023;
	TElem21003	E21003[100];
	TElem21499	E21499[50];
	TElem21499	E2000_21499[100];
	TElem5101	E5101;
	PreFill(&E5101,	5101);
	TElem21499	E21002_21499[50];
	PreFill(&E5100,	5100);
	PreFill(&E2060,	2060);
	PreFill(&E21020,	21020);
	PreFill(&E3023,	3023);
	char OrigEAN[14][100];
	char fuel_ean[15];
	char fuel_desc[45];
	int c_21099=0;
	int unit_price_int;
	int rounding_amount=0;
	int c_21499=0;
	int c_2000=0,c_2000_21499=0,c_21002_21499=0;
	int c_21001=0,c_21003=0,c_21002=0;
	int i,sale_line=0,chg_rate=1;
	long dept_id_sum[10001];
	long dept_id_counter[10001];
	char Borig_date[11];
	SINT16 hp;
	ISS_T_LOG_BLOCK_HANDLE  handle;
	SYSTEMTIME st,et,it;
	ISS_T_REPLY retval=0;
	if ((retval=iss46_hlog_get_current_hp(&hp))!=ISS_SUCCESS) {
		L.LogE("ERROR: NAMOS_ProcSale.get_current_hp: %d, retval=%d\n",hp,retval);
		L.LogE("==> ProcSale will be aborted or retried!\n");
		return retval;
	}
	L.Log("Info: NAMOS_ProcSale.get_current_hp: %d\n",hp);
	bool open=false; //transaction opened
	long tenders=0;	//payment records logged already (to reject sales too)
	short round_amnt=0; //rounding amount
	long sold;		//total sales
	long total=0;		//current balance
	long rounded;	//payment value rounded (if cash)
	long totrounddiff=0; //Total rounding difference collected by all payment lines
	long posn;		//pos number
	long trantime;	//transaction total time
//	long availtime; //available time - time left from total transaction time
	long elements;	//counting logged elements
	long items=0;		//count of item records
	long weight;
	long quantity;
//	long prevquantity;
	ISS_GDECIMAL_4 unitprice;
	long extprice;
	bool ean2x[100];	//Will be true for EAN21-29
	char s[30];
//	unsigned char custno[12];
	bool gotcust;
	char transuser[9]={0,0,0,0,0,0,0,0,0};
	ISS_T_LOG_ELEM_ID element;
//	ISS_T_INT_DATE date;
	ISS_T_INT_TIME time;
	ISS_GDECIMAL_6 ean, eano[100];
//	ISS_GDECIMAL_6 eannosubst;
	char logon[20]; //Buser--date--=time=p# from Hp#user--date--=time=...
	char tranuser[5];
	char progress=0;
	char seps[]   = ":";
	int counter,seq_nr=0;
	char atoken[500];
	char *token;
	char temporary[100],temp2[100];
	long amnt=1, quant=1;
	long qq,vv;
	char ss[21];
	long off_total=0,disc_total=0,disc_counter=0;
	TenderDetails_list t_details[100],loyalty_details[100];
	char vtsz_text[20];
	int vat_rate=0;
	bool return_mode=false;
	char itemcollector[sizeof(ISS_GDECIMAL_6)+3*sizeof(long)];
	ISS_GDECIMAL_6 ean1;
	header.Total=0;
	header.TicketNum=0;
	header.voided=false;
	bool isCustomer=false;
	long sales_id=0;
	long ext_price_tmp=0;
	bool is_promo=false;
	int is_21001_offer[100];
	int is_21001_offer_pieces[100];
	int is_21001_loyalty[100],is_21002_loyalty[100];
//	int is_21001_loyalty_pieces[100];
	for (i=1;i<10001;i++){
		dept_id_sum[i]=0;
		dept_id_counter[i]=0;
		if(i<100) {
			tender_2000[i]=-1;
			r_21003[i]=-1;
			r_21002[i]=-1;
			is_21001_offer[i]=-1;
			is_21001_loyalty[i]=-1;
			is_21002_loyalty[i]=-1;
			loyalty_details[i].data[0]=0;
		}
	}
	strcpy(fuel_ean,"");
	strcpy(vtsz_text,"");
	strcpy(fuel_desc,"");
	TiXmlElement* node = 0;
	TiXmlElement* root = 0;
	TiXmlElement* todoElement= 0;
	TiXmlElement* XMLelement= 0;
//	TiXmlElement* CardElement;
	TiXmlHandle hRoot(0);
	TiXmlDocument doc( F );
	
	if ( !doc.LoadFile() )	{
		L.LogE("ERROR: NAMOS_ProcSale.PreProc: Could not load %s file.!  Error='%s'. Exiting.\n",F, doc.ErrorDesc());
		return 2;
	}
	sprintf(header.custnr,"%s","0");
	sprintf(header.zip,"%","000000");
	header.custinqueue=0;
	header.invoice=false;
	sprintf(header.Lautoken,"%s","0");
	sprintf(header.AP,"%s","000000000");
	sprintf(header.CloseNR,"%","1");
	sprintf(header.ReceiptNR,"%","1");
	header.transID=1; header.chg_amnt=0;
	root = doc.FirstChildElement( "Transaction" );
	if (IsNumber(root->Attribute( "PosNo"),strlen(root->Attribute( "PosNo")))) {
			printf("Pos number %s \n",root->Attribute( "PosNo"));
			seq_nr=atoi(root->Attribute( "PosNo"));
			posn=atoi(root->Attribute( "PosNo"))-1;
			header.sid=atoi(root->Attribute( "PosNo"))-1;
		}
		else { 
			L.LogE("ERROR: ProcSale: Pos number isn't a number! (%s) -> File rejected!\n",root->Attribute( "PosNo"));
			return ISS_SUCCESS;
		}
		if(posn>9) {
			L.LogE("ERROR: ProcSale: Pos number is bigger than 9! (%d) -> File rejected!\n",posn);
			return ISS_SUCCESS;
		}
	//header.sid=atoi(root->Attribute( "PosNo"))-1;
	/*<IXMFDay>
		<MF_DAYCLOSE Type="101" SubType="0" OwnerId="0" DayNumber="3" ControllerId="Y02100260" FiscalDate="0" FiscalTime="0" />
	</IXMFDay>*/
	node = root->FirstChildElement( "IXMFDay" );
	if(node){
		TiXmlElement* DayClose = node->FirstChildElement( "MF_DAYCLOSE" );	
		if(DayClose) {
			r.SetValueDW("Fuel.System.Close",1);
			
			NamosCurrentTime=0;
			FuelClose=1;
			NAMOS_ProcClose();
			return -97;
		}
		DayClose = node->FirstChildElement( "MF_DAYOPEN" );	
		if(DayClose) {
			NamosCurrentTime=0;
//			r.SetValueDW("Fuel.System.Close",0);
//			FuelClose=0;
			return 0;
		}
	}
	//Regiszter �ra�ll�sok f�jl, ezt kell majd feldolgozni
	node = root->FirstChildElement( "IXPumpCounter" );
	if(node){
		L.Log("Found pumpcounter file\n");
		TiXmlElement* PumpCounter = node->FirstChildElement( "WD_PUMPCOUNTER" );
		for(PumpCounter->FirstChildElement();PumpCounter;PumpCounter=PumpCounter->NextSiblingElement()) {
			sprintf(atoken,"INSERT INTO `VPOP`.`registers` (`value`, `nozzle`, `dispenser_id`, `timestamp`) VALUES ('%s','%s','%s','%s')",
				PumpCounter->Attribute( "AmountTotal" ),PumpCounter->Attribute( "Nozzle" ),PumpCounter->Attribute( "Fuelpoint" ),PumpCounter->Attribute( "TimeStamp" ));
			sold =Insert_MYSQL(atoken);
			if(sold==0)
				L.LogE("Succesfully write Registers (%s-%s-%s) to Registers table!\n",PumpCounter->Attribute( "Fuelpoint" ),PumpCounter->Attribute( "Nozzle" ),PumpCounter->Attribute( "AmountTotal" ));
			else
				L.LogE("ERROR - Cannot write Registers (%s-%s-%s) to Registers table!\n",PumpCounter->Attribute( "Fuelpoint" ),PumpCounter->Attribute( "Nozzle" ),PumpCounter->Attribute( "AmountTotal" ));
			sold=0;
		//	return 0;
		}
	}

	strcpy(header.Lautoken,root->Attribute( "TicketNo"));
	node = root->FirstChildElement( "IXHeader" );
	if(!node) {
		L.LogE("ERROR -- No IXHeader tag!\n");
		return 0;
	}
/************  SIGN OFF  *************************************************************************/
	TiXmlElement* JournalReport = node->FirstChildElement( "HE_SIGNOFF" );
	if (JournalReport){
		if(JournalReport->Attribute( "Operator" )) {
			if (IsNumber(JournalReport->Attribute( "Operator" ),strlen(JournalReport->Attribute( "Operator" )))) {
				printf("CashierID %s \n",JournalReport->Attribute( "Operator" ));
				header.cashier=atoi(JournalReport->Attribute( "Operator" ));
				if (header.cashier>999) {
					L.LogE("ERROR: ProcSale: Cashier id more than tree digits! (%s) -> File rejected!\n",JournalReport->Attribute( "Operator" ));
					return ISS_SUCCESS;
				}
			}
			else { 
				L.LogE("ERROR: ProcSale: Cashier isn't a number! (%s) -> File rejected!\n",JournalReport->Attribute( "Operator" ));
				return ISS_SUCCESS;
			}
			printf("BeginDate %s \n",JournalReport->Attribute( "TicketDateTime" ));
			strncpy(header.BDate,JournalReport->Attribute( "TicketDateTime" ),8);
			//Transaction START time
			strncpy(header.BTime,JournalReport->Attribute( "TicketDateTime" )+8,6);
			//Transaction END Date
			printf("EndDate %s \n",JournalReport->Attribute( "PrintDateTime" ));
			strncpy(header.EDate,JournalReport->Attribute( "PrintDateTime" ),8);
			
			//Transaction END time
			strncpy(header.ETime,JournalReport->Attribute( "PrintDateTime" )+8,6);
			header.BDate[8]=0;header.EDate[8]=0;
			header.ETime[6]=0;header.BTime[6]=0;
			sprintf(logon,"%.4d%s%s%.2d",header.cashier,remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid+1);
			if ((retval=ProcLogOff(logon))!=0) {
					L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOff returned %d!\n",retval);
					return retval;//retry allowed
			}
			L.Log("Succesfully logged out!\n");
			/******************  Z�R�S ELLEN�RZ�SE  *************************/
			long v,vv,id;
			node = root->FirstChildElement( "IXShiftBalance" );
			if(node) {
				JournalReport = node->FirstChildElement( "SH_BALANCE" );
				if(JournalReport){
					for( JournalReport; JournalReport; JournalReport=JournalReport->NextSiblingElement())
					{
						v=atol(JournalReport->Attribute( "AmtExpected" ));
						id=atol(JournalReport->Attribute( "MopNum" ));
						if (TenderTotals.CmpFigures(id,v)) {
							L.Log("Total sales on tender %d matched. Value=%d\n",id,v);
							TenderTotals.Delete(id);
						}
						else {
							if (TenderTotals.GetCounters(id,&vv)) {
								L.LogE("ERROR: Total sales for tender %d mismatches! ValueIn=%d ValueMy=%d\n"
									,id,v,vv);
								TenderTotals.Delete(id);
							}
							else {
								L.LogE("ERROR: There was no sales for tender %d according to internal values! ValueIn=%d\n"
									,id,v);
							}
						}
					}
				}
			}
			/********************************************************************/
			/******************  Z�R�S ELLEN�RZ�SE 2  *************************/
			node = root->FirstChildElement( "IXShiftStats" );
			JournalReport = node->FirstChildElement( "SH_STATISTICS" );
			if(JournalReport){
				v=atol(JournalReport->Attribute( "ticket_amount" ));
				char *s;
				 char *token;
				char a[430];
				//sprintf(a,"select ifnull(header_id,-1),ifnull(sum(total),0) from Infodesk2.Header where date='%s' and hostess=%d",header.BDate,header.cashier);
				sprintf(a,"select ifnull(header_id,-1),ifnull(sum(if(return_mode=1,-1*total,total)),0)-(SELECT  ifnull(sum(daily_sum),0) FROM GL_interface.namos_hostess_close where date='%s' and hostess=%d) from Infodesk2.Header where date='%s' and return_mode=0 and hostess=%d and subtracted=0",header.BDate,header.cashier,header.BDate,header.cashier);
				L.Log("MYSQL:%s\n",a);
				s=Select_MYSQL(a);
				//L.Log("Answer:%s\n",s);
				char sep[]   = "|\n";
				token=strtok( s, sep );
				long ret_ins=0;
				if(atol(token)==-1){
					L.LogE("SHIFT CLOSE: Total sales for hostess: %d, namos: %d in the database 0 difference: %d! \n",header.cashier,v,v);
					sprintf(a,"INSERT INTO `GL_interface`.`namos_hostess_close` (`date`, `hostess`, `daily_sum`, `iss_sum`, `difference`, `last_mode`) VALUES ('%s', '%d', '%d', '0', '%d', now())",header.BDate,header.cashier,v,v);					
				}
				else {
					//strtok( s, sep );
					token=strtok( NULL, sep );
					token=strtok( NULL, sep );
					long db=atol(token);
					L.LogE("SHIFT CLOSE: db Total sales for hostess: %d, namos: %d in the database %d difference: %d! \n",header.cashier,v,db,v-db);
					sprintf(a,"INSERT INTO `GL_interface`.`namos_hostess_close` (`date`, `hostess`, `daily_sum`, `iss_sum`, `difference`, `last_mode`) VALUES ('%s', %d, %d, %d, %d, now())",header.BDate,header.cashier,v,db,v-db);					
				}
				ret_ins =Insert_MYSQL(a);
				if(!ret_ins)
					L.LogE("Succesfully write hostess close (%s) to namos_hostess_close table!\n",a);
				else
					L.LogE("ERROR - Cannot write hostess close (%s) to namos_hostess_close table!\n",a);
			}
			return 0;
		}
		return 0;
	}
	/************  SIGN ON  *************************************************************************/
	JournalReport = node->FirstChildElement( "HE_SIGNON" );
	if (JournalReport){
		if(JournalReport->Attribute( "Operator" )) {
			if (IsNumber(JournalReport->Attribute( "Operator" ),strlen(JournalReport->Attribute( "Operator" )))) {
				printf("CashierID %s \n",JournalReport->Attribute( "Operator" ));
				header.cashier=atoi(JournalReport->Attribute( "Operator" ));
				if (header.cashier>999) {
					L.LogE("ERROR: ProcSale: Cashier id more than tree digits! (%s) -> File rejected!\n",JournalReport->Attribute( "Operator" ));
					return ISS_SUCCESS;
				}
			}
			else { 
				L.LogE("ERROR: ProcSale: Cashier isn't a number! (%s) -> File rejected!\n",JournalReport->Attribute( "Operator" ));
				return ISS_SUCCESS;
			}
			printf("BeginDate %s \n",JournalReport->Attribute( "TicketDateTime" ));
			strncpy(header.BDate,JournalReport->Attribute( "TicketDateTime" ),8);
			//Transaction START time
			strncpy(header.BTime,JournalReport->Attribute( "TicketDateTime" )+8,6);
			//Transaction END Date
			printf("EndDate %s \n",JournalReport->Attribute( "PrintDateTime" ));
			strncpy(header.EDate,JournalReport->Attribute( "PrintDateTime" ),8);
			
			//Transaction END time
			strncpy(header.ETime,JournalReport->Attribute( "PrintDateTime" )+8,6);
			header.BDate[8]=0;header.EDate[8]=0;
			header.ETime[6]=0;header.BTime[6]=0;
			sprintf(logon,"%.4d%s%s%.2d",header.cashier,remove_char(header.BDate,"-"),remove_char(header.BTime,":"),(header.sid+1));
			if ((retval=ProcLogOn(logon))!=0) {
					L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOff returned %d!\n",retval);
					return retval;//retry allowed
			}
			L.Log("Succesfully logged in!\n");
			r.SetValueDW("Fuel.System.Close",0);
			FuelClose=0;
			return 0;
		}
		return 0;
	}
	JournalReport = node->FirstChildElement( "HE_NORMAL" );
	if(!JournalReport) {
		L.LogE("ERROR -- Wrong file! %s\n",short_F);
		return 0;
	}
	printf("TRANSACTION HEADER\n");
	
	//Voided/Return transaction
	if(atoi(JournalReport->Attribute( "SubType" ))==1){
		return_mode=true;
		header.sid=2;
		posn=2;
		seq_nr=2;
	}
	
	//Transaction Date
	printf("BeginDate %s \n",JournalReport->Attribute( "TicketDateTime" ));
	strncpy(header.BDate,JournalReport->Attribute( "TicketDateTime" ),8);
	header.BDate[8]=0;
	strncpy(Borig_date,JournalReport->Attribute( "TicketDateTime" ),8);
	Borig_date[8]=0;
	//Transaction START time
	strncpy(header.BTime,JournalReport->Attribute( "TicketDateTime" )+8,6);
	//Transaction END Date
	printf("EndDate %s \n",JournalReport->Attribute( "PrintDateTime" ));
	strncpy(header.EDate,JournalReport->Attribute( "PrintDateTime" ),8);
	header.EDate[8]=0;
	//Transaction END time
	strncpy(header.ETime,JournalReport->Attribute( "PrintDateTime" )+8,6);
	header.Total=atoi(JournalReport->Attribute( "TicketTotal" ));
	/************  ELllen�rizni kell, hogy ez a tranzakci� befordult-e m�r az ISS-be  ****************/
	/*strcpy(atoken,header.Lautoken);
	strncpy(header.Lautoken,header.BDate+2,2);
	strcat(header.Lautoken,atoken);
	*/
	long is_avail=GetNamosTranFromDB(Borig_date,header.Lautoken,header.sid);
	if(is_avail>-1){
		L.LogE("ERROR -- The transaction is in the ISS400! Date:%s Lautoken:%s Pos:%d\n",header.BDate,header.Lautoken,header.sid);
		return -99;
	}
	ISS_T_INT_DATE orig_date,new_date;
	ISS_T_INT_TIME end_time;
	GetLocalTime(&st);
	new_date=DateYMD2Int(st.wYear,st.wMonth,st.wDay);
	header.BDate[8]=0;header.EDate[8]=0;
	header.ETime[6]=0;header.BTime[6]=0;
	strcpy(ss,JournalReport->Attribute( "PrintDateTime" ));
	ss[14]=0;
	sprintf(s,"%s%s",remove_char(header.BDate,"-"),remove_char(header.BTime,":"));
	s[14]=0;
	SysTimeFromS14(&st,s);
	orig_date=DateYMD2Int(st.wYear,st.wMonth,st.wDay);
	Date=DateYMD2Int(st.wYear,st.wMonth,st.wDay);
	time=st.wHour*3600+st.wMinute*60+st.wSecond;
			
	
	sprintf(s,"%s%s",remove_char(header.EDate,"-"),remove_char(header.ETime,":"));
	s[14]=0;
	SysTimeFromS14(&et,s);
	end_time=et.wHour*3600+et.wMinute*60+et.wSecond;
	
	// Namos time syncronization	
	if(end_time>NamosCurrentTime && header.sid<2) {
		GetLocalTime(&it);
		NamosCurrentTime=end_time;
		NamosISSTimeDIfferent=end_time-(it.wHour*3600+it.wMinute*60+it.wSecond);
		ltoa(NamosISSTimeDIfferent,NITD,10);
		r.SetValueSZ("Fuel.NamosISSTimeDIfferent",NITD);
	}
	
	if(NewACISDateCor && (orig_date<new_date) && (header.sid>2)) {
			GetLocalTime(&st);
			Date=DateYMD2Int(st.wYear,st.wMonth,st.wDay);
			time=st.wHour*3600+st.wMinute*60+st.wSecond;
			sprintf(header.BDate,"%04d-%02d-%02d",st.wYear,st.wMonth,st.wDay);
			sprintf(header.BTime,"%02d:%02d:00",st.wHour,st.wMinute);//,st.wSecond
			sprintf(header.EDate,"%04d-%02d-%02d",st.wYear,st.wMonth,st.wDay);
			sprintf(header.ETime,"%02d:%02d:28",st.wHour,st.wMinute);//,st.wSecond
			sprintf(s,"%s%s",remove_char(header.EDate,"-"),remove_char(header.ETime,":"));
			s[14]=0;
			SysTimeFromS14(&et,s);
			end_time=et.wHour*3600+et.wMinute*60+et.wSecond;
			CheckTranDateTime(&Date,&time,posn);
			CheckTranDateTime(&Date,&end_time,posn);
		}
	
	
	/********************* Log ON/OFF   ***********************/
	//CashierID
	if(JournalReport->Attribute( "Operator" )) {
		if (IsNumber(JournalReport->Attribute( "Operator" ),strlen(JournalReport->Attribute( "Operator" )))) {
			printf("CashierID %s \n",JournalReport->Attribute( "Operator" ));
			header.cashier=atoi(JournalReport->Attribute( "Operator" ));
			if (header.cashier>999) {
				L.LogE("ERROR: ProcSale: Cashier id more than tree digits! (%s) -> File rejected!\n",JournalReport->Attribute( "Operator" ));
				return ISS_SUCCESS;
			}
		}
		else { 
			L.LogE("ERROR: ProcSale: Cashier isn't a number! (%s) -> File rejected!\n",JournalReport->Attribute( "Operator" ));
			return ISS_SUCCESS;
		}
//		char onoff[20];
		bool LoggedOn=true;
		sprintf(logon,"%.4d%s%s%.2d",header.cashier,remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid+1);
		if (!(*(APos[posn].user))) {//noone is currently logged on
				sprintf(logon,"%.4d%s%s%.2d",header.cashier,remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid+1);
				if (!LoggedOn)
					if((retval=ProcLogOn(logon))!=0) {
						L.LogE("ERROR: ProcSale.AutoLogOn: ProcLogOn returned %d!\n",retval);
						return 1;//no retries
					}
		}
		else { // Ha m�s van bel�pve, akkor ki kell l�ptetni, majd bel�ptetni
			sprintf(tranuser,"%.4d",header.cashier);
			//tranuser[5]=0;
			CleanUserID(tranuser);
			if ((strcmp(APos[posn].user,tranuser))!=0) {
				char u[5];
				memcpy(u,APos[posn].user,4);
				u[4]=0;
				tranuser[4]=0;
				sprintf(logon,"%.4d%s%s%.2d",atoi(APos[posn].user),remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid+1);
				logon[20]=0;
				if ((retval=ProcLogOff(logon))!=0) {
					L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOff returned %d!\n",retval);
					return retval;//retry allowed
				}
				if ((retval=ProcLogOn(logon))!=0) {
					L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOn returned %d!\n",retval);
					return retval;//retry allowed
				}
			}
		}
	}
	else {
		L.LogE("ERROR: Missing the operator attribute!\n");
		return 0;
	}
	/****************  SIMA KEDVEZM�NY LEK�RDEZ�SE   **********/
	int promo_id=0;
	TiXmlElement* promo = root->FirstChildElement( "IXSitePromo" );
	TiXmlElement* PromoDetail;
	if(promo){
		PromoDetail = promo->FirstChildElement( "PC_PROMOTION" );
		for(PromoDetail->FirstChildElement();PromoDetail;PromoDetail=PromoDetail->NextSiblingElement()) {
			r_21003[promo_id]=atol(PromoDetail->Attribute( "Salesid" ));
			r_21003_promo_no[promo_id]=atol(PromoDetail->Attribute( "promo_no" ));
			promo_id++;
		}
	}

	/****************  LOYALTY KEDVEZM�NY LEK�RDEZ�SE   **********/
	int loyalty_id=0;
	promo = root->FirstChildElement( "IXCardDiscount" );
	if(promo){
		PromoDetail = promo->FirstChildElement( "PC_CARDDISCOUNT" );
		for(PromoDetail->FirstChildElement();PromoDetail;PromoDetail=PromoDetail->NextSiblingElement()) {
			r_21002[loyalty_id]=atol(PromoDetail->Attribute( "Salesid" ));
			LoyaltyDesc[loyalty_id].ext_id=atol(PromoDetail->Attribute( "external_id" ));
			LoyaltyDesc[loyalty_id].disc_value=atol(PromoDetail->Attribute( "discount_value" ));
			strncpy(LoyaltyDesc[loyalty_id].disc_name,PromoDetail->Attribute( "discount_name" ),30);
			LoyaltyDesc[loyalty_id].disc_name[strlen(PromoDetail->Attribute( "discount_name" ))]=0;
			loyalty_id++;
		}
	}
	/******************  LEK�RDEZEM A FIZET�SHEZ �S A KEDVEZM�NYEKHEZ TARTOZ� PLUSZ INFORM�CI�KAT   *****************/
	int transparent_id=0;
	char tss[20];
	TiXmlElement* Details = root->FirstChildElement( "IXTransparent" );
	TiXmlElement* TenderDetail;
	if(Details){
		TenderDetail = Details->FirstChildElement( "TR_TRANSPARENT" );
		for(TenderDetail->FirstChildElement();TenderDetail;TenderDetail=TenderDetail->NextSiblingElement()) {
			transparent_id++;
			//Ha leh�ztak valamilyen vev�azonos�t�t
			if(atoi(TenderDetail->Attribute( "DataSpec" ))==701){ //V�s�rl� k�rtyasz�m
				//21099|06:22:32|2|0|13/11/17|91|17|2070200028157        |975|
				sprintf(header.custnr,"%s",TenderDetail->Attribute( "DataBuf" ));
				if(!isCustomer){
					if(strncmp(header.custnr,"207",3))
						strcpy(header.custname,"Bizalomk�rty�s vev�");
					else
						strcpy(header.custname,"�zemanyagk�rty�s vev�");
					strcpy(header.custaddr," ");
					strcpy(header.custcity,"Budapest");
					strcpy(header.custzip,"1113");
					strcpy(header.cust_taxnr," ");
					strcpy(header.licence_plate," ");
					c_21499=2;
				}
			}
			//Bankk�rty�s fizet�shez tartoz� �rt�kek
			//<TR_TRANSPARENT Type="200" SubType="0" DataSpec="95" DataBuf="0#D:536442XXXXXX6259|E:887163|K:MasterCard|N:04000025|O:2018-06-05T10:21:08Z|Y:7|S:608" LineID="0" />
    		if(atoi(TenderDetail->Attribute( "DataSpec" ))==94 || atoi(TenderDetail->Attribute( "DataSpec" ))==95){ //Fizet�shez tartoz� le�r�sok
				i=atoi(slice_string((char*)TenderDetail->Attribute( "DataBuf" ),'#',1))+1;
				strcpy(atoken,TenderDetail->Attribute( "DataBuf" ));
				strcpy(t_details[i].data,slice_string((char*)atoken,'#',2));
			}
			//K�rty�s kedvezm�nyhez tartoz� le�r�s
			//<TR_TRANSPARENT Type="200" SubType="0" DataSpec="601" DataBuf="0#LDT: 1|LDP: 7" LineID="0" />
			if(atoi(TenderDetail->Attribute( "DataSpec" ))==601){
				strcpy(temporary,slice_string((char*)TenderDetail->Attribute( "DataBuf" ),'#',1));
				temporary[strlen(temporary)-1]=0;
				i=atoi(temporary);
				strcpy(atoken,TenderDetail->Attribute( "DataBuf" ));

				strcpy(loyalty_details[i].data,slice_string((char*)atoken,'#',2));
			}
			//Lev�s�rl�si k�dok be�r�sa az adatb�zisba
			if(atoi(TenderDetail->Attribute( "DataSpec" ))==998){
				
				/* Ezt itt enged�lyezni kell, ha majd k�nyvelni kell a lev�s�rl�si k�dokat */
				i=atoi(slice_string((char*)TenderDetail->Attribute( "DataBuf" ),'#',1))+1;
				strcpy(atoken,TenderDetail->Attribute( "DataBuf" ));
				strcpy(t_details[i].data,slice_string((char*)atoken,'#',2));
				
				strcpy(atoken,"INSERT INTO `GL_interface`.`FuelVoucher` (`VoucherID`, `eventdate`, `status`, `paydate`) VALUES ('");
				strcat(atoken,slice_string((char*)TenderDetail->Attribute( "DataBuf" ),'#',2));
				strcat(atoken,"',now(),2,curdate())");
				sold =Insert_MYSQL(atoken);
				if(sold!=0)
					L.LogE("ERROR - Cannot write Voucher (%s) to FuelVoucher table!\n",slice_string((char*)TenderDetail->Attribute( "DataBuf" ),'#',2));
				else
					L.LogE("Succesfully write Voucher (%s) to FuelVoucher table!\n",slice_string((char*)TenderDetail->Attribute( "DataBuf" ),'#',2));
				sold=0;
			}
			//Regiszter �ra �ll�s be�r�sa az adatb�zisba
			if(atoi(TenderDetail->Attribute( "DataSpec" ))==999){
				strcpy(atoken,slice_string((char*)TenderDetail->Attribute( "DataBuf" ),'|',1));
				atoken[strlen(atoken)-1]=0;
				_itoa( (atoi(atoken)+FirstDispenser), temporary, 10 );
				strcpy(atoken,"INSERT INTO `VPOP`.`registers` (`value`, `nozzle`, `dispenser_id`, `timestamp`) VALUES ('");
				strcat(atoken,slice_string((char*)TenderDetail->Attribute( "DataBuf" ),'|',3));
				strcpy(tss,slice_string((char*)TenderDetail->Attribute( "DataBuf" ),'|',3));
				strcat(atoken,"',");
				strcpy(temp2,slice_string((char*)TenderDetail->Attribute( "DataBuf" ),'|',2));
				temp2[strlen(temp2)-1]=0;
				strcat(atoken,temp2);
				strcat(atoken,",");
				strcat(atoken,temporary);
				strcat(atoken,",'");
				strcat(atoken,ss);
				strcat(atoken,"')");
				WriteRegister2Reg(atoi(temporary),atoi(temp2),tss);
				sold =Insert_MYSQL(atoken);
				if(sold==0)
					L.LogE("Succesfully write Registers (%s-%s-%s) to Registers table!\n",temporary,temp2,tss);
				else
					L.LogE("ERROR - Cannot write Registers (%s-%s-%s) to Registers table!\n",temporary,temp2,tss);
				sold=0;
			}
		}
		TenderDetail = Details->FirstChildElement( "TR_TRANSPARENT" );
	}



	node = root->FirstChildElement( "IXMFTicket" );
	if (node){
	/*********************  SALE *****************************/
		//AP
		JournalReport = node->FirstChildElement( "MF_TICKET" );
		r.SetValueDW("Fuel.System.Close",0);
		FuelClose=0;
		printf("AP %s \n",JournalReport->Attribute( "ControllerId" )+1);
		strcpy(header.AP,JournalReport->Attribute( "ControllerId" )+1);
		
		//CloseNR
		printf("CloseNR %s \n",JournalReport->Attribute( "DayNumber" ));
		sprintf(header.CloseNR,"%s",JournalReport->Attribute( "DayNumber" ));
		//ReceiptNR
		printf("FiscalReceipt %s \n",JournalReport->Attribute( "TicketNo" ));
		sprintf(header.ReceiptNR,"%s",JournalReport->Attribute( "TicketNo" ));
		
		//Print Invoice
		char tmp[50];
		sprintf(tmp,"%s",JournalReport->Attribute( "DocumentID" ));
		if(tmp[0]=='S' && tmp[1]=='Z') {
			printf("Print Invoice %s \n",tmp);
			header.invoice=true;
		}
		if(tmp[0]=='U' && tmp[1]=='K') {
			printf("PostPaid %s \n",tmp);
			postpaid=true;
		}
		if(return_mode && strlen(JournalReport->Attribute( "DocumentIDLink" ))>10){
			sprintf(atoken,"%s",slice_string((char*)JournalReport->Attribute( "DocumentIDLink" ),'/',2));
			atoken[strlen(atoken)-1]=0;
			printf("VOID AP %s \n",atoken+1);
			strcpy(header.Voided_AP,atoken+1);
			//header.Voided_AP[strlen(header.Voided_AP)]=0;
			//CloseNR
			sprintf(atoken,"%s",slice_string((char*)JournalReport->Attribute( "DocumentIDLink" ),'/',3));
			atoken[strlen(atoken)-1]=0;
			printf("VOIDED CLOSE NR %s \n",atoken);
			strcpy(header.Voided_CloseNR,atoken);
			//ReceiptNR
			sprintf(atoken,"%s",slice_string((char*)JournalReport->Attribute( "DocumentIDLink" ),'/',4));
//			atoken[strlen(atoken)-1]=0;
			printf("VOIDED RECEIPT NR %s \n",atoken);
			strcpy(header.Voided_ReceiptNR,atoken);
			//VOIDED TYPE
			sprintf(atoken,"%s",slice_string((char*)JournalReport->Attribute( "DocumentIDLink" ),'/',1));
			atoken[strlen(atoken)-1]=0;
			printf("Orig receipt type %s \n",atoken);
			strcpy(header.Voided_Type,atoken);
//			header.Voided_AP[strlen(header.Voided_AP)]=0;
			//Print Invoice
			header.invoice=true;
		}
		else {
			strcpy(header.Voided_AP,"");
		}
		//CustInQueue
		/*XMLelement = Sale->FirstChildElement( "CustInQueue" );
		if(XMLelement) {
			printf("CustInQueue %s \n",XMLelement->GetText());
			header.custinqueue=atoi(XMLelement->GetText());
		}*/
		//TransactionID
		header.transID=atol(header.Lautoken);
		//Customer ZIP code
		/*XMLelement = Sale->FirstChildElement( "gvr:ZipCode" );
		if(XMLelement) {
			printf("Zip code %s \n",XMLelement->GetText());
			sprintf(header.zip,"%s",XMLelement->GetText());
		}*/
	}
/****************  TENDERS   *********************************/
	/***************** visszaj�r� ******************/
	header.chg_amnt=0;
	round_amnt=0;
	TiXmlElement* TransDetail = root->FirstChildElement( "IXChange" );
	if(TransDetail) {
		TiXmlElement* TRound = TransDetail->FirstChildElement( "CH_NORMAL" );
		if(TRound){
			if(atoi(TRound->Attribute( "SubType" ))==3){
				header.chg_amnt+=abs(atoi(TRound->Attribute( "MopChange" )));	
				rounding_amount=atof(TRound->Attribute( "RoundDiff" ));
			}
		}
	}

	 TransDetail = root->FirstChildElement( "IXCard" );
	TiXmlElement* Tender;
	bool go=true;
	bool goCard=true;
	int bc_c=0;
	
	if(TransDetail){
		Tender = TransDetail->FirstChildElement( "PA_CARD" );
		
		/*******   BANKK�RTY�S FIZET�S  *******/
		while((Tender) && (go)) {
			//for(Tender->FirstChildElement();Tender;Tender=Tender->NextSiblingElement()) {
				printf("************ TENDERS BANKCARD ***********\n");
				c_2000++;
				
				PreFill(&E2000_21499[c_2000_21499],	21499);
				PreFill(&E2000[c_2000],	2000);
				memset( E2000_21499[2*c_2000-1].Data, ' ', 180 );
				memset( E2000_21499[2*c_2000].Data, ' ', 36 );

				E2000_21499[c_2000_21499].Record_Type=-1;
				//E2000_21499[2*c_2000-1].Record_Type=-1;
				
				L.Log("TenderID %s \n",Tender->Attribute( "MopNum" ));
				if(IsNumber(Tender->Attribute( "MopNum" ),strlen(Tender->Attribute( "MopNum" ))))
					E2000[c_2000].TenderID=atoi(Tender->Attribute( "MopNum" ));
				else 
					E2000[c_2000].TenderID=4;
				/*****  EZT ITT MEG KELL K�RDEZNI? HOGY VAN_E ILYEN NEKIK  ****/
				chg_rate=100;
				
				L.Log("Change amount %s \n",Tender->Attribute( "RoundDiff" ));
				if(IsNumber(Tender->Attribute( "RoundDiff" ),strlen(Tender->Attribute( "RoundDiff" ))))
					header.chg_amnt+=atof(Tender->Attribute( "RoundDiff" ));

				XMLelement = Tender->FirstChildElement( "TenderAmount" );
				L.Log("TenderAmount %s \n",Tender->Attribute( "AmtPayed" ));
				if(IsNumber(Tender->Attribute( "AmtPayed" ),strlen(Tender->Attribute( "AmtPayed" )))){
					tendval[c_2000]=abs(atol(Tender->Attribute( "AmtPayed" )));
					LongVal2Cobol6(E2000[c_2000].TenderVal,100*abs(tendval[c_2000]));
						//E2000[c_2000].TenderVal=atoi(XMLelement->GetText());
/************ Itt a kerek�t�st meg kell n�zni  **/	
					if(tendval[c_2000]<200000)
						tenders=tenders+((chg_rate*100*tendval[c_2000]+5001)/10000);
					else 
						tenders=tenders+((100*tendval[c_2000]+51)/100);
					//tenders=tenders+((chg_rate*100*abs(atof(Tender->Attribute( "AmtPayed" )))+5001)/10000);
				}
				 
				E2000[c_2000].DrwIDDate=Date;
				E2000[c_2000].DrwIDTime=end_time;
				E2000[c_2000].DrwIDLocID=header.sid;
				if(E2000[c_2000].TenderID==4 || E2000[c_2000].TenderID==3){// Bankk�rty�s fizet�s
						tender_2000[c_2000]=c_2000_21499;
						PreFill(&E2000_21499[c_2000_21499+1],	21499);
						memset(E2000_21499[c_2000_21499].Data,' ',180);
						memset(E2000_21499[c_2000_21499+1].Data,' ',180);
						E2000_21499[c_2000_21499].Record_Type=6;
						E2000_21499[c_2000_21499+1].Record_Type=6;
						//"D:543758XXXXXX7714|E:123456|K:MasterCard|N:DPCT0001|O:2017-11-03T09:38:52Z|Y:7|S:59"
						sprintf(atoken,"%s",t_details[c_2000].data);
						seps[0]   = '|';
						if (strlen(atoken)>50) { //Egy sorban van minden
							counter=0;
							token = strtok( atoken, seps );
							sprintf(atoken,"%s",token);
							atoken[strlen(atoken)]=0;
							strcpy(E2000_21499[c_2000_21499].Data+(counter*36),atoken);
							E2000_21499[c_2000_21499].Data[(counter)*36+strlen(atoken)]=' ';
							while( token != NULL )	{
								token = strtok( NULL, seps );
								counter++;
								if(counter<5) {
									sprintf(atoken,"%s",token);
									atoken[strlen(atoken)]=0;
									strcpy(E2000_21499[c_2000_21499].Data+(counter)*36,atoken);
									E2000_21499[c_2000_21499].Data[(counter)*36+strlen(atoken)]=' ';
								}
								if(counter==5) {
									c_2000_21499++;
									E2000_21499[c_2000_21499].Record_Type=6;
									strcpy(E2000_21499[c_2000_21499].Data,"Y:");
									if (token!=NULL)
										sprintf(atoken,"%s",token);
									else
										sprintf(atoken,"%s","  ");
									atoken[strlen(atoken)]=0;
									
									strcpy(E2000_21499[c_2000_21499].Data,atoken);
									E2000_21499[c_2000_21499].Data[strlen(atoken)]=' ';
									c_2000_21499++;
								}	
							}
						}				
				}
				if(E2000[c_2000].TenderID==23 || E2000[c_2000].TenderID==18 || E2000[c_2000].TenderID==5){ //Erzs�bet utalv�ny
						E2000_21499[c_2000_21499].Record_Type=4;
						memset(E2000_21499[c_2000_21499].Data,' ',180);
						memcpy(E2000_21499[c_2000_21499].Data,t_details[c_2000].data,strlen(t_details[c_2000].data));
						E2000_21499[c_2000_21499].Data[strlen(t_details[c_2000].data)+2]=' ';
						tender_2000[c_2000]=c_2000_21499;
						c_2000_21499++;
				}
				Tender=Tender->NextSiblingElement();
		}
	}
/***************  EGY�B FIZET�SEK   **********************/
	long overload=0,cash=0;
	char stmp[1500];
	//long tendered[120];
	long fuel_voucher=0;
	//for(i=0;i<120;i++) tendered[i]=0;
	TransDetail = root->FirstChildElement( "IXPayment" );
	if(TransDetail) {
		Tender = TransDetail->FirstChildElement( "PA_NORMAL" );
		for( Tender; Tender; Tender=Tender->NextSiblingElement()){ //Megn�zem, mennyi a visszaj�r� a fizet�sb�l
			if(atoi(Tender->Attribute( "MopNum" ))==32){
				L.LogE("FuelVoucher amount:%s\n",Tender->Attribute( "AmtTendered" ));					
				fuel_voucher=abs(atol(Tender->Attribute( "AmtTendered" )));
			}
			//tendered[atoi(Tender->Attribute( "MopNum" ))]+=abs(atol(Tender->Attribute( "AmtTendered" )));

		}
		Tender = TransDetail->FirstChildElement( "PA_NORMAL" );
		go=true;
		goCard=true;
		while((Tender) && (go)) {
			printf("************ TENDERS ***********\n");
			c_2000++;
			PreFill(&E2000[c_2000],	2000);
			L.Log("TenderID %s \n",Tender->Attribute( "MopNum" ));
			if(IsNumber(Tender->Attribute( "MopNum" ),strlen(Tender->Attribute( "MopNum" )))){
				E2000[c_2000].TenderID=atoi(Tender->Attribute( "MopNum" ));
			}
			else 
				E2000[c_2000].TenderID=1;
			/*****  EZT ITT MEG KELL K�RDEZNI? HOGY VAN_E ILYEN NEKIK  ****/
			chg_rate=100;
			if(E2000[c_2000].TenderID==32 || E2000[c_2000].TenderID==33){ //ezeket a fizet�eszk�z�ket nem kell be�rni az ISS-be
				//header.chg_amnt+=abs(atol(Tender->Attribute( "AmtTendered" )));
/**********  EZT BE KELL �RNI A FUELVOUCHER TABL�BA  **********/			
				if(E2000[c_2000].TenderID==32){
					L.LogE("FuelVoucher amount:%s\n",Tender->Attribute( "AmtTendered" ));					
				}
/**********  EZT BE KELL �RNI AZ OVERLOAD TABL�BA  **********/			
				if(E2000[c_2000].TenderID==33){
					sprintf(stmp,"INSERT INTO `GL_interface`.`over_load` (`amount`, `ap`, `close_nr`, `receipt_nr`, `day`)VALUES ('%s', '%s', '%s', '%s', '%s')",Tender->Attribute( "AmtTendered" ),header.AP,header.CloseNR,header.ReceiptNR,header.BDate);
					overload=atoi(Tender->Attribute( "AmtTendered" ));
					//total-=overload;
					//LongVal2Cobol6(E21001[c_21001].ExtPrice,100*total);//Extprice ok
					long ret_ins =Insert_MYSQL(stmp);
						if(ret_ins!=0)
							L.LogE("ERROR - Cannot write OVERLOAD (%s) to Tills table!\n",Tender->Attribute( "AmtTendered" ));
						else
							L.LogE("Succesfully write OVERLOAD (%s) to Tills table!\n",Tender->Attribute( "AmtTendered" ));
					E2000[c_2000].TenderID=91;
					tendval[c_2000]=abs(atol(Tender->Attribute( "AmtTendered" )));
					LongVal2Cobol6(E2000[c_2000].TenderVal,100*abs(tendval[c_2000]));
					if(tendval[c_2000]<200000)
						tenders=tenders+((chg_rate*100*tendval[c_2000]+5001)/10000);
					else 
						tenders=tenders+((100*tendval[c_2000]+51)/100);
					//tenders=tenders+((chg_rate*100*tendval[c_2000]+5001)/10000);
					c_2000++;
				}
				c_2000--;
			}			
			else{
				/********************* Write CASH to database  *****************************/
				if(E2000[c_2000].TenderID==1 && header.sid>2){
					cash=abs(atol(Tender->Attribute( "AmtTendered" )));
					L.Log("The cash value:%d",cash);
					if (cash!=0) {
						char sep[]   = "|\n";
						sprintf(stmp,"SELECT id FROM GL_interface.Accounts Where auto_insert=89");
						char *rt =Select_MYSQL(stmp);
						strtok( rt, sep );
						long acct_id=atol(strtok( NULL, sep ));
						L.Log("The id is: %d\n",acct_id);
						sprintf(stmp,"SELECT id,amount FROM GL_interface.Tills Where acct_id=%d and day=curdate()",acct_id);
						L.Log("%s\n",stmp);
						rt =Select_MYSQL(stmp);
						long till_id=-1;
						strtok( rt, sep );
						if(rt!="-1")
							till_id=atol(strtok( NULL, sep ));
						if(till_id>=0) 
							sprintf(stmp,"UPDATE `GL_interface`.`Tills` SET `amount`='%d' WHERE `id`='%d'",(atol(strtok( NULL, sep ))+cash),till_id);
						else
							sprintf(stmp,"INSERT INTO `GL_interface`.`Tills` (`day`, `acct_id`, `amount`) VALUES (curdate(), '%d', '%d')",acct_id,(cash));
						long ret_ins =Insert_MYSQL(stmp);
						if(ret_ins!=0)
							L.LogE("ERROR - Cannot write csah (%d) to Tills table!\n",cash);
						else
							L.LogE("Succesfully write cash (%d) to Tills table!\n",cash);
					}
				}
				
				
				
				
				L.Log("Change amount %s \n",Tender->Attribute( "RoundDiff" ));
				if(rounding_amount==0 && IsNumber(Tender->Attribute( "RoundDiff" ),strlen(Tender->Attribute( "RoundDiff" ))))
					rounding_amount+=abs(atof(Tender->Attribute( "RoundDiff" )));
				L.Log("TenderAmount %s \n",Tender->Attribute( "AmtTendered" ));
				if(IsNumber(Tender->Attribute( "AmtTendered" ),strlen(Tender->Attribute( "AmtTendered" )))){
					if(E2000[c_2000].TenderID==5 || E2000[c_2000].TenderID==1){ 
						if(abs(atol(Tender->Attribute( "AmtTendered" )))>=fuel_voucher){
							tendval[c_2000]=abs(atol(Tender->Attribute( "AmtTendered" )))-fuel_voucher;	
							fuel_voucher=0;
						}
						else{
							tendval[c_2000]=0;
							fuel_voucher-=abs(atol(Tender->Attribute( "AmtTendered" )));
						}
						if(E2000[c_2000].TenderID==5){ // IGAS lev�s�rl�si
								E2000_21499[c_2000_21499].Record_Type=4;
								memset(E2000_21499[c_2000_21499].Data,' ',180);
								memcpy(E2000_21499[c_2000_21499].Data,t_details[c_2000].data,strlen(t_details[c_2000].data));
								E2000_21499[c_2000_21499].Data[strlen(t_details[c_2000].data)+2]=' ';
								tender_2000[c_2000]=c_2000_21499;
								//E2000[c_2000].TenderID=32; Ezt nem k�rik, mivel k�l�n akarj�k l�tni a kasszasori forgalmat
								c_2000_21499++;
						}
						
					}
					else
						tendval[c_2000]=abs(atol(Tender->Attribute( "AmtTendered" )));
					LongVal2Cobol6(E2000[c_2000].TenderVal,100*abs(tendval[c_2000]));
					if(tendval[c_2000]<200000)
						tenders=tenders+((chg_rate*100*tendval[c_2000]+5001)/10000);
					else 
						tenders=tenders+((100*tendval[c_2000]+51)/100);
				}
				E2000[c_2000].DrwIDDate=Date;
				E2000[c_2000].DrwIDTime=end_time;
				E2000[c_2000].DrwIDLocID=header.sid;
			}
			goCard=true;
			Tender=Tender->NextSiblingElement();
		}
	}

/************************** TRANSACTION ********************************/
	//Transaction detail group
	TransDetail = root->FirstChildElement( "IXItemSale" );
	if(!TransDetail){
		L.LogE("ERROR -- It's not a sale file! %s\n",short_F);
		return 0;
	}
	TiXmlElement* TransLine = TransDetail->FirstChildElement( "SA_ITEM" );
	for( TransLine; TransLine; TransLine=TransLine->NextSiblingElement())
	{
		if(atoi(TransLine->Attribute( "SubType" ))==0){
			printf("******** FUEL Transaction **********\n");
			c_21001++;
			PreFill(&E21001[c_21001],	21001);
			gdec_init_z(ean,6,0,iss_gdec_round_normal);
			gdec_init_z(eano[c_21001],6,0,iss_gdec_round_normal);
			sprintf(s,"%s",TransLine->Attribute( "Artno" ));
			remove_lchar(s," ");
			sprintf(OrigEAN[c_21001],"%s",s);
			//DoItemSubst(s,false);
			L.Log("The original EAN %s \n",TransLine->Attribute( "Artno" ));
			gdec_from_ascii(ean,s);
			gdec_from_ascii(eano[c_21001],s);
			if (GetItemDataDept(unitprice,&E21001[c_21001].TaxID,&E21001[c_21001].ReportGroup,&E21001[c_21001].EANType,&E21001[c_21001].DeptId,ean)==ISS_E_SDBMS_NOREC) {
				L.LogE("ERROR: NAMOS.Item: No record in ISS400 for EAN %s! Needed values will be guessed.\n"
					,s);
				E21001[c_21001].ReportGroup=atoi(TransLine->Attribute( "Martno" ));
			/********************  EZZEL ITT VIGY�ZNI KELL !!!!!   *****************************/
				E21001[c_21001].TaxID=atoi(TransLine->Attribute( "VatNumber" ))-1;
				E21001[c_21001].EANType=5;
				E21001[c_21001].DeptId=atol(TransLine->Attribute( "Itemgrp" ));
			}
			ean2x[c_21001]=((s[0]=='2')&&(s[1]>'0')&&(s[1]<='9'));
			L.Log("ItemCode %s \n",s);
			GDec62Cobol6(E21001[c_21001].ItemCode,ean);
			E21001[c_21001].InputDevice=12;
			/*if(XMLelement) {
				printf("InputType %s \n",XMLelement->GetText());
				E21001[c_21001].InputDevice=atoi(XMLelement->GetText());					
			}*/
			E21001[c_21001].ReturnMode=return_mode;
/****** EZ ITT NEM BIZTOS   ********/
//			E21001[c_21001].ReportGroup=atoi(TransLine->Attribute( "Martno" ));
			E21001[c_21001].NegativeItem=0;//return_mode;
			if(return_mode==1) E21001[c_21001].ReturnType=2;
			float price_diff=0;
			price_diff=atof(TransLine->Attribute( "Oldamount" ))-atof(TransLine->Attribute( "Sellamount" ));
			printf("ActualSalesPrice %s \n",TransLine->Attribute( "Sellprice" ));
			
			LongVal2Cobol6(E21001[c_21001].UnitPrice,100*atof(TransLine->Attribute( "Oldprice" )));//Unitprice ok
			LongVal2Cobol6(E21001[c_21001].PriceFromPlu,100*atof(TransLine->Attribute( "Oldprice" )));
			E21001[c_21001].SplitQuantity=1;
			weight=0;
			printf("SalesQuantity %s  Weight: %s\n",TransLine->Attribute( "Quantity" ),TransLine->Attribute( "Weight" ));
			E21001[c_21001].Quantity=1;
			if(atoi(TransLine->Attribute( "Salestyp" ))==1){
				weight=abs(1000*atof(TransLine->Attribute( "Quantity" )));
				E21001[c_21001].Quantity=1;
				unit_price_int=atoi(TransLine->Attribute( "Oldprice" ));
				strcpy(fuel_ean,OrigEAN[c_21001]);
				strcpy(fuel_desc,TransLine->Attribute( "Shorttext" ));
				strcpy(vtsz_text,TransLine->Attribute( "ArtSpec" ));
				vat_rate=atoi(TransLine->Attribute( "Vatrate" ));
				ext_price_tmp=abs(atof(TransLine->Attribute( "Oldamount" )));
			}
			else
				E21001[c_21001].Quantity=abs(atoi(TransLine->Attribute( "Quantity" )));
			
			itemweight[c_21001]=weight;
			LongVal2Cobol4(E21001[c_21001].Weight,weight);

			printf("SalesAmount %s \n",TransLine->Attribute( "Oldamount" ));
			ext_price[c_21001]=abs(atof(TransLine->Attribute( "Oldamount" )));
			LongVal2Cobol6(E21001[c_21001].ExtPrice,100*ext_price[c_21001]);//Extprice ok
			dept_id_sum[E21001[c_21001].DeptId]+=abs(atol(TransLine->Attribute( "Oldamount" )));
			dept_id_counter[E21001[c_21001].DeptId]+=abs(atoi(TransLine->Attribute( "Quantity" )));
			total=total+abs(atol(TransLine->Attribute( "Sellamount" )));

			/******************** ItemTAX  ********************************/		
/*			printf("TAX\n");
			printf("TaxLevelID %s \n",TransLine->Attribute( "VatNumber" ));
			E21001[c_21001].TaxID=atoi(TransLine->Attribute( "VatNumber" ))-1;
*/	
			/****************** Ha kapott kedvezm�nyt ******************************/

			if(return_mode){
				PreFill(&E1021[c_21001],1021);
				GDec62Cobol6(E1021[c_21001].ItemCode ,ean);
				E1021[c_21001].ReturnType=2;
				E1021[c_21001].Quantity=E21001[c_21001].Quantity;
				LongVal2Cobol6(E1021[c_21001].ExtPrice,100*ext_price[c_21001]);
				E1021[c_21001].DeptId=E21001[c_21001].DeptId;
				E1021[c_21001].ReturnMode=true;
				E1021[c_21001].SubtractedItem=false;
				sprintf(E1021[c_21001].UserID,"%d",header.cashier);
			}
			is_promo=false;
			sales_id=atol(TransLine->Attribute( "Salesid" ));
			if(price_diff!=0){
				/*
				Bizalomk�rty�s kedvezm�ny
				21001|11:41:02|5999086413475|AUC.TEJF�L 20% 140G           |V 04039059000|828|728520|0|0|0|0|0|6|0|708|118|0|0|1|0|114|7|
				21499|11:41:02|5|LDP:5.00                            LDV:3500                            LDT:2                               |
				21002|11:41:02|3|Bizkartyas kedvezmeny|0|0|0|0|6|36|5999086413475|828|1|101|114|

				Dolgoz�i kedvezm�ny
				21004|10:41:46|5999086423610|AUC CS�.SZAL�MI 75G           |V 16010091000|0|0|2|259|878|916700|259|155|7|
				21499|10:41:46|5|E:13.00                             |
				21002|10:41:46|101|Dolgoz�i kedvezm�ny|0|0|0|0|1|13|5999086423610|878|2|101|155|

				Pont egy term�kre
				21004|07:58:08|7613035866782|FELIX FANTAST.24X100          |V 23091011000|0|0|2|2499|2755|994448|2499|114|7|
				21499|07:58:08|5|LP:400.00                           LT:0                                |

				*/
				//ExternalId �rt�ke alapj�n -- 0: no discount, 1: employee discount, 2: loyalty discount, 3: Auchan card discount
			  for(i=0;i<loyalty_id;i++){
					if(r_21002[i]==sales_id) is_promo=true;
					if(is_promo){
						disc_counter++;
						disc_total+=price_diff;
						is_21002_loyalty[c_21001]=c_21002;
						PreFill(&E21002[c_21002],21002);
						PreFill(&E21002_21499[c_21002],21499);
						E21002[c_21002].DeptID=E21001[c_21001].DeptId;
						E21002[c_21002].DiscountID=101;
						LongVal2Cobol6(E21002[c_21002].DiscountRate,0);
						E21002[c_21002].Quantity=E21001[c_21001].Quantity;
						E21002[c_21002].TaxID=E21001[c_21001].TaxID;
						LongVal2Cobol6(E21002[c_21002].DiscountValue,100*(long)abs(price_diff));
						E21002[c_21002].ReturnMode=E21001[c_21001].ReturnMode;
						E21002[c_21002].ReturnType=E21001[c_21001].ReturnType;
						E21002[c_21002].PromoType=0;
						E21002[c_21002].EanType=5;
						E21002[c_21002].SaleType=0;
						E21002[c_21002].SaleInfo='1';
						E21002[c_21002].DiscountAuthFlag='0';
						//E21002[c_21002].OfferExclusive=false;
						GDec62Cobol6(E21002[c_21002].ItemCode,ean);
						E21002[c_21002].TaxID=E21001[c_21001].TaxID;
						E21002[c_21002].ReportGroup=E21001[c_21001].ReportGroup;
						E21002_21499[c_21002].Record_Type=5;
					
						/***********  21499-es rekord a kedvezm�nyhez  **************/
						memset( E21002_21499[c_21002].Data, ' ', 180 );
						E21002_21499[c_21002].Record_Type=5;
						sprintf(atoken,"%s",slice_string(loyalty_details[c_21001].data,'|',2));
						atoken[strlen(atoken)]=0;

						strcpy(E21002_21499[c_21002].Data,atoken);
						E21002_21499[c_21002].Data[strlen(atoken)]=' ';
						if(LoyaltyDesc[i].ext_id==1){//Dolgoz�i kedvezm�ny
							sprintf(atoken,"E:%d",((long)abs(price_diff)));
							atoken[strlen(atoken)]=0;
							strcpy(E21002_21499[c_21002].Data+36,atoken);
							E21002_21499[c_21002].Data[36+strlen(atoken)]=' ';
							sprintf(atoken,"%s",slice_string(loyalty_details[c_21001].data,'|',1));
							atoken[strlen(atoken)]=0;
							strcpy(E21002_21499[c_21002].Data+72,atoken);
							E21002_21499[c_21002].Data[72+strlen(atoken)]=' ';
							E21002_21499[c_21002].Data[71+strlen(atoken)]=' ';
						}
						else { //Ha nem dolgoz�i kedvezm�ny
							sprintf(atoken,"LDV:%d",(100*(long)abs(price_diff)));
							atoken[strlen(atoken)]=0;
							strcpy(E21002_21499[c_21002].Data+36,atoken);
							E21002_21499[c_21002].Data[36+strlen(atoken)]=' ';
							sprintf(atoken,"%s",slice_string(loyalty_details[c_21001].data,'|',1));
							atoken[strlen(atoken)]=0;
							strcpy(E21002_21499[c_21002].Data+72,atoken);
							E21002_21499[c_21002].Data[72+strlen(atoken)]=' ';
							E21002_21499[c_21002].Data[71+strlen(atoken)]=' ';
						}
						c_21002++;
						i=loyalty_id;
					}					
				}
				if(!is_promo){
					for(i=0;i<promo_id;i++){
						if(r_21003[i]==sales_id) is_promo=true;
						if(is_promo){
							is_21001_offer[c_21001]=c_21003;
							counter=0;
							PreFill(&E21003[c_21003],21003);
							E21003[c_21003].DeptID=E21001[c_21001].DeptId;
							E21003[c_21003].OfferID=r_21003_promo_no[i];
							E21003[c_21003].OfferType=1;
							E21003[c_21003].TypeOffer=1;
							E21003[c_21003].SaleType=1;
							if(atoi(TransLine->Attribute( "Salestyp" ))==1){
								//LongVal2Cobol4(E21003[c_21003].OfferQuantity,weight);
								E21003[c_21003].OfferQuantity=E21001[c_21001].Quantity;
							}
							else
								E21003[c_21003].OfferQuantity=E21001[c_21001].Quantity;
							off_total+=price_diff;
							LongVal2Cobol6(E21003[c_21003].OfferValue,100*abs(price_diff));
							E21003[c_21003].ReturnMode=return_mode;
							E21003[c_21003].OfferExclusive=false;
							memcpy(E21003[c_21003].ItemCode,E21001[c_21001].ItemCode, sizeof(E21001[c_21001].ItemCode));
							E21003[c_21003].TaxID=E21001[c_21001].TaxID;
							E21003[c_21003].ReportGroup=E21001[c_21001].ReportGroup;
							c_21003++;
							counter++;
							is_21001_offer_pieces[c_21001]=counter;
						}					
					}
				}
				if(!is_promo && strlen(loyalty_details[c_21001].data)>0){
					PreFill(&E21002_21499[c_21002],21499);
					memset( E21002_21499[c_21002].Data, ' ', 180 );
					if(strncmp(loyalty_details[c_21001].data,"LP",2)==0){
						sprintf(atoken,"%s",slice_string(loyalty_details[c_21001].data,'|',1));
						atoken[strlen(atoken)]=0;
						strcpy(E21002_21499[c_21002].Data,atoken);
						E21002_21499[c_21002].Data[strlen(atoken)]=' ';
						sprintf(atoken,"%s",slice_string(loyalty_details[c_21001].data,'|',2));
						atoken[strlen(atoken)]=0;
						strcpy(E21002_21499[c_21002].Data+36,atoken);
						E21002_21499[c_21002].Data[strlen(atoken)+36]=' ';
						E21002_21499[c_21002].Record_Type=5;
					}
					else {
						sprintf(atoken,"%s",slice_string(loyalty_details[c_21001].data,'|',2));
						atoken[strlen(atoken)]=0;
						strcpy(E21002_21499[c_21002].Data,atoken);
						E21002_21499[c_21002].Data[strlen(atoken)]=' ';
						sprintf(atoken,"%s",slice_string(loyalty_details[c_21001].data,'|',1));
						atoken[strlen(atoken)]=0;
						strcpy(E21002_21499[c_21002].Data+36,atoken);
						E21002_21499[c_21002].Data[strlen(atoken)+36]=' ';
					}
					c_21002++;
				}
			}
			else {
				if(strlen(loyalty_details[c_21001].data)>0){
					PreFill(&E21002_21499[c_21002],21499);
					memset( E21002_21499[c_21002].Data, ' ', 180 );
					if(strncmp(loyalty_details[c_21001].data,"LP",2)==0){
						is_21002_loyalty[c_21001]=c_21002;
						sprintf(atoken,"%s",slice_string(loyalty_details[c_21001].data,'|',1));
						atoken[strlen(atoken)]=0;
						strcpy(E21002_21499[c_21002].Data,atoken);
						E21002_21499[c_21002].Data[strlen(atoken)]=' ';
						sprintf(atoken,"%s",slice_string(loyalty_details[c_21001].data,'|',2));
						atoken[strlen(atoken)]=0;
						strcpy(E21002_21499[c_21002].Data+36,atoken);
						E21002_21499[c_21002].Data[strlen(atoken)+36]=' ';
						E21002_21499[c_21002].Record_Type=5;
						c_21002++;
					}
					if(strncmp(loyalty_details[c_21001].data,"LT",2)==0){
						is_21002_loyalty[c_21001]=c_21002;
						sprintf(atoken,"%s",slice_string(loyalty_details[c_21001].data,'|',2));
						atoken[strlen(atoken)]=0;
						strcpy(E21002_21499[c_21002].Data,atoken);
						E21002_21499[c_21002].Data[strlen(atoken)]=' ';
						sprintf(atoken,"%s",slice_string(loyalty_details[c_21001].data,'|',1));
						atoken[strlen(atoken)]=0;
						strcpy(E21002_21499[c_21002].Data+36,atoken);
						E21002_21499[c_21002].Data[strlen(atoken)+36]=' ';
						E21002_21499[c_21002].Record_Type=5;
						c_21002++;
					}
				}
			}
		}

	}


	/****************  CUSTOMER   *********************************/
	TransDetail = root->FirstChildElement( "IXShiftInfo" );
	TiXmlElement* Customer;
	if(TransDetail){
		Customer = TransDetail->FirstChildElement( "SH_INFO" );
		if(Customer) {
			c_21499=2;
			char csctmp[120];
			wchar_t *wText;
			char *ansiText;
//			int room;
			if(strlen(Customer->Attribute( "custname" ))>0){
				isCustomer=true;
				strcpy(header.custname," ");
				L.Log("Name: %s \n",Customer->Attribute( "custname" ));
				wText = CodePageToUnicode(65001,Customer->Attribute( "custname" ));
				ansiText = UnicodeToCodePage(852,wText);	
				strcpy(header.custname,ansiText);
				//strcpy(header.custname,Customer->Attribute( "custname" ));
				L.Log("Name: %s \n",ansiText);
				//wcstombs(header.custname, csctmp, room);
				/*room = MultiByteToWideChar(CP_THREAD_ACP, MB_ERR_INVALID_CHARS, XMLelement->GetText(), strlen(XMLelement->GetText()), NULL, 0);
				MultiByteToWideChar(852, 0, Customer->Attribute( "custname" ), strlen(Customer->Attribute( "custname" )), csctmp, 80);
				wcstombs(header.custname, csctmp, room);*/
				strcpy(header.custaddr," ");
				strcpy(csctmp,Customer->Attribute( "street" ));
				strcat(csctmp," ");
				strcat(csctmp,Customer->Attribute( "street2" ));
				strcat(csctmp," ");
				strcat(csctmp,Customer->Attribute( "houseno" ));
				//sprintf(csctmp,"%s %s %s",Customer->Attribute( "street" ),Customer->Attribute( "street2" ),Customer->Attribute( "houseno" ));
				
				L.Log("Addr: %s\n",csctmp);
				wText = CodePageToUnicode(65001,csctmp);
				ansiText = UnicodeToCodePage(852,wText);	
				strcpy(header.custaddr,ansiText);
				//strcpy(header.custaddr,XMLelement->GetText());
				L.Log("CustAddr %s \n",header.custaddr);
				
				strcpy(header.custcity," ");
				L.Log("City: %s \n",Customer->Attribute( "city" ));
				wText = CodePageToUnicode(65001,Customer->Attribute( "city" ));
				ansiText = UnicodeToCodePage(852,wText);	
				strcpy(header.custcity,ansiText);
				//strcpy(header.custcity,XMLelement->GetText());
				L.Log("CustCity %s \n",header.custcity);

				strcpy(header.custzip," ");
				L.Log("CustZip %s \n",Customer->Attribute( "zipcode" ));
				sprintf(header.custzip,"%s",Customer->Attribute( "zipcode" ));

				strcpy(header.cust_taxnr," ");
				L.Log("Cust TaxNr %s \n",Customer->Attribute( "taxid" ));
				sprintf(header.cust_taxnr,"%s",Customer->Attribute( "taxid" ));

				strcpy(header.licence_plate," ");
				L.Log("Licence plate %s \n",Customer->Attribute( "carid" ));
				char lp1[100];
				sprintf(lp1,"%s",Customer->Attribute( "carid" ));
				lp1[9]=0;
				sprintf(header.licence_plate,"%s",lp1);
				if(strcmp(header.licence_plate,"(null)")==0) strcpy(header.licence_plate," ");
				
				char n1[30],n2[30];
				n1[0]=0;n2[0]=0;
				int cl=strlen(header.custname);
				if (cl>30) {
					strcpyi(n2,header.custname,30);
				
					strcpyi(n1,header.custname+30,cl-30);
					n2[30]=0;n1[cl-30]=0;
				}
				else {
					strcpyi(n2,header.custname,cl);
					n2[cl]=0;
				}
				char addr1[30],addr2[30];
				addr1[0]=0;addr2[0]=0;
				cl=strlen(header.custaddr);
				if (cl>30) {
					strcpyi(addr1,header.custaddr,30);
					strcpyi(addr2,header.custaddr+30,cl-30);
					addr1[30]=0;addr2[cl-30]=0;
				}
				else {
					strcpyi(addr1,header.custaddr,cl);
					addr1[cl]=0;
				}
	/*			if((strlen(header.custnr)==13) && (strncmp(header.custnr,"980000",2)==0)) {
					L.Log("Write customer to DB! %s\n",header.custnr);
					//RegCSC(header.custnr,n1,n2,addr1,addr2,header.custcity,header.custzip,header.cust_taxnr,header.licence_plate);
					WriteCSC2DB(header.custnr,header.custname,header.custzip,header.custcity,header.custaddr,header.licence_plate,header.cust_taxnr,seq_nr);
					
				}
				else {
					L.Log("It's not a new customer card! %s\n",header.custnr);
				}
		*/			
			}
		}
	}
	
	if(header.Total==0) {
		L.LogE("ERROR -- The ticket total is 0 file:%s\n",F);
		sprintf(atoken,"INSERT INTO `Infodesk2`.`namos_conv` (`orig_date`, `trans_id`, `filename`, `pos_no`,updated) VALUES ('%s', '%s', '%s', '%d',now())",Borig_date,header.Lautoken,short_F,header.sid);
		if(Insert_MYSQL(atoken)<0)
			L.LogE("ERROR -- Failed to write the transaction details to the database!\n");
		return 0;
	}






	/****************  PUMPTEST  *****************/
	TransDetail = root->FirstChildElement( "IXPumpTest" );
	if(TransDetail) {
		Tender = TransDetail->FirstChildElement( "PA_PUMPTEST" );
		go=true;
		goCard=true;
		while((Tender) && (go)) {
			printf("************ TENDERS ***********\n");
			c_2000++;
			PreFill(&E2000[c_2000],	2000);
			L.Log("TenderID 90 \n");
			E2000[c_2000].TenderID=90;
			/*****  EZT ITT MEG KELL K�RDEZNI? HOGY VAN_E ILYEN NEKIK  ****/
			chg_rate=100;
			rounding_amount+=0;
			
			L.Log("TenderAmount %s \n",Tender->Attribute( "Amount" ));
			if(IsNumber(Tender->Attribute( "Amount" ),strlen(Tender->Attribute( "Amount" )))){
				tendval[c_2000]=abs(atol(Tender->Attribute( "Amount" )));
				LongVal2Cobol6(E2000[c_2000].TenderVal,100*abs(tendval[c_2000]));
				if(tendval[c_2000]<200000)
						tenders=tenders+((chg_rate*100*tendval[c_2000]+5001)/10000);
					else 
						tenders=tenders+((100*tendval[c_2000]+51)/100);
				//tenders=tenders+((chg_rate*100*abs(atof(Tender->Attribute( "Amount") ))+5001)/10000);
			}
			 
			E2000[c_2000].DrwIDDate=Date;
			E2000[c_2000].DrwIDTime=end_time;
			E2000[c_2000].DrwIDLocID=header.sid;
			goCard=true;
			Tender=Tender->NextSiblingElement();
			/*if(Tender) {
				//TenderID = Tender->Attribute( "MopNum" );
				if(!TenderID)
					go=false;
			}
			else 	
				go=false;
				*/
		}
	}
	if(tenders==0) {
		sprintf(atoken,"INSERT INTO `Infodesk2`.`namos_conv` (`orig_date`, `trans_id`, `filename`, `pos_no`,updated) VALUES ('%s', '%s', '%s', '%d',now())",Borig_date,header.Lautoken,short_F,header.sid);
		if(Insert_MYSQL(atoken)<0)
			L.LogE("ERROR -- Failed to write the transaction details to the database!\n");
		L.LogE("ERROR -- The amount paid is 0\n");
		return 0;
	}

	time_t t1,t2;
	t1=TimeFromSystemTime(&st);
	t2=TimeFromSystemTime(&et);
	trantime=difftime(t2,t1);  //Fizet�s kezdete �s v�ge k�z�tt eltelt id�
	round_amnt=total-(tenders-header.chg_amnt); //total=cikkek �sszesen(Sellamount)  
	//round_amnt=total-tenders;
	if (abs(round_amnt)>2) {//Fizet�eszk�z�k �rt�ke nem egyezik meg a cikkek �sszeg�vel
		if(!header.voided){
			sprintf(atoken,"INSERT INTO `Infodesk2`.`namos_conv` (`orig_date`, `trans_id`, `filename`, `pos_no`,updated) VALUES ('%s', '%s', '%s', '%d',now())",Borig_date,header.Lautoken,short_F,header.sid);
			if(Insert_MYSQL(atoken)<0)
				L.LogE("ERROR -- Failed to write the transaction details to the database!\n");
			L.LogE("ERROR - The total: %d and the tender total: %d rounding amount:%d doesn't match!\n",total,tenders-header.chg_amnt,round_amnt);
			L.LogE("	==>Void this transaction!! File name: %s\n",F);
			return -98;
		}
	}
	short y,m,d;
	DateInt2YMD(APos[posn].signondate,&y,&m,&d);
	L.Log("Sign on date: %d.%d.%d\n",y,m,d);
	L.Log("Check the user! Apos.User: %s , F�jl user: %d\n",APos[posn].user,header.cashier);
	if(header.cashier>999) {
		sprintf(atoken,"INSERT INTO `Infodesk2`.`namos_conv` (`orig_date`, `trans_id`, `filename`, `pos_no`,updated) VALUES ('%s', '%s', '%s', '%d',now())",Borig_date,header.Lautoken,short_F,header.sid);
		if(Insert_MYSQL(atoken)<0)
			L.LogE("ERROR -- Failed to write the transaction details to the database!\n");
		L.LogE("ERROR - Bad user %d!\n",header.cashier);
		return 0;
	}

	if (!(*(APos[posn].user))) {//noone is currently logged on
				if ((posn>2)||((AutoActions&0x00000001)!=0)) {
					if (posn<=2) L.LogE("Warning: ProcSale: Sequence error - noone has been logged on (PoS #%d)!\n",posn+1);
					
					if (posn<=2) L.LogE("==> Now performing autologon by sales transaction header: %s\n",s);
					else L.Log("Performing autologon by sales transaction header (pos #%d): %s\n",posn+1,s);
					sprintf(logon,"%.4d%s%s%.2d",header.cashier,remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid+1);
					if ((retval=ProcLogOn(logon))!=0) {
						sprintf(atoken,"INSERT INTO `Infodesk2`.`namos_conv` (`orig_date`, `trans_id`, `filename`, `pos_no`,updated) VALUES ('%s', '%s', '%s', '%d'now())",Borig_date,header.Lautoken,short_F,header.sid);
						if(Insert_MYSQL(atoken)<0)
							L.LogE("ERROR -- Failed to write the transaction details to the database!\n");
						L.LogE("ERROR: ProcSale.AutoLogOn: ProcLogOn returned %d!\n",retval);
						return 0;//no retries
					}
					sprintf(APos[posn].user,"%d",header.cashier);
				}
				else {
					sprintf(atoken,"INSERT INTO `Infodesk2`.`namos_conv` (`orig_date`, `trans_id`, `filename`, `pos_no`,updated) VALUES ('%s', '%s', '%s', '%d',now())",Borig_date,header.Lautoken,short_F,header.sid);
					if(Insert_MYSQL(atoken)<0)
						L.LogE("ERROR -- Failed to write the transaction details to the database!\n");
					L.LogE("ERROR: ProcSale: Transaction sequence error - Noone has been logged on (PoS #%d)!\n",posn+1);
					return 0;//no retries
				}
	}
	else {
				
				char tranuser[5];
				sprintf(tranuser,"%.4d",header.cashier);
				//tranuser[5]=0;
				CleanUserID(tranuser);
				L.Log("Tranuser: %s APos.User: %s \n",tranuser,APos[posn].user);
				if ((strcmp(APos[posn].user,tranuser))!=0) {
					char u[5];
					memcpy(u,APos[posn].user,4);
					u[4]=0;
					tranuser[4]=0;
					if ((AutoActions&0x00000002)!=0) {
						char logon[20]; //user--date--=time=p# from Hp#user--date--=time=...
						long retval;
//						long len;
						L.LogE("Warning: ProcSale: Not the logged on user does sales! (Selling:%s Logged_on:%d))! (PoS #%d)\n"
							,tranuser,header.cashier,posn+1);
						L.LogE("==> Now performing automatic logoff/logon by sales transaction ID: %d\n",header.transID);
						sprintf(logon,"%.4d%s%s%.2d",header.cashier,remove_char(header.BDate,"-"),remove_char(header.BTime,":"),header.sid+1);
						logon[20]=0;
						if ((retval=ProcLogOff(logon))!=0) {
							sprintf(atoken,"INSERT INTO `Infodesk2`.`namos_conv` (`orig_date`, `trans_id`, `filename`, `pos_no`,updated) VALUES ('%s', '%s', '%s', '%d',now())",Borig_date,header.Lautoken,short_F,header.sid);
							if(Insert_MYSQL(atoken)<0)
								L.LogE("ERROR -- Failed to write the transaction details to the database!\n");
							L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOff returned %d!\n",retval);
							return retval;//retry allowed
						}
						if ((retval=ProcLogOn(logon))!=0) {
							sprintf(atoken,"INSERT INTO `Infodesk2`.`namos_conv` (`orig_date`, `trans_id`, `filename`, `pos_no`,updated) VALUES ('%s', '%s', '%s', '%d',now())",Borig_date,header.Lautoken,short_F,header.sid);
							if(Insert_MYSQL(atoken)<0)
								L.LogE("ERROR -- Failed to write the transaction details to the database!\n");
							L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOn returned %d!\n",retval);
							return retval;//retry allowed
						}
					}
					else {
						sprintf(atoken,"INSERT INTO `Infodesk2`.`namos_conv` (`orig_date`, `trans_id`, `filename`, `pos_no`,updated) VALUES ('%s', '%s', '%s', '%d',now())",Borig_date,header.Lautoken,short_F,header.sid);
						if(Insert_MYSQL(atoken)<0)
							L.LogE("ERROR -- Failed to write the transaction details to the database!\n");
						L.LogE("ERROR: ProcSale: Not the logged on user does sales! (Selling:%s Logged_on:%s))! (PoS #%d)\n"
							,tranuser,u,posn+1);
						return 0;//no retries
					}
				}
	}
	//Advancing performance period(s) if needed
//	char tmp[50];
	int g;
/*	sprintf(tmp,"%s%s",remove_char(header.BDate,"-"),remove_char(header.BTime,":"));
	tmp[14]=0;
	SysTimeFromS14(&st,tmp);
	
*/

    c_21099++;
	/****************  POST CODE  *****************/
	TransDetail = root->FirstChildElement( "IXTicketProfile" );
	if(TransDetail) {
		Tender = TransDetail->FirstChildElement( "TI_PROFILE" );
		if(Tender)
			if(atoi(Tender->Attribute( "ResponseType" ))==1 && strlen(Tender->Attribute( "ResponseBuffer" ))>0){
				PreFill(&E21099[c_21099],21099);
				E21099[c_21099].CurrDate=Date;
				E21099[c_21099].TransLoc=APos[posn].sid/2;
				E21099[c_21099].DataType=7;
				E21099[c_21099].TransStore=StoreNum;
				memset(E21099[c_21099].DataEnter,' ',21);
				g=13-strlen(Tender->Attribute( "ResponseBuffer" ));
				memset(E21099[c_21099].DataEnter,'0',g);
				strcpy(E21099[c_21099].DataEnter+g,Tender->Attribute( "ResponseBuffer" ));
				E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
				strcpy(header.zip,Tender->Attribute( "ResponseBuffer" ));
				c_21099++;
			}
	}
	//Lautoken
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=81;
	E21099[c_21099].TransStore=StoreNum;
	g=21-strlen(header.Lautoken);
	memset(E21099[c_21099].DataEnter,'0',g);
	E21099[c_21099].DataEnter[2]='1';
	strcpy(E21099[c_21099].DataEnter+g,header.Lautoken);
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	c_21099++;
	//Nyugta sz�mla
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=81;
	E21099[c_21099].TransStore=StoreNum;
	memset(E21099[c_21099].DataEnter,' ',21);
	if(!header.invoice) //Nyugta
		strcpy(E21099[c_21099].DataEnter,"006NY                ");
	else   //Sz�mla
		strcpy(E21099[c_21099].DataEnter,"006SZ                ");
	if(return_mode)
		strcpy(E21099[c_21099].DataEnter,"006V                 ");
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	
	c_21099++;
	//AP
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=81;
	E21099[c_21099].TransStore=StoreNum;
	g=21-strlen(header.AP);			
	memset(E21099[c_21099].DataEnter,'0',g);
	E21099[c_21099].DataEnter[2]='4';
	strcpy(E21099[c_21099].DataEnter+g,header.AP);
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	//Z�r�ssz�m
	c_21099++;
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=81;
	E21099[c_21099].TransStore=StoreNum;
	g=21-strlen(header.CloseNR);
	memset(E21099[c_21099].DataEnter,'0',g);
	E21099[c_21099].DataEnter[2]='5';
	strcpy(E21099[c_21099].DataEnter+g,header.CloseNR);
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	int cust_cnt=0;
	if(return_mode && strlen(header.Voided_AP)>5){
		c_21099++;
		//AP
		PreFill(&E21099[c_21099],21099);
		E21099[c_21099].CurrDate=Date;
		E21099[c_21099].TransLoc=APos[posn].sid/2;
		E21099[c_21099].DataType=81;
		E21099[c_21099].TransStore=StoreNum;
		g=21-strlen(header.Voided_AP);
		memset(E21099[c_21099].DataEnter,'0',g);
		E21099[c_21099].DataEnter[2]='9';
		strcpy(E21099[c_21099].DataEnter+g,header.Voided_AP);
		E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
		//Z�r�ssz�m
		c_21099++;
		PreFill(&E21099[c_21099],21099);
		E21099[c_21099].CurrDate=Date;
		E21099[c_21099].TransLoc=APos[posn].sid/2;
		E21099[c_21099].DataType=81;
		E21099[c_21099].TransStore=StoreNum;
		g=21-strlen(header.Voided_CloseNR);
		memset(E21099[c_21099].DataEnter,'0',g);
		E21099[c_21099].DataEnter[1]='1';
		E21099[c_21099].DataEnter[2]='2';
		strcpy(E21099[c_21099].DataEnter+g,header.Voided_CloseNR);
		E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
		//Nyugtasz�m
		c_21099++;
		PreFill(&E21099[c_21099],21099);
		E21099[c_21099].CurrDate=Date;
		E21099[c_21099].TransLoc=APos[posn].sid/2;
		E21099[c_21099].DataType=81;
		E21099[c_21099].TransStore=StoreNum;
		g=21-strlen(header.Voided_ReceiptNR);
		memset(E21099[c_21099].DataEnter,'0',g);
		E21099[c_21099].DataEnter[1]='1';
		E21099[c_21099].DataEnter[2]='0';
		strcpy(E21099[c_21099].DataEnter+g,header.Voided_ReceiptNR);
		E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	}
	
	if (strcmp(header.custnr,"0")!=0) {
		cust_cnt=4;
		gotcust=true;
		c_21099++;
		
		//V�s�rl� azonos�t�
		PreFill(&E21099[c_21099],21099);
		E21099[c_21099].CurrDate=Date;
		E21099[c_21099].TransLoc=APos[posn].sid/2;
		E21099[c_21099].DataType=17;
		E21099[c_21099].TransStore=StoreNum;
		memset(E21099[c_21099].DataEnter,' ',21);
		g=13-strlen(header.custnr);
		memset(E21099[c_21099].DataEnter,'0',g);
		strcpy(E21099[c_21099].DataEnter+g,header.custnr);
		E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
		//c_21499++;
		//V�s�rl�i adatok
		PreFill(&E21499[0],21499);
		E21499[0].Record_Type=1;
		memset(E21499[0].Data,' ',184);
		g=18-strlen(header.custnr);
		memset(E21499[0].Data,'0',g);
		strcpy(E21499[0].Data+g,header.custnr);
		//n�v
		strcpy(E21499[0].Data+18,header.custname);
		E21499[0].Data[18+strlen(header.custname)]=' ';
		//irsz�m
		strcpy(E21499[0].Data+63,header.custzip);
		E21499[0].Data[63+strlen(header.custzip)]=' ';
		//v�ros
		strcpy(E21499[0].Data+68,header.custcity);
		E21499[0].Data[68+strlen(header.custcity)]=' ';
		//c�m
		strcpy(E21499[0].Data+108,header.custaddr);
		E21499[0].Data[108+strlen(header.custaddr)]=' ';
		E21499[0].Data[184]=0;
		//c_21499++;
		//V�s�rl�i adatok
		PreFill(&E21499[1],21499);
		E21499[1].Record_Type=2;
		memset(E21499[1].Data,' ',45);
		strcpy(E21499[1].Data,header.cust_taxnr);
		E21499[1].Data[strlen(header.cust_taxnr)]=' ';
		E21499[1].Data[45]='1';

		c_21099++;
		//V�s�rl� azonos�t�
		PreFill(&E21099[c_21099],21099);
		E21099[c_21099].CurrDate=Date;
		E21099[c_21099].TransLoc=APos[posn].sid/2;
		E21099[c_21099].DataType=81;
		E21099[c_21099].TransStore=StoreNum;
		memset(E21099[c_21099].DataEnter,' ',21);
		memset(E21099[c_21099].DataEnter,'0',2);
		E21099[c_21099].DataEnter[2]='2';
		strcpy(E21099[c_21099].DataEnter+3,header.custnr);
		E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	}
	c_21099++;
	//D�tum
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=51;
	E21099[c_21099].TransStore=StoreNum;
	//CheckTranDateTime(&Date,&time,posn);
	sprintf(E21099[c_21099].DataEnter,"%04dA%02dA%02d",st.wYear,st.wMonth,st.wDay);	//Fiscal date gets stored
	memset(E21099[c_21099].DataEnter+10,' ',11);
	c_21099++;
	//Napi nyugta sz�ml�l� ISS
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=16;
	E21099[c_21099].TransStore=StoreNum;
	//CheckTranDateTime(&Date,&time,posn);
	memset(E21099[c_21099].DataEnter,' ',21);
	sprintf(E21099[c_21099].DataEnter,"%d",atol(header.ReceiptNR));
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	//Fiscalis bejegyz�s
	c_21099++;
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=92;
	E21099[c_21099].TransStore=StoreNum;
	//CheckTranDateTime(&Date,&time,posn);
	memset(E21099[c_21099].DataEnter,' ',21);
	sprintf(E21099[c_21099].DataEnter,"%s/%.4d/%.5d",header.AP,atoi(header.CloseNR),atol(header.ReceiptNR));
	E21099[c_21099].DataEnter[strlen(E21099[c_21099].DataEnter)]=' ';
	//Ir�ny�t�sz�m bejegyz�s
	/*c_21099++;
	PreFill(&E21099[c_21099],21099);
	E21099[c_21099].CurrDate=Date;
	E21099[c_21099].TransLoc=APos[posn].sid/2;
	E21099[c_21099].DataType=7;
	E21099[c_21099].TransStore=StoreNum;
	//CheckTranDateTime(&Date,&time,posn);
	memset(E21099[c_21099].DataEnter,' ',21);
	sprintf(E21099[c_21099].DataEnter,"%s",header.zip);
	E21099[c_21099].DataEnter[strlen(header.zip)]=' ';
	*/
	while (Check4NewPerfSlot(posn,Date,time)); //Advancing performance slots until it's necessary

	//Checks done before opening a new transaction
	SINT32        transno;
	ISS_T_BOOLEAN inprogress;
	SINT16        element_num;
	L.Log("Write transaction to database, to not reinsert it!\n");
	int zt=-1;
	char tmp_cust_nr[21];
	sprintf(tmp_cust_nr,"%s",header.custnr);
	if(!postpaid) sprintf(tmp_cust_nr,"%s","");
	if(strlen(fuel_ean)>12 && unit_price_int>0){
	/*	for(int tz=0;tz<10;tz++){
			if(strcmp(FuelPriceArray[tz].ean,fuel_ean)==0){
				zt=tz;
				tz=20;
			}
		}*/
		double fp=weight/1000;
		//Adatb�zisb�l lek�rem, hogy milyen �ron adt�k el az �zemanyagot kedvezm�nyes, vagy norm�l 
		GetActualFuelPrice(fuel_ean,unit_price_int,header.BDate );
		if(unit_price_int==default_price) disc_price=0;
		if(default_price==0 && disc_price==0) default_price=unit_price_int;
		sprintf(atoken,"INSERT INTO `Infodesk2`.`namos_conv` (`orig_date`, `trans_id`, `filename`, `pos_no`,default_price,disc_price,quantity,card_nr,ean,item_description,updated,vtsz,vat,ext_price) VALUES ('%s', '%s', '%s', '%d', '%d', '%d',%d/1000,'%s','%s','%s',now(),'%s',%d,%d)",
					Borig_date,header.Lautoken,short_F,header.sid,default_price,disc_price,weight,tmp_cust_nr,fuel_ean,fuel_desc,vtsz_text,vat_rate,ext_price_tmp);

	}
	else 
		sprintf(atoken,"INSERT INTO `Infodesk2`.`namos_conv` (`orig_date`, `trans_id`, `filename`, `pos_no`,updated) VALUES ('%s', '%s', '%s', '%d',now())",Borig_date,header.Lautoken,short_F,header.sid);
	if(Insert_MYSQL(atoken)<0)
		L.LogE("ERROR -- Failed to write the transaction details to the database!\n");
	if(strlen(fuel_ean)>12 && unit_price_int>0){
		sprintf(atoken,"INSERT INTO `Infodesk2`.`namos_conv` (store_nr,`orig_date`, `trans_id`, `filename`, `pos_no`,default_price,disc_price,quantity,card_nr,ean,item_description,updated,vtsz,vat,ext_price) VALUES (%d,'%s', '%s', '%s', '%d', '%d', '%d',%d/1000,'%s','%s','%s',now(),'%s',%d,%d)",
					StoreNum,Borig_date,header.Lautoken,short_F,header.sid,default_price,disc_price,weight,tmp_cust_nr,fuel_ean,fuel_desc,vtsz_text,vat_rate,ext_price_tmp);
	}
	else 
		sprintf(atoken,"INSERT INTO `Infodesk2`.`namos_conv` (store_nr,`orig_date`, `trans_id`, `filename`, `pos_no`,updated) VALUES (%d,'%s', '%s', '%s', '%d',now())",StoreNum,Borig_date,header.Lautoken,short_F,header.sid);
	if(Insert_MYSQL910(atoken)<0)
		L.LogE("ERROR -- Failed to write the transaction details to the R910 database!\n");
	
	retval=iss46_hlog_get_curr_trans_num(APos[posn].sid,&transno,&inprogress,&element_num);
	L.Log("Info: ProcSale.check_before_start.get_curr_trans: tr=%d, open=%d, elem=%d\n"
		,transno,inprogress,element_num);
	if (retval!=ISS_SUCCESS) {
		L.LogE("ERROR: ProcSale.check_before_start.get_curr_trans: Bad return value! (%d)\n",retval);
		return retval;
	}
	if (inprogress) {
		L.LogE("ERROR: ProcSale.check_before_start: Transaction must not be open at this point!\n");
		return(-1);
	}
	APos[posn].lasttran++;//Preparing new transaction's number
	if (APos[posn].lasttran!=transno) {//checks whether ISS thinks the same
		L.LogE("Warning: ProcSale.check_before_start: Transaction numbers doesn't match!\n");
		L.LogE("==> APos[%d].lasttran=%d ISSLOG.transno=%d SID=%d\n"
			,posn,APos[posn].lasttran,transno,APos[posn].sid);
		APos[posn].lasttran=transno; //Correction by ISS
	}

	//Preparing opening a new transaction
	element.element_num=1;
	element.element_time=time;
	memcpy(transuser,APos[posn].user,4);

	if ((retval=iss46_hlog_start_block(APos[posn].sid,hp,APos[posn].lasttran,Date,time
		,0,0x00018000,element,9,transuser,&handle))!=ISS_SUCCESS) {
		L.LogE("ERROR: ProcSale.start returned: %d -> retrying!\n",retval);
		return(retval);
	}
	else L.Log("ProcSale.start: Successful.\n");
	

	elements=1;
	if(return_mode) {
		E5101.ReturnType=2;
		E5101.RtnReasonCode=0;
		if ((retval=iss46_hlog_send_element(handle,5101,time,0x0000,sizeof(E5101),(char*)&E5101))
			!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcSale.5101 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.5101: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcSale.5101: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("Info: ProcSale.5101: Successful.\n");
	}
	else{
		if ((retval=iss46_hlog_send_element(handle,5100,time,0x0000,sizeof(E5100),(char*)&E5100))
			!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcSale.5100 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.5100: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcSale.5100: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("Info: ProcSale.5100: Successful.\n");
	}
	open=true;
	elements++;
	sold=0;
	APos[posn].p_custcnt++;
//cust_cnt=0;
	/*********************   21099   *******************/
	for(i=1;i<=2+cust_cnt;i++) {
				retval=SafeSendHLogElement(posn,&handle,21099,time,0x0000,sizeof(E21099[i])
					,(char*)&E21099[i]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21099.fiscal_date returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21099.fiscal_date: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21099.fiscal_date: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21099.fiscal_date: successful.\n");
				elements++;
	}
	/*********************   21499   *******************/
	if (cust_cnt>0)
		for(i=0;i<2;i++) {
					retval=SafeSendHLogElement(posn,&handle,21499,time,0x0000,sizeof(E21499[i])
						,(char*)&E21499[i]);
					if (retval!=ISS_SUCCESS) {
						ISS_T_REPLY ret2;
						L.LogE("ERROR: ProcSale.21499.fiscal_date returned: %d -> abort tran & retry!\n",retval);
						if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
							L.LogE("ERROR: ProcSale.21499.fiscal_date: AbortTran: %d!\n",ret2);
						}
						else L.Log("Info: ProcSale.21499.fiscal_date: AbortTran successful.\n");
						return(retval);
					}
					else L.Log("Info: ProcSale.21499.fiscal_date: successful.\n");
					elements++;
		}
	/*********************   21001   *******************/
	for(i=1;i<=c_21001;i++) {
		sprintf(ss,"0000000%s",OrigEAN[i]);
		ss[21]=0;
		L.Log("EAN code: %s\n",ss);
		gdec_init_z(ean1,6,0,iss_gdec_round_normal);
		gdec_from_ascii(ean1,ss);
		//gdec_init_z(ean,6,0,iss_gdec_round_normal);
		memcpy(itemcollector,&posn,sizeof(long));
		memcpy(itemcollector+sizeof(long),ean1,sizeof(ISS_GDECIMAL_6));
		if (GItemTotals.GetData(itemcollector,NULL,false)) {
			L.Log("The addad Value=%d Quantity=%d\n",ext_price[i],itemweight[i]);
			L.Log("This is the first sale of this ITEM %s at this till %d.\n",OrigEAN[i],header.sid+PosSIDBase/2);
			qq=0;vv=0;
		}
		else {
			qq=*((long*)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)));
			vv=*((long*)(itemcollector+sizeof(long)*2+sizeof(ISS_GDECIMAL_6)));
			L.Log("The addad Value=%d Quantity=%d\n",ext_price[i],itemweight[i]);
			L.Log("Total sales for item %s on this till %d! ValueMy=%d QuantityMy=%d\n"
					,OrigEAN[i],header.sid+PosSIDBase/2,vv,qq);
		}
		extprice=ext_price[i];quantity=itemweight[i];
		memcpy(itemcollector,&posn,sizeof(long));
		memcpy(itemcollector+sizeof(long),ean1,sizeof(ISS_GDECIMAL_6));
		memcpy(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6),&quantity,sizeof(long));
		memcpy(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6),&extprice,sizeof(long));
		GItemTotals.AddFigures(itemcollector);

		retval=SafeSendHLogElement(posn,&handle,21001,time,0x0000,sizeof(E21001[i])
			,(char*)&E21001[i]);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcSale.21001 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.21001: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcSale.21001: AbortTran successful.\n");
			return (ISS_SUCCESS);
		}
		else L.Log("Info: ProcSale.21001: successful.\n");
		element.element_time+=(trantime/c_21001);
	//	time+=(trantime/c_21001);
		elements++;
		items+=E21001[i].Quantity;
		if(return_mode){
			retval=SafeSendHLogElement(posn,&handle,1021,time,0x0000,sizeof(E1021[i])
				,(char*)&E1021[i]);
			if (retval!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				L.LogE("ERROR: ProcSale.1021 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.1021: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.1021: AbortTran successful.\n");
				return (ISS_SUCCESS);
			}
			else L.Log("Info: ProcSale.1021: successful.\n");
			elements++;
		}
		if (!header.voided) {
			APos[posn].p_regtime+=(trantime/c_21001);
			APos[posn].regtime+=(trantime/c_21001);
			APos[posn].items+=E21001[i].Quantity;
			APos[posn].p_itemcnt+=E21001[i].Quantity;
		}
		/*********************   21003   *******************/
		if(is_21001_offer[i]>-1){
			for(counter=is_21001_offer[i];counter<is_21001_offer[i]+is_21001_offer_pieces[i];counter++) {
				retval=SafeSendHLogElement(posn,&handle,21003,time,0x0000,sizeof(E21003[counter])
					,(char*)&E21003[counter]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21003 returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21003: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21003: AbortTran successful.\n");
					return (ISS_SUCCESS);
				}
				else L.Log("Info: ProcSale.21001: successful.\n");
				elements++;
			/*	element.element_time+=(trantime/c_21003);
				time+=(trantime/c_21003);
				elements++;
				//items+=E21001[c_21001].Quantity;

				if (!header.voided) {
					APos[posn].p_regtime+=(trantime/c_21003);
					APos[posn].regtime+=(trantime/c_21003);
					//APos[posn].items+=E21001[c_21003].Quantity;
					//APos[posn].p_itemcnt+=E21001[c_21003].Quantity;
				}
				*/
			}
		}
		/*********************   21002   *******************/
		if(is_21002_loyalty[i]>-1){
				retval=SafeSendHLogElement(posn,&handle,21499,time,0x0000,sizeof(E21002_21499[is_21002_loyalty[i]])
					,(char*)&E21002_21499[is_21002_loyalty[i]]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21002_21499 returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21002_21499: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21002_21499: AbortTran successful.\n");
					return (ISS_SUCCESS);
				}
				else L.Log("Info: ProcSale.21002_21499: successful.\n");
				/*element.element_time+=(trantime/c_21001);
				time+=(trantime/c_21001);
				*/
				elements++;
				if(E21002[is_21002_loyalty[i]].DeptID>0 && E21002[is_21002_loyalty[i]].DeptID<10000){ 
					retval=SafeSendHLogElement(posn,&handle,21002,time,0x0000,sizeof(E21002[is_21002_loyalty[i]])
						,(char*)&E21002[is_21002_loyalty[i]]);
					if (retval!=ISS_SUCCESS) {
						ISS_T_REPLY ret2;
						L.LogE("ERROR: ProcSale.21002 returned: %d -> abort tran & retry!\n",retval);
						if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
							L.LogE("ERROR: ProcSale.21002: AbortTran: %d!\n",ret2);
						}
						else L.Log("Info: ProcSale.21002: AbortTran successful.\n");
						return (ISS_SUCCESS);
					}
					else L.Log("Info: ProcSale.21002: successful.\n");
					elements++;
				}
		}
		else {
			if(strlen(loyalty_details[i].data)>0){
				retval=SafeSendHLogElement(posn,&handle,21499,time,0x0000,sizeof(E21002_21499[is_21002_loyalty[i]])
					,(char*)&E21002_21499[is_21002_loyalty[i]]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale LOYALTY.21499 returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale LOYALTY.21499: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale LOYALTY.21499: AbortTran successful.\n");
					return (ISS_SUCCESS);
				}
				else L.Log("Info: ProcSale LOYALTY.21499: successful.\n");
				elements++;
			
			}
		
		
		}
	}
	if (!header.voided) 
		APos[posn].p_salesval+=total;
	sold=total;
   
if (!header.voided) {
/********************    2000    ************************/
char truser[9];
//long q1,v1;
	for(i=1;i<=c_2000;i++) {//
		//memcpy(E2000[i].DrwIDUserID,transuser,9);
		strcpy(truser,transuser);
		truser[strlen(transuser)]=0;
		strcpy(E2000[i].DrwIDUserID,truser);
		memcpy(E2060.DrwIDUserID,transuser,9);
		E2000[i].DrwIDDate=APos[posn].drwdate;
		E2000[i].DrwIDTime=APos[posn].drwtime;
		E2060.DrwIDDate=APos[posn].drwdate;
		E2060.DrwIDTime=APos[posn].drwtime;
		E2000[i].ReturnMode=return_mode;
		rounded=total;//Initially not rounded
		if ((CashRounding)&&(E2000[i].TenderID==1)) {//Do rounding
			rounded=((total+CashRounding/2)/CashRounding)*CashRounding;
			totrounddiff+=rounded-total;
		}
		
		//TenderTotals.AddFigures(E2000[i].TenderID,tendval[i],1);
		//AddTendering(posn,E2000.TenderID,extprice);
		AddTendering(posn,(long) E2000[i].TenderID,tendval[i]);
		retval=SafeSendHLogElement(posn,&handle,2000,time,0x0000,sizeof(E2000[i]),(char*)&E2000[i]);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcSale.2000 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.2000: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcSale.2000: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("Info: ProcSale.2000: successful.\n");
		elements++;
		//if(E2000_21499[2*i-1].Record_Type!=-1) {
		if(tender_2000[i]!=-1 && E2000_21499[tender_2000[i]].Record_Type!=-1) {
				retval=SafeSendHLogElement(posn,&handle,21499,time,0x0000,sizeof(E2000_21499[tender_2000[i]])
						,(char*)&E2000_21499[tender_2000[i]]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21499 Tender details returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21499.Tender details: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21499.Tender details: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21499.Tender details: successful.\n");
				elements++;
				if(E2000[i].TenderID==4 || E2000[i].TenderID==3){//Csak bankk�rty�n�l van k�t 21499-es rekord
					retval=SafeSendHLogElement(posn,&handle,21499,time,0x0000,sizeof(E2000_21499[tender_2000[i]+1])
							,(char*)&E2000_21499[tender_2000[i]+1]);
					if (retval!=ISS_SUCCESS) {
						ISS_T_REPLY ret2;
						L.LogE("ERROR: ProcSale.21499 Tender details Y returned: %d -> abort tran & retry!\n",retval);
						if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
							L.LogE("ERROR: ProcSale.21499.Tender details Y: AbortTran: %d!\n",ret2);
						}
						else L.Log("Info: ProcSale.21499.Tender details Y: AbortTran successful.\n");
						return(retval);
					}
					else L.Log("Info: ProcSale.21499.Tender details Y: successful.\n");
					elements++;
				}
		}
	}
/*********************    2060      ***************************/
	if (header.chg_amnt>0) {//change was given (even after rounding applied)
		LongVal2Cobol6(E2060.ChangeGiven,header.chg_amnt*100);
		retval=SafeSendHLogElement(posn,&handle,2060,time,0x0000,sizeof(E2060)
			,(char*)&E2060);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcSale.2600 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.2600: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcSale.2600: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("Info: ProcSale.2600: successful.\n");
		elements++;
		//AddTendering(posn,1,-rounded); //Subtract rounded cash given back
		AddTendering(posn,E2060.TenderID,-1*header.chg_amnt); //Subtract cash given back
	}
} //voided transakci�
else {
	TElem4000	E4000;
	PreFill(&E4000,	4000);
	sprintf(E4000.AuthUser,"%d",header.cashier);
	E4000.AuthRefused=FALSE;
	sprintf(E4000.Function,"%s","TAWW-AUTHVOID");
	retval=SafeSendHLogElement(posn,&handle,4000,time,0x0000,sizeof(E4000)
		,(char*)&E4000);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcSale.4000 returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcSale.4000: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcSale.4000: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("Info: ProcSale.4000: successful.\n");
	elements++;
}
	/*********************   21099   *******************/
	for(i=3+cust_cnt;i<=c_21099;i++) {
				retval=SafeSendHLogElement(posn,&handle,21099,time,0x0000,sizeof(E21099[i])
					,(char*)&E21099[i]);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21099.fiscal_date returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21099.fiscal_date: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21099.fiscal_date: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21099.fiscal_date: successful.\n");
				elements++;
	}
	if (cust_cnt>0) cust_cnt=3;

/**********************    21020     ***********************************/	
	//Sale perf record
	LongVal2Cobol6(E21020.SalesTotal,sold*100);
	LongVal2Cobol6(E21020.ItemSoldTtl,(sold+disc_total+off_total)*100);
	LongVal2Cobol6(E21020.RoundingTtl,0);
	LongVal2Cobol6(E21020.DiscountTtl,disc_total*100);
	LongVal2Cobol6(E21020.OfferTtl,off_total*100);
	if (CashRounding) {//Rounding active
		LongVal2Cobol6(E21020.TenderTotal,(tenders)*100);
		LongVal2Cobol6(E21020.RoundingTtl,0);
	}
	else LongVal2Cobol6(E21020.TenderTotal,(tenders)*100);
	E21020.ItemCnt=c_21001;
	E21020.DiscountCnt=disc_counter;
	E21020.OfferCnt=c_21003;
	E21020.ItemSoldCnt=c_21001;
	E21020.TenderCnt=c_2000;
	E21020.ReturnMode=return_mode;
	retval=SafeSendHLogElement(posn,&handle,21020,time,0x0000,sizeof(E21020)
		,(char*)&E21020);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcSale.21020 returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcSale.21020: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcSale.21020: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("Info: ProcSale.21020: successful.\n");
	elements++;
	if (header.voided) {  //Voided tranzakci�
		ISS_T_REPLY ret2;
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.4000: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.4000: AbortTran successful.\n");
				return -98;
	}
/***********************   3023     ******************/
	//dept perf records
	E3023.CountSales=items;
	E3023.ItemKey;

	if(return_mode){//Ha vissz�ru
		PreFill(&E21024,21024);
		for (i=1;i<10001;i++) {
			if (dept_id_sum[i]!=0) {
				E21024.Dept=i;
				E21024.SalesCnt=dept_id_counter[i];
				E21024.ItemScanCnt=dept_id_counter[i];
				LongVal2Cobol6(E21024.SalesVal,dept_id_sum[i]*100);	
				retval=SafeSendHLogElement(posn,&handle,21024,time,0x0000,sizeof(E21024)
					,(char*)&E21024);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21024 returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21024: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21024: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21024: successful.\n");
				elements++;
			}
		}
		LongVal2Cobol6(E21024.SalesVal,total*100);
		E21024.SalesCnt=items;
		E21024.ItemScanCnt=items;
		for(extprice=1;extprice<8;extprice++) {
			if (ItemDept[extprice]!=0) {
				E21024.Dept=ItemDept[extprice];
				retval=SafeSendHLogElement(posn,&handle,21024,time,0x0000,sizeof(E21024)
					,(char*)&E21024);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21024 returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21024: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21024: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21024: successful.\n");
				elements++;
			}
		}

	}
	else {// Ha elad�s
		for (i=1;i<10001;i++) {
			if (dept_id_sum[i]!=0) {
				E3023.DeptNo=i;
				E3023.CountSales=dept_id_counter[i];
				LongVal2Cobol6(E3023.ValueSales,dept_id_sum[i]*100);	
				retval=SafeSendHLogElement(posn,&handle,3023,time,0x0000,sizeof(E3023)
					,(char*)&E3023);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.3023 returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.3023: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.3023: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.3023: successful.\n");
				elements++;
			}
		}
		LongVal2Cobol6(E3023.ValueSales,total*100);
		E3023.CountSales=items;
		for(extprice=1;extprice<8;extprice++) {
			if (ItemDept[extprice]!=0) {
				E3023.DeptNo=ItemDept[extprice];
				retval=SafeSendHLogElement(posn,&handle,3023,time,0x0000,sizeof(E3023)
					,(char*)&E3023);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.3023 returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.3023: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.3023: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.3023: successful.\n");
				elements++;
			}
		}
	}
	//close tran
	
	element.element_time=time+trantime;
	element.element_num=elements;
	retval=iss46_hlog_end_trans(handle,Date,(unsigned short)elements);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcSale.EndTran returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcSale.EndTran: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcSale.EndTran: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("Info: ProcSale.EndTran: successful.\n");
	APos[posn].lasttrandate=Date;
	APos[posn].lasttrantime=time+trantime;

	GItemTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegGItemTotals);
	SaveStatus(false,posn);
	memcpy(itemcollector,&posn,sizeof(long));
	GItemTotals.InitCursor();
	int index;
	for (index=0;index<GItemTotals.GetCount();index++) {
		GItemTotals.GetDataByIndex(index,itemcollector);
		gdec2s((unsigned char *)(itemcollector+sizeof(long)),6,s);
		L.Log("Sales for item %s on till %d! Quantity=%d Value=%d.\n"
			,s,*((long *)(itemcollector)),*((long *)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)))
			,*((long *)(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6))));
	}
/*	sprintf(atoken,"INSERT INTO `Infodesk2`.`namos_conv` (`orig_date`, `trans_id`, `filename`, `pos_no`) VALUES ('%s', '%s', '%s', '%d')",Borig_date,header.Lautoken,short_F,header.sid);
	if(Insert_MYSQL(atoken)<0)
		L.LogE("ERROR -- Failed to write the transaction details to the database!\n");
*/
	r.SetValueDW("Fuel.System.Close",0); //Engedem a benzink�t f�jlok feldolgoz�s�t	
	open=false;
	return ISS_SUCCESS;
}

/****************************************************************************************************************************************/






long ProcSale(FILE *F, bool acis) //Return value: 0=no_retries others=retries
{
	TElem5100	E5100;
	TElem21001	E21001;
	TElem2000	E2000;
	TElem2060	E2060;
	TElem21099V2	E21099_16,E21099_51,E21099_13;
	TElem21020	E21020;
	TElem3023	E3023;
//	TElem4100	E4100;

	PreFill(&E5100,	5100);
	PreFill(&E21001,	21001);
	PreFill(&E2000,	2000);
	PreFill(&E2060,	2060);
	PreFill(&E21099_13,	21099);
	PreFill(&E21099_51,	21099);
	PreFill(&E21099_16,	21099);
	PreFill(&E21020,	21020);
	PreFill(&E3023,	3023);
//	PreFill(&E4100,	4100);

	E21001.DeptId=ItemDept[0];

	SINT16 hp;
	ISS_T_LOG_BLOCK_HANDLE  handle;
	SYSTEMTIME st;
	ISS_T_REPLY retval=0;
	if ((retval=iss46_hlog_get_current_hp(&hp))!=ISS_SUCCESS) {
		L.LogE("ERROR: ProcSale.get_current_hp: %d, retval=%d\n",hp,retval);
		L.LogE("==> ProcSale will be aborted or retried!\n");
		return retval;
	}
	L.Log("Info: ProcSale.get_current_hp: %d\n",hp);
	bool open=false; //transaction opened
	long tenders;	//payment records logged already (to reject sales too)
	long sold;		//total sales
	long total=0;		//current balance
	long rounded;	//payment value rounded (if cash)
	long totrounddiff=0; //Total rounding difference collected by all payment lines
	long posn;		//pos number
	long trantime;	//transaction total time
	long availtime; //available time - time left from total transaction time
	long elements;	//counting logged elements
	long items;		//count of item records
	long weight;
	long quantity;
	long prevquantity;
	long unitprice;
	long extprice;
	bool ean2x=false;	//Will be true for EAN21-29
	char s[64];
	char oean[14];
	char custcode[21];
	unsigned char custno[12];
	bool gotcust;
	char transuser[9]={0,0,0,0,0,0,0,0,0};
	ISS_T_LOG_ELEM_ID element;
//	ISS_T_INT_DATE date;
	ISS_T_INT_TIME time;
	s[62]=0; //protection to freely use s[63]
	ISS_GDECIMAL_6 ean;
	ISS_GDECIMAL_6 eannosubst;
	char progress=0;
	char itemcollector[sizeof(ISS_GDECIMAL_6)+3*sizeof(long)];
	long qq,vv;

	while (fgets(s,62,F)) {
		switch(*s|32) {
		case 'h'://header
			if ((strlen(s)<55)||(s[54]<' ')||(s[53]<' ')) {
				L.LogE("ERROR: ProcSale.PreProc: Receipt opening record is too small! -> Rejected!\n");
				break;
			}
			if (progress) {
				L.LogE("ERROR: ProcSale.PreProc: Another receipt opening record! -> Rejected!\n");
				break;
			}
			progress++;
			break;
		case 'i'://Item
			if (progress==0) {
				L.LogE("ERROR: ProcSale.PreProc: Item record without opening (header) record! -> Rejected!\n");
				break;
			}
			if (progress==3) {
				L.LogE("ERROR: ProcSale.PreProc: Item record between payment records! -> Rejected!\n");
				break;
			}
			if ((strlen(s)<37)||(s[36]<' ')||(s[35]<' ')) {
				L.LogE("ERROR: ProcSale.PreProc: Item record is too small! -> Rejected!\n");
				break;
			}
			if (strncmp(s+27,"0000000000",10)!=0) total++; //counting nonzero items only

			progress=2;
			break;
		case 'p'://payment
			if (progress<2) {
				L.LogE("ERROR: ProcSale.PreProc: Payment record without header and/or item record! -> Rejected!\n");
				break;
			}
			if ((strlen(s)<13)||(s[12]<' ')||(s[11]<' ')) {
				L.LogE("ERROR: ProcSale.PreProc: Payment record is too small! -> Rejected!\n");
				break;
			}
			progress=3;
			break;
		default://unknown, or not in this file
			L.LogE("ERROR: ProcSale.PreProc: No other record types than 'H','I' & 'P' allowed in this file!\n");
		}
	}
	fseek(F,0,SEEK_SET);
	if (progress!=3) {
		L.LogE("ERROR: ProcSale.PreProc: One 'H'eader and at least one 'I'tem and 'P'ayment record is neccesary!\n");
		L.LogE("  ===> For this reason the transaction file cannot be processed! Here is the dump of the file:\n");
		long cnt=1;
		while (fgets(s,62,F)) {
			while ((strlen(s)>0)&&(s[strlen(s)-1]<32)) s[strlen(s)-1]=0;
			L.LogE("  ===> Line #%d: %s\n",cnt++,s);
		}
		return 0;
	}
	if (!total) {
		L.LogE("Info: ProcSale.PreProc: Transaction has zero item total therefore it has been dropped.\n");
		return 0;
	}
	while (fgets(s,62,F)) {
		switch(*s|32) {
		case 'h'://header
			if (open) {
				L.LogE("ERROR: ProcSale: Another receipt opening record! -> Rejected!\n");
				break;
			}
			if ((strlen(s)<55)||(s[54]<' ')||(s[53]<' ')) {
				L.LogE("ERROR: ProcSale: Receipt opening record is too small! -> Rejected!\n");
				break;
			}
			posn=(s[1]-'0')*10+s[2]-'0'-1;
			if ((posn>=PosCount)||(posn<0)) {
				L.LogE("ERROR: ProcSale: POS number is bad! (%d) -> File rejected!\n",posn+1);
				return ISS_SUCCESS;
			}
			if (!(*(APos[posn].user))) {//noone is currently logged on
				if ((posn>2)||((AutoActions&0x00000001)!=0)) {
					if (posn<=2) L.LogE("Warning: ProcSale: Sequence error - noone has been logged on (PoS #%d)!\n",posn+1);
					char logon[32]; //Buser--date--=time=p# from Hp#user--date--=time=...
					long retval;
					if (posn<=2) L.LogE("==> Now performing autologon by sales transaction header: %s",s);
					else L.Log("Performing autologon by sales transaction header (pos #%d): %s",posn+1,s);
					logon[0]=s[3];
					logon[1]=s[4];
					logon[2]=s[5];
					logon[3]=s[6];
					memcpy(logon+4,s+7,14);
					logon[18]=s[1];
					logon[19]=s[2];
					logon[20]=0;
					if ((retval=ProcLogOn(logon))!=0) {
						L.LogE("ERROR: ProcSale.AutoLogOn: ProcLogOn returned %d!\n",retval);
						return 0;//no retries
					}
				}
				else {
					L.LogE("ERROR: ProcSale: Transaction sequence error - Noone has been logged on (PoS #%d)!\n",posn+1);
					return 0;//no retries
				}
			}
			else {
				char tranuser[5];
				memcpy(tranuser,s+3,4);
				CleanUserID(tranuser);
				if ((memcmp(APos[posn].user,tranuser,4))!=0) {
					char u[5];
					memcpy(u,APos[posn].user,4);
					u[4]=0;
					tranuser[4]=0;
					if ((AutoActions&0x00000002)!=0) {
						char logon[32]; //user--date--=time=p# from Hp#user--date--=time=...
						long retval;
						long len;
						L.LogE("Warning: ProcSale: Not the logged on user does sales! (Selling:%s Logged_on:%s))! (PoS #%d)\n"
							,tranuser,u,posn+1);
						L.LogE("==> Now performing automatic logoff/logon by sales transaction header: %s\n",s);
						memcpy(logon+4,s+7,14);
						logon[18]=s[1];
						logon[19]=s[2];
						logon[20]=0;
						len=strlen(APos[posn].user);
						memset(logon,'0',4);
						memcpy(logon+(4-len),APos[posn].user,len);
						if ((retval=ProcLogOff(logon))!=0) {
							L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOff returned %d!\n",retval);
							return retval;//retry allowed
						}
						logon[0]=s[3];
						logon[1]=s[4];
						logon[2]=s[5];
						logon[3]=s[6];
						if ((retval=ProcLogOn(logon))!=0) {
							L.LogE("ERROR: ProcSale.AutoLogOffOn: ProcLogOn returned %d!\n",retval);
							return retval;//retry allowed
						}
					}
					else {
						L.LogE("ERROR: ProcSale: Not the logged on user does sales! (Selling:%s Logged_on:%s))! (PoS #%d)\n"
							,tranuser,u,posn+1);
						return 0;//no retries
					}
				}
			}
			//Advancing performance period(s) if needed
			SysTimeFromS14(&st,s+7);
			Date=DateYMD2Int(st.wYear,st.wMonth,st.wDay);
			time=st.wHour*3600+st.wMinute*60+st.wSecond;

			CheckTranDateTime(&Date,&time,posn);

			sprintf(E21099_51.DataEnter,"%04d.%02d.%02d",st.wYear,st.wMonth,st.wDay);	//Fiscal date gets stored
			memset(E21099_51.DataEnter+10,' ',11);

			while (Check4NewPerfSlot(posn,Date,time)); //Advancing performance slots until it's necessary

			//Checks done before opening a new transaction
			SINT32        transno;
			ISS_T_BOOLEAN inprogress;
			SINT16        element_num;
// L.LogE("Kapd be : %d", APos[posn].sid);
			retval=iss46_hlog_get_curr_trans_num(APos[posn].sid,&transno,&inprogress,&element_num);
			L.Log("Info: ProcSale.check_before_start.get_curr_trans: tr=%d, open=%d, elem=%d\n"
				,transno,inprogress,element_num);
			if (retval!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.check_before_start.get_curr_trans: Bad return value! (%d)\n",retval);
				return retval;
			}
			if (inprogress) {
				L.LogE("ERROR: ProcSale.check_before_start: Transaction must not be open at this point!\n");
				return(-1);
			}
			APos[posn].lasttran++;//Preparing new transaction's number
			if (APos[posn].lasttran!=transno) {//checks whether ISS thinks the same
				L.LogE("Warning: ProcSale.check_before_start: Transaction numbers doesn't match!\n");
				L.LogE("==> APos[%d].lasttran=%d ISSLOG.transno=%d SID=%d\n"
					,posn,APos[posn].lasttran,transno,APos[posn].sid);
				APos[posn].lasttran=transno; //Correction by ISS
			}

			//Preparing opening a new transaction
			element.element_num=1;
			element.element_time=time;
//			strcpy(E5100.TransUser,APos[posn].user);
			memcpy(transuser,APos[posn].user,4);

			if ((retval=iss46_hlog_start_block(APos[posn].sid,hp,APos[posn].lasttran,Date,time
				,0,0x00018000,element,9,transuser,&handle))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcSale.start returned: %d -> retrying!\n",retval);
				return(retval);
			}
			else L.Log("ProcSale.start: Successful.\n");

			elements=1;
			if ((retval=iss46_hlog_send_element(handle,5100,time,0x0000,sizeof(E5100),(char*)&E5100))
				!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				L.LogE("ERROR: ProcSale.5100 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.5100: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.5100: AbortTran successful.\n");
				return(retval);
			}
			else L.Log("Info: ProcSale.5100: Successful.\n");
			
//			memcpy(E21001.TransUser,E5100.TransUser,9);			//Prefill transuser detail
//			memcpy(E2000.TransUser,E5100.TransUser,9);
			memcpy(E2000.DrwIDUserID,transuser,9);
//			memcpy(E2060.TransUser,E5100.TransUser,9);
			memcpy(E2060.DrwIDUserID,transuser,9);
//			memcpy(E21099.TransUser,E5100.TransUser,9);
//			memcpy(E21020.TransUser,E5100.TransUser,9);
//			memcpy(E3023.TransUser,E5100.TransUser,9);
			E2000.DrwIDDate=APos[posn].drwdate;
			E2000.DrwIDTime=APos[posn].drwtime;
			E2060.DrwIDDate=APos[posn].drwdate;
			E2060.DrwIDTime=APos[posn].drwtime;
			E21099_16.CurrDate=Date;
			E21099_51.CurrDate=Date;
			E21099_13.CurrDate=Date;
			E21099_16.TransLoc=APos[posn].sid/2;
			E21099_51.TransLoc=APos[posn].sid/2;
			E21099_13.TransLoc=APos[posn].sid/2;
			E21099_16.DataType=16;
			E21099_51.DataType=51;
			E21099_13.DataType=13;
			E21099_16.TransStore=StoreNum;
			E21099_51.TransStore=StoreNum;
			E21099_13.TransStore=StoreNum;

			open=true;
			tenders=0;
			elements++;
			items=0;
			sold=0;
			total=0;
			//Getting total transaction time
			s[63]=s[29];
			s[29]=0;
			trantime=atol(s+21);
			if (trantime==0) trantime=ZeroTimeTxnTime;
			availtime=trantime;
			s[29]=s[63];
			//Extracting receipt number
			s[63]=s[35];
			s[35]=0;
			//LongVal2Cobol10(E21099.DataEnter,atol(s+29)); //Old version
			memset(E21099_16.DataEnter,' ',21);
			sprintf(E21099_16.DataEnter,"%d",atol(s+29));
			E21099_16.DataEnter[strlen(E21099_16.DataEnter)]=' ';
			s[35]=s[63];
			//Extracting customer number
			memcpy(custcode,s+35,20);
			custcode[20]=0;
			while ((strlen(custcode)>0)&&(custcode[strlen(custcode)-1]==' '))
				custcode[strlen(custcode)-1]=0;
			if (s2gdec(custno,10,custcode)) { //conversation succeeded
				if (gdecequ0(custno,10)) gotcust=false; //customer 0 will not be logged
				else {//custno!=0, should be logged
					memset(E21099_13.DataEnter,'0',21);
					memcpy(E21099_13.DataEnter+(21-strlen(custcode)),custcode,strlen(custcode));
					//GDec102Cobol10(E21099B.DataEnter,custno);
					gotcust=true;
				}
			}
			else gotcust=false; //conversation failed
			APos[posn].p_custcnt++;

			break;
		case 'i'://item
			if (!open) {
				L.LogE("ERROR: ProcSale: Item record without opening (header) record! -> Rejected!\n");
				break;
			}
			if (tenders) {
				L.LogE("ERROR: ProcSale: Item record between payment records! -> Rejected!\n");
				break;
			}
			if ((strlen(s)<37)||(s[36]<' ')||(s[35]<' ')) {
				L.LogE("ERROR: ProcSale: Item record is too small! -> Rejected!\n");
				break;
			}
			gdec_init_z(ean,6,0,iss_gdec_round_normal);
			gdec_init_z(eannosubst,6,0,iss_gdec_round_normal);
			s[63]=s[21];
			s[21]=0;
			sprintf(oean,"%s",s+8);
			gdec_from_ascii(eannosubst,s+1);
			DoItemSubst(s+8,false);
			gdec_from_ascii(ean,s+1);
			ean2x=((s[8]=='2')&&(s[9]>'0')&&(s[9]<='9'));
			s[21]=s[63];
			s[63]=s[27];
			s[27]=0;
			quantity=atol(s+21);
			weight=10*atol(s+21);
			s[27]=s[63];
			s[63]=s[37];
			s[37]=0;
			if (acis)
				extprice=atol(s+27)/100;
			else
				extprice=atol(s+27);
			s[37]=s[63];
			if (quantity==0) {
				ISS_T_REPLY ret2;
				L.LogE("Warning: ProcSale: Item has zero quantity -> aborting transaction!\n");
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.Item.QuantityEQ0: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.Item.QuantityEQ0: AbortTran successful.\n");
				return(retval);
			}
			if (TaxRate==-1) GetTaxRate(&TaxRate,ean);
			if (extprice<=1000000) unitprice=(((extprice*2000)/quantity)+1)/2;
			else {
				long l=extprice/quantity;
				long r=((((extprice-(l*quantity))*2000)/quantity)+1)/2;
				unitprice=l*1000+r;
			}
/*			if (!(ItemTotals.GetCounters(*((long *)&(ean[2])),NULL,&prevquantity))) 
				prevquantity=0;
			if (acis)
				ItemTotals.AddFigures(*((long *)&(eannosubst[2])),extprice,10*quantity);
			else
				ItemTotals.AddFigures(*((long *)&(eannosubst[2])),extprice,quantity);
*/

			memcpy(itemcollector,&posn,sizeof(long));
			memcpy(itemcollector+sizeof(long),eannosubst,sizeof(ISS_GDECIMAL_6));
			if (GItemTotals.GetData(itemcollector,NULL,false)) {
				L.LogE("This is the first sale of this ITEM %s at this till %d.\n",oean,posn+PosSIDBase/2);
				qq=0;vv=0;
			}
			else {
				qq=*((long*)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)));
				vv=*((long*)(itemcollector+sizeof(long)*2+sizeof(ISS_GDECIMAL_6)));
				L.Log("Total sales for item %s on this till %d! ValueMy=%d QuantityMy=%d\n"
						,s+8,posn+PosSIDBase/2,vv,qq);
			}
			if (acis)
				quantity=10*quantity;
			
			memcpy(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6),&quantity,sizeof(long));
			memcpy(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6),&extprice,sizeof(long));
			GItemTotals.AddFigures(itemcollector);


			GDec62Cobol6(E21001.ItemCode,ean);//itemcode ok
			E21001.TaxID=TaxRate;//taxrate ok
			if (AmountAsQuantity) {
				switch (QuantityDigitLoss) {
				case 2:
					prevquantity%=100;
					if (prevquantity>=50) prevquantity-=100;
					quantity=(quantity+prevquantity+50)/100;
					break;
				case 1:
					prevquantity%=10;
					if (prevquantity>=5) prevquantity-=10;
					quantity=(quantity+prevquantity+5)/10;
					break;
				case 0:
					break;
				default:
					L.LogE("ERROR: ProcSale: QuantityDigitLoss is invalid -> 0 used instead!\n");
				}
				E21001.Quantity=quantity;
				weight=0;
				E21001.EANType=1;
			}
			else {
				quantity=1;
				//E21001.SpecialItem=1;
				E21001.EANType=(ean2x?5:1);
			}
			LongVal2Cobol4(E21001.Weight,weight);//Weight ok
			LongVal2Cobol6(E21001.ExtPrice,extprice*100);//Extprice ok
			if (PriceByCentiLiters==0) {
				LongVal2Cobol6(E21001.PriceFromPlu,unitprice*10);//pricefromplu somewhat ok
				LongVal2Cobol6(E21001.UnitPrice,unitprice*10);//Unitprice ok-ish
			}
			else {
				LongVal2Cobol6(E21001.PriceFromPlu,unitprice/10);//pricefromplu somewhat ok
				LongVal2Cobol6(E21001.UnitPrice,unitprice/10);//Unitprice ok-ish
			}
			//ReportGroupt left to be 0

			retval=SafeSendHLogElement(posn,&handle,21001,time,0x0000,sizeof(E21001)
				,(char*)&E21001);
			if (retval!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				L.LogE("ERROR: ProcSale.21001 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.21001: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.21001: AbortTran successful.\n");
				return (ISS_SUCCESS);
			}
			else L.Log("Info: ProcSale.21001: successful.\n");

			if (availtime>5) {
				availtime-=5;
				time+=5;
				APos[posn].p_regtime+=5;
				APos[posn].regtime+=5;
			}
			APos[posn].items+=quantity;
			APos[posn].p_itemcnt+=quantity;
			APos[posn].p_salesval+=extprice;
			sold+=extprice;
			total+=extprice;
			elements++;
			items+=quantity;

			break;
		case 'p'://payment
			if (!open) {
				L.LogE("ERROR: ProcSale: Payment record without opening (header) record! -> Rejected!\n");
				break;
			}
			if ((strlen(s)<13)||(s[12]<' ')||(s[11]<' ')) {
				L.LogE("ERROR: ProcSale: Payment record is too small! -> Rejected!\n");
				break;
			}
			s[63]=s[3];
			s[3]=0;
			E2000.TenderID=atol(s+1);
			s[3]=s[63];
			s[63]=s[13];
			s[13]=0;
			if (acis)
				extprice=atol(s+3)/100;//TenderValue
			else
				extprice=atol(s+3);//TenderValue
			s[13]=s[63];
			rounded=extprice;//Initially not rounded
			if ((CashRounding)&&(E2000.TenderID==1)) {//Do rounding
				rounded=((extprice+CashRounding/2)/CashRounding)*CashRounding;
				totrounddiff+=rounded-extprice;
			}
			LongVal2Cobol6(E2000.TenderVal,rounded*100);

			AddTendering(posn,E2000.TenderID,extprice);
			//AddTendering(posn,E2000.TenderID,rounded);

			retval=SafeSendHLogElement(posn,&handle,2000,time,0x0000,sizeof(E2000),(char*)&E2000);
			if (retval!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				L.LogE("ERROR: ProcSale.2000 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					L.LogE("ERROR: ProcSale.2000: AbortTran: %d!\n",ret2);
				}
				else L.Log("Info: ProcSale.2000: AbortTran successful.\n");
				return(retval);
			}
			else L.Log("Info: ProcSale.2000: successful.\n");
			if (availtime>=10) {
				availtime-=10;
				time+=10;
				APos[posn].p_tendertime+=10;
				APos[posn].tendertime+=10;
			}
			total-=extprice;
			elements++;
			tenders++;

			if (total<=0) {//There should be no more payments - finish trans
				time+=availtime;					//eat up remaining times
				APos[posn].p_tendertime+=availtime;
				APos[posn].tendertime+=availtime;

				//calculate change value
				if (CashRounding) {//apply rounding for change (always cash)
					rounded=(((totrounddiff-total)+CashRounding/2)/CashRounding)*CashRounding;
					//totrounddiff-=rounded+total;
				}
				else rounded=-total;
				if (rounded>0) {//change was given (even after rounding applied)
					LongVal2Cobol6(E2060.ChangeGiven,rounded*100);
					retval=SafeSendHLogElement(posn,&handle,2060,time,0x0000,sizeof(E2060)
						,(char*)&E2060);
					if (retval!=ISS_SUCCESS) {
						ISS_T_REPLY ret2;
						L.LogE("ERROR: ProcSale.2600 returned: %d -> abort tran & retry!\n",retval);
						if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
							L.LogE("ERROR: ProcSale.2600: AbortTran: %d!\n",ret2);
						}
						else L.Log("Info: ProcSale.2600: AbortTran successful.\n");
						return(retval);
					}
					else L.Log("Info: ProcSale.2600: successful.\n");
					elements++;
					//AddTendering(posn,1,-rounded); //Subtract rounded cash given back
					AddTendering(posn,1,total); //Subtract cash given back
				}
				//Fiscal date record in data entry (21099)
				retval=SafeSendHLogElement(posn,&handle,21099,time,0x0000,sizeof(E21099_51)
					,(char*)&E21099_51);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21099.fiscal_date returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21099.fiscal_date: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21099.fiscal_date: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21099.fiscal_date: successful.\n");
				elements++;
				if (gotcust) {//Customer number record in data entry (21099)
					//E21099B.DataType=12;
					retval=SafeSendHLogElement(posn,&handle,21099,time,0x0000,sizeof(E21099_13)
						,(char*)&E21099_13);
					if (retval!=ISS_SUCCESS) {
						ISS_T_REPLY ret2;
						L.LogE("ERROR: ProcSale.21099.custno returned: %d -> abort tran & retry!\n",retval);
						if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
							L.LogE("ERROR: ProcSale.21099.custno: AbortTran: %d!\n",ret2);
						}
						else L.Log("Info: ProcSale.21099.custno: AbortTran successful.\n");
						return(retval);
					}
					else L.Log("Info: ProcSale.21099.custno: successful.\n");
					elements++;
				}
				//Receipt number record in data entry (21099)
				retval=SafeSendHLogElement(posn,&handle,21099,time,0x0000,sizeof(E21099_16)
					,(char*)&E21099_16);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21099.receiptno returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21099.receiptno: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21099.receiptno: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21099.receiptno: successful.\n");
				elements++;
				//Sale perf record
				LongVal2Cobol6(E21020.SalesTotal,sold*100);
				LongVal2Cobol6(E21020.ItemSoldTtl,sold*100);
				if (CashRounding) {//Rounding active
					LongVal2Cobol6(E21020.TenderTotal,(sold+rounded+totrounddiff)*100);
					LongVal2Cobol6(E21020.RoundingTtl,(sold+totrounddiff)*100);
				}
				else LongVal2Cobol6(E21020.TenderTotal,(sold-total)*100);
				E21020.ItemCnt=items;
				E21020.ItemSoldCnt=items;
				E21020.TenderCnt=tenders;
				retval=SafeSendHLogElement(posn,&handle,21020,time,0x0000,sizeof(E21020)
					,(char*)&E21020);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.21020 returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.21020: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.21020: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.21020: successful.\n");
				elements++;
				//dept perf records
				E3023.CountSales=items;
				E3023.ItemKey;
				LongVal2Cobol6(E3023.ValueSales,sold*100);
				for(extprice=0;extprice<8;extprice++) {
					if (ItemDept[extprice]!=0) {
						E3023.DeptNo=ItemDept[extprice];
						retval=SafeSendHLogElement(posn,&handle,3023,time,0x0000,sizeof(E3023)
							,(char*)&E3023);
						if (retval!=ISS_SUCCESS) {
							ISS_T_REPLY ret2;
							L.LogE("ERROR: ProcSale.3023 returned: %d -> abort tran & retry!\n",retval);
							if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
								L.LogE("ERROR: ProcSale.3023: AbortTran: %d!\n",ret2);
							}
							else L.Log("Info: ProcSale.3023: AbortTran successful.\n");
							return(retval);
						}
						else L.Log("Info: ProcSale.3023: successful.\n");
						elements++;
					}
				}
				//close tran
				retval=iss46_hlog_end_trans(handle,Date,(unsigned short)elements);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					L.LogE("ERROR: ProcSale.EndTran returned: %d -> abort tran & retry!\n",retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						L.LogE("ERROR: ProcSale.EndTran: AbortTran: %d!\n",ret2);
					}
					else L.Log("Info: ProcSale.EndTran: AbortTran successful.\n");
					return(retval);
				}
				else L.Log("Info: ProcSale.EndTran: successful.\n");

//				retval=iss46_hlog_send_block(handle);
//				L.Log("sendb: %d\n",retval);

				APos[posn].lasttrandate=Date;
				APos[posn].lasttrantime=time;

				ItemTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegGItemTotals);
				SaveStatus(false,posn);
				open=false;
			}
//	long l;
/*			if ((l=ItemTotals.GetCount())!=0) {
		long id,v,q;
		bool show=false;
		while ((l)&&(!show)) {
			l--;
			ItemTotals.GetElement(l,&id,&v,&q);
			if ((q!=0)||(v!=0)) {show=true;break;}
		}
		if (show) {
			L.LogE("ERROR: ProcClose: The following sales records were missing from the closure file:\n");
			for (l=ItemTotals.GetCount()-1;l>=0;l--) {
				ItemTotals.GetElement(l,&id,&v,&q);
				if ((q!=0)||(v!=0)) 
					L.LogE("==> ItemCode=%d Value=%d Quantity=%d\n",id,v,q);
			}
		}
	}*/
			
			
			
			
			break;
		default://unknown, or not in this file
			L.LogE("ERROR: ProcSale: No other record types than 'H','I' & 'P' allowed in this file!\n");
		}
	}
	return ISS_SUCCESS;
}

long ProcLogOff(char *s)
{
	SYSTEMTIME st;
	char tranuser[5];
	//ISS_T_INT_DATE date;
	ISS_T_INT_TIME time;
	long posn=(s[18]-'0')*10+s[19]-'0'-1;
	L.Log("ProcLogOff -- POS number is : %d\n",posn);
	if ((posn>=PosCount)||(posn<0)) {
		L.LogE("ERROR: ProcSale: POS number is bad! (%d) -> File rejected!\n",posn+1);
		return ISS_SUCCESS;
	}
	L.Log("ProcLogOff -- User is : %s\n",APos[posn].user);
	if (!(*APos[posn].user)) {
		L.LogE("ERROR: ProcLogoff: No user logged onto the given location %d!\n",posn+1);
		return 0;
	}
	memcpy(tranuser,s,4);
	CleanUserID(tranuser);
	L.Log("ProcLogOff -- TranUser is : %s Apos.User:%s\n",tranuser,APos[posn].user);
	if ((memcmp(APos[posn].user,tranuser,4))!=0) {
		char u[5];
		memcpy(u,APos[posn].user,4);
		u[4]=0;
		tranuser[4]=0;
		L.LogE("ERROR: ProcLogoff: Not the same user (%s) logs off who was logged on (%s)! (PoS #%d)\n"
			,tranuser,u,posn+1);
		if ((RefusedActions&0x00000001)!=0) {
			L.LogE("==> Logoff transaction has been refused.\n");
			return 0;
		}
	}
	SysTimeFromS14(&st,s+4);
	Date=DateYMD2Int(st.wYear,st.wMonth,st.wDay);
	time=st.wHour*3600+st.wMinute*60+st.wSecond;

	CheckTranDateTime(&Date,&time,posn);

	while (Check4NewPerfSlot(posn,Date,time));

	TElem21026 E21026;  //sign off (*1)
	TElem2070  E2070;	//tender count (*n)
	TElem4300  E4300;	//sign on to sign off user perf detail (*1)
	TElem21097 E21097;	//finalising last performance slot (*1)
	PreFill(&E21026,21026);
	PreFill(&E2070,2070);
	PreFill(&E4300,4300);
	FillPerfElement(posn,E21097,Date,time);

	E21026.SignOnTransNo=APos[posn].signontransno;
	memcpy(E2070.DrwIDUserID,APos[posn].user,4);
	E2070.DrwIDDate=APos[posn].drwdate;
	E2070.DrwIDTime=APos[posn].drwtime;
	E4300.ItemKeyCodeTtl=APos[posn].items;
	E4300.RegTimeTtl=APos[posn].regtime;
	E4300.SignOnTtl=(Date-APos[posn].signondate)*86400+(time-APos[posn].signontime);
	E4300.TenderTimeTtl=APos[posn].tendertime;

	long elements;	//counting logged elements
	char transuser[9]={0,0,0,0,0,0,0,0,0};
	long cashiertotal=0;
	ISS_T_LOG_ELEM_ID element;
	SINT16 hp;
	ISS_T_LOG_BLOCK_HANDLE  handle;
	ISS_T_REPLY retval=0;
	if ((retval=iss46_hlog_get_current_hp(&hp))!=ISS_SUCCESS) {
		L.LogE("ERROR: ProcLogOff.get_current_hp: %d, retval=%d\n",hp,retval);
		L.LogE("==> ProcLogOff will be aborted or retried!\n");
		return retval;
	}
	L.Log("Info: ProcLogOff.get_current_hp: %d\n",hp);
	//Preparing opening a new transaction
	APos[posn].lasttran++;
	element.element_num=1;
	element.element_time=time;
	memcpy(transuser,APos[posn].user,4);

	if ((retval=iss46_hlog_start_block(APos[posn].sid,hp,APos[posn].lasttran,Date,time
		,0,0x00018000,element,9,transuser,&handle))!=ISS_SUCCESS) {
		L.LogE("ERROR: ProcLogoff.start returned: %d -> retrying!\n",retval);
		return(retval);
	}
	else L.Log("ProcLogoff.start: Successful.\n");

	retval=iss46_hlog_send_element(handle,21026,time,0x0000,sizeof(E21026),(char*)&E21026);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcLogoff.21026 returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,2))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcLogoff.21026: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcLogoff.21026: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("ProcLogoff.21026: successful.\n");
	elements=2; //prepared next element's number
	//Tenders list comes now:
	L.Log("Info: ProcLogoff: Sending tenders from drawer. Total number of tender elements=%d.\n"
		,APos[posn].tenders);

	for (long l=0; l<APos[posn].tenders; l++) {//tenders loop
		E2070.TenderID=APos[posn].tender[l].id;
		E2070.TenderCnt=APos[posn].tender[l].cnt;
		LongVal2Cobol6(E2070.TenderVal,APos[posn].tender[l].val*100);

		retval=SafeSendHLogElement(posn,&handle,2070,time,0x0000,sizeof(E2070)
			,(char*)&E2070);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcLogoff.2070 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcLogoff.2070: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcLogoff.2070: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("ProcLogoff.2070: TenderID=%d val=%d cnt=%d successful.\n"
			,APos[posn].tender[l].id,APos[posn].tender[l].val,APos[posn].tender[l].cnt);

		elements++;

		TenderTotals.AddFigures(APos[posn].tender[l].id,APos[posn].tender[l].val);
		cashiertotal+=APos[posn].tender[l].val;
	}

	CashierTotals.AddFigures(*((long *)APos[posn].user),cashiertotal);
	TenderTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegTenderTotals);
	CashierTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegCashierTotals);

	if ((NoPerfLogMask&0x0000002)==0) {
		retval=SafeSendHLogElement(posn,&handle,4300,time,0x0000,sizeof(E4300)
			,(char*)&E4300);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcLogoff.4300 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcLogoff.4300: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcLogoff.4300: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("ProcLogoff.4300: successful.\n");
		elements++;
	}
	else {
		L.Log("ProcLogoff.4300: skipped - logging performance records are disabled.(L2)\n");
	}

	if ((NoPerfLogMask&0x00000001)==0) {
		retval=SafeSendHLogElement(posn,&handle,21097,time,0x0000,sizeof(E21097)
			,(char*)&E21097);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			L.LogE("ERROR: ProcLogoff.21097 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
				L.LogE("ERROR: ProcLogoff.21097: AbortTran: %d!\n",ret2);
			}
			else L.Log("Info: ProcLogoff.21097: AbortTran successful.\n");
			return(retval);
		}
		else L.Log("ProcLogoff.21097: successful.\n");
		elements++;
	}
	else {
		L.Log("ProcLogoff.21097: skipped - logging performance records are disabled.(L1)\n");
	}

	//close tran
	retval=iss46_hlog_end_trans(handle,Date,(unsigned short)elements);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcLogoff.EndTran returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcLogoff.EndTran: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcLogoff.EndTran: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("ProcLogoff.EndTran: successful.\n");

//	retval=iss46_hlog_send_block(handle);
//	L.Log("ProcLogoff.sendb: %d\n",retval);
	L.Log("PROCLOGOFF - Pos: %d transaction details! User: %d Total sale: %d Total count: %d \n",APos[posn].sid,APos[posn].user,APos[posn].p_salesval,APos[posn].p_itemcnt);
	APos[posn].lasttrandate=Date;
	APos[posn].lasttrantime=time;

	//Gotta selete record from LLRSGN
	ISS_T_REPLY    rc,rc2;
    UINT16         buf;
	ISS_T_DB_KEY key[2];
	SINT32 keyloc=APos[posn].sid/2;
	char keyuser2[10]={0,0,0,0,0,0,0,0,0,0};
    key[0].field_name           = "LOCATION_ID";
    key[0].field_length         = 4;
    key[0].field_value          = &keyloc;
    key[0].field_value_unknown  = FALSE;
    key[1].field_name           = "USER_ID";
    key[1].field_length         = 9;
    key[1].field_value          = keyuser2;
    key[1].field_value_unknown  = FALSE;
    if ( (rc = ReadISS(&buf,TRUE,"LLRSGN",2,key,ISS_C_DB_AFTER_OR_EQUAL,ISS_C_DB_EXCLUSIVE_LOCK
		,"LOCATION_ID",4,&keyloc,FALSE)) == ISS_SUCCESS) {
		if (keyloc==APos[posn].sid/2) {
			L.Log("Info: Proclogoff.llrsgn.FindLocnRec: Found record for location %d!\n",keyloc);
			//Do the deletion
			if ((rc=iss_db_delete_record(&buf)) != ISS_SUCCESS)
				L.LogE("ERROR: Proclogoff.llrsgn.DelLocnRec: Delete failed! Error code=%d\n",rc);
		}
		else {
			L.Log("Info: Proclogoff.llrsgn.FindLocnRec: No record for location %d!\n"
				,APos[posn].sid/2);
			if ((rc=iss_db_free_buffer(&buf))!=ISS_SUCCESS) 
				L.Log("Info: Proclogoff.llrsgn.freebuf: Failed with %d!\n",rc);
		}
	}
	else {
		L.LogE("ERROR: Proclogoff.llrsgn: Get record failed with error code %d!\n",rc);
	}
	//LLTDYN now
	if ( (rc = ReadISS(&buf,FALSE,"LLTDYN",1,key,ISS_C_DB_EQUAL,ISS_C_DB_EXCLUSIVE_LOCK
		,NULL,0,NULL,FALSE)) != ISS_SUCCESS) {
		L.LogE("ERROR: Proclogoff.lltdyn.FindLocnRec: Can't get record for location %d! Result=%d\n"
			,keyloc,rc);
	}
	else {
		char ts='N';
		rc=iss_db_update_field(buf, "TRADING_STAT",sizeof(ts),0,&ts,FALSE);
		if (rc!=ISS_SUCCESS) {
			L.LogE("ERROR: Proclogoff.lltdyn.updfield: Update field failed! Error code %d!\n",rc);
		}
		else {
			if ((rc=iss_db_write_buffer(&buf))!=ISS_SUCCESS) {
				L.LogE("ERROR: Proclogoff.lltdyn.writebuf: Write buffer failed! Error code %d!\n",rc);
			}
		}
	}
	//LLRDYN now
	if ( (rc = ReadISS(&buf,FALSE,"LLRDYN",1,key,ISS_C_DB_EQUAL,ISS_C_DB_EXCLUSIVE_LOCK
		,NULL,0,NULL,FALSE)) != ISS_SUCCESS) {
		L.LogE("ERROR: Proclogoff.llrdyn.FindLocnRec: Can't get record for location %d! Result=%d\n"
			,keyloc,rc);
	}
	else {
		rc=iss_db_update_field(buf, "LAST_SIGN_OFF_DATE",sizeof(Date),0,&Date,FALSE);
		if (rc!=ISS_SUCCESS) {
			L.LogE("ERROR: Proclogoff.llrdyn.updfield1: Update field failed! Error code %d!\n",rc);
		}
		if (rc==ISS_SUCCESS) {
			rc=iss_db_update_field(buf, "LAST_SIGN_OFF_TIME",sizeof(time),0,&time,FALSE);
			if (rc!=ISS_SUCCESS) {
				L.LogE("ERROR: Proclogoff.llrdyn.updfield2: Update field failed! Error code %d!\n",rc);
			}
		}
		if (rc==ISS_SUCCESS) {
			rc=iss_db_update_field(buf, "LAST_USER_ID",9,0,transuser,FALSE);
			if (rc!=ISS_SUCCESS) {
				L.LogE("ERROR: Proclogoff.llrdyn.updfield3: Update field failed! Error code %d!\n",rc);
			}
		}
		if (rc==ISS_SUCCESS) {
			if ((rc=iss_db_write_buffer(&buf))!=ISS_SUCCESS) {
				L.LogE("ERROR: Proclogoff.llrdyn.writebuf: Write buffer failed! Error code %d!\n",rc);
			}
		}
	}
	//Finish the transaction (if delete was successful)
	if (rc==ISS_SUCCESS) {
		if ((rc=iss_db_end_su())!=ISS_SUCCESS)
			L.LogE("ERROR: Proclogoff.llrsgn.EndSU: Failed with %d!\n",rc);
	}
	if (rc!=ISS_SUCCESS) {//Rollback transaction if deletion or ending transaction was failing
		if ((rc2=iss_db_abort_su())!=ISS_SUCCESS) 
			L.LogE("ERROR: Proclogoff.llrsgn.AbortSU: Failed with %d!\n",rc2);
	}
	L.Log("User %s succesfully sign off from location %d\n",APos[posn].user,APos[posn].sid/2) ;
	*APos[posn].user=0; //Signal logoff in pos data
	SaveStatus(false,posn); //Save status to registry
	return 0;
}

long ProcLogOn(char *s)
{
	TElem5000 E;
	long posn=(s[18]-'0')*10+s[19]-'0'-1;
	L.Log("ProcLogOn -- POS number is : %d\n",posn);
	if ((posn>=PosCount)||(posn<0)) {
		L.LogE("ERROR: ProcSale: POS number is bad! (%d) -> File rejected!\n",posn+1);
		return ISS_SUCCESS;
	}
	L.Log("ProcLogOn -- Apos.User is : %s\n",APos[posn].user);
	if (*APos[posn].user) {//someone is already signed on to this location, gotta sign off
		char tran[24];
		s[20]=0;
		L.LogE("Warning: Cashier %s has been forced log off on location %d, another user signed on!\n"
			,APos[posn].user,posn+1);
		strcpy(tran,s);
		memcpy(tran,APos[posn].user,4);
		ProcLogOff(tran);
	}
	strncpy(APos[posn].user,s,4);
	L.Log("ProcLogOn -- Apos.User is : %s\n",APos[posn].user);
	if (!CleanUserID(APos[posn].user)) {
		L.LogE("ERROR: Logon transaction cannot be processed, user name is empty!\n");
		return 0;
	}

	char transuser[9]={0,0,0,0,0,0,0,0,0};

	PreFill(&E,5000);
	memcpy(transuser,APos[posn].user,4);

	SINT16 hp;
	SYSTEMTIME st;
	ISS_T_LOG_BLOCK_HANDLE  handle;
	ISS_T_REPLY retval=0;

	if ((retval=iss46_hlog_get_current_hp(&hp))!=ISS_SUCCESS) {
		L.LogE("ERROR: ProcLogOn.get_current_hp: %d, retval=%d\n",hp,retval);
		L.LogE("==> ProcLogOn will be aborted or retried!\n");
		return retval;
	}
	L.Log("Info: ProcLogOn.get_current_hp: %d\n",hp);
	
	SysTimeFromS14(&st,s+4);
	Date=DateYMD2Int(st.wYear,st.wMonth,st.wDay);
	ISS_T_INT_TIME time=st.wHour*3600+st.wMinute*60+st.wSecond;

	CheckTranDateTime(&Date,&time,posn);

	ISS_T_LOG_ELEM_ID element;
	element.element_num=1;
	element.element_time=time;

	if ((retval=iss46_hlog_start_block(APos[posn].sid,hp,APos[posn].lasttran+1,Date,time
		,0,0x00018000,element,9,transuser,&handle))!=ISS_SUCCESS) {
		L.LogE("ERROR: ProcLogon.start returned: %d -> retrying!\n",retval);
		return(retval);
	}
	else L.Log("ProcLogon.start: Successful. User:%s\n",transuser);

	retval=iss46_hlog_send_element(handle,5000,time,0x0000,sizeof(TElem5000),(char*)&E);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcLogon.5000 returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,2))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcLogon.5000: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcLogon.5000: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("Proclogon.5000: Successful.User:%s, Sid:%d\n",transuser,APos[posn].sid);

	retval=iss46_hlog_end_trans(handle,Date,2);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		L.LogE("ERROR: ProcLogon.EndTran returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,3))!=ISS_SUCCESS) {
			L.LogE("ERROR: ProcLogon.EndTran: AbortTran: %d!\n",ret2);
		}
		else L.Log("Info: ProcLogon.EndTran: AbortTran successful.\n");
		return(retval);
	}
	else L.Log("ProcLogon.EndTran: Successful.\n");

//	retval=iss46_hlog_send_block(handle);
//	L.Log("Proclogon.sendb: %d\n",retval);
	L.Log("PROCLOGON - Pos: %d transaction details! User: %s Total sale: %d Total count: %d \n",APos[posn].sid/2,APos[posn].user,APos[posn].p_salesval,APos[posn].p_itemcnt);
	APos[posn].lasttrandate=Date;
	APos[posn].lasttrantime=time;

	APos[posn].lasttran++;
	APos[posn].signondate=Date;
	APos[posn].signontime=time;
	APos[posn].signontransno=APos[posn].lasttran;
	APos[posn].items=0;
	APos[posn].p_custcnt=0;
	APos[posn].p_itemcnt=0;
	APos[posn].p_regtime=0;
	APos[posn].p_salesval=0;
	APos[posn].p_startdate=Date;
	APos[posn].p_starttime=time;
	APos[posn].p_tendertime=0;
	APos[posn].regtime=0;
	APos[posn].tendertime=0;
	APos[posn].tenders=0;
	memset(APos[posn].tender,0,C_TENDERS*sizeof(TTender));

	ISS_T_REPLY    rc;
    ISS_T_BOOLEAN  unknown;
	UINT16         precision;
    UINT16         buf,buf2;
	ISS_T_DB_KEY key[4],key2[2];
	char keyuser[10]={0,0,0,0,0,0,0,0,0,0};
	char keyuser2[10]={0,0,0,0,0,0,0,0,0,0};
	SINT32 keyloc=0;
	ISS_T_INT_DATE keydate=0x7FFF;
	ISS_T_INT_DATE keytime=0x7FFF;
	SINT32 state=0;
	memcpy(keyuser,APos[posn].user,4);
    key[0].field_name           = "USER_ID";
    key[0].field_length         = 9;
    key[0].field_value          = keyuser;
    key[0].field_value_unknown  = FALSE;
    key[1].field_name           = "LOCATION_ID";
    key[1].field_length         = 4;
    key[1].field_value          = &keyloc;
    key[1].field_value_unknown  = TRUE;
    key[2].field_name           = "DRAWER_DATE";
    key[2].field_length         = 4;
    key[2].field_value          = &keydate;
    key[2].field_value_unknown  = FALSE;
    key[3].field_name           = "DRAWER_TIME";
    key[3].field_length         = 4;
    key[3].field_value          = &keytime;
    key[3].field_value_unknown  = FALSE;
    key2[0].field_name           = "LOCATION_ID";
    key2[0].field_length         = 4;
    key2[0].field_value          = &keyloc;
    key2[0].field_value_unknown  = FALSE;
    key2[1].field_name           = "USER_ID";
    key2[1].field_length         = 9;
    key2[1].field_value          = keyuser2;
    key2[1].field_value_unknown  = FALSE;
    if ( (rc = ReadISS(&buf,FALSE,"CCD",4,key,ISS_C_DB_BEFORE_OR_EQUAL,ISS_C_DB_NO_LOCK
		,"USER_ID",9,keyuser,FALSE)) == ISS_SUCCESS) {
		if (strcmp(keyuser,APos[posn].user)!=0) {
			L.LogE("ERROR: Proclogon.GetDrawer: Get record of User %s failed!\n"
				,APos[posn].user);
		}
		else {
			rc=iss_db_extract_field(buf,"STATE",4,0,&state,&precision,&unknown);
			if (rc!=ISS_SUCCESS) 
				L.LogE("ERROR: Proclogon.GetDrawer: Extract field STATE failed with error code %d!\n",rc);
			else {
				if (state==0) {
					L.Log("Info: Proclogon.GetDrawer: Got opened state drawer record for User %s.\n"
						,APos[posn].user);
					rc=iss_db_extract_field(buf,"DRAWER_DATE",sizeof(APos[posn].drwdate)
						,0,&(APos[posn].drwdate),&precision,&unknown);
					if (rc!=ISS_SUCCESS) 
						L.LogE("ERROR: Proclogon.GetDrawer: Extract field DRAWER_DATE failed with error code %d!\n",rc);
					else {
						short y,m,d;
						DateInt2YMD(APos[posn].drwdate,&y,&m,&d);
						L.Log("Info: Proclogon.GetDrawer: Extract field DRAWER_DATE succeeded. Value=%d -> %04d.%02d.%02d.\n"
							,APos[posn].drwdate,y,m,d);
					}
					rc=iss_db_extract_field(buf,"DRAWER_TIME",sizeof(APos[posn].drwtime)
						,0,&(APos[posn].drwtime),&precision,&unknown);
					if (rc!=ISS_SUCCESS) 
						L.LogE("ERROR: Proclogon.GetDrawer: Extract field DRAWER_TIME failed with error code %d!\n",rc);
					else L.Log("Info: Proclogon.GetDrawer: Extract field DRAWER_TIME succeeded. Value=%d.\n",APos[posn].drwtime);
				}
				else {
					L.LogE("ERROR: Proclogon.GetDrawer: No opened state record for User %s!\n"
						,APos[posn].user);
				}
			}
		}
		if ((iss_db_free_buffer(&buf))!=ISS_SUCCESS) {
			L.LogE("ERROR: Proclogon.GetDrawer: Freeing buffer failed!\n");
		}
	}
	else {
		L.LogE("ERROR: Proclogon.GetDrawer: Get record of User %s failed with error code %d!\n"
			,APos[posn].user,rc);
	}
	//Gotta signal sign on in db!
	keyloc=APos[posn].sid/2;
    if ( (rc = ReadISS(&buf,TRUE,"LLRSGN",2,key2,ISS_C_DB_AFTER_OR_EQUAL,ISS_C_DB_EXCLUSIVE_LOCK
		,"LOCATION_ID",4,&keyloc,FALSE)) == ISS_SUCCESS) {
		if (keyloc==APos[posn].sid/2)
			L.Log("Info: Proclogon.SetSignon.llrsgn.FindLocnRec: Found record for location %d!\n",keyloc);
		else {//not a record for the requested loc id
			keyloc=APos[posn].sid/2;
			L.Log("Info: Proclogon.SetSignon.llrsgn.FindLocnRec: Found record is not for location %d!\n",keyloc);
			if ((rc=iss_db_free_buffer(&buf))!=ISS_SUCCESS)
				L.LogE("Error: Proclogon.SetSignon.llrsgn.FindLocnRec: Free buffer failed! %d\n"
					,rc);
			rc=ISS_E_SDBMS_NOREC; //Make it llook like no record was found
		}
	}
	if (rc==ISS_E_SDBMS_NOREC) {//Need a new record into LLRSGN
        if ((rc=iss_db_create_buffer("LLRSGN", &buf))!=ISS_SUCCESS)
			L.LogE("ERROR: Proclogon.SetSignon.llrsgn.CreateBuf: Create buffer failed with error code %d!\n",rc);
		else {//gotta write location_id into
			rc=iss_db_update_field(buf, "LOCATION_ID",4,0,&keyloc,FALSE);
			if (rc!=ISS_SUCCESS) 
				L.LogE("ERROR: Proclogon.SetSignon.llrsgn.updfield1: Update field failed! Error code %d!\n",rc);
		}
    }
	else if (rc!=ISS_SUCCESS)
		L.Log("ERROR: Proclogon.SetSignon.llrsgn.FindLocnRec: Unexpected error reading LLRSGN! %d\n",rc);
	if (rc==ISS_SUCCESS) {
		rc=iss_db_update_field(buf, "USER_ID",9,0,keyuser,FALSE);
		if (rc!=ISS_SUCCESS) 
			L.LogE("ERROR: Proclogon.SetSignon.llrsgn.updfield2: Update field failed! Error code %d!\n",rc);
	    if (rc==ISS_SUCCESS) {
			if ((rc=iss_db_write_buffer(&buf))!=ISS_SUCCESS) {
				L.LogE("ERROR: Proclogon.SetSignon.llrsgn.writebuf: Write buffer failed! Error code %d!\n",rc);
			}
		}
    }
    if (rc==ISS_SUCCESS) {//Now LLRDYN comes
		if ( (rc = ReadISS(&buf2,FALSE,"LLRDYN",1,key2,ISS_C_DB_EQUAL,ISS_C_DB_EXCLUSIVE_LOCK
			,NULL,0,NULL,FALSE)) != ISS_SUCCESS) {
			L.LogE("ERROR: Proclogon.SetSignon.llrdyn.FindLocnRec: Can't get record for location %d! Result=%d\n"
				,keyloc,rc);
		}
		else {
			ISS_T_BOOLEAN fo=FALSE;
			rc=iss_db_update_field(buf2, "FORCED_OFF",sizeof(fo),0,&fo,FALSE);
			if (rc!=ISS_SUCCESS) {
				L.LogE("ERROR: Proclogon.SetSignon.llrdyn.updfield: Update field failed! Error code %d!\n",rc);
			}
			else {
				if ((rc=iss_db_write_buffer(&buf2))!=ISS_SUCCESS) {
					L.LogE("ERROR: Proclogon.SetSignon.llrdyn.writebuf: Write buffer failed! Error code %d!\n",rc);
				}
			}
		}
	}
    if (rc==ISS_SUCCESS) {//Now LLTDYN comes
		if ( (rc = ReadISS(&buf2,FALSE,"LLTDYN",1,key2,ISS_C_DB_EQUAL,ISS_C_DB_EXCLUSIVE_LOCK
			,NULL,0,NULL,FALSE)) != ISS_SUCCESS) {
			L.LogE("ERROR: Proclogon.SetSignon.lltdyn.FindLocnRec: Can't get record for location %d! Result=%d\n"
				,keyloc,rc);
		}
		else {
			char ts='T';
			rc=iss_db_update_field(buf2, "TRADING_STAT",sizeof(ts),0,&ts,FALSE);
			if (rc!=ISS_SUCCESS) {
				L.LogE("ERROR: Proclogon.SetSignon.lltdyn.updfield: Update field failed! Error code %d!\n",rc);
			}
			else {
				if ((rc=iss_db_write_buffer(&buf2))!=ISS_SUCCESS) {
					L.LogE("ERROR: Proclogon.SetSignon.lltdyn.writebuf: Write buffer failed! Error code %d!\n",rc);
				}
			}
		}
	}
    if (rc==ISS_SUCCESS) {//close success unit
        if ((rc=iss_db_end_su())!=ISS_SUCCESS) {
			L.LogE("ERROR: Proclogon.SetSignon.EndSU: Failed! Error code %d!\n",rc);
		}
    }
    //Abort success unit upon error
    if (rc!=ISS_SUCCESS) {
        if (iss_db_abort_su()!=ISS_SUCCESS) {
			L.LogE("ERROR: Proclogon.SetSignon.AbortSU: Failed! Error code %d!\n",rc);
		}
    }
//    if (rc==ISS_SUCCESS) {//Now LCSDYN comes
//	}
	SaveStatus(false,posn);
	L.Log("PROCLOGON - Pos: %d transaction details! User: %d Total sale: %d Total count: %d \n",APos[posn].sid,APos[posn].user,APos[posn].p_salesval,APos[posn].p_itemcnt);
	L.Log("User %s succesfully sign on to location %d\n",APos[posn].user,APos[posn].sid/2) ;
	return ISS_SUCCESS;
}

long ProcLogOnOff(FILE *F)
{
	char s[24];
	while (fgets(s,23,F)) {
		while ((strlen(s))&&((s[strlen(s)-1]==10)||(s[strlen(s)-1]==13))) s[strlen(s)-1]=0;
		if (strlen(s)<21) {
			L.LogE("ERROR: Login or Logout record is too short! File cannot be processed!\n");
		}
		else {
			if ((strlen(s)>21)&&(s[21]>=' '))
				L.LogE("Warning: Login or Logout record is too long!\n");
			switch ((*s)|32) {
			case 'k': return ProcLogOff(s+1);
			case 'b': return ProcLogOn(s+1); 
			default: L.LogE("ERROR: Data type code '%c' is invalid for Login or Logout record!\n");
			}
		}
	}
	L.Log("Info: Login or Logout file has been processed.\n");
	return 0;
}

void ProcClose(FILE *F)
{
	char s[80];
	long l;
	L.Log("Info: ProcClose: Started performing checkings at closure...\n");
	for(l=0; l<PosCount; l++) {
		if (APos[l].user[0]!=0) {
			SYSTEMTIME t;
			GetLocalTime(&t);
			sprintf(s,"0000%04d%02d%02d%02d%02d%02d%02d",t.wYear,t.wMonth,t.wDay,t.wHour
				,t.wMinute,t.wSecond,l+1);
			memcpy(s+4-strlen(APos[l].user),APos[l].user,strlen(APos[l].user));
			if ((l>2)||(ForceLogOffAtClosure)) {
				if (l>2) L.Log("Info: ProcClose.ChkLoggedOn: User %s is still logged on for PoS #%d! Forcing sign off.\n"
					,APos[l].user,l+1);
				else L.LogE("ERROR: ProcClose.ChkLoggedOn: User %s is still logged on for PoS #%d! Forcing sign off.\n"
					,APos[l].user,l+1);
				ProcLogOff(s);
			}
			else L.LogE("Warning: ProcClose.ChkLoggedOn: User %s is still logged on for PoS #%d! Closure figures may mismatch!\n"
					,APos[l].user,l+1);
		}
	}
	L.Log("Info: ProcClose: Processing and comparing closure records now...\n");
	while (fgets(s,79,F)) {
		l=strlen(s)-1;
		while ((l>0)&&(s[l]<' ')) {s[l]=0; l--;}
		L.Log("Info: ProcClose: Dump: %s\n",s);
		switch (*s|32) {
		case 's'://item sales
			{
				ISS_GDECIMAL_6 ean;
				long v,q,id;
				gdec_init_z(ean,6,0,iss_gdec_round_normal);
				memmove(s,s+8,13);
				s[13]=0;
				memmove(s+20,s+21,10);
				s[30]=0;
				s[44]=0;
				gdec_from_ascii(ean,s);
				id=*((long *)&(ean[2]));
				v=atol(s+31);
				q=atol(s+20);
				if (ItemTotals.CmpFigures(id,v,q)) {
					L.Log("Total sales of item %s matched. Value=%d Quantity=%d\n",s,v,q);
					ItemTotals.Delete(id);
				}
				else {
					long vv,qq;
					if (ItemTotals.GetCounters(id,&vv,&qq)) {
						L.LogE("ERROR: Total sales for item %s mismatches! ValueIn=%d ValueMy=%d QuantityIn=%d QuantityMy=%d\n"
							,s,v,vv,q,qq);
						ItemTotals.Delete(id);
					}
					else {
						L.LogE("ERROR: There was no sales on item %s according to internal values! ValueIn=%d QuantityIn=%d\n"
							,s,v,q);
					}
				}
			}
			break;
		case 'f'://tender totals
			{
				long v,id;
				id=(s[1]-'0')*10+(s[2]-'0');
				s[16]=0;
				v=atol(s+3);
				if (TenderTotals.CmpFigures(id,v)) {
					L.Log("Total sales on tender %s matched. Value=%d\n",s,v);
					TenderTotals.Delete(id);
				}
				else {
					long vv;
					if (TenderTotals.GetCounters(id,&vv)) {
						L.LogE("ERROR: Total sales for tender %d mismatches! ValueIn=%d ValueMy=%d\n"
							,id,v,vv);
						TenderTotals.Delete(id);
					}
					else {
						L.LogE("ERROR: There was no sales for tender %d according to internal values! ValueIn=%d\n"
							,id,v);
					}
				}
			}
			break;
		case 'u'://cashier totals
			{
				long v,id;
				memmove(s,s+1,4);
				s[4]=0;
				CleanUserID(s);
				id=*((long *)s);
				s[18]=0;
				v=atol(s+5);
				
				if (CashierTotals.CmpFigures(id,v)) {
					L.Log("Total sales of cashier %s matched. Value=%d\n",s,v);
					CashierTotals.Delete(id);
				}
				else {
					long vv;
					if (CashierTotals.GetCounters(id,&vv)) {
						L.LogE("ERROR: Total sales for cashier %s mismatches! ValueIn=%d ValueMy=%d\n"
							,s,v,vv);
						CashierTotals.Delete(id);
					}
					else {
						L.LogE("ERROR: There was no sales for cashier %s according to internal values! ValueIn=%d\n"
							,s,v);
					}
				}
			}
			break;
		case 'l'://fuel level meter
			memmove(s,s+8,13);
			s[13]=0;
			s[30]=0;
			L.Log("Level meter shows %scl for fuel item %s.\n",s+21,s);
			break;
		default:
			L.LogE("ERROR: Don't know how to handle this closure record! %s\n",s);
		}
	}
	if ((l=ItemTotals.GetCount())!=0) {
		long id,v,q;
		bool show=false;
		while ((l)&&(!show)) {
			l--;
			ItemTotals.GetElement(l,&id,&v,&q);
			if ((q!=0)||(v!=0)) {show=true;break;}
		}
		if (show) {
			L.LogE("ERROR: ProcClose: The following sales records were missing from the closure file:\n");
			for (l=ItemTotals.GetCount()-1;l;l--) {
				ItemTotals.GetElement(l,&id,&v,&q);
				if ((q!=0)||(v!=0)) L.LogE("==> ItemCode=%d Value=%d Quantity=%d\n",id,v,q);
			}
		}
	}
	if ((l=TenderTotals.GetCount())!=0) {
		long id,v;
		L.LogE("ERROR: ProcClose: The following tender totals were missing from the closure file:\n");
		while(l) {
			l--;
			TenderTotals.GetElement(l,&id,&v);
			L.LogE("==> Tender ID=%d Total value=%d\n",id,v);
		}
	}
	if ((l=CashierTotals.GetCount())!=0) {
		long id,v;
		bool show=false;
		while ((l)&&(!show)) {
			l--;
			CashierTotals.GetElement(l,&id,&v);
			if (v!=0) {show=true;break;}
		}
		if (show) {
			char sss[5];
			sss[4]=0;
			L.LogE("ERROR: ProcClose: The following cashier totals were missing from the closure file:\n");
			for (l=CashierTotals.GetCount()-1;l;l--) {
				CashierTotals.GetElement(l,&id,&v);
				if (v!=0) {
					memmove(sss,&id,4);
					L.LogE("	==> Cashier:%s Total sales value=%d\n",sss,v);
				}
			}
		}
	}
	L.Log("Info: ProcClose: Processing closure records finished.\n");
	TenderTotals.EmptyArray();
	ItemTotals.EmptyArray();
	CashierTotals.EmptyArray();
	TenderTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegTenderTotals);
	CashierTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegCashierTotals);
	ItemTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegGItemTotals);
}

int FLEXSYS_ProcClose(char *fname)
{
	Reg r(false, C_RegKey);
	GilbClose dif_type[50];
	GilbClose dif_t[200];
	int ean_max=0, icount=0;
//	THeader header;
//	TankVol tankvol;
//	Counters counters;
	int counters_num=0,index,i;
	char s[80];
	long l;
//	ISS_GDECIMAL_6 ean;
//	long v,id;//q
	bool show=false;
	char itemcollector[sizeof(ISS_GDECIMAL_6)+3*sizeof(long)];
//	char t_ean[13];
	long quantity=0,samount=0;
	float aa;
	char sq[50];
	TiXmlNode* root = 0;
	TiXmlElement* XMLelement= 0;
	SYSTEMTIME st;

	memcpy(itemcollector,&l,sizeof(long));
	GItemTotals.InitCursor();
	icount=GItemTotals.GetCount();
	for (index=0;index<icount;index++) {
		GItemTotals.GetDataByIndex(index,itemcollector);
		gdec2s((unsigned char *)(itemcollector+sizeof(long)),6,s);
		L.LogE("Sales for item %s on till %d! Quantity=%d Value=%d.\n"
			,s,*((long *)(itemcollector))+PosSIDBase/2,*((long *)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)))
			,*((long *)(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6))));
		sprintf(dif_t[index].ean,"%s",s);
		aa=*((long *)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)));
		if (aa<900) // Ha shop �ru
			dif_t[index].quantity=aa;
		else
			dif_t[index].quantity=aa/1000;
		dif_t[index].value=*((long *)(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6)));
		//GItemTotals.DeleteByIndex(index);
	}
	l=0;
	for (index=0;index<icount;index++) {
		l=0;
		dif_type[ean_max].done=0;
		if(ean_max==0) {
			sprintf(dif_type[ean_max].ean,"%s",dif_t[index].ean);
			dif_type[ean_max].quantity=dif_t[index].quantity;
			dif_type[ean_max].value=dif_t[index].value;
			ean_max++;
		}
		else {
			for(i=0;i<ean_max;i++) {
				if(strcmp(dif_t[index].ean,dif_type[i].ean)==0) {
					dif_type[i].quantity+=dif_t[index].quantity;
					dif_type[i].value+=dif_t[index].value;
					l=1;
				}
			}
			if(l==0){
				sprintf(dif_type[ean_max].ean,"%s",dif_t[index].ean);
				dif_type[ean_max].quantity=dif_t[index].quantity;
				dif_type[ean_max].value=dif_t[index].value;
				ean_max++;
			}
		}
	}
	L.LogE("The items total sale:\n");
	for (index=0;index<ean_max;index++) {
		sprintf(sq,"%.3f",dif_type[index].quantity);
		L.LogE("	==>%s QuantityMy=%s ValueMy=%d\n",dif_type[index].ean,sq,dif_type[index].value);
	}
	L.Log("Info: FlexysProcClose: Started performing checkings at closure...\n");
	for(l=0; l<PosCount; l++) {
		if (APos[l].user[0]!=0) {
			SYSTEMTIME t;
			GetLocalTime(&t);
			L.Log("Flexys ProcClose - SignOff this user: %d\n",APos[l].user);
			sprintf(s,"0000%04d%02d%02d%02d%02d%02d%02d",t.wYear,t.wMonth,t.wDay,t.wHour
				,t.wMinute,t.wSecond,l+1);
			memcpy(s+4-strlen(APos[l].user),APos[l].user,strlen(APos[l].user));
			if ((l>2)||(ForceLogOffAtClosure)) {
				if (l>2) L.Log("Info: ProcClose.ChkLoggedOn: User %s is still logged on for PoS #%d! Forcing sign off.\n"
					,APos[l].user,l+1);
				else L.LogE("ERROR: ProcClose.ChkLoggedOn: User %s is still logged on for PoS #%d! Forcing sign off.\n"
					,APos[l].user,l+1);
				ProcLogOff(s);
			}
			else L.LogE("Warning: ProcClose.ChkLoggedOn: User %s is still logged on for PoS #%d! Closure figures may mismatch!\n"
					,APos[l].user,l+1);
		}
	}
	MYSQL *hnd=NULL; // mysql connection handle
	const char *sinf=NULL; // mysql server information
	bool db_ok=true;
	hnd = mysql_init(NULL);
	//MYSQL_RES *res=NULL; // result of asking the database for a listing of its tables
	long res;
	MYSQL_ROW row; // one row from the result set
	MYSQL_RES *queryResult_ptr;
	char sql[1024]; // sql statement used to get all rows
	long row_id=0;
	char tdate[12],ttime[10];			
	TiXmlDocument doc( fname );


	if(NULL == mysql_real_connect(hnd,VPOPServerIP,VPOPUser,VPOPPassw,VPOPDataBase,0,NULL,0)){
		db_ok=false;
		L.LogE("Problem encountered connecting to the %s database on %s with user %s. %d \n %s\n",VPOPDataBase,VPOPServerIP,VPOPUser,GetLastError(),mysql_error(hnd));
	}
	else
		L.Log("Succesfully connected to the %s database on %s.\n",VPOPDataBase,VPOPServerIP);	
	if ( !doc.LoadFile() )	{
		L.LogE("ERROR: FLEXYS_ProcSale.PreProc: Could not load %s file.!  Error='%s'. Exiting.\n",fname, doc.ErrorDesc());
		return 2;
	}
	else {
		root = doc.FirstChild( "zaras" );
		XMLelement = root->FirstChildElement( "datum" );
		GetLocalTime(&st);
		if (XMLelement) {
			sprintf(tdate,"%s",XMLelement->GetText());
		}
		else
			sprintf(tdate,"%04d-%02d-%02d",st.wYear,st.wMonth,st.wDay);
		XMLelement = root->FirstChildElement( "idopont" );
		if (XMLelement) {
			sprintf(ttime,"%s",XMLelement->GetText());
		}
		else
			sprintf(ttime,"%02d:%02d:%02d",st.wHour,st.wMinute,st.wSecond);
		int kezelo=0;
		TiXmlElement* kezel;
		TiXmlElement* forgalom = root->FirstChildElement( "forgalom" );
		if (forgalom) {
				for( forgalom; forgalom; forgalom=forgalom->NextSiblingElement())
				{
					kezel = forgalom->FirstChildElement( "kezelo" );
					if(kezel){
						kezel->QueryIntAttribute("id",&kezelo);
						L.LogE("UserID: %d", kezelo);
						XMLelement = kezel->FirstChildElement( "osszeg" );
						if(XMLelement)
							L.LogE("takings: %s Ft\n", XMLelement->GetText());
					}
				}
		}

		int koszlop=0,pisztoly=0;
		char oallas[12];
		TiXmlElement* kutoszlop;
		TiXmlElement* oldal;
		TiXmlElement* ptoly;

		TiXmlElement* regiszterora = root->FirstChildElement( "regiszterora" );
		kutoszlop = regiszterora->FirstChildElement( "kutoszlop" );
		if(kutoszlop){
				for( kutoszlop; kutoszlop; kutoszlop=kutoszlop->NextSiblingElement())
				{
					kutoszlop->QueryIntAttribute("id",&koszlop);
					L.Log("Oszlop: %d", koszlop);
					oldal = kutoszlop->FirstChildElement( "oldal" );
					if (oldal) {
						ptoly = oldal->FirstChildElement( "pisztoly" );
						if (ptoly) {
								for( ptoly; ptoly; ptoly=ptoly->NextSiblingElement())
								{	
									ptoly->QueryIntAttribute("id",&pisztoly);
									XMLelement = ptoly->FirstChildElement( "oraallas" );
									if(XMLelement){
										sprintf(oallas,"%s",XMLelement->GetText());
										L.LogE("Oszlop: %d Pisztoly: %d Oraallas: %s \n", koszlop,pisztoly,XMLelement->GetText());
										row_id=-1;
										sprintf(sql,"select fuel_type from %s.pumps where dispenser_id=%d and pump_id=%d;",VPOPDataBase,koszlop,pisztoly);
										res=mysql_query(hnd,sql);
										queryResult_ptr = mysql_store_result(hnd);
										row = mysql_fetch_row(queryResult_ptr);
										if (mysql_num_rows(queryResult_ptr)>0) {
											row_id = atol(row[0]);
											if (row_id >=0) {  // there are rows
												L.Log("The fuel_type is : %d\n",row_id);
											}
											else 
												L.LogE("ERROR -FLEXYS Close- Cannot find Pump ID!\n",row_id);
										}
										else 
											L.Log("ERROR -FUEL PUMP ID- I didn't find FUEL PUMP ID record!\n");
										mysql_free_result(queryResult_ptr);
										if(row_id>=0) {
											sprintf(sql,"INSERT INTO `%s`.`counters` (`date`, `dispenser_id`, `pump_id`, `fuel_id`, `e_counter`, `e_timestamp`, `type`) VALUES ('%s', %d, %d, %d, %s, '%s %s', 0);"
												,VPOPDataBase,tdate,koszlop,pisztoly,row_id,XMLelement->GetText(),tdate,ttime);
											L.LogE("Write to counters table! \n %s\n",sql);
											res=mysql_query(hnd,sql);
											if (!res){
												L.LogE("FLEXYS Close - The INSERT process was successful!\n"); 
											}					
											else
												L.LogE("ERROR -FLEXYS Close- The INSERT process was unsuccessful! Error code: %s\n",mysql_error(hnd)); 
										}
									}

								}
						} //ptoly
					}//oldal
				}//for
				L.Log("Info: -FLEXYS Close- ProcClose: Processing closure records finished.\n");
				TenderTotals.EmptyArray();
				GItemTotals.EmptyArray();
				CashierTotals.EmptyArray();
				TenderTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegTenderTotals);
				CashierTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegCashierTotals);
				ItemTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegGItemTotals);
				r.SetValueDW("Fuel.System.Close",1); //Engedem a benzink�t f�jlok feldolgoz�s�t	
				

		}//kutoszlop

	}

	return 0;
}

int NAMOS_ProcClose(){
	GilbClose dif_type[50];
	GilbClose dif_t[200];
	int ean_max=0, icount=0;
//	THeader header;
//	TankVol tankvol;
//	Counters counters;
	int counters_num=0,index,i;
	char s[80];
	long l;
//	ISS_GDECIMAL_6 ean;
//	long v;//,id;//q
	bool show=false;
	char itemcollector[sizeof(ISS_GDECIMAL_6)+3*sizeof(long)];
//	char t_ean[13];
	long quantity=0,samount=0;
	float aa;
	char sq[50];
	
	memcpy(itemcollector,&l,sizeof(long));
	GItemTotals.InitCursor();
	icount=GItemTotals.GetCount();
	for (index=0;index<icount;index++) {
		GItemTotals.GetDataByIndex(index,itemcollector);
		gdec2s((unsigned char *)(itemcollector+sizeof(long)),6,s);
		L.LogE("Sales for item %s on till %d! Quantity=%d Value=%d.\n"
			,s,*((long *)(itemcollector))+PosSIDBase/2,*((long *)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)))
			,*((long *)(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6))));
		sprintf(dif_t[index].ean,"%s",s);
		aa=*((long *)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)));
		if (aa<900) // Ha shop �ru
			dif_t[index].quantity=aa;
		else
			dif_t[index].quantity=aa/1000;
		dif_t[index].value=*((long *)(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6)));
		//GItemTotals.DeleteByIndex(index);
	}
	l=0;
	for (index=0;index<icount;index++) {
		l=0;
		dif_type[ean_max].done=0;
		if(ean_max==0) {
			sprintf(dif_type[ean_max].ean,"%s",dif_t[index].ean);
			dif_type[ean_max].quantity=dif_t[index].quantity;
			dif_type[ean_max].value=dif_t[index].value;
			ean_max++;
		}
		else {
			for(i=0;i<ean_max;i++) {
				if(strcmp(dif_t[index].ean,dif_type[i].ean)==0) {
					dif_type[i].quantity+=dif_t[index].quantity;
					dif_type[i].value+=dif_t[index].value;
					l=1;
				}
			}
			if(l==0){
				sprintf(dif_type[ean_max].ean,"%s",dif_t[index].ean);
				dif_type[ean_max].quantity=dif_t[index].quantity;
				dif_type[ean_max].value=dif_t[index].value;
				ean_max++;
			}
		}
	}
	L.LogE("The items total sale:\n");
	for (index=0;index<ean_max;index++) {
		sprintf(sq,"%.3f",dif_type[index].quantity);
		L.LogE("	==>%s QuantityMy=%s ValueMy=%d\n",dif_type[index].ean,sq,dif_type[index].value);
	}
	L.Log("Info: GilbarcoProcClose: Started performing checkings at closure...\n");
	for(l=0; l<PosCount; l++) {
		if (APos[l].user[0]!=0) {
			SYSTEMTIME t;
			GetLocalTime(&t);
			L.Log("Gilbarco ProcClose - SignOff this user: %d\n",APos[l].user);
			sprintf(s,"0000%04d%02d%02d%02d%02d%02d%02d",t.wYear,t.wMonth,t.wDay,t.wHour
				,t.wMinute,t.wSecond,l+1);
			memcpy(s+4-strlen(APos[l].user),APos[l].user,strlen(APos[l].user));
			if ((l>2)||(ForceLogOffAtClosure)) {
				if (l>2) L.Log("Info: ProcClose.ChkLoggedOn: User %s is still logged on for PoS #%d! Forcing sign off.\n"
					,APos[l].user,l+1);
				else L.LogE("ERROR: ProcClose.ChkLoggedOn: User %s is still logged on for PoS #%d! Forcing sign off.\n"
					,APos[l].user,l+1);
				ProcLogOff(s);
			}
			else L.LogE("Warning: ProcClose.ChkLoggedOn: User %s is still logged on for PoS #%d! Closure figures may mismatch!\n"
					,APos[l].user,l+1);
		}
	}
	return 0;
}

int GILBARCO_ProcClose(char *fname)
{
	GilbClose dif_type[50];
	GilbClose dif_t[200];
	int ean_max=0, icount=0;
	THeader header;
	TankVol tankvol;
	Counters counters;
	int counters_num=0,index,i;
	char s[80];
	long l;
//	ISS_GDECIMAL_6 ean;
	long v,id;//q
	bool show=false;
	char itemcollector[sizeof(ISS_GDECIMAL_6)+3*sizeof(long)];
	char t_ean[13];
	long quantity=0,samount=0;
	float aa;
	char sq[50];
	
	memcpy(itemcollector,&l,sizeof(long));
	GItemTotals.InitCursor();
	icount=GItemTotals.GetCount();
	for (index=0;index<icount;index++) {
		GItemTotals.GetDataByIndex(index,itemcollector);
		gdec2s((unsigned char *)(itemcollector+sizeof(long)),6,s);
		L.LogE("Sales for item %s on till %d! Quantity=%d Value=%d.\n"
			,s,*((long *)(itemcollector))+PosSIDBase/2,*((long *)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)))
			,*((long *)(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6))));
		sprintf(dif_t[index].ean,"%s",s);
		aa=*((long *)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)));
		if (aa<900) // Ha shop �ru
			dif_t[index].quantity=aa;
		else
			dif_t[index].quantity=aa/1000;
		dif_t[index].value=*((long *)(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6)));
		//GItemTotals.DeleteByIndex(index);
	}
	l=0;
	for (index=0;index<icount;index++) {
		l=0;
		dif_type[ean_max].done=0;
		if(ean_max==0) {
			sprintf(dif_type[ean_max].ean,"%s",dif_t[index].ean);
			dif_type[ean_max].quantity=dif_t[index].quantity;
			dif_type[ean_max].value=dif_t[index].value;
			ean_max++;
		}
		else {
			for(i=0;i<ean_max;i++) {
				if(strcmp(dif_t[index].ean,dif_type[i].ean)==0) {
					dif_type[i].quantity+=dif_t[index].quantity;
					dif_type[i].value+=dif_t[index].value;
					l=1;
				}
			}
			if(l==0){
				sprintf(dif_type[ean_max].ean,"%s",dif_t[index].ean);
				dif_type[ean_max].quantity=dif_t[index].quantity;
				dif_type[ean_max].value=dif_t[index].value;
				ean_max++;
			}
		}
	}
	L.LogE("The items total sale:\n");
	for (index=0;index<ean_max;index++) {
		sprintf(sq,"%.3f",dif_type[index].quantity);
		L.LogE("	==>%s QuantityMy=%s ValueMy=%d\n",dif_type[index].ean,sq,dif_type[index].value);
	}
	L.Log("Info: GilbarcoProcClose: Started performing checkings at closure...\n");
	for(l=0; l<PosCount; l++) {
		if (APos[l].user[0]!=0) {
			SYSTEMTIME t;
			GetLocalTime(&t);
			L.Log("Gilbarco ProcClose - SignOff this user: %d\n",APos[l].user);
			sprintf(s,"0000%04d%02d%02d%02d%02d%02d%02d",t.wYear,t.wMonth,t.wDay,t.wHour
				,t.wMinute,t.wSecond,l+1);
			memcpy(s+4-strlen(APos[l].user),APos[l].user,strlen(APos[l].user));
			if ((l>2)||(ForceLogOffAtClosure)) {
				if (l>2) L.Log("Info: ProcClose.ChkLoggedOn: User %s is still logged on for PoS #%d! Forcing sign off.\n"
					,APos[l].user,l+1);
				else L.LogE("ERROR: ProcClose.ChkLoggedOn: User %s is still logged on for PoS #%d! Forcing sign off.\n"
					,APos[l].user,l+1);
				ProcLogOff(s);
			}
			else L.LogE("Warning: ProcClose.ChkLoggedOn: User %s is still logged on for PoS #%d! Closure figures may mismatch!\n"
					,APos[l].user,l+1);
		}
	}
	TiXmlElement* node = 0;
	TiXmlNode* root = 0;
	TiXmlElement* XMLelement= 0;
	//TiXmlHandle hRoot(0);
	TiXmlDocument doc( fname );
	MYSQL *hnd=NULL; // mysql connection handle
	const char *sinf=NULL; // mysql server information
	bool db_ok=true;
	hnd = mysql_init(NULL);
	//MYSQL_RES *res=NULL; // result of asking the database for a listing of its tables
	long res;
	MYSQL_ROW row; // one row from the result set
	MYSQL_RES *queryResult_ptr;
	char sql[1024]; // sql statement used to get all rows
	long row_id=0;
				



	if(NULL == mysql_real_connect(hnd,VPOPServerIP,VPOPUser,VPOPPassw,VPOPDataBase,0,NULL,0)){
		db_ok=false;
		L.LogE("Problem encountered connecting to the %s database on %s with user %s. %d \n %s\n",VPOPDataBase,VPOPServerIP,VPOPUser,GetLastError(),mysql_error(hnd));
	}
	else
		L.Log("Succesfully connected to the %s database on %s.\n",VPOPDataBase,VPOPServerIP);	
	if ( !doc.LoadFile() )	{
		L.LogE("ERROR: GILBARCO_ProcSale.PreProc: Could not load %s file.!  Error='%s'. Exiting.\n",fname, doc.ErrorDesc());
		return 2;
	}
	else {

		root = doc.FirstChild( "NAXML-MovementReport" );
		node = root->FirstChildElement( "TankProductMovement" );
		if (node) {
			TiXmlNode* TPMHeader = node->FirstChildElement( "MovementHeader" );
			printf("TRANSACTION HEADER\n");
			//Transaction Date
			XMLelement = TPMHeader->FirstChildElement( "BeginDate" );
			//XMLelement = Journal->ToElement();
			printf("BeginDate %s \n",XMLelement->GetText());
			strcpy(header.BDate,XMLelement->GetText());
			//Transaction START time
			XMLelement = TPMHeader->FirstChildElement( "BeginTime" );
			printf("BeginTime %s \n",XMLelement->GetText());
			strcpy(header.BTime,XMLelement->GetText());
			//Transaction END Date
			XMLelement = TPMHeader->FirstChildElement( "EndDate" );
			//XMLelement = node->ToElement();
			printf("EndDate %s \n",XMLelement->GetText());
			strcpy(header.EDate,XMLelement->GetText());
			
			//Transaction END time
			XMLelement = TPMHeader->FirstChildElement( "EndTime" );
			printf("EndTime %s \n",XMLelement->GetText());
			strcpy(header.ETime,XMLelement->GetText());
			
			L.Log("Info: ProcClose: Processing and comparing closure records now...\n");

			
			/**********  FUEL Sales   ************/
			TiXmlElement* TPMDetail= 0;
			//TiXmlElement* XMLelement= 0;
			TPMDetail = node->FirstChildElement( "TPMDetail" );
			if (TPMDetail) {
				for( TPMDetail; TPMDetail; TPMDetail=TPMDetail->NextSiblingElement())
				{
					XMLelement = TPMDetail->FirstChildElement( "TankID" );
					strcpy(tankvol.tank_id,XMLelement->GetText());
					XMLelement = TPMDetail->FirstChildElement( "FuelProductID" );
					strcpy(tankvol.fuel_product_id,XMLelement->GetText());
					XMLelement = TPMDetail->FirstChildElement( "FuelProductVolume" );
					strcpy(tankvol.FuelProductVolume,XMLelement->GetText());
					XMLelement = TPMDetail->FirstChildElement( "FuelProductTemperature" );
					strcpy(tankvol.FuelProductTemperature,XMLelement->GetText());
					XMLelement = TPMDetail->FirstChildElement( "Ullage" );
					strcpy(tankvol.Ullage,XMLelement->GetText());
					XMLelement = TPMDetail->FirstChildElement( "gvr:Density" );
					strcpy(tankvol.Density,XMLelement->GetText());
					XMLelement = TPMDetail->FirstChildElement( "gvr:DensityTC" );
					strcpy(tankvol.DensityTC,XMLelement->GetText());
					XMLelement = TPMDetail->FirstChildElement( "gvr:Mass" );
					strcpy(tankvol.Mass,XMLelement->GetText());
					XMLelement = TPMDetail->FirstChildElement( "WaterVolume" );
					strcpy(tankvol.WaterVolume,XMLelement->GetText());
					XMLelement = TPMDetail->FirstChildElement( "ClosingTemperatureCorrectedVolume" );
					strcpy(tankvol.ClosingTemperatureCorrectedVolume,XMLelement->GetText());
					l=0;
					for(i=0;i<ean_max;i++) {
						if(strcmp(tankvol.fuel_product_id,dif_type[i].ean)==0) {
							l=1;
							dif_type[i].done=1;
							sprintf(sq,"%.3f",dif_type[i].quantity);
							/*if (dif_type[i].quantity==atof(tankvol.FuelProductVolume))
								L.Log("The %s item is equal with the stored value! Quantity=%s\n",dif_type[i].ean,tankvol.FuelProductVolume);
							else 
								L.LogE("ERROR - TankProductMovement - The %s item quantity doesn't match with the stored value! QuantityIn=%s ValueStored=%s \n",dif_type[i].ean,tankvol.FuelProductVolume,sq);
							*/
							break;
						}
					}
					if (!l) 
						L.LogE("ERROR - TankProductMovement - Cannot find this item %s in the registry! Quantity=%s \n",tankvol.fuel_product_id,tankvol.FuelProductVolume);
					if (db_ok) {
						row_id=-1;
						sprintf(sql,"SELECT id FROM %s.`fuel` where ean='%s';",VPOPDataBase,tankvol.fuel_product_id);
						res=mysql_query(hnd,sql);
						queryResult_ptr = mysql_store_result(hnd);
						row = mysql_fetch_row(queryResult_ptr);
						if (mysql_num_rows(queryResult_ptr)>0) {
							row_id = atol(row[0]);
							if (row_id >=0) {  // there are rows
								L.Log("The fuel id is : %d",row_id);
							}
							else 
								L.LogE("ERROR -TankProductMovement- The fuel id is bad : %d",row_id);
						}
						else 
							L.Log("ERROR -TankProductMovement- I didn't find FUEL record!\n %s\n");
						mysql_free_result(queryResult_ptr);
						if(row_id>=0) {
							sprintf(sql,"INSERT INTO %s.`tank_volume` (`tank_id`, `fuel_id`, `FuelProductVolume`, `FuelProductTemperature`, `Ullage`, `Density`, `DensityTC`, `Mass`, `WaterVolume`, `ClosingTemperatureCorrectedVolume`, `ReadingTime`) VALUES (%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,'%s %s');",
								VPOPDataBase,tankvol.tank_id,row_id,tankvol.FuelProductVolume,tankvol.FuelProductTemperature,tankvol.Ullage,tankvol.Density,tankvol.DensityTC,tankvol.Mass,tankvol.WaterVolume,tankvol.ClosingTemperatureCorrectedVolume,header.EDate,header.ETime);
							L.Log("Write to Tank_Volume table! \n %s\n",sql);
							res=mysql_query(hnd,sql);
							if (!res) {
								L.Log("The -TankProductMovement- INSERT process was successful!\n"); 
							}					
							else
								L.LogE("ERROR - TankProductMovement - The INSERT process was unsuccessful! Error code : %s\n",mysql_error(hnd)); 
						}
					}
				}
			}
		} //If (node)
		/**********  ITEM Sales   ************/
		TiXmlElement* ISMDetail= 0;
		//TiXmlElement* XMLelement= 0;
		XMLelement = root->FirstChildElement( "ItemSalesMovement" );
		if (XMLelement) {
			ISMDetail = root->FirstChildElement( "ItemSalesMovement" )->FirstChildElement( "ISMDetail" );;
			for( ISMDetail; ISMDetail; ISMDetail=ISMDetail->NextSiblingElement())
			{
				quantity=0,samount=0;
				XMLelement = ISMDetail->FirstChildElement( "ItemID" );
				strcpy(t_ean,XMLelement->GetText());
				XMLelement = ISMDetail->FirstChildElement( "ISMSalesTotals" )->FirstChildElement( "SalesQuantity" );
				quantity=atol(XMLelement->GetText());
				XMLelement = ISMDetail->FirstChildElement( "ISMSalesTotals" )->FirstChildElement( "SalesAmount" );
				samount=atol(XMLelement->GetText());
				l=0;
				for(i=0;i<ean_max;i++) {
					if(strcmp(t_ean,dif_type[i].ean)==0) {
						l=1;
						dif_type[i].done=1;
						sprintf(sq,"%.3f",dif_type[i].quantity);
						if (dif_type[i].quantity==quantity)
							L.Log("-- ItemSalesMovement -- The %s item is equal with the stored value! Quantity=%d\n",dif_type[i].ean,quantity);
						else 
							L.LogE("ERROR -- ItemSalesMovement -- The %s item quantity doesn't match with the stored value! QuantityIn=%d ValueStored=%s \n",dif_type[i].ean,quantity,sq);
						break;
					}
				}
				if (!l) 
					L.LogE("ERROR -- ItemSalesMovement -- Cannot find this item %s in the registry! Quantity=%d \n",t_ean,quantity);
			}
		}
		/************  COUNTERS   ****************/
		TiXmlElement* FPMDetail= 0;
		TiXmlElement* FPMNRT= 0;
		float VolumeNum=0;
		
		//TiXmlElement* XMLelement= 0;
		XMLelement = root->FirstChildElement( "FuelProductMovement" );
		if (XMLelement) {
			TiXmlNode* TPMHeader = root->FirstChildElement( "FuelProductMovement" )->FirstChildElement( "MovementHeader" );
			printf("TRANSACTION HEADER\n");
			//Transaction Date
			XMLelement = TPMHeader->FirstChildElement( "BeginDate" );
			//XMLelement = Journal->ToElement();
			printf("BeginDate %s \n",XMLelement->GetText());
			strcpy(header.BDate,XMLelement->GetText());
			//Transaction START time
			XMLelement = TPMHeader->FirstChildElement( "BeginTime" );
			printf("BeginTime %s \n",XMLelement->GetText());
			strcpy(header.BTime,XMLelement->GetText());
			//Transaction END Date
			XMLelement = TPMHeader->FirstChildElement( "EndDate" );
			//XMLelement = node->ToElement();
			printf("EndDate %s \n",XMLelement->GetText());
			strcpy(header.EDate,XMLelement->GetText());
			
			//Transaction END time
			XMLelement = TPMHeader->FirstChildElement( "EndTime" );
			printf("EndTime %s \n",XMLelement->GetText());
			strcpy(header.ETime,XMLelement->GetText());

			FPMDetail = root->FirstChildElement( "FuelProductMovement" )->FirstChildElement( "FPMDetail" );
			for( FPMDetail; FPMDetail; FPMDetail=FPMDetail->NextSiblingElement())
			{
				XMLelement = FPMDetail->FirstChildElement( "FuelProductID" );
				strcpy(counters.fuel_product_id,XMLelement->GetText());
				FPMNRT = FPMDetail->FirstChildElement( "FPMNonResettableTotals" );
				VolumeNum=0;
				for( FPMNRT; FPMNRT; FPMNRT=FPMNRT->NextSiblingElement())
				{
					XMLelement = FPMNRT->FirstChildElement( "FuelPositionID" );
					strcpy(counters.position_id,XMLelement->GetText());
					XMLelement = FPMNRT->FirstChildElement( "gvr:FuelNozzleID" );
					strcpy(counters.FuelNozzleID,XMLelement->GetText());
					XMLelement = FPMNRT->FirstChildElement( "FuelProductNonResettableAmountNumber" );
					strcpy(counters.FuelProductNonResettableAmountNumber,XMLelement->GetText());
					XMLelement = FPMNRT->FirstChildElement( "FuelProductNonResettableVolumeNumber" );
					strcpy(counters.FuelProductNonResettableVolumeNumber,XMLelement->GetText());
					VolumeNum+=atof(XMLelement->GetText());
					row_id=-1;
					if (db_ok) {
						sprintf(sql,"SELECT id FROM %s.`fuel` where ean='%s';",VPOPDataBase,counters.fuel_product_id);
						res=mysql_query(hnd,sql);
						queryResult_ptr = mysql_store_result(hnd);
						row = mysql_fetch_row(queryResult_ptr);
						if (mysql_num_rows(queryResult_ptr)>0) {
							row_id = atol(row[0]);
							if (row_id >0) {  // there are rows
								L.Log("FuelProductMovement - The fuel id is : %d\n",row_id);
							}
							else 
								L.LogE("ERROR -FuelProductMovement- The fuel id is bad : %d\n",row_id);
						}
						else 
							L.Log("ERROR -FuelProductMovement- I didn't find FUEL record!\n %s\n");
						mysql_free_result(queryResult_ptr);
						if (row_id>=0) {
							sprintf(sql,"INSERT INTO `%s`.`counters` (`date`, `dispenser_id`, `pump_id`, `fuel_id`, `e_counter`, `e_timestamp`, `type`) VALUES ('%s', %s, %s, %d, %s, '%s %s', 0);"
								,VPOPDataBase,header.EDate,counters.position_id,counters.FuelNozzleID,row_id,counters.FuelProductNonResettableVolumeNumber,header.EDate,header.ETime);
							L.Log("Write to counters table! \n %s\n",sql);
							res=mysql_query(hnd,sql);
							if (!res){
								L.Log("FuelProductMovement 1- The INSERT process was successful!\n"); 
							}					
							else
								L.LogE("ERROR -FuelProductMovement- The INSERT process was unsuccessful! Error code: %s\n",mysql_error(hnd)); 
						}
					}
				}// For

				long vv=0;
				float qq=0;
				l=0;
				for(i=0;i<ean_max;i++) {
					if(strcmp(counters.fuel_product_id,dif_type[i].ean)==0) {
						l=1;
						qq=dif_type[i].quantity;
						break;
					}
				}
				if (!l) 
					L.LogE("ERROR -- FuelProductMovement -- Cannot find this item %s in the registry! \n",counters.fuel_product_id);

				if (db_ok) {
					sprintf(sql,"INSERT INTO `%s`.`daily_sales` (`fuel_id`, `day`, `CountersVolume`, `TransVolume`) VALUES (%d, '%s', %e, %f);"
						,VPOPDataBase,row_id,header.EDate,VolumeNum,qq);
					L.Log("Write to daily_sale table! \n %s\n",sql);
					res=mysql_query(hnd,sql);
					if (!res){
						L.Log("FuelProductMovement 2- The INSERT process was successful!\n"); 
					}					
					else
						L.LogE("ERROR -FuelProductMovement- The INSERT process was unsuccessful! Error code: %s\n",mysql_error(hnd)); 
				}
			}
		}
		Reg r(false, C_RegKey);
		char tmp[250],did[3],pid[2],cter[15];
		char *Registers=NULL;
		int good[20];
		int fid=0;
		for (int i=0;i<20;i++)
			good[i]=0;
		if (!Registers) r.ValueSZ("Fuel.ACIS.Registers",&Registers,"");
		int lgth=strlen(Registers);
		if (lgth==0){
			L.Log("ACIS Registers - There isn't any counter in the Registry! %s\n",tmp);
		}
		else {
			for (int i=0; i<=lgth;i=i+15) {
				strncpy(did,Registers+i+13,2);
				did[2]=0;
				strncpy(pid,Registers+i+2,1);
				pid[1]=0;
				strncpy(cter,Registers+i+3,10);
				cter[10]='.';
				strcpy(cter+11,did);
				cter[13]=0;
				strncpy(did,Registers+i,2);
				did[2]=0;
				row_id=0;
				good[((atoi(did)-FirstACISPumpID)*5)+atoi(pid)]=1;
				if (db_ok) {
					sprintf(sql,"SELECT fuel_type FROM %s.`pumps` where dispenser_id=%d and pump_id=%s;",VPOPDataBase,atoi(did),pid);
					res=mysql_query(hnd,sql);
					if (!res) {
						queryResult_ptr = mysql_store_result(hnd);
						row = mysql_fetch_row(queryResult_ptr);
						if (mysql_num_rows(queryResult_ptr)>0) {
							row_id = atol(row[0]);
							if (row_id >0) {  // there are rows
								L.Log("FuelProductMovement - The fuel id is : %d\n",row_id);
							}
							else 
								L.LogE("ERROR -FuelProductMovement- The fuel id is bad : %d\n",row_id);
						}
						else 
							L.Log("ERROR -FuelProductMovement- I didn't find FUEL record!\n %s\n");
						mysql_free_result(queryResult_ptr);
						if (row_id>=0) {
							sprintf(sql,"INSERT INTO `%s`.`counters` (`date`, `dispenser_id`, `pump_id`, `fuel_id`, `e_counter`, `e_timestamp`, `type`) VALUES ('%s', %d, %s, %d, %s, '%s %s', 0);"
								,VPOPDataBase,header.EDate,atoi(did),pid,row_id,cter,header.EDate,header.ETime);
							L.Log("Write to counters table! \n %s\n",sql);
							res=mysql_query(hnd,sql);
							if (!res){
								L.Log("FuelProductMovement 3- The INSERT process was successful!\n"); 
							}					
							else
								L.LogE("ERROR -FuelProductMovement- The INSERT process was unsuccessful! Error code: %s\n",mysql_error(hnd)); 
						}
					}
				}
			}
			for (int x=0;x<ACISCounter;x++) 
				for (int y=1;y<6;y++)
					if (good[(x*5)+y]==0) {
						if (db_ok) {
							sprintf(cter,"0");
							fid=0;
							sprintf(sql,"SELECT e_counter,fuel_id FROM %s.`pump` where dispenser_id=%d and pump_id=%d  order by date desc limit 1;",VPOPDataBase,FirstACISPumpID+x,y);
							res=mysql_query(hnd,sql);
							if (!res) {
								queryResult_ptr = mysql_store_result(hnd);
								row = mysql_fetch_row(queryResult_ptr);
								if (mysql_num_rows(queryResult_ptr)>0) {
									sprintf(cter,"%s",row[0]);
									fid=atoi(row[1]);
								}
								else{ 
									sprintf(cter,"0");
									fid=0;
									L.Log("ERROR -FuelProductMovement- I didn't find FUEL record!\n %s\n");
								}
								L.Log("FuelProductMovement - The counter is : %d\n",cter);
								mysql_free_result(queryResult_ptr);
							}
							else {
								sprintf(sql,"SELECT fuel_type FROM %s.`pumps` where dispenser_id=%d and pump_id=%d;",VPOPDataBase,FirstACISPumpID+x,y);
								res=mysql_query(hnd,sql);
								if (!res) {
									queryResult_ptr = mysql_store_result(hnd);
									row = mysql_fetch_row(queryResult_ptr);
									if (mysql_num_rows(queryResult_ptr)>0) {
										fid = atol(row[0]);
										if (fid >0) {  // there are rows
											L.Log("FuelProductMovement - No record in registry The fuel id is : %d\n",fid);
										}
										else 
											L.LogE("ERROR -FuelProductMovement- No record in registry The fuel id is bad : %d\n",fid);
									}
									else 
										L.Log("ERROR -FuelProductMovement- No record in registry I didn't find FUEL record!\n %s\n",sql);
									mysql_free_result(queryResult_ptr);
								}
							}
							if(fid!=0) {
								sprintf(sql,"INSERT INTO `%s`.`counters` (`date`, `dispenser_id`, `pump_id`, `fuel_id`, `e_counter`, `e_timestamp`, `type`) VALUES ('%s', %d, %d, %d, %s, '%s %s', 0);"
														,VPOPDataBase,header.EDate,FirstACISPumpID+x,y,fid,cter,header.EDate,header.ETime);
								L.Log("Write to counters table! \n %s\n",sql);
								res=mysql_query(hnd,sql);
								if (!res){
										L.Log("FuelProductMovement 4- The INSERT process was successful!\n"); 
								}					
								else
										L.LogE("ERROR -FuelProductMovement- The INSERT process was unsuccessful! Error code: %s\n",mysql_error(hnd)); 
							}
						}
					}
		}
		//if (db_ok) 	r.SetValueSZ("Fuel.ACIS.Registers","");








		/********* TENDERS  **********************/
		TiXmlElement* TSMDetail= 0;
		//TiXmlElement* XMLelement= 0;
		int tender=0;
		L.Log("TenderSummaryMovement \n" );
		XMLelement = root->FirstChildElement( "TenderSummaryMovement" );
		if (XMLelement) {
			TSMDetail = root->FirstChildElement( "TenderSummaryMovement" )->FirstChildElement( "TSMDetail" );;
			for( TSMDetail; TSMDetail; TSMDetail=TSMDetail->NextSiblingElement())
			{
				tender=1,samount=0;
				XMLelement = TSMDetail->FirstChildElement( "Tender" )->FirstChildElement( "TenderSubCode" );
				tender=atoi(XMLelement->GetText());
				XMLelement = TSMDetail->FirstChildElement( "TSMSalesTotals" )->FirstChildElement( "TenderSalesAmount" );
				samount=atol(XMLelement->GetText());
				id=tender;
				v=samount;
				if (TenderTotals.CmpFigures(id,v)) {
					L.Log("Total sales on tender %s matched. Value=%d\n",s,v);
					TenderTotals.Delete(id);
				}
				else {
					long vv;
					if (TenderTotals.GetCounters(id,&vv)) {
						L.LogE("ERROR -TenderSummaryMovement- Total sales for tender %d mismatches! ValueIn=%d ValueMy=%d\n"
							,id,v,vv);
						TenderTotals.Delete(id);
					}
					else {
						L.LogE("ERROR -TenderSummaryMovement- There was no sales for tender %d according to internal values! ValueIn=%d\n"
							,id,v);
					}
				}
			}
		}

		//********************** TANK DELIVERY MOVEMENT  ******************************/
		TiXmlElement* TDM = root->FirstChildElement( "TankDeliveryMovement" );
		char iv[22]="",fv[22]="",tgq[20]="",qo[20]="",qr[20]="",docid[80]="",supl[80]="",sid[10]="",fpid[15]="",dens[20]="",temper[4]="";
//		wchar_t csctmp[80];
		wchar_t *wText;
		char *ansiText;

		if (TDM) {
			L.Log("Info: ProcClose: TankDeliveryMovement Processing closure records now...\n");
			/**********  DELIVERY   ************/
			TiXmlElement* TDMDetail= 0;
			//TiXmlElement* XMLelement= 0;
			TDMDetail = TDM->FirstChildElement( "TDMDetail" );
			if (TDMDetail) {
				for( TDMDetail; TDMDetail; TDMDetail=TDMDetail->NextSiblingElement())
				{
					XMLelement = TDMDetail->FirstChildElement( "TankID" );
					strcpy(tankvol.tank_id,XMLelement->GetText());
					XMLelement = TDMDetail->FirstChildElement( "gvr:BeginDate" );
					printf("BeginDate %s \n",XMLelement->GetText());
					strncpy(header.BDate,XMLelement->GetText(),10);
					//Transaction START time
					XMLelement = TDMDetail->FirstChildElement( "gvr:BeginTime" );
					printf("BeginTime %s \n",XMLelement->GetText());
					strncpy(header.BTime,XMLelement->GetText(),8);
					//Transaction END Date
					XMLelement = TDMDetail->FirstChildElement( "gvr:EndDate" );
					//XMLelement = node->ToElement();
					printf("EndDate %s \n",XMLelement->GetText());
					strncpy(header.EDate,XMLelement->GetText(),10);
					//Transaction END time
					XMLelement = TDMDetail->FirstChildElement( "gvr:EndTime" );
					printf("EndTime %s \n",XMLelement->GetText());
					strncpy(header.ETime,XMLelement->GetText(),8);
					XMLelement = TDMDetail->FirstChildElement( "gvr:InitialVolume" );
					strcpy(iv,XMLelement->GetText());
					XMLelement = TDMDetail->FirstChildElement( "gvr:FinalVolume" );
					strcpy(fv,XMLelement->GetText());
					XMLelement = TDMDetail->FirstChildElement( "gvr:TankGaugeQuantity" );
					strcpy(tgq,XMLelement->GetText());
					TiXmlElement* DataDelivery = TDMDetail->FirstChildElement( "gvr:DataDelivery" );
					if (DataDelivery){
						XMLelement = DataDelivery->FirstChildElement( "gvr:QuantityOrdered" );
						strcpy(qo,XMLelement->GetText());
						XMLelement = DataDelivery->FirstChildElement( "gvr:QuantityReceived" );
						strcpy(qr,XMLelement->GetText());
						XMLelement = DataDelivery->FirstChildElement( "gvr:DocumentID" );
						wText = CodePageToUnicode(65001,XMLelement->GetText());
						ansiText = UnicodeToCodePage(1252,wText);	
						strcpy(docid,ansiText);
						//strcpy(docid,XMLelement->GetText());
						TiXmlElement* suplier = DataDelivery->FirstChildElement( "gvr:Supplier" );
						if (suplier){
							XMLelement = suplier->FirstChildElement( "gvr:SupplierName" );
							wText = CodePageToUnicode(65001,XMLelement->GetText());
							ansiText = UnicodeToCodePage(1252,wText);	
							strcpy(supl,ansiText);
							XMLelement = suplier->FirstChildElement( "gvr:SupplierCode" );
							strcpy(sid,XMLelement->GetText());
						}
					}
					XMLelement = TDMDetail->FirstChildElement( "FuelProductID" );
					strcpy(fpid,XMLelement->GetText());
					XMLelement = TDMDetail->FirstChildElement( "gvr:Density" );
					strcpy(dens,XMLelement->GetText());
					XMLelement = TDMDetail->FirstChildElement( "FuelProductTemperature" );
					strcpy(temper,XMLelement->GetText());
					if (db_ok) {
						row_id=-1;
						sprintf(sql,"SELECT id FROM %s.`fuel` where ean='%s';",VPOPDataBase,fpid);
						res=mysql_query(hnd,sql);
						queryResult_ptr = mysql_store_result(hnd);
						row = mysql_fetch_row(queryResult_ptr);
						if (mysql_num_rows(queryResult_ptr)>0) {
							row_id = atol(row[0]);
							if (row_id >0) {  // there are rows
								L.Log("FuelProductMovement - The fuel id is : %d\n",row_id);
							}
							else 
								L.LogE("ERROR -FuelProductMovement- The fuel id is bad : %d\n",row_id);
						}
						else 
							L.Log("ERROR -FuelProductMovement- I didn't find FUEL record!\n %s\n");
						mysql_free_result(queryResult_ptr);
						sprintf(sql,"SELECT id FROM %s.`deliveries` where tank_id='%s' and BeginDate='%s' and BeginTime='%s' and EndDate='%s' and EndTime='%s';",VPOPDataBase,tankvol.tank_id,header.BDate,header.BTime,header.EDate,header.ETime);
						res=mysql_query(hnd,sql);
						queryResult_ptr = mysql_store_result(hnd);
						row = mysql_fetch_row(queryResult_ptr);
						if (mysql_num_rows(queryResult_ptr)>0) {
							sprintf(sql,"DELETE FROM %s.`deliveries` where id=%s;",VPOPDataBase,row[0]);
							int res1=mysql_query(hnd,sql);
							if (!res1) {  // there are rows
								L.LogE("The fuel id was deleted : %s",row[0]);
							}
							else 
								L.LogE("ERROR -TankProductMovement- The fuel id didn't delete : %s \n%s\n",sql,mysql_error(hnd));
						}
						else 
							L.Log("ERROR -TankProductMovement- I didn't find TankDeliveryMovement record!\n");
						mysql_free_result(queryResult_ptr);
						//res=mysql_query(hnd,"SET NAMES '852'");
						sprintf(sql,"INSERT INTO %s.`deliveries` (`tank_id`,`fuel_id`, `BeginDate`, `BeginTime`, `EndDate`, `EndTime`, `InitialVolume`, `FinalVolume`, `TankGaugeQuantity`, `QuantityOrdered`, `QuantityReceived`, `DocumentID`, `SupplierName`, `FuelProductID`, `FuelProductTemperature`) VALUES ('%s',%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');",
								VPOPDataBase,tankvol.tank_id,row_id,header.BDate,header.BTime,header.EDate,header.ETime,iv,fv,tgq,qo,qr,docid,supl,fpid,temper);
						L.Log("Write to Tank_Volume table! \n %s\n",sql);
						res=mysql_query(hnd,sql);
						if (!res) {
							L.Log("The -TankDeliveryMovement- INSERT process was successful!\n"); 
						}					
						else
							L.LogE("ERROR - TankDeliveryMovement - The INSERT process was unsuccessful! Error code : %s\n",mysql_error(hnd)); 
						
					}
				}
			}
		} //If (node)



		L.LogE("ERROR: ProcClose: The following sales records were missing from the closure file:\n");
		for(i=0;i<ean_max;i++) {
			if(dif_type[i].done==0) {
				L.LogE("ERROR: Total sales for item %s !\n",dif_type[i].ean);
				sprintf(sq,"%.3f",dif_type[i].quantity);
				L.LogE("	==>  QuantityMy= %s \n",sq);
				L.LogE("	==>  ValueMy= %d\n",dif_type[i].value);
				dif_type[i].done=1;
			}
		}
		if ((l=TenderTotals.GetCount())!=0) {
			long id,v;
			L.LogE("ERROR: ProcClose: The following tender totals were missing from the closure file:\n");
			while(l) {
				l--;
				TenderTotals.GetElement(l,&id,&v);
				L.LogE("==> Tender ID=%d Total value=%d\n",id,v);
			}
		}
		if ((l=CashierTotals.GetCount())!=0) {
			long id,v;
			bool show=false;
			while ((l)&&(!show)) {
				l--;
				CashierTotals.GetElement(l,&id,&v);
				if (v!=0) {show=true;break;}
			}
			if (show) {
				char sss[5];
				sss[4]=0;
				L.LogE("ERROR: ProcClose: The following cashier totals were missing from the closure file:\n");
				for (l=CashierTotals.GetCount()-1;l>=0;l--) {
					CashierTotals.GetElement(l,&id,&v);
					if (v!=0) {
						memmove(sss,&id,4);
						L.LogE("==> Cashier:%s Total sales value=%d\n",sss,v);
					}
				}
			}
		}
		L.Log("Info: ProcClose: Processing closure records finished.\n");
		TenderTotals.EmptyArray();
		GItemTotals.EmptyArray();
		CashierTotals.EmptyArray();
		TenderTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegTenderTotals);
		CashierTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegCashierTotals);
		ItemTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegGItemTotals);

	}
	
		return 0;
}


long GetTradingDayData(void)
{
	ISS_T_REPLY    rc;
	ISS_T_BOOLEAN  unknown;
	UINT16         precision;
	UINT16         buf;
	ISS_T_DB_KEY key[2];
	char keyvalU[7]={0,0,0,0,0,0,0};
	char keyuser2[10]={0,0,0,0,0,0,0,0,0,0};
	ISS_T_INT_DATE keydate=0x7FFF;

	key[0].field_name           = "SASTDHTY_KEY";
	key[0].field_length         = 7;
	key[0].field_value          = keyvalU;
	key[0].field_value_unknown  = TRUE;
	key[1].field_name           = "TD_DATE";
	key[1].field_length         = 4;
	key[1].field_value          = &keydate;
	key[1].field_value_unknown  = TRUE;

	TDNoGo=false;
	if ( (rc = ReadISS(&buf,FALSE,"SASTDHTY",2,key,ISS_C_DB_BEFORE_OR_EQUAL,ISS_C_DB_NO_LOCK
		,"TD_START_DATE",4,&TDStartDate,FALSE)) == ISS_SUCCESS) {
//	if ( (rc = ReadISS(&buf,FALSE,"SASTDHTY",0,NULL,ISS_C_DB_LAST,ISS_C_DB_NO_LOCK
//		,"TD_START_DATE",4,&TDStartDate,FALSE)) == ISS_SUCCESS) {
		rc=iss_db_extract_field(buf,"TD_START_TIME",4,0,&TDStartTime,&precision,&unknown);
		if (rc!=ISS_SUCCESS) {
			RL.LogE("ERROR: GetTradingDayData: Extract field TD_START_TIME failed with error code %d!\n",rc);
			TDNoGo=true;
		}
		rc=iss_db_extract_field(buf,"TD_END_DATE",4,0,&keydate,&precision,&unknown);
		if (rc!=ISS_SUCCESS) {
			RL.LogE("ERROR: GetTradingDayData: Extract field TD_END_DATE failed with error code %d!\n",rc);
			TDNoGo=true;
		}
		else {
			if (unknown==FALSE) {
				TDNoGo=true;
				L.Log("Info: GetTradingDayData: TD_ST_D=%d TD_ST_T=%d TD_END_D=%d (closed).\n",TDStartDate,TDStartTime,keydate);
			}
			else L.Log("Info: GetTradingDayData: TD_ST_D=%d TD_ST_T=%d TD_END_D=unknown (open).\n",TDStartDate,TDStartTime);
		}
		if ((iss_db_free_buffer(&buf))!=ISS_SUCCESS) {
			RL.LogE("ERROR: GetTradingDayData: Freeing buffer failed!\n");
		}
	}
	else {
		L.LogE("ERROR: GetTradingDayData: Get record failed with error code %d!\n",rc);
		TDNoGo=true;
	}
	return rc;
}

bool TDRefresh(bool force=false) //returns true: can proceed
{
	SYSTEMTIME st;
	long l;
	GetLocalTime(&st);
	l=3600*st.wHour+60*st.wMinute+st.wSecond;
	if ((force)||(l<TDRefreshTime)||(l>(TDRefreshTime+((TDNoGo)?300:30)))) {
		GetTradingDayData();
		TDRefreshTime=l;
	}
	return TDNoGo;
}

long RGetDrawer(void)
{
	ISS_T_REPLY    rc;
	ISS_T_BOOLEAN  unknown;
	UINT16         precision;
	UINT16         buf;
	ISS_T_DB_KEY key[4],key2[2];
	char keyuser[10]={0,0,0,0,0,0,0,0,0,0};
	char keyuser2[10]={0,0,0,0,0,0,0,0,0,0};
	SINT32 keyloc=0;
	ISS_T_INT_DATE keydate=0x7FFF;
	ISS_T_INT_DATE keytime=0x7FFF;
	SINT32 state=0;
	memcpy(keyuser,RPos.user,4);
	key[0].field_name           = "USER_ID";
	key[0].field_length         = 9;
	key[0].field_value          = keyuser;
	key[0].field_value_unknown  = FALSE;
	key[1].field_name           = "LOCATION_ID";
	key[1].field_length         = 4;
	key[1].field_value          = &keyloc;
	key[1].field_value_unknown  = TRUE;
	key[2].field_name           = "DRAWER_DATE";
	key[2].field_length         = 4;
	key[2].field_value          = &keydate;
	key[2].field_value_unknown  = FALSE;
	key[3].field_name           = "DRAWER_TIME";
	key[3].field_length         = 4;
	key[3].field_value          = &keytime;
	key[3].field_value_unknown  = FALSE;
	key2[0].field_name           = "LOCATION_ID";
	key2[0].field_length         = 4;
	key2[0].field_value          = &keyloc;
	key2[0].field_value_unknown  = FALSE;
	key2[1].field_name           = "USER_ID";
	key2[1].field_length         = 9;
	key2[1].field_value          = keyuser2;
	key2[1].field_value_unknown  = FALSE;
	if ( (rc = ReadISS(&buf,FALSE,"CCD",4,key,ISS_C_DB_BEFORE_OR_EQUAL,ISS_C_DB_NO_LOCK
		,"USER_ID",9,keyuser,FALSE)) == ISS_SUCCESS) {
		if (strcmp(keyuser,RPos.user)!=0) {
			RL.LogE("ERROR: ProcRVMTran.GetDrawer: Get record of User %s failed!\n"
				,RPos.user);
		}
		else {
			rc=iss_db_extract_field(buf,"STATE",4,0,&state,&precision,&unknown);
			if (rc!=ISS_SUCCESS) 
				RL.LogE("ERROR: ProcRVMTran.GetDrawer: Extract field STATE failed with error code %d!\n",rc);
			else {
				if (state==0) {
					RL.Log("Info: ProcRVMTran.GetDrawer: Got opened state drawer record for User %s.\n"
						,RPos.user);
					rc=iss_db_extract_field(buf,"DRAWER_DATE",sizeof(RPos.drwdate)
						,0,&(RPos.drwdate),&precision,&unknown);
					if (rc!=ISS_SUCCESS) 
						RL.LogE("ERROR: ProcRVMTran.GetDrawer: Extract field DRAWER_DATE failed with error code %d!\n",rc);
					else {
						short y,m,d;
						DateInt2YMD(RPos.drwdate,&y,&m,&d);
						RL.Log("Info: ProcRVMTran.GetDrawer: Extract field DRAWER_DATE succeeded. Value=%d -> %04d.%02d.%02d.\n"
							,RPos.drwdate,y,m,d);
					}
					rc=iss_db_extract_field(buf,"DRAWER_TIME",sizeof(RPos.drwtime)
						,0,&(RPos.drwtime),&precision,&unknown);
					if (rc!=ISS_SUCCESS) 
						RL.LogE("ERROR: ProcRVMTran.GetDrawer: Extract field DRAWER_TIME failed with error code %d!\n",rc);
					else RL.Log("Info: ProcRVMTran.GetDrawer: Extract field DRAWER_TIME succeeded. Value=%d.\n"
						,RPos.drwtime);
				}
				else {
					RL.LogE("ERROR: ProcRVMTran.GetDrawer: No opened state record for User %s!\n"
						,RPos.user);
				}
			}
		}
		if ((iss_db_free_buffer(&buf))!=ISS_SUCCESS) {
			RL.LogE("ERROR: ProcRVMTran.GetDrawer: Freeing buffer failed!\n");
		}
	}
	else {
		RL.LogE("ERROR: ProcRVMTran.GetDrawer: Get record of User %s failed with error code %d!\n"
			,RPos.user,rc);
	}
	return rc;
}

bool RCheck4NewPerfSlot(ISS_T_INT_DATE date, ISS_T_INT_TIME time, bool closepartialslot=false) 
//returns true: moved to another slot
//Checking whether needed to advance to the next performance slot.
//If needed one transaction will be made, and perf slot rolled to the next period.
//So gotta be called with while to write more than one intermediate period!
{
	ISS_T_INT_DATE dt=RPos.p_startdate;
	ISS_T_INT_DATE tm=RPos.p_starttime;
	if (closepartialslot) {
		if ((dt==date)&&(tm==time)) {//Given date & time already covered
			RL.Log("Info: RCheck4NewPerfSlot: Performance slot already covered. End date=%d time=%d\n"
				,dt,tm);
			return false;
		}
	}
	if ((date>dt+2)||((date>dt)&&(time>=tm))) {//more than one day remained - skip putting slots
		RL.LogE("Warning: RCheck4NewPerfSlot: Performance slot too far. End date=%d time=%d, Req date=%d time=%d!\n"
				,dt,tm,date,time);
		
		RPos.p_startdate=date;
		RPos.p_starttime=time;
		RPos.p_custcnt=0;
		RPos.p_itemcnt=0;
		RPos.p_regtime=0;
		RPos.p_salesval=0;
		RPos.p_tendertime=0;
		RGetDrawer();
		RSaveStatus();
		return false; //No more perf slots to register
	}
	tm+=PerfPeriod;
	tm-=tm%PerfPeriod;
	if (tm>86400) {//next day comes
		tm-=86400;
		dt++;
	}
	if (closepartialslot) {
		if (dt>date) {
			dt=date; //next slot is on the next day compared to tran -> end is tran
			tm=time;
		}
		else {
			if ((dt==date)&&(tm>time)) tm=time; //next slot is later than tran -> end is tran
		}
	}
	else {
		if (dt>date) return false; //next slot is on the next day compared to tran -> no advance
		if ((dt==date)&&(tm>time)) return false; //next slot is later than tran -> no advance
	}
	RL.Log("Info: RCheck4NewPerfSlot: Gotta close a previous performance slot. End date=%d time=%d\n"
		,dt,tm);

	TElem21097 E;
	PreFill(&E,21097);

	E.PerfSlotStartDate=RPos.p_startdate;
	E.PerfSlotStartTime=RPos.p_starttime;
	E.LocSignOnTtl=(dt-E.PerfSlotStartDate)*86400+(tm-E.PerfSlotStartTime);
	E.LocRegTimeTtl=RPos.p_regtime;
	E.LocTenderTimeTtl=RPos.p_tendertime;
	E.LocItemKeyCodeTtl=RPos.p_itemcnt;
	E.PerCntSales=0;
	LongVal2Cobol6(E.PerValSales,0);
	E.PerCntItem=0;
	LongVal2Cobol6(E.PerValItem,0);
	E.PerCntReturn=RPos.p_custcnt;
	LongVal2Cobol6(E.PerValReturn,RPos.p_salesval);
	E.PerCntItemReturn=RPos.p_itemcnt;
	LongVal2Cobol6(E.PerValItemReturn,RPos.p_salesval);
	LongVal2Cobol6(E.PerValItemNeg,0);

	//E.NumberOfCustomers=RPos.p_custcnt;

	RPos.p_startdate=dt;
	RPos.p_starttime=tm;
	RPos.p_custcnt=0;
	RPos.p_itemcnt=0;
	RPos.p_regtime=0;
	RPos.p_salesval=0;
	RPos.p_tendertime=0;

	if ((NoPerfLogMask&0x0000001)==0) {
		SINT16 hp;
		ISS_T_LOG_BLOCK_HANDLE  handle;
		ISS_T_REPLY retval=0;
		if ((retval=iss46_hlog_get_current_hp(&hp))!=ISS_SUCCESS) {
			RL.LogE("ERROR: RCheck4NewPerfSlot.get_current_hp: %d, retval=%d\n",hp,retval);
			RL.LogE("==> RCheck4NewPerfSlot will be aborted or retried!\n");
			return retval;
		}
		RL.Log("Info: RCheck4NewPerfSlot.get_current_hp: %d\n",hp);

		//Checks done before opening a new transaction
		SINT32        transno;
		ISS_T_BOOLEAN inprogress;
		SINT16        element_num;

		retval=iss46_hlog_get_curr_trans_num(RPos.sid,&transno,&inprogress,&element_num);
		RL.Log("Info: RCheck4NewPerfSlot.check_before_start.get_curr_trans: tr=%d, open=%d, elem=%d\n"
			,transno,inprogress,element_num);
		if (retval!=ISS_SUCCESS) {
			RL.LogE("ERROR: RCheck4NewPerfSlot.check_before_start.get_curr_trans: Bad return value! (%d)\n",retval);
			return false;
		}
		if (inprogress) {
			RL.LogE("ERROR: RCheck4NewPerfSlot.check_before_start: Transaction must not be open at this point!\n");
			return false;
		}
		RPos.lasttran++;//Advance to the new transaction number
		if (RPos.lasttran!=transno) {
			L.LogE("Warning: Check4NewPerfSlot.check_before_start: Transaction numbers doesn't match!\n");
			L.LogE("==> RPos.lasttran=%d ISSLOG.transno=%d SID=%d\n"
				,RPos.lasttran,transno,RPos.sid);
		}

		//Opening a transaction
		char transuser[9];
		memcpy(transuser,RUserS,9);
		
		ISS_T_LOG_ELEM_ID element;
		element.element_num=1;
		element.element_time=time;


		if ((retval=iss46_hlog_start_block(RPos.sid,hp,RPos.lasttran,dt,tm
			,0,0x00018000,element,9,transuser,&handle))!=ISS_SUCCESS) {
			RL.LogE("ERROR: RCheck4NewPerfSlot.start_tran returned: %d -> retrying!\n",retval);
			return(retval);
		}
		else RL.Log("RCheck4NewPerfSlot.start_tran: Successful.\n");

		retval=iss46_hlog_send_element(handle,21097,tm,0x0000,sizeof(TElem21097),(char*)&E);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			RL.LogE("ERROR: RCheck4NewPerfSlot.21097 returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,dt,2))!=ISS_SUCCESS) {
				RL.LogE("ERROR: RCheck4NewPerfSlot.21097: AbortTran: %d!\n",ret2);
			}
			else RL.Log("Info: RCheck4NewPerfSlot.21097: AbortTran successful.\n");
			return(retval);
		}
		else RL.Log("ProcLogoff.21097: successful.\n");

		retval=iss46_hlog_end_trans(handle,dt,2);
		if (retval!=ISS_SUCCESS) {
			ISS_T_REPLY ret2;
			RL.LogE("ERROR: RCheck4NewPerfSlot.end_tran returned: %d -> abort tran & retry!\n",retval);
			if ((ret2=iss46_hlog_void_trans(handle,dt,2))!=ISS_SUCCESS) {
				RL.LogE("ERROR: RCheck4NewPerfSlot.end_tran: AbortTran: %d!\n",ret2);
			}
			else RL.Log("Info: RCheck4NewPerfSlot.end_tran: AbortTran successful.\n");
			return(retval);
		}
		else RL.Log("RCheck4NewPerfSlot.end_tran: successful.\n");

//		retval=iss46_hlog_send_block(handle);
//		L.Log("Check4NewPerfSlot.sendb: %d\n",retval);

	}
	else {
		RL.Log("RCheck4NewPerfSlot: Making performance log records has been disabled.(L1)\n");
	}

	RSaveStatus();

	return true;
}

long RegVoucher(ISS_GDECIMAL_6 id,long value,short year,short month,short day)
{
	long expdate;
	long dt;
	bool hasprev=false;	//When a voucher with same ID has been found on file

	expdate=DateYMD2Int(year,month,day);

    ISS_T_REPLY        result=ISS_SUCCESS;
    ISS_T_REPLY        result2=ISS_SUCCESS;
    UINT16             buffer_id;
    ISS_T_DB_KEY       key[1];
    //ISS_GDECIMAL_6     item_code;

    // Get the item read from the database first, with exclusive lock

    key[0].field_name          = "VOUCHER_NUMBER";
    key[0].field_length        = sizeof(id);
    key[0].field_value         = id;
    key[0].field_value_unknown = FALSE;

    result=ReadISS(&buffer_id,true,"ZEVVOUCH",1,key,ISS_C_DB_EQUAL,ISS_C_DB_EXCLUSIVE_LOCK
		,"EXPIRY_DATE",4,&dt,false);

	if (result==ISS_SUCCESS) {//found on file!
		if ((dt+VoucherSafeOverWriteDays)<expdate) {
			ISS_T_BOOLEAN flag=FALSE;
			RL.Log("Info: RegVoucher: Voucher has been found with same ID but long time expired in the database.\n");
			hasprev=true;
			if ((result=iss_db_update_field(buffer_id, "VOUCHER_USED", sizeof(flag), 0
					,&flag, FALSE))!=ISS_SUCCESS) {
				RL.LogE("ERROR: RegVoucher: Cannot update field VOUCHER_USED! retval=%d\n",result);
				if ((result2=iss_db_end_su())!=ISS_SUCCESS)
					RL.LogE("ERROR: RegVoucher: Cannot end success unit! retval=%d\n",result2);
				return result;
			}
			if ((result=iss_db_update_field(buffer_id, "VOUCHER_SENT", sizeof(flag), 0
					,&flag, FALSE))!=ISS_SUCCESS) {
				RL.LogE("ERROR: RegVoucher: Cannot update field VOUCHER_SENT! retval=%d\n",result);
				if ((result2=iss_db_end_su())!=ISS_SUCCESS)
					RL.LogE("ERROR: RegVoucher: Cannot end success unit! retval=%d\n",result2);
				return result;
			}
			if ((result=iss_db_update_field(buffer_id, "LOCATION_ID", 0, 0
					,NULL, TRUE))!=ISS_SUCCESS) {
				RL.LogE("ERROR: RegVoucher: Cannot update field LOCATION_ID! retval=%d\n",result);
				if ((result2=iss_db_end_su())!=ISS_SUCCESS)
					RL.LogE("ERROR: RegVoucher: Cannot end success unit! retval=%d\n",result2);
				return result;
			}
			if ((result=iss_db_update_field(buffer_id, "USER_ID", 0, 0
					,NULL, TRUE))!=ISS_SUCCESS) {
				RL.LogE("ERROR: RegVoucher: Cannot update field USER_ID! retval=%d\n",result);
				if ((result2=iss_db_end_su())!=ISS_SUCCESS)
					RL.LogE("ERROR: RegVoucher: Cannot end success unit! retval=%d\n",result2);
				return result;
			}
			if ((result=iss_db_update_field(buffer_id, "TRANS_NUM", 0, 0
					,NULL, TRUE))!=ISS_SUCCESS) {
				RL.LogE("ERROR: RegVoucher: Cannot update field TRANS_NUM! retval=%d\n",result);
				if ((result2=iss_db_end_su())!=ISS_SUCCESS)
					RL.LogE("ERROR: RegVoucher: Cannot end success unit! retval=%d\n",result2);
				return result;
			}
			if ((result=iss_db_update_field(buffer_id, "TRANS_DATE", 0, 0
					,NULL, TRUE))!=ISS_SUCCESS) {
				RL.LogE("ERROR: RegVoucher: Cannot update field TRANS_DATE! retval=%d\n",result);
				if ((result2=iss_db_end_su())!=ISS_SUCCESS)
					RL.LogE("ERROR: RegVoucher: Cannot end success unit! retval=%d\n",result2);
				return result;
			}
			if ((result=iss_db_update_field(buffer_id, "TRANS_TIME", 0, 0
					,NULL, TRUE))!=ISS_SUCCESS) {
				RL.LogE("ERROR: RegVoucher: Cannot update field TRANS_TIME! retval=%d\n",result);
				if ((result2=iss_db_end_su())!=ISS_SUCCESS)
					RL.LogE("ERROR: RegVoucher: Cannot end success unit! retval=%d\n",result2);
				return result;
			}
			if ((result=iss_db_update_field(buffer_id, "TENDER_VAL", 0, 0
					,NULL, TRUE))!=ISS_SUCCESS) {
				RL.LogE("ERROR: RegVoucher: Cannot update field TENDER_VAL! retval=%d\n",result);
				if ((result2=iss_db_end_su())!=ISS_SUCCESS)
					RL.LogE("ERROR: RegVoucher: Cannot end success unit! retval=%d\n",result2);
				return result;
			}
		}
		else {
			if (dt==expdate) 
				RL.LogE("Warning: RegVoucher: Voucher has been found with same expiration in the database. This might be an error.\n");
			else RL.LogE("ERROR: RegVoucher: Voucher ID collision with different data!\n");
			if ((result=iss_db_free_buffer(&buffer_id))!=ISS_SUCCESS)
				RL.LogE("ERROR: RegVoucher: Cannot free buffer! retval=%d\n",result);
			if ((result=iss_db_end_su())!=ISS_SUCCESS)
				RL.LogE("ERROR: RegVoucher: Cannot end success unit! retval=%d\n",result);
			return result;
		}
	}
    else {
		if (result!=ISS_E_SDBMS_NOREC) {
			RL.LogE("ERROR: RegVoucher: Unexpected error during checking existence: %d!\n",result);
			if ((result2=iss_db_end_su())!=ISS_SUCCESS)
				RL.LogE("ERROR: RegVoucher: Cannot end success unit! retval=%d\n",result2);
			return result;
		}
		//Begin success unit for writing the database
//		if ( (result = iss_db_begin_su () ) != ISS_SUCCESS) {
//			RL.LogE("ERROR: RegVoucher: Cannot begin success unit! retval=%d\n",result);
//			return result;
//		}
		//Gotta create buffer - voucher is not in the database yet
		if ((result=iss_db_create_buffer("ZEVVOUCH", &buffer_id))!=ISS_SUCCESS) {
			RL.LogE("ERROR: RegVoucher: Cannot create buffer! retval=%d\n",result);
			if ((result2=iss_db_end_su())!=ISS_SUCCESS)
				RL.LogE("ERROR: RegVoucher: Cannot end success unit! retval=%d\n",result2);
			return result;
		}
		if ((result=iss_db_update_field(buffer_id, "VOUCHER_NUMBER", sizeof(id), 0
				,id, FALSE))!=ISS_SUCCESS) {
			RL.LogE("ERROR: RegVoucher: Cannot update field VOUCHER_NUMBER! retval=%d\n",result);
			if ((result=iss_db_free_buffer(&buffer_id))!=ISS_SUCCESS)
				RL.LogE("ERROR: RegVoucher: Cannot free buffer! retval=%d\n",result);
			if ((result=iss_db_end_su())!=ISS_SUCCESS)
				RL.LogE("ERROR: RegVoucher: Cannot end success unit! retval=%d\n",result);
			return result;
		}
    }
	//Common part of the update
	if ((result=iss_db_update_field(buffer_id, "VOUCHER_VAL", sizeof(value), 0
			,&value, FALSE))!=ISS_SUCCESS) {
		RL.LogE("ERROR: RegVoucher: Cannot update field VOUCHER_VAL! retval=%d\n",result);
		if ((result=iss_db_free_buffer(&buffer_id))!=ISS_SUCCESS)
			RL.LogE("ERROR: RegVoucher: Cannot free buffer! retval=%d\n",result);
		if ((result=iss_db_end_su())!=ISS_SUCCESS)
			RL.LogE("ERROR: RegVoucher: Cannot end success unit! retval=%d\n",result);
		return result;
	}
	if ((result=iss_db_update_field(buffer_id, "EXPIRY_DATE", sizeof(expdate), 0
			,&expdate, FALSE))!=ISS_SUCCESS) {
		RL.LogE("ERROR: RegVoucher: Cannot update field EXPIRY_DATE! retval=%d\n",result);
		if ((result=iss_db_free_buffer(&buffer_id))!=ISS_SUCCESS)
			RL.LogE("ERROR: RegVoucher: Cannot free buffer! retval=%d\n",result);
		if ((result=iss_db_end_su())!=ISS_SUCCESS)
			RL.LogE("ERROR: RegVoucher: Cannot end success unit! retval=%d\n",result);
		return result;
	}
    //Write buffer to DB
    if (result==ISS_SUCCESS) {
        if ((result=iss_db_write_buffer(&buffer_id))!=ISS_SUCCESS)
			RL.LogE("ERROR: RegVoucher: Cannot write buffer to DB! retval=%d\n",result);
    }
    //Finish transaction
    if (result==ISS_SUCCESS) {
        if ((result=iss_db_end_su())!=ISS_SUCCESS)
			RL.LogE("ERROR: RegVoucher: Cannot end success unit! retval=%d\n",result);
    }
    //Abort transaction upon error
    if (result!=ISS_SUCCESS) {
        if (iss_db_abort_su()!=ISS_SUCCESS) 
			RL.LogE("ERROR: RegVoucher: Cannot abort success unit! retval=%d\n",result);
    }
    return result;
}

void RegTicketIssue(long machine, long ticketnum)
{
	long firstticketnum;//first registered ticket number
	long lastticketnum;	//last registered ticket number
	//if (TicketIssue.GetIndex(machine,false)) {//not the first ticket
	if (!(TicketIssue.GetCounters(machine,&firstticketnum,&lastticketnum))) {
		//first ticket/no previous registration
		firstticketnum=ticketnum;
	}
	else {
		if ((ticketnum!=lastticketnum+1)&&(ticketnum!=0)&&(ticketnum!=1)) {
			RL.LogE("ERROR: ProcRVMTran.TicketReg: Unexpected ticket number %d on RVM #%d! Previous:%d\n"
				,ticketnum,machine,lastticketnum);
		}
	}
	TicketIssue.SetFigures(machine,firstticketnum,ticketnum);
	TicketIssue.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegTicketIssue);
}

bool Chk4RVMIdle(void)
{
	if (!(*RPos.user)) return true; //Can sleep if already signed off
	SYSTEMTIME st;
	GetLocalTime(&st);
	long mode=0;//0: nothing 1:close slot 2:escape logoff
	long dt=DateYMD2Int(st.wYear,st.wMonth,st.wDay);
	long ti=st.wHour*3600+st.wMinute*60+st.wSecond;
	if ((dt-RPos.lasttrandate>1)||(dt-RPos.lasttranlocdate>1)) {
		RL.LogE("Warning: Chk4RVMIdle: Logoff: Mode:-1 (Huge local date difference: %d vs. %d or %d).\n",dt,RPos.lasttranlocdate,RPos.lasttrandate);
		mode=-1;//Time difference is too much
	}
	else {
		if (ti+((dt>RPos.lasttranlocdate)?86400:0)>RPos.lasttranloctime+LocalTimeIdleLimit) {
			RL.Log("Info: Chk4RVMIdle: Logoff: Mode:1 (Local time based: %d,%d).\n",RPos.lasttranlocdate,RPos.lasttranloctime);
			mode=1;
		}
		else if (ti+((dt>RPos.lasttrandate)?86400:0)>RPos.lasttrantime+TranTimeIdleLimit) {
			RL.Log("Info: Chk4RVMIdle: Logoff: Mode:2 (Trans time based: %d,%d).\n",RPos.lasttrandate,RPos.lasttrantime);
			mode=2;
		}
	}
	if (mode==0) return true; //idle, nothing changed, can sleep
	if (mode>0) {
		RL.Log("Info: Chk4RVMIdle: Perform closing partial performance slot by last tran time: %d,%d(+1).\n",RPos.lasttrandate,RPos.lasttrantime);
		RCheck4NewPerfSlot(RPos.lasttrandate,RPos.lasttrantime+((RPos.lasttrantime<86399)?1:0),true);//close partial slot
	}
	*RPos.user=0;//logoff
	return false;//did something, no sleep
}

void RProcLogon(long date, long time)
{
	memcpy(RPos.user,RUserS,9);
	RPos.p_startdate=date;
	RPos.p_starttime=time;

	RGetDrawer();
}

long ProcRVMTran(FILE *F)
{
	TElem5101	E5101;
	TElem21001	E21001;
	TElem1021	E1021;
	TElem2000	E2000;
	//TElem21099	E21099;
	TElem21020	E21020;
	TElem21024	E21024;

	SINT16 hp;
	ISS_T_LOG_BLOCK_HANDLE  handle;
	SYSTEMTIME st;
	ISS_T_REPLY retval=0;
	if ((retval=iss46_hlog_get_current_hp(&hp))!=ISS_SUCCESS) {
		RL.LogE("ERROR: ProcRVMTran.get_current_hp: %d, retval=%d\n",hp,retval);
		RL.LogE("==> ProcRVMTran will be aborted or retried!\n");
		return -1; //Negative for retry
	}
	RL.Log("Info: ProcRVMTran.get_current_hp: %d\n",hp);

	int mod;

	short vday;
	short vmonth;
	short vyear;
	long vvalue;
	char vnum[14];
	char exp_date[12];
	ISS_GDECIMAL_6 vid;

	bool open=false; //transaction opened
	long tenders;	//payment records logged already (to reject sales too)
	long sold;		//total sales
	long total=-1;	//current balance
	//long posn;		//pos number
	//long trantime;	//transaction total time
	long elements;	//counting logged elements
	long items;		//count of item records
	long quantity;
	ISS_GDECIMAL_4 unitprice;
	long extprice;
	long storenum;
	long machine;
	long ticketnum;
	//long firstticketnum;//first registered ticket number
	//long lastticketnum;	//last registered ticket number
	long l;
	char s[64];
	char transuser[9]={0,0,0,0,0,0,0,0,0};
	ISS_T_LOG_ELEM_ID element;
	ISS_T_INT_TIME time;
	s[62]=0; //protection to freely use s[63]
	ISS_GDECIMAL_6 ean;
	char itemcollector[sizeof(ISS_GDECIMAL_6)+3*sizeof(long)];
	char progress=0;
	while (fgets(s,62,F)) {
		switch(*s|32) {
		case 'h'://header
			if ((strlen(s)<51)||(s[50]<' ')||(s[49]<' ')) {
				RL.LogE("ERROR: ProcRVMTran.PreProc: Receipt opening record is too small! -> File rejected!\n");
				return ISS_SUCCESS;
			}
			for (l=1;l<51;l++) {
				if ((s[l]<'0')||(s[l]>'9')) {
					RL.LogE("ERROR: ProcRVMTran.PreProc: Invalid character ('%c') in header record! -> File rejected!\n",s[l]);
					return ISS_SUCCESS;
				}
			}
			if (progress) {
				RL.LogE("ERROR: ProcRVMTran.PreProc: Another receipt opening record! -> Transaction rejected!\n");
				return ISS_SUCCESS;
			}
			progress++;
			s[43]=0;
			total=atol(s+31);
			if (strncmp(s+16+strlen(VoucherLeadStr),s+1,2)!=0) {
				RL.LogE("ERROR: ProcRVMTran.PreProc: Store ID mismatched in record and voucher ID! -> Transaction rejected!\n");
				return ISS_SUCCESS;
			}
			if (strncmp(s+18,VoucherLeadStr,strlen(VoucherLeadStr))!=0) {
				RL.LogE("ERROR: ProcRVMTran.PreProc: Voucher ID mismatches leading number structure! -> Transaction rejected!\n");
				return ISS_SUCCESS;
			}
			s[63]=s[31];
			s[31]=0;
			if (s[30]!=GenCD(s+18)) {
				RL.LogE("ERROR: ProcRVMTran.PreProc: Voucher ID has invalid EAN13 check digit! -> Transaction rejected!\n");
				return ISS_SUCCESS;
			}
			s[31]=s[63];
			machine=(s[3]-'0');
			if (((machine!=9)&&(machine>RPosCount))||(machine<1)) {
				RL.LogE("ERROR: ProcRVMTran: RVM number is bad! (%d) -> File rejected!\n",machine);
				return ISS_SUCCESS;
			}
			if (s[3]!=s[18+strlen(VoucherLeadStr)]) {
				RL.LogE("ERROR: ProcRVMTran: RVM number (%d) not reflected (%c) in voucher barcode (%s)! -> File rejected!\n"
					,machine,s[18+strlen(VoucherLeadStr)],s+18);
				return ISS_SUCCESS;
			}
			if (VoucherPadChar) {
				if (VoucherPadChar!=s[29]) {
					RL.LogE("Warning: ProcRVMTran.PreProc: Voucher ID has invalid trailing padding character! Expecting closure problems!\n");
				}
				s[29]=0;
				ticketnum=atol(s+19+strlen(VoucherLeadStr));
				s[29]=(char)VoucherPadChar;
			}
			else {
				s[63]=s[30];
				s[30]=0;
				ticketnum=atol(s+19+strlen(VoucherLeadStr));
				s[30]=s[63];
			}
			break;
		case 'i'://Item
			if (progress==0) {
				RL.LogE("ERROR: ProcRVMTran.PreProc: Item record without opening (header) record! -> File rejected!\n");
				return ISS_SUCCESS;
			}
			if ((strlen(s)<32)||(s[31]<' ')||(s[30]<' ')) {
				RL.LogE("ERROR: ProcRVMTran.PreProc: Item record is too small! -> File rejected!\n");
				return ISS_SUCCESS;
			}
			for (l=1;l<32;l++) {
				if ((s[l]<'0')||(s[l]>'9')) {
					RL.LogE("ERROR: ProcRVMTran.PreProc: Invalid character ('%c') in item record! -> File rejected!\n",s[l]);
					return ISS_SUCCESS;
				}
			}
			progress=2;
			s[32]=0;
			total-=atol(s+20);
			s[20]=0;
			if (atol(s+14)==0) {
				RL.LogE("ERROR: ProcRVMTran: Item has zero quantity -> rejecting transaction!\n");
				return(0);
			}
			break;
		default://unknown, or not in this file
			L.LogE("ERROR: ProcRVMTran.PreProc: No other record types than 'H' & 'I' are allowed in this file!\n");
		}
	}
	fseek(F,0,SEEK_SET);
	if ((progress!=2)||(total!=0)) {
		if (progress!=2) {
			if (total!=0) {
				RL.LogE("ERROR: ProcRVMTran.PreProc: One 'H'eader and at least one 'I'tem record is neccesary!\n");
			}
			else {
				RL.LogE("Warning: ProcRVMTran.PreProc: File has only a 'H'eader record with zero total.\n");
			}
		}
		else RL.LogE("ERROR: ProcRVMTran.PreProc: 'H'eader and 'I'tem totals are mismatched!\n");
		RL.LogE("  ===> For this reason the transaction file cannot be processed! Here is the dump of the file:\n");
		long cnt=1;
		while (fgets(s,62,F)) {
			while ((strlen(s)>0)&&(s[strlen(s)-1]<32)) s[strlen(s)-1]=0;
			RL.LogE("  ===> Line #%d: %s\n",cnt++,s);
		}
		return ISS_SUCCESS;
	}
	while (fgets(s,62,F)) {
		switch(*s|32) {
		case 'h'://header
			//Extract date and time of tran
			SysTimeFromS14(&st,s+4);
			Date=DateYMD2Int(st.wYear,st.wMonth,st.wDay);
			time=st.wHour*3600+st.wMinute*60+st.wSecond;

			storenum=(s[1]-'0')*10+(s[2]-'0');
			
			machine=s[3]-'0';
			s[51]=0;
			strcpy(exp_date,s+43);
			vday=(s[49]-'0')*10+(s[50]-'0');
			vmonth=(s[47]-'0')*10+(s[48]-'0');
			vyear=(s[45]-'0')*10+(s[46]-'0')+2000;
			s[43]=0;
			vvalue=atol(s+31);
			if (RoundVocher==1) {
				vvalue=floor((vvalue/100)+0.5)*100;
				mod=(vvalue/100) % 5;
				if (mod!=0)
					vvalue=vvalue+((5-mod)*100);
			}
			s[31]=0;
			strcpy(vnum,s+18);
			gdec_init_z(vid,6,0,iss_gdec_round_normal);
			gdec_from_ascii(vid,s+18);

			RegVoucher(vid,vvalue,vyear,vmonth,vday);

			if (!(*(RPos.user))) {//noone is currently logged on - no drawer data
				RL.Log("Info: ProcRVMTran: No user logged on. Must process logon to get drawer data!\n");

				RProcLogon(Date,time);
				
			}
			//Advancing performance period(s) if needed
			while (RCheck4NewPerfSlot(Date,time)); //Advancing performance slots until it's necessary

			//Checks done before opening a new transaction
			SINT32        transno;
			ISS_T_BOOLEAN inprogress;
			SINT16        element_num;

			retval=iss46_hlog_get_curr_trans_num(RPos.sid,&transno,&inprogress,&element_num);
			RL.Log("Info: ProcRVMTran.check_before_start.get_curr_trans: tr=%d, open=%d, elem=%d\n"
				,transno,inprogress,element_num);
			if (retval!=ISS_SUCCESS) {
				RL.LogE("ERROR: ProcRVMTran.check_before_start.get_curr_trans: Bad return value! (%d)\n",retval);
				return retval;
			}
			if (inprogress) {
				RL.LogE("ERROR: ProcRVMTran.check_before_start: Transaction must not be open at this point!\n");
				return(-1);
			}
			RPos.lasttran++;//Preparing new transaction's number
			if (RPos.lasttran!=transno) {//checks whether ISS thinks the same
				RL.LogE("Warning: ProcRVMTran.check_before_start: Transaction numbers doesn't match!\n");
				RL.LogE("==> RPos.lasttran=%d ISSLOG.transno=%d SID=%d\n"
					,RPos.lasttran,transno,RPos.sid);
				RPos.lasttran=transno; //Correction by ISS
			}

			//Preparing opening a new transaction
			PreFill(&E5101,	5101);
			element.element_num=1;
			element.element_time=time;
			memcpy(transuser,RPos.user,4);

			if ((retval=iss46_hlog_start_block(RPos.sid,hp,RPos.lasttran,Date,time
				,0,0x00018000,element,9,transuser,&handle))!=ISS_SUCCESS) {
				RL.LogE("ERROR: ProcRVMTran.start returned: %d -> retrying!\n",retval);
				return(retval);
			}
			else RL.Log("ProcRVMTran.start: Successful.\n");

			elements=1;
			if ((retval=iss46_hlog_send_element(handle,5101,time,0x0000,sizeof(E5101),(char*)&E5101))
				!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				RL.LogE("ERROR: ProcRVMTran.5101 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					RL.LogE("ERROR: ProcRVMTran.5101: AbortTran: %d!\n",ret2);
				}
				else RL.Log("Info: ProcRVMTran.5101: AbortTran successful.\n");
				return(retval);
			}
			else RL.Log("Info: ProcRVMTran.5101: Successful.\n");
			
			//21001 long sales
			PreFill(&E21001, 21001);
			E21001.DeptId=(machine==9) ? RItemDeptPET[0] : RItemDept[0];
			E21001.ReturnMode=TRUE;
			E21001.ReturnType=13;
			//E21001.EANType=1;
			//LongVal2Cobol6(E21001.Weight,0);

			//Linked return
			PreFill(&E1021,	1021);
			E1021.DeptId=(machine==9) ? RItemDeptPET[0] : RItemDept[0];
			memcpy(E1021.UserID,transuser,9);

			//payback
			PreFill(&E2000,	2000);
			memcpy(E2000.DrwIDUserID,transuser,9);
			E2000.DrwIDDate=RPos.drwdate;
			E2000.DrwIDTime=RPos.drwtime;

			//Data (not provided)
			//PreFill(&E21099, 21099);
			//E21099.CurrDate=RPos.drwdate;

			//cashier performance
			PreFill(&E21020, 21020);

			//department sales for returns
			PreFill(&E21024, 21024);

			open=true;
			tenders=0;
			elements++;
			items=0;
			sold=0;
			total=0;

			break;
		case 'i'://item
			s[32]=0;
			extprice=atol(s+20);
			s[20]=0;
			quantity=atol(s+14);
			s[14]=0;
			gdec_init_z(ean,6,0,iss_gdec_round_normal);
			gdec_from_ascii(ean,s+1);
long up;
			if (GetItemData(unitprice,&E21001.TaxID,&E21001.ReportGroup,&E21001.EANType,ean)==ISS_E_SDBMS_NOREC) {
				
				RL.LogE("ERROR: ProcRVMTran.Item: No record in ISS400 for EAN %s! Needed values will be guessed.\n"
					,s+1);
				E21001.ReportGroup=1000000;
				E21001.TaxID=2;
				E21001.EANType=4;
				if (quantity) up=extprice/quantity;
				else up=extprice;
				l2gdec(unitprice,6,up);
			}
			up=extprice/quantity;
			GDec62Cobol6(E21001.ItemCode,ean);//itemcode ok
			GDec62Cobol6(E1021.ItemCode,ean);//itemcode ok
			
			E21001.Quantity=quantity;
			E1021.Quantity=quantity;

			LongVal2Cobol6(E21001.ExtPrice,extprice);//Extprice ok
			LongVal2Cobol6(E1021.ExtPrice,extprice);//Extprice ok
			
			LongVal2Cobol6(E21001.UnitPrice,up);//Extprice ok
			LongVal2Cobol6(E21001.PriceFromPlu,up);//Extprice ok

			//gdec2cobol((char *)E21001.UnitPrice,unitprice,6);//Unitprice ok-ish
			//gdec2cobol((char *)E21001.PriceFromPlu,unitprice,6);

			retval=RSafeSendHLogElement(&handle,21001,time,0x0000,sizeof(E21001)
				,(char*)&E21001);
			if (retval!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				RL.LogE("ERROR: ProcRVMTran.21001 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					RL.LogE("ERROR: ProcRVMTran.21001: AbortTran: %d!\n",ret2);
				}
				else RL.Log("Info: ProcRVMTran.21001: AbortTran successful.\n");
				return (ISS_SUCCESS);
			}
			else RL.Log("ProcRVMTran.21001: successful.\n");

			retval=RSafeSendHLogElement(&handle,1021,time,0x0000,sizeof(E1021)
				,(char*)&E1021);
			if (retval!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				RL.LogE("ERROR: ProcRVMTran.1021 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					RL.LogE("ERROR: ProcRVMTran.1021: AbortTran: %d!\n",ret2);
				}
				else RL.Log("Info: ProcRVMTran.1021: AbortTran successful.\n");
				return (ISS_SUCCESS);
			}
			else RL.Log("ProcRVMTran.1021: successful.\n");

			time+=RVMTxnTimePerItem;
			RPos.p_regtime+=RVMTxnTimePerItem;
			RPos.regtime+=RVMTxnTimePerItem;
			RPos.items+=quantity;
			RPos.p_itemcnt+=quantity;
			RPos.p_salesval+=extprice;
			sold+=extprice;
			total+=extprice;
			elements+=2;
			items+=quantity;

			memcpy(itemcollector,&machine,sizeof(long));
			memcpy(itemcollector+sizeof(long),ean,sizeof(ISS_GDECIMAL_6));
			memcpy(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6),&quantity,sizeof(long));
			memcpy(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6),&extprice,sizeof(long));
			RItemTotals.AddFigures(itemcollector);

			break;
		default://unknown, or not in this file
			RL.LogE("ERROR: ProcRVMTran: No other record types than 'H' & 'I' allowed in this file!\n");
		}
	}
	//payment
	E2000.TenderID=((machine==9) ? RTenderIdPET : RTenderId);
	E2000.ReturnMode=TRUE;
	LongVal2Cobol6(E2000.TenderVal,vvalue);

	//AddTendering(posn,E2000.TenderID,extprice);

	retval=RSafeSendHLogElement(&handle,2000,time,0x0000,sizeof(E2000),(char*)&E2000);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		RL.LogE("ERROR: ProcRVMTran.2000 returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			RL.LogE("ERROR: ProcRVMTran.2000: AbortTran: %d!\n",ret2);
		}
		else RL.Log("Info: ProcRVMTran.2000: AbortTran successful.\n");
		return(retval);
	}
	else RL.Log("ProcSale.2000: successful.\n");
	time+=2;

	elements++;
	tenders++;

	RPos.p_tendertime+=2;
	RPos.tendertime+=2;

	//Sale perf record
	E21020.ReturnMode=TRUE;
	LongVal2Cobol6(E21020.SalesTotal,vvalue);
	LongVal2Cobol6(E21020.ItemSoldTtl,vvalue);
	LongVal2Cobol6(E21020.TenderTotal,vvalue);
	E21020.ItemCnt=items;
	E21020.ItemSoldCnt=items;
	E21020.TenderCnt=1;
	retval=RSafeSendHLogElement(&handle,21020,time,0x0000,sizeof(E21020)
		,(char*)&E21020);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		RL.LogE("ERROR: ProcRVMTran.21020 returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			RL.LogE("ERROR: ProcRVMTran.21020: AbortTran: %d!\n",ret2);
		}
		else RL.Log("Info: ProcRVMTran.21020: AbortTran successful.\n");
		return(retval);
	}
	else RL.Log("ProcRVMTran.21020: successful.\n");
	elements++;
	//dept perf records
	E21024.ReturnMode=TRUE;
	E21024.SalesCnt=items;
	E21024.ItemScanCnt=items;
	LongVal2Cobol6(E21024.SalesVal,vvalue);
	if (machine!=9) {
		for(extprice=0;extprice<8;extprice++) {
			if (RItemDept[extprice]!=0) {
				E21024.Dept=RItemDept[extprice];
				retval=RSafeSendHLogElement(&handle,21024,time,0x0000,sizeof(E21024)
					,(char*)&E21024);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					RL.LogE("ERROR: ProcRVMTran.21024 (pass #%d) returned: %d -> abort tran & retry!\n"
						,extprice,retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						RL.LogE("ERROR: ProcRVMTran.21024: AbortTran: %d!\n",ret2);
					}
					else RL.Log("Info: ProcRVMTran.21024: AbortTran successful.\n");
					return(retval);
				}
				else RL.Log("ProcRVMTran.21024: pass #%d successful.\n",extprice);
				elements++;
			}
		}
	}
	else {
		for(extprice=0;extprice<8;extprice++) {
			if (RItemDeptPET[extprice]!=0) {
				E21024.Dept=RItemDeptPET[extprice];
				retval=RSafeSendHLogElement(&handle,21024,time,0x0000,sizeof(E21024)
					,(char*)&E21024);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					RL.LogE("ERROR: ProcRVMTran.PET.21024 (pass #%d) returned: %d -> abort tran & retry!\n"
						,extprice,retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						RL.LogE("ERROR: ProcRVMTran.PET.21024: AbortTran: %d!\n",ret2);
					}
					else RL.Log("Info: ProcRVMTran.PET.21024: AbortTran successful.\n");
					return(retval);
				}
				else RL.Log("ProcRVMTran.PET.21024: pass #%d successful.\n",extprice);
				elements++;
			}
		}
	}
	//close tran
	retval=iss46_hlog_end_trans(handle,Date,(unsigned short)elements);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		RL.LogE("ERROR: ProcRVMTran.EndTran returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			RL.LogE("ERROR: ProcRVMTran.EndTran: AbortTran: %d!\n",ret2);
		}
		else RL.Log("Info: ProcRVMTran.EndTran: AbortTran successful.\n");
		return(retval);
	}
	else RL.Log("ProcRVMTran.EndTran: successful.\n");

//				retval=iss46_hlog_send_block(handle);
//				L.Log("sendb: %d\n",retval);

	MachineTotals.AddFigures(machine,vvalue);

	RegTicketIssue(machine,ticketnum);
	
	RItemTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegRItemTotals);
	MachineTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegMachineTotals);
	RPos.lasttrandate=Date;
	RPos.lasttrantime=time;
	GetLocalTime(&st);
	RPos.lasttranlocdate=DateYMD2Int(st.wYear, st.wMonth, st.wDay);
	RPos.lasttranloctime=st.wHour*3600+st.wMinute*60+st.wSecond;
	RPos.lasttranhp=hp;
	RSaveStatus();
	open=false;
	Insert_BBON_MYSQL(vnum,vvalue/100,exp_date);
	return ISS_SUCCESS;
}

long ProcTOMRATran(FILE *F)
{
	TElem5101	E5101;
	TElem21001	E21001;
	TElem1021	E1021;
	TElem2000	E2000;
	//TElem21099	E21099;
	TElem21020	E21020;
	TElem21024	E21024;

	SINT16 hp;
	ISS_T_LOG_BLOCK_HANDLE  handle;
	SYSTEMTIME st;
	ISS_T_REPLY retval=0;
	if ((retval=iss46_hlog_get_current_hp(&hp))!=ISS_SUCCESS) {
		RL.LogE("ERROR: ProcRVMTran.get_current_hp: %d, retval=%d\n",hp,retval);
		RL.LogE("==> ProcRVMTran will be aborted or retried!\n");
		return -1; //Negative for retry
	}
	RL.Log("Info: ProcTomraTran.get_current_hp: %d\n",hp);

	int mod,counter=0, row_counter=0, row_total=0, row_quantity=0, unit_price=0;

	short vday;
	short vmonth;
	short vyear;
	long vvalue;
	ISS_GDECIMAL_6 vid;

	bool open=false; //transaction opened
	long tenders;	//payment records logged already (to reject sales too)
	long sold;		//total sales
	long total=0;	//current balance
	//long posn;		//pos number
	//long trantime;	//transaction total time
	long elements;	//counting logged elements
	long items;		//count of item records
	long quantity;
	ISS_GDECIMAL_4 unitprice;
	long extprice;
	long storenum;
	long machine;
	long ticketnum;
	//long firstticketnum;//first registered ticket number
	//long lastticketnum;	//last registered ticket number
	long l;
	char s[64],str_tmp[50],str1[50];
	char transuser[9]={0,0,0,0,0,0,0,0,0};
	ISS_T_LOG_ELEM_ID element;
	ISS_T_INT_TIME time;
	s[62]=0; //protection to freely use s[63]
	ISS_GDECIMAL_6 ean;
	char itemcollector[sizeof(ISS_GDECIMAL_6)+3*sizeof(long)];
	char progress=0,ean_full[13];
//	char *chd;
	FILE *IF;
	char seps[]   = "|\n";
	char *token;
	char a[100];
	short y,m,d;
	int rcounter=0,items_counter=0;
	TomraEAN tomralist[200];
	if ((IF=fopen(FNTomraEAN,"rt"))!=NULL) {
		if( fseek( IF,0, SEEK_SET) )	{
			RL.LogE("ERROR - Fseek failed %s\n",FNTomraEAN );
		}
		else   {
			while (!feof(IF)){
			  fgets( a,250,IF);
				token = strtok( a, seps );
				sprintf(tomralist[items_counter].tomra,"%s",token);
				counter = 0;
				while( token != NULL )	{
					counter++;
					token = strtok( NULL, seps );
					switch(counter) {
						case 1: sprintf(tomralist[items_counter].ean,"%s",token); break;
					}
				}
				items_counter++;
			}
		}
		fclose(IF);
		RL.Log("The Tomra converter table is loaded. Counter: %d\n",items_counter);
		counter=0;
	}



		
	//                      01#1#000013#000018#170711192631#001      version#1#sorsz�m#id�pont#sorok sz�ma
	//                      0000000001001#000009#000018#00002#M#00	 csoport azonos�t�#�r#tot�l#darab#M vagy E#�fa k�d

	while (fgets(s,62,F)) {
		counter++;
		if(counter==1) {
			if ((strlen(s)<35)||(s[34]<' ')||(s[33]<' ')){ 
					RL.LogE("ERROR: ProcTomraTran.PreProc: Receipt opening record is too small! -> File rejected!\n");
					return ISS_SUCCESS;
			}
			for (l=1;l<35;l++) {
				if ((s[l]<'0')||(s[l]>'9'))
					if(s[l]!='#')
						if(s[l]!='M')
							if(s[l]!='E') {
								RL.LogE("ERROR: ProcTomraTran.PreProc: Invalid character ('%c') in header record! -> File rejected!\n",s[l]);
								return ISS_SUCCESS;
							}
			}
			/*if (progress) {
				RL.LogE("ERROR: ProcTomraTran.PreProc: Another receipt opening record! -> Transaction rejected!\n");
				return ISS_SUCCESS;
			}*/
			progress++;
			s[63]=s[11];
			s[11]=0;
			strcpy(ean_full,VoucherLeadStr);
			strcat(ean_full,"10");
			strcat(ean_full,s+6);
			str_tmp[0]=VoucherPadChar;
			str_tmp[1]=0;
			//itoa(VoucherPadChar,str_tmp,10);
			strcat(ean_full,str_tmp);
			strcat(ean_full,"0");
//			strcpy(chd,ean_full);
			ean_full[13]=0;
			str_tmp[0]=GenCD(ean_full);
			ean_full[12]=0;
			strcat(ean_full,str_tmp);
			ticketnum=atol(ean_full); //vonalk�d
			s[11]=s[63];
			machine=1;
			row_counter=atoi(s+32); //h�ny �veg sor van
		}
		else{//                      0000000001001#000009#000018#00002#M#00	 csoport azonos�t�#�r#tot�l#darab#M vagy E#�fa k�d
			if (progress==0) {
				RL.LogE("ERROR: ProcTomraTran.PreProc: Item record without opening (header) record! -> File rejected!\n");
				return ISS_SUCCESS;
			}
			if ((strlen(s)<38)||(s[37]<'0')||(s[36]<'0')) {
				RL.LogE("ERROR: ProcTomraTran.PreProc: Item record is too small! -> File rejected!\n");
				return ISS_SUCCESS;
			}
			for (l=1;l<38;l++) {
				if ((s[l]<'0')||(s[l]>'9'))
				  if(s[l]!='#')
					if(s[l]!='M')
						if(s[l]!='E') {
						RL.LogE("ERROR: ProcTomraTran.PreProc: Invalid character ('%c') in item record! -> File rejected!\n",s[l]);
						return ISS_SUCCESS;
					}
			}
			progress=2;
			s[63]=s[27];
			s[27]=0;
			total+=atol(s+21);
			row_total=atol(s+21);
			s[27]=s[63];
			strcpyi(str_tmp,s+14,6);
			str_tmp[6]=0;
			unit_price=atoi(str_tmp);
			strcpyi(str_tmp,s+28,5);
			str_tmp[5]=0;
			row_quantity=atoi(str_tmp);
			if(row_quantity*unit_price!=row_total){
				RL.LogE("ERROR: ProcTomraTran: quantity(%d) * price(%d) != total(%d) -> rejecting transaction! %s\n",row_quantity,unit_price,row_total,s);
				return(0);
			}
			if (row_total==0) {
				RL.LogE("ERROR: ProcTomraTran: Item has zero quantity -> rejecting transaction!\n");
				return(0);
			}
		}
	}//while
	fseek(F,0,SEEK_SET);
	if (progress!=2) {
		if (total!=0) {
			RL.LogE("ERROR: ProcTomraTran.PreProc: One 'H'eader and at least one 'I'tem record is neccesary!\n");
		}
		else {
			RL.LogE("Warning: ProcTomraTran.PreProc: File has only a 'H'eader record with zero total.\n");
		}
		RL.LogE("  ===> For this reason the transaction file cannot be processed! Here is the dump of the file:\n");
		long cnt=1;
		while (fgets(s,62,F)) {
			while ((strlen(s)>0)&&(s[strlen(s)-1]<32)) s[strlen(s)-1]=0;
			RL.LogE("  ===> Line #%d: %s\n",cnt++,s);
		}
		return ISS_SUCCESS;
	}
	if (counter-1!=row_counter){
		RL.LogE("ERROR: ProcTomraTran.PreProc: 'H'eader and 'I'tem totals are mismatched!\n");
		RL.LogE("  ===> For this reason the transaction file cannot be processed! Here is the dump of the file:\n");
		long cnt=1;
		while (fgets(s,62,F)) {
			while ((strlen(s)>0)&&(s[strlen(s)-1]<32)) s[strlen(s)-1]=0;
			RL.LogE("  ===> Line #%d: %s\n",cnt++,s);
		}
		return ISS_SUCCESS;
	}
	counter=0;
	fseek(F,0,SEEK_SET);
	while (fgets(s,62,F)) {
	//                      01#1#000013#000018#170711192631#001      version#1#sorsz�m#id�pont#sorok sz�ma
		if(counter==0) {
			//Extract date and time of tran
			strcpyi(str_tmp,s+19,12);
			strcpy(str1,"20");
			strcat(str1,str_tmp);
			SysTimeFromS14(&st,str1);
			Date=DateYMD2Int(st.wYear,st.wMonth,st.wDay);
			TomraExpiryDate=Date;
			TomraExpiryDate+=TomraExpireDays; //Mert itt az �veg bedob�s ideje van, de meg kell toldani 31 nappal
			
			DateInt2YMD(TomraExpiryDate,&y,&m,&d);
			RL.Log("Expiry date: %d.%d.%d\n",y,m,d);
			time=st.wHour*3600+st.wMinute*60+st.wSecond;

			storenum=StoreNum;
			
			machine=1;

			vday=(str1[6]-'0')*10+(str1[7]-'0');
			vmonth=(str1[4]-'0')*10+(str1[5]-'0');
			vyear=(str1[2]-'0')*10+(str1[3]-'0')+2000;
			//s[43]=0;
			vvalue=total;
			if (RoundVocher==1) {
				vvalue=floor(vvalue+0.5)*100;
				mod=(vvalue/100) % 5;
				if (mod!=0)
					vvalue=vvalue+((5-mod)*100);
			}
			s[31]=0;
			gdec_init_z(vid,6,0,iss_gdec_round_normal);
			gdec_from_ascii(vid,ean_full);//vonalk�d

			RegVoucher(vid,vvalue,y,m,d);
			//RegVoucher(vid,vvalue,vyear,vmonth,vday);
			if (!(*(RPos.user))) {//noone is currently logged on - no drawer data
				RL.Log("Info: ProcTomraTran: No user logged on. Must process logon to get drawer data!\n");

				RProcLogon(Date,time);
				
			}
			//Advancing performance period(s) if needed
			while (RCheck4NewPerfSlot(Date,time)); //Advancing performance slots until it's necessary

			//Checks done before opening a new transaction
			SINT32        transno;
			ISS_T_BOOLEAN inprogress;
			SINT16        element_num;

			retval=iss46_hlog_get_curr_trans_num(RPos.sid,&transno,&inprogress,&element_num);
			RL.Log("Info: ProcTomraTran.check_before_start.get_curr_trans: tr=%d, open=%d, elem=%d\n"
				,transno,inprogress,element_num);
			if (retval!=ISS_SUCCESS) {
				RL.LogE("ERROR: ProcTomraTran.check_before_start.get_curr_trans: Bad return value! (%d)\n",retval);
				return retval;
			}
			if (inprogress) {
				RL.LogE("ERROR: ProcTomraTran.check_before_start: Transaction must not be open at this point!\n");
				return(-1);
			}
			RPos.lasttran++;//Preparing new transaction's number
			if (RPos.lasttran!=transno) {//checks whether ISS thinks the same
				RL.LogE("Warning: ProcTomraTran.check_before_start: Transaction numbers doesn't match!\n");
				RL.LogE("==> RPos.lasttran=%d ISSLOG.transno=%d SID=%d\n"
					,RPos.lasttran,transno,RPos.sid);
				RPos.lasttran=transno; //Correction by ISS
			}

			//Preparing opening a new transaction
			PreFill(&E5101,	5101);
			element.element_num=1;
			element.element_time=time;
			memcpy(transuser,RPos.user,4);

			if ((retval=iss46_hlog_start_block(RPos.sid,hp,RPos.lasttran,Date,time
				,0,0x00018000,element,9,transuser,&handle))!=ISS_SUCCESS) {
				RL.LogE("ERROR: ProcTomraTran.start returned: %d -> retrying!\n",retval);
				return(retval);
			}
			else RL.Log("ProcTomraTran.start: Successful.\n");

			elements=1;
			if ((retval=iss46_hlog_send_element(handle,5101,time,0x0000,sizeof(E5101),(char*)&E5101))
				!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				RL.LogE("ERROR: ProcTomraTran.5101 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					RL.LogE("ERROR: ProcTomraTran.5101: AbortTran: %d!\n",ret2);
				}
				else RL.Log("Info: ProcTomraTran.5101: AbortTran successful.\n");
				return(retval);
			}
			else RL.Log("Info: ProcTomraTran.5101: Successful.\n");
			
			//21001 long sales
			PreFill(&E21001, 21001);
			E21001.DeptId=(machine==9) ? RItemDeptPET[0] : RItemDept[0];
			E21001.ReturnMode=TRUE;
			E21001.ReturnType=13;
			//E21001.EANType=1;
			//LongVal2Cobol6(E21001.Weight,0);

			//Linked return
			PreFill(&E1021,	1021);
			E1021.DeptId=(machine==9) ? RItemDeptPET[0] : RItemDept[0];
			memcpy(E1021.UserID,transuser,9);

			//payback
			PreFill(&E2000,	2000);
			memcpy(E2000.DrwIDUserID,transuser,9);
			E2000.DrwIDDate=RPos.drwdate;
			E2000.DrwIDTime=RPos.drwtime;

			//Data (not provided)
			//PreFill(&E21099, 21099);
			//E21099.CurrDate=RPos.drwdate;

			//cashier performance
			PreFill(&E21020, 21020);

			//department sales for returns
			PreFill(&E21024, 21024);

			open=true;
			tenders=0;
			elements++;
			items=0;
			sold=0;
			total=0;
		}
		else{
	//                      0000000001001#000009#000018#00002#M#00	 csoport azonos�t�#�r#tot�l#darab#M vagy E#�fa k�d
			strcpyi(str_tmp,s+21,6);
			str_tmp[6]=0;
			extprice=atol(str_tmp);
			strcpyi(str_tmp,s+28,5);
			str_tmp[5]=0;
			quantity=atol(str_tmp);
/*****************************************************************************************************************/
/****************** IDE KELL BERAKNI A FORD�T�T, HOGY MELYIK CSOPORTB�L MILYEN VONALK�DOT CSIN�LJON  *************/
/*****************************************************************************************************************/
			s[13]=0;
			remove_lchar(s,"0");
			sprintf(str_tmp,"%s",s);
			for(rcounter=0;rcounter<items_counter;rcounter++){
				if(strcmp(str_tmp,tomralist[rcounter].tomra)==0) {
					sprintf(str_tmp,"%s",tomralist[rcounter].ean);
					rcounter=items_counter+5;
				}
			}
			if(rcounter<=items_counter) 
				RL.Log("ERROR -- Cannot find TOMRA code in file! code:%s\n",str_tmp);
			
			gdec_init_z(ean,6,0,iss_gdec_round_normal);
			gdec_from_ascii(ean,str_tmp);
			long up=0;
			if (quantity) up=extprice/quantity;
				else up=extprice;
			if (GetItemData(unitprice,&E21001.TaxID,&E21001.ReportGroup,&E21001.EANType,ean)==ISS_E_SDBMS_NOREC) {
				
				RL.LogE("ERROR: ProcTomraTran.Item: No record in ISS400 for EAN %s! Needed values will be guessed.\n"
					,ean_full);
				E21001.ReportGroup=1000000;
				E21001.TaxID=2;
				E21001.EANType=4;
				
				l2gdec(unitprice,6,up);
			}
//			up=extprice/quantity;
			
			GDec62Cobol6(E21001.ItemCode,ean);//itemcode ok
			GDec62Cobol6(E1021.ItemCode,ean);//itemcode ok
			
			E21001.Quantity=quantity;
			E1021.Quantity=quantity;

			LongVal2Cobol6(E21001.ExtPrice,extprice);//Extprice ok
			LongVal2Cobol6(E1021.ExtPrice,extprice);//Extprice ok

			LongVal2Cobol6(E21001.UnitPrice,up);//Extprice ok
			LongVal2Cobol6(E21001.PriceFromPlu,up);//Extprice ok

			//gdec2cobol((char *)E21001.UnitPrice,unitprice,6);//Unitprice ok-ish
			//gdec2cobol((char *)E21001.PriceFromPlu,unitprice,6);

			retval=RSafeSendHLogElement(&handle,21001,time,0x0000,sizeof(E21001)
				,(char*)&E21001);
			if (retval!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				RL.LogE("ERROR: ProcTomraTran.21001 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					RL.LogE("ERROR: ProcTomraTran.21001: AbortTran: %d!\n",ret2);
				}
				else RL.Log("Info: ProcTomraTran.21001: AbortTran successful.\n");
				return (ISS_SUCCESS);
			}
			else RL.Log("ProcTomraTran.21001: successful.\n");

			retval=RSafeSendHLogElement(&handle,1021,time,0x0000,sizeof(E1021)
				,(char*)&E1021);
			if (retval!=ISS_SUCCESS) {
				ISS_T_REPLY ret2;
				RL.LogE("ERROR: ProcTomraTran.1021 returned: %d -> abort tran & retry!\n",retval);
				if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
					RL.LogE("ERROR: ProcTomraTran.1021: AbortTran: %d!\n",ret2);
				}
				else RL.Log("Info: ProcTomraTran.1021: AbortTran successful.\n");
				return (ISS_SUCCESS);
			}
			else RL.Log("ProcTomraTran.1021: successful.\n");

			time+=RVMTxnTimePerItem;
			RPos.p_regtime+=RVMTxnTimePerItem;
			RPos.regtime+=RVMTxnTimePerItem;
			RPos.items+=quantity;
			RPos.p_itemcnt+=quantity;
			RPos.p_salesval+=extprice;
			sold+=extprice;
			total+=extprice;
			elements+=2;
			items+=quantity;

			memcpy(itemcollector,&machine,sizeof(long));
			memcpy(itemcollector+sizeof(long),ean,sizeof(ISS_GDECIMAL_6));
			memcpy(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6),&quantity,sizeof(long));
			memcpy(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6),&extprice,sizeof(long));
			RItemTotals.AddFigures(itemcollector);

		}
		counter++;
	}
	//payment
	E2000.TenderID=((machine==9) ? RTenderIdPET : RTenderId);
	E2000.ReturnMode=TRUE;
	LongVal2Cobol6(E2000.TenderVal,vvalue);

	//AddTendering(posn,E2000.TenderID,extprice);

	retval=RSafeSendHLogElement(&handle,2000,time,0x0000,sizeof(E2000),(char*)&E2000);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		RL.LogE("ERROR: ProcTomraTran.2000 returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			RL.LogE("ERROR: ProcTomraTran.2000: AbortTran: %d!\n",ret2);
		}
		else RL.Log("Info: ProcTomraTran.2000: AbortTran successful.\n");
		return(retval);
	}
	else RL.Log("ProcSale.2000: successful.\n");
	time+=2;

	elements++;
	tenders++;

	RPos.p_tendertime+=2;
	RPos.tendertime+=2;

	//Sale perf record
	E21020.ReturnMode=TRUE;
	LongVal2Cobol6(E21020.SalesTotal,vvalue);
	LongVal2Cobol6(E21020.ItemSoldTtl,vvalue);
	LongVal2Cobol6(E21020.TenderTotal,vvalue);
	E21020.ItemCnt=items;
	E21020.ItemSoldCnt=items;
	E21020.TenderCnt=1;
	retval=RSafeSendHLogElement(&handle,21020,time,0x0000,sizeof(E21020)
		,(char*)&E21020);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		RL.LogE("ERROR: ProcTomraTran.21020 returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			RL.LogE("ERROR: ProcTomraTran.21020: AbortTran: %d!\n",ret2);
		}
		else RL.Log("Info: ProcTomraTran.21020: AbortTran successful.\n");
		return(retval);
	}
	else RL.Log("ProcTomraTran.21020: successful.\n");
	elements++;
	//dept perf records
	E21024.ReturnMode=TRUE;
	E21024.SalesCnt=items;
	E21024.ItemScanCnt=items;
	LongVal2Cobol6(E21024.SalesVal,vvalue);
	if (machine!=9) {
		for(extprice=0;extprice<8;extprice++) {
			if (RItemDept[extprice]!=0) {
				E21024.Dept=RItemDept[extprice];
				retval=RSafeSendHLogElement(&handle,21024,time,0x0000,sizeof(E21024)
					,(char*)&E21024);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					RL.LogE("ERROR: ProcTomraTran.21024 (pass #%d) returned: %d -> abort tran & retry!\n"
						,extprice,retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						RL.LogE("ERROR: ProcTomraTran.21024: AbortTran: %d!\n",ret2);
					}
					else RL.Log("Info: ProcTomraTran.21024: AbortTran successful.\n");
					return(retval);
				}
				else RL.Log("ProcTomraTran.21024: pass #%d successful.\n",extprice);
				elements++;
			}
		}
	}
	else {
		for(extprice=0;extprice<8;extprice++) {
			if (RItemDeptPET[extprice]!=0) {
				E21024.Dept=RItemDeptPET[extprice];
				retval=RSafeSendHLogElement(&handle,21024,time,0x0000,sizeof(E21024)
					,(char*)&E21024);
				if (retval!=ISS_SUCCESS) {
					ISS_T_REPLY ret2;
					RL.LogE("ERROR: ProcTomraTran.PET.21024 (pass #%d) returned: %d -> abort tran & retry!\n"
						,extprice,retval);
					if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
						RL.LogE("ERROR: ProcTomraTran.PET.21024: AbortTran: %d!\n",ret2);
					}
					else RL.Log("Info: ProcTomraTran.PET.21024: AbortTran successful.\n");
					return(retval);
				}
				else RL.Log("ProcTomraTran.PET.21024: pass #%d successful.\n",extprice);
				elements++;
			}
		}
	}
	//close tran
	retval=iss46_hlog_end_trans(handle,Date,(unsigned short)elements);
	if (retval!=ISS_SUCCESS) {
		ISS_T_REPLY ret2;
		RL.LogE("ERROR: ProcTomraTran.EndTran returned: %d -> abort tran & retry!\n",retval);
		if ((ret2=iss46_hlog_void_trans(handle,Date,elements+1))!=ISS_SUCCESS) {
			RL.LogE("ERROR: ProcTomraTran.EndTran: AbortTran: %d!\n",ret2);
		}
		else RL.Log("Info: ProcTomraTran.EndTran: AbortTran successful.\n");
		return(retval);
	}
	else RL.Log("ProcTomraTran.EndTran: successful.\n");

//				retval=iss46_hlog_send_block(handle);
//				L.Log("sendb: %d\n",retval);

	MachineTotals.AddFigures(machine,vvalue);

	RegTicketIssue(machine,ticketnum);
	
	RItemTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegRItemTotals);
	MachineTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegMachineTotals);
	RPos.lasttrandate=Date;
	RPos.lasttrantime=time;
	GetLocalTime(&st);
	RPos.lasttranlocdate=DateYMD2Int(st.wYear, st.wMonth, st.wDay);
	RPos.lasttranloctime=st.wHour*3600+st.wMinute*60+st.wSecond;
	RPos.lasttranhp=hp;
	RSaveStatus();
	open=false;
	//RegVoucher(vid,vvalue,y,m,d);
	char exp_date[12];
	sprintf(exp_date,"%04d%02d%02d",y,m,d);
	Insert_BBON_MYSQL(ean_full,vvalue/100,exp_date);
	return ISS_SUCCESS;
}




long ProcRVMClose(FILE *F)
{
	char s[80];
	long l;
	ISS_GDECIMAL_6 ean;
	long thismachine=-1;
	long machine,vmy;
	char itemcollector[2*sizeof(ISS_GDECIMAL_6)+4*sizeof(long)];
	RL.Log("Info: ProcRVMClose: Processing and comparing closure records now...\n");
	//Finish logging performance records that neccesary
	if (*RPos.user) {
		*RPos.user=0;//Logoff to request new drawer @ the next transaction
		RSaveStatus();
	}
	while (fgets(s,79,F)) {
		l=strlen(s)-1;
		while ((l>0)&&(s[l]<' ')) {s[l]=0; l--;}
		RL.Log("Info: ProcRVMClose: Dump: %s\n",s);
		switch (*s|32) {
		case 'd'://closure date
			{
				RL.Log("Info: ProcRVMClose: Closure date: %s\n",s+1);
			}
			break;
		case 's'://item returns
			{
				long qmy,qin,vin;
				gdec_init_z(ean,6,0,iss_gdec_round_normal);

				s[33]=0;
				vin=atol(s+21);
				s[21]=0;
				qin=atol(s+15);
				s[15]=0;
				gdec_from_ascii(ean,s+2);
				//s[35]=0;
				//s[34]=s[1];
				//machine=atol(s+34);
				machine=(s[1]-'0');
				if (((machine!=9)&&(machine>RPosCount))||(machine<1)) {
					RL.LogE("ERROR: ProcRVMClose: Invalid RVM ID: %d! -> Record rejected!\n",machine);
					break;
				}
				if (thismachine<0) thismachine=machine;
				else {
					if (thismachine!=machine) {
						RL.LogE("ERROR: ProcRVMClose: Mixed RVM IDs (first=%d current=%d) on closure file! -> Record rejected!\n",thismachine+1,machine+1);
						break;
					}
				}
				memcpy(itemcollector,&machine,sizeof(long));
				memcpy(itemcollector+sizeof(long),ean,sizeof(ISS_GDECIMAL_6));
				if (RItemTotals.GetData(itemcollector,NULL,false)) {
					RL.LogE("ERROR: ProcRVMClose: Reported sales for not sold item %s on RVM #%d - quantity=%d, value=%d.\n"
						,s+2,machine,qin,vin);
				}
				else {
					qmy=*((long*)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)));
					vmy=*((long*)(itemcollector+sizeof(long)*2+sizeof(ISS_GDECIMAL_6)));
					if ((qin!=qmy)||(vin!=vmy)) {
						RL.LogE("ERROR: Total sales for item %s on RVM #%d mismatches! ValueIn=%d ValueMy=%d QuantityIn=%d QuantityMy=%d\n"
							,s+2,machine,vin,vmy,qin,qmy);
					}
					else {
						RL.Log("Total sales for item %s on RVM #%d matched. Value=%d Quantity=%d\n"
							,s+2,machine,vin,qin);
					}
					RItemTotals.Delete(itemcollector);
				}
			}
			break;
		case 'p'://machine specific totals
			{
				long vin,ftin,ltin,ftmy,ltmy;

				if (VoucherPadChar) {
					s[38]=0;
					s[25]=0;
				}
				else {
					s[39]=0;
					s[26]=0;
				}
				ftin=atol(s+15+strlen(VoucherLeadStr));
				ltin=atol(s+28+strlen(VoucherLeadStr));
				s[14]=0;
				vin=atol(s+2);
				//s[42]=0;
				//s[41]=s[1];
				//machine=atol(s+41);
				machine=(s[1]-'0');
				if (((machine!=9)&&(machine>RPosCount))||(machine<1)) {
					RL.LogE("ERROR: ProcRVMClose: Invalid RVM ID: %d! -> Record rejected!\n",machine);
					break;
				}
				if (thismachine<0) thismachine=machine;
				else {
					if (thismachine!=machine) {
						RL.LogE("ERROR: ProcRVMClose: Mixed RVM IDs (first=%d current=%d) on closure file! -> Record rejected!\n",thismachine,machine);
						break;
					}
				}
				if (!(MachineTotals.GetCounters(machine,&vmy))) {//no totals for this RVM
					if (vin!=0) {
						RL.LogE("ERROR: ProcRVMClose: Unexpected total sales for RVM #%d! Ticket serial differences are expected too! ValueIn=%d\n"
							,machine,vin);
						break;
					}
					RL.Log("Info: ProcRVMClose: No sales for RVM #%d. This was expected. Ticket serials are not checked.\n"
						,machine);
					break;
				}
				MachineTotals.Delete(machine);
				if (vin!=vmy) {
					RL.LogE("ERROR: ProcRVMClose: Total sales on RVM #%d mismatches! ValueIn=%d ValueMy=%d\n"
						,machine,vin,vmy);
				}
				else {
					RL.Log("Info: ProcRVMClose: Total sales on RVM #%d matches. Value=%d\n"
						,machine,vmy);
				}
				if (TicketIssue.GetCounters(machine,&ftmy,&ltmy)) {//has data
					if ((ftmy!=ftin)||(ltin!=ltmy)) {
						RL.LogE("ERROR: ProcRVMClose: First and last ticket serial on RVM #%d mismatches! FirstIn=%d FirstMy=%d LastIn=%d LastMy=%d\n"
							,machine,ftin,ftmy,ltin,ltmy);
					}
					else {
						RL.Log("Info: ProcRVMClose: First and last ticket serial on RVM #%d matches. First=%d Last=%d\n"
							,machine,ftmy,ltmy);
					}
					TicketIssue.Delete(machine);
				}
				else {//has no data
					RL.LogE("ERROR: ProcRVMClose: Unexpected error! Missing ticket serial data for RVM #%d! FirstIn=%d LastIn=%d\n"
						,machine,ftin,ltin);
				}
			}
			break;
		default:
			RL.LogE("ERROR: Don't know how to handle this closure record! %s\n",s);
		}
	}
	if (thismachine<0) {//closure file is useless

	}
	//Get unreferenced items
	memcpy(itemcollector,&thismachine,sizeof(long));
	RItemTotals.InitCursor();
	while (RItemTotals.GetNextBySizedId(sizeof(long),itemcollector)) {
		gdec2s((unsigned char *)(itemcollector+sizeof(long)),6,s);
		RL.LogE("ERROR: ProcRVMClose: Reported no sales for item %s on RVM #%d! Quantity=%d Value=%d.\n"
			,s,thismachine,*((long *)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)))
			,*((long *)(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6))));
	}
	RItemTotals.DeleteBySizedId(sizeof(long),itemcollector);
	//Get unreferenced machine totals	
	if (MachineTotals.GetCounters(thismachine,&vmy)) {//has record!
		MachineTotals.Delete(thismachine);
		long ftmy=0;
		long ltmy=0;
		if (!(TicketIssue.GetCounters(thismachine,&ftmy,&ltmy))) {//unexpected miss of data
			RL.LogE("ERROR: ProcRVMClose: Reported no totals for RVM #%d - No valid 'P' record! ValueMy=%d.\n"
				,thismachine,vmy);
			RL.LogE(" ==> ERROR: ProcRVMClose: Unexpected error: Got total for RVM #%d but no ticket serial data!\n"
				,thismachine);
		}
		else {
			TicketIssue.Delete(thismachine);
			RL.LogE("ERROR: ProcRVMClose: No valid 'P' record for RVM #%d! ValueMy=%d FirstSerial=%d LastSerial=%d.\n"
				,thismachine,vmy,ftmy,ltmy);
		}
	}
	if (thismachine==9) {
		RL.Log("Info: ProcClose: Processing closure records for PET RVM (#9) finished.\n");
	}
	else {
		bool remained=false;
		if (TicketIssue.GetCount()>0) {
			if (TicketIssue.GetCount()>1) remained=true;
			else if (TicketIssue.GetIndex(9)<0) remained=true;
		}
		if ((!remained)&&(MachineTotals.GetCount()>0)) {
			if (MachineTotals.GetCount()>1) remained=true;
			else if (MachineTotals.GetIndex(9)<0) remained=true;
		}
		if ((!remained)&&(RItemTotals.GetCount()>0)) {
			long id=9;
			if (RItemTotals.GetSizedIdCount(sizeof(long),&id)<RItemTotals.GetCount()) remained=true;
		}
		if (remained) {
			if (PendingClosure==0) {
				PendingClosure=1;
				Reg r(false, C_RegKey);
				r.SetValueDW(C_RegPendingClosure,PendingClosure);
			}
			RL.Log("Info: ProcClose: Processing closure records for RVM #%d finished. Waiting for more closure files.\n",thismachine);
		}
		else {
			if (PendingClosure==1) {
				PendingClosure=0;
				Reg r(false, C_RegKey);
				r.SetValueDW(C_RegPendingClosure,PendingClosure);
			}
			RL.Log("Info: ProcClose: Processing closure records for RVM #%d finished.\n",thismachine);
			RL.Log("Info: ProcClose: Closure completed as there are no more RVM details remained.\n");
		}
	}
	MachineTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegMachineTotals);
	TicketIssue.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegTicketIssue);
	RItemTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegRItemTotals);
	return 0;
}

void ForcedRVMClosure(void)
{
	char s[80];
	long machine,vmy,ftmy,ltmy,index;
	char itemcollector[sizeof(ISS_GDECIMAL_6)+3*sizeof(long)];
	//Get unreferenced items
	for (index=0;RItemTotals.GetDataByIndex(index,itemcollector);) {
		if (*((long *)itemcollector)==9) index++; //Skipping PET
		else {
			gdec2s((unsigned char *)(itemcollector+sizeof(long)),6,s);
			RL.LogE("ERROR: ForcedRVMClosure: Reported no sales for item %s on RVM #%d! Quantity=%d Value=%d.\n"
				,s,*((long *)itemcollector)+1
				,*((long *)(itemcollector+sizeof(long)+sizeof(ISS_GDECIMAL_6)))
				,*((long *)(itemcollector+2*sizeof(long)+sizeof(ISS_GDECIMAL_6))));
			RItemTotals.DeleteByIndex(index);
		}
	}
	//Get unreferenced machine totals	
	for (index=0;MachineTotals.GetElement(index,&machine,&vmy);) {//has record!
		if (machine==9) index++;//PET details are kept - skip it
		else {
			MachineTotals.Delete(machine);
			if (!(TicketIssue.GetCounters(machine,&ftmy,&ltmy))) {//unexpected miss of data
				RL.LogE("ERROR: ForcedRVMClosure: Reported no totals for RVM #%d! ValueMy=%d.\n"
					,machine+1,vmy);
				if (vmy) 
					RL.LogE(" ==> ERROR: ForcedRVMClosure: Unexpected error: Got total for RVM #%d but no ticket serial data!\n"
						,machine+1);
			}
			else {
				TicketIssue.Delete(machine);
				RL.LogE("ERROR: ProcRVMClose: Reported no sales for RVM #%d! ValueMy=%d FirstSerial=%d LastSerial=%d.\n"
					,machine+1,vmy,ftmy,ltmy);
			}
		}
	}
	//Get unreferenced ticket issues totals	
	for (index=0;TicketIssue.GetElement(index,&machine,&ftmy,&ltmy);) {//unexpected miss of data
		if (machine==9) index++;//PET details are kept - skip it
		else {
			TicketIssue.DeleteByIndex(index);
			RL.LogE("ERROR: ForcedRVMClosure: Reported no serials for RVM #%d! First=%d Last=%d.\n"
				,machine+1,ftmy,ltmy);
			RL.LogE(" ==> ERROR: ForcedRVMClosure: Unexpected error: Got serials for RVM #%d but no sales!\n"
				,machine+1);
		}
	}
	//Closure is no longer pending
	if (PendingClosure==1) {
		PendingClosure=0;
		Reg r(false, C_RegKey);
		r.SetValueDW(C_RegPendingClosure,PendingClosure);
	}
	MachineTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegMachineTotals);
	TicketIssue.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegTicketIssue);
	RItemTotals.WriteToRegistry(false,(char *)C_RegKey,(char *)C_RegRItemTotals);
	return;
}

bool Chk4FuelTran(void)	//Get a file to process (find the best one by name, cache the content
{
		char n[MAX_PATH];
	long retval=0;
	char *p;
	char GilbF[200];
	char *wn;	//Workname (filename in work directory)
	char *wn2,*dn2;
	bool blockautopos=true;
	Reg r(false, C_RegKey);
	SYSTEMTIME t;
	DWORD fs;
	WIN32_FIND_DATA fd;
	HANDLE hf;
	FILE* F;
	if (!FuelFunction) return false;
	if (ItemTotals.GetCount()>0) blockautopos=false;
	else {
		GetLocalTime(&t);
		long l=t.wHour*3600+t.wMinute*60+t.wSecond;
		if ((AutoOpenEnabled>0)&&(l>=AutoOpenStartTime)&&(l<AutoOpenEndTime)) blockautopos=false;
	}
	strcpy(n,DNIn);
	p=n+strlen(n)+1;
	strcat(n,"\\*.*");
	bool gilbarco=false;
	bool ACIS=false;
	bool closeday=false;
	char ACISF[60];
	char BilbarcF[60];
	char CloseGilbF[60];
	ACIS=false;gilbarco=false;closeday=false;
	strcpy(ACISF,"WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
	strcpy(BilbarcF,"WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
	strcpy(CloseGilbF,"WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");

	if ((hf=FindFirstFile(n,&fd))!=INVALID_HANDLE_VALUE) { //At least 1 file is there
		if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
			sprintf(GilbF,"%s",fd.cFileName);
			GilbF[7]=0;
			closeday=false;
			if ((strcmp(GilbF,"PXML_PJ")==0) && ((strcmp(fd.cFileName+strlen(fd.cFileName)-3,"xml")==0) || (strcmp(fd.cFileName+strlen(fd.cFileName)-3,"XML")==0))){
					gilbarco=true;
					TDRefresh();
					if (!TDNoGo) //Ha nincs z�rva az �ruh�z
						r.SetValueDW("Fuel.System.Close",0); //Engedem a benzink�t f�jlok feldolgoz�s�t
					
					//ACIS=false;
					long l=strncmp (BilbarcF+14,fd.cFileName+14,15);
					if ((l>0)||((l==0))) //new one is better
						strcpy(BilbarcF,fd.cFileName);
			}
		   if ( (_stricmp(GilbF,"PXML_DC")==0) && (_stricmp(fd.cFileName+strlen(fd.cFileName)-3,"XML")==0) ) {
					gilbarco=false;
					closeday=true;
					r.SetValueDW("Fuel.System.Close",1);
					long l=strncmp (CloseGilbF+14,fd.cFileName+14,15);
					if ((l>0)||((l==0))) //new one is better
						strcpy(CloseGilbF,fd.cFileName);

		   }
			if(!gilbarco) { 
				sprintf(GilbF,"%s",fd.cFileName);
				GilbF[2]=0;
				if (((_stricmp(GilbF,"T_")) ||(_stricmp(GilbF,"R_"))) && (_stricmp(fd.cFileName+strlen(fd.cFileName)-3,"dat")==0)){
					ACIS=true;
					L.Log("ACIS file found %s \n",fd.cFileName);
					long l=strncmp (ACISF,fd.cFileName,10);
					if ((l>0)||((l==0))) //new one is better
						strcpy(ACISF,fd.cFileName);
				}
				else {
						if ((FileNameOk(fd.cFileName,blockautopos))&&(!fd.nFileSizeHigh)) {
							strcpy(p,fd.cFileName);
							fs=fd.nFileSizeLow;
						}
						else *p=0;
					}
			} else
				strcpy(p,fd.cFileName);
		}
		else *p=0;
		//L.Log("The new file name: %s\n",fd.cFileName);
		while (FindNextFile(hf,&fd)) {
			if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
				sprintf(GilbF,"%s",fd.cFileName);
				GilbF[2]=0;
				if (((_stricmp(GilbF,"T_")) ||(_stricmp(GilbF,"R_"))) && (_stricmp(fd.cFileName+strlen(fd.cFileName)-3,"dat")==0)){
					ACIS=true;
					L.Log("ACIS file found %s \n",fd.cFileName);
					long l=strncmp (ACISF,fd.cFileName,10);
					if ((l>0)||((l==0))) //new one is better
						strcpy(ACISF,fd.cFileName);
				}
				sprintf(GilbF,"%s",fd.cFileName);
				GilbF[7]=0;
				if ( (_stricmp(GilbF,"PXML_PJ")==0) && (_stricmp(fd.cFileName+strlen(fd.cFileName)-3,"XML")==0)) {
					//L.Log("GILBARCO file found %s \n",fd.cFileName);
					gilbarco=true;
					TDRefresh();
					/*if (!TDNoGo) //Ha nincs z�rva az �ruh�z
						r.SetValueDW("Fuel.System.Close",0); //Engedem a benzink�t f�jlok feldolgoz�s�t
						*/
					
					//ACIS=false;
					long l=strncmp (BilbarcF+14,fd.cFileName+14,15);
					if ((l>0)||((l==0))) //new one is better
						strcpy(BilbarcF,fd.cFileName);
				}
			   if ( (_stricmp(GilbF,"PXML_DC")==0) && (_stricmp(fd.cFileName+strlen(fd.cFileName)-3,"XML")==0)) {
					gilbarco=false;
				   //L.Log("CLOSE file found %s \n",fd.cFileName);
					closeday=true;
					r.SetValueDW("Fuel.System.Close",1);
					long l=strncmp (CloseGilbF+14,fd.cFileName+14,15);
					if ((l>0)||((l==0))) //new one is better
						strcpy(CloseGilbF,fd.cFileName);
			   }
			   if(!gilbarco && !ACIS) { 
			  		    if ((FileNameOk(fd.cFileName,blockautopos))&&(!fd.nFileSizeHigh)) {
							if (*p) {
								long l=12;
								bool b=false;
								char *p1=p+1;
								char *p2=fd.cFileName+1;
								if ((*p|32)=='z') {
									b=true; //prefer new file
									if ((*fd.cFileName|32)=='z') l=6;
									else l=2;
								}
								else if ((*fd.cFileName|32)=='z') l=2;
								if ((*p1|32)=='i') p1++;
								if ((*p2|32)=='i') p2++;
								l=strncmp (p1,p2,l);
								if ((l>0)||((l==0)&&b)) {//new one is better
									strcpy(p,fd.cFileName);	
									fs=fd.nFileSizeLow;
								}
							}
							else {
								strcpy(p,fd.cFileName);
								fs=fd.nFileSizeLow;
							}
						}
			   } 
			}
			//else { // Ha GILBARCO rendszer fut
			//		strcpy(p,fd.cFileName);
			//}
		}
	}
	
	FindClose(hf);
	//L.Log("Acis: %d, Gilbarco: %d , Close: %d\n",gilbarco,closeday);
	if(ACIS) 
		strcpy(p,ACISF);
	else { 
		if (gilbarco) {
			strcpy(p,BilbarcF);
			closeday=false;
		}
		else 
			if (closeday) {
				strcpy(p,CloseGilbF);
				r.SetValueDW("Fuel.System.Close",1); //Nem engedem a benzink�t f�jlok feldolgoz�s�t
				gilbarco=true;
			}
	}
	
	//L.Log("The filename: %s\n",p);
	if (!(*p)) return false; //No good filename has been found
	TDRefresh();
	if (TDNoGo && !closeday) return false; //No good trading day condition
	wn=new char[strlen(DNWork)+strlen(p)+2];
	strcpy(wn,DNWork);
	strcat(wn,"\\");
	strcat(wn,p);
	wn2=new char[strlen(DNWork)+strlen(p)+2];
	strcpy(wn2,DNWork);
	strcat(wn2,"\\");
	strcat(wn2,p);
	dn2=new char[strlen(DNDone)+strlen(p)+2];
	strcpy(dn2,DNDone);
	strcat(dn2,"\\");
	strcat(dn2,p);
	/***********  ACIS REGISTER FILES  ************/
	sprintf(GilbF,"%s",fd.cFileName);
	GilbF[2]=0;
	if ( (_stricmp(GilbF,"R_")==0) && (_stricmp(fd.cFileName+strlen(fd.cFileName)-3,"dat")==0) ) {
		char *dn=new char[strlen(DNDone)+strlen(p)+2];
		strcpy(dn,DNDone);
		strcat(dn,"\\");
		strcat(dn,p);
		L.Log("Moving ACIS REGISTER file to Done directory! %s\n",p);
		if (!(MoveFile(wn,dn))) {//Moving to done was not successful
			L.LogE("ERROR: Moving ACIS REGISTER file to Done directory was unsuccessful!\n");
		}
		else
			L.Log("Moving processed file to Done directory was successful!%s %s\n",wn,dn);

		delete [] dn;
		delete [] wn;
		return true;
	}
	strcpy(GilbF,wn);
	GilbF[strlen(DNWork)+strlen(p)+2]=0;
	if (!(CopyFile(n,wn,false))) {
		delete [] wn;
		return false; //Copy was not successful - probably still locked.
	}
	if ((F=fopen(wn,"rt"))==NULL) {
		L.LogE("ERROR: Opening copy was unsuccessful but CopyFile finished with success!\n  File: %s\n",wn);
		DeleteFile(wn);
		delete [] wn;
		return false; //Copied file cannot be opened;
	}
	
	//workfile opened
	fseek(F,0,SEEK_END);
	fs=ftell(F);
	fseek(F,0,SEEK_SET);
	if (fs==0) {
		if (FuelZeroSizeRetries==0) {
			FuelZeroSizeRetries++;
			L.LogE("Warning: Workfile (%s) size is 0! There will be %d retries before dropping.\n"
				,wn,FuelZeroSizeRetriesMax);
			fclose(F);
			DeleteFile(wn);
			delete [] wn;
			return false; //Copied file cannot be opened;
		}
		else {
			if (FuelZeroSizeRetries==FuelZeroSizeRetriesMax) {
				FuelZeroSizeRetries=0;
				L.LogE("ERROR: Workfile (%s) size is still 0! No more retries left. File will be parsed.\n"
					,wn,FuelZeroSizeRetriesMax);
			}
			else {
				L.LogE("Warning: Workfile (%s) size is still 0! (Retry #%d)\n"
					,wn,FuelZeroSizeRetries);
				FuelZeroSizeRetries++;
				fclose(F);
				DeleteFile(wn);
				delete [] wn;
				return false; //Copied file cannot be opened;
			}
		}
	}
	//workfile size valid or no more retries left
	L.Log("Delete file! %s\n",n);
	if (!(DeleteFile(n))) {//deleting incoming file was not successful
		int retries=15;
		L.LogE("ERROR: Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
			,retries,n);
		Sleep(2000);
		while (!(DeleteFile(n))) {//deleting incoming file was not successful
			L.LogE("ERROR: Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
				,retries,n);
			if (!(--retries)) {//no more retries left
				fclose(F);
				L.Log("Delete file, after 15 retries! %s\n",wn);
				DeleteFile(wn);
				delete [] wn;
				return false;
			}
			Sleep(2000);
		}
	}
	
	//Original file was deleted
	L.Log("Info: Chk4Tran: Got new file to process: %s.\n",p);
	if(!gilbarco) {
		if ((*p|32)=='z') ProcClose(F);
		else {
			if ((p[1]|32)=='i') {
				retval=1;
				while((KeepLooping)&&(retval!=0)) {
					retval=ProcLogOnOff(F);
					if (retval!=0) {
						L.LogE("Warning: Chk4Tran: ProcLogOnOff needs retry! (%d)\n",retval);
						fseek(F,0,SEEK_SET);
						Sleep(4000);
					}
				}
				if (retval!=0) {
					L.LogE("ERROR: Chk4Tran: ProcLogOnOff retry aborted - manual intervention required!\n");
				}
			}
			else {
				retval=1;
				while((KeepLooping)&&(retval!=0)) {
					retval=ProcSale(F,ACIS);
					if (retval!=0) {
						L.LogE("Warning: Chk4Tran: ProcSale needs retry! (%d)\n",retval);
						fseek(F,0,SEEK_SET);
						Sleep(4000);
					}
				}
				if (retval!=0) {
					L.LogE("ERROR: Chk4Tran: ProcSale retry aborted - manual intervention required!\n");
				}
			}
		}
		fclose(F);
	} 
	if(gilbarco) { //GILBARCO system
		fclose(F);
		retval=1;
		while((KeepLooping)&&(retval!=0)) {
			if (closeday==false)
				retval=	GILBARCO_ProcSale(GilbF);
			else {
				/********* Register files to Registry   ****************/
				strcpy(n,ACISDNIn);
				strcat(n,"\\R_*.dat");
				L.Log("Move ACIS Register files to DONE directory!\n");
				char t1file[50],t2file[250],line[100],cid[3],nid[3],counter[20];
				long l=12;
				bool go=true,gotfile;
				char *dn3=new char[MAX_PATH];
				char *tt=new char[MAX_PATH];

				strcpy(t1file,"R_99_991212104414_000008.dat");
				gotfile=true;
				FILETIME ft1;
				FILETIME ft2;
				while (go) {
				 if ((hf=FindFirstFile(n,&fd))!=INVALID_HANDLE_VALUE) { //At least 1 file is there
					ft1=fd.ftCreationTime;
					strcpy(t1file,fd.cFileName);
  				  
					while (FindNextFile(hf,&fd)) {
						if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
										ft2=fd.ftCreationTime;
										strcpy(t2file,fd.cFileName);
										if (CompareFileTime(&ft2,&ft1)>0) {//new one is better
											strcpy(t1file,fd.cFileName);
											ft1=fd.ftCreationTime;
											fs=fd.nFileSizeLow;
											gotfile=true;
										}
						}
					}
					if (gotfile) {
						sprintf(t2file,"%s\\%s",ACISDNIn,t1file);
						if ((F=fopen(t2file,"rt"))!=NULL) {
							if( fgets( line, 90, F ) == NULL)
								L.LogE("ERROR - Chk4FuelTran Cannot open file! %s \n",t2file );
							else {
								l=14-strlen(line);
								strncpy(cid,line,2-l);
								strncpy(nid,line+2-l,1);
								nid[1]=0;
								cid[2-l]=0;
								strcpy(counter,line+3-l);
								counter[10]='0';
								counter[11]='0';
								counter[12]=0;
								WriteRegister2Reg(atoi(cid),atoi(nid),counter);

							}
							fclose( F );
							sprintf(tt,"%s\\%s",DNDone,t1file);
							//strcpy(tt,DNDone);
							//strcat(tt,t1file);
							L.Log("ProcClose - Copy %s to %s \n",t2file,tt);
							if (!(CopyFile(t2file,tt,false))) {//copy to hist wasn't made
								L.LogE("ERROR: CopyFile has been failed! %s -> %s Error code=%d\n"
									,t2file,tt,GetLastError());
							}
							else {
								if (!(DeleteFile(t2file))) {//Deleting from done was failed
									L.LogE("ERROR: DeleteFile has been failed! (%s) Error code=%d\n"
										,t2file,GetLastError());
								}
							}
						}
					}
				 }
				 else
						go=false;
				}

				retval=	GILBARCO_ProcClose(GilbF);
			}
	}


	}
	if (retval==0) {
		char *dn=new char[strlen(DNDone)+strlen(p)+2];
		strcpy(dn,DNDone);
		strcat(dn,"\\");
		strcat(dn,p);
		L.Log("Move file %s to done %s \n",wn2,dn2);
		if (!(MoveFile(wn2,dn2))) {//Moving to done was not successful
			L.LogE("ERROR: Moving processed file to Done directory was unsuccessful!%s %s\n",wn,dn);
		}
		else 
			L.Log("Moving processed file to Done directory was successful!%s %s\n",wn,dn);

		delete [] dn;
		delete [] dn2;
		delete [] wn2;
		

	}
	delete [] wn;
//	sprintf(GilbF,"%s",p);
	GilbF[7]=0;
	if (((*p|32)=='z') || (_stricmp(GilbF,"PXML_DC")==0) || (closeday)) {//Closure -> Moving files from done to history
		char *dn=new char[MAX_PATH];
		char *AIN=new char[MAX_PATH];
		char *tt=new char[MAX_PATH];
		bool gotfile=true;
		strcpy(dn,DNDone);
		strcat(dn,"\\");
		FindClose(hf);
		delete [] AIN;
		delete [] tt;
		//L.Log("Move ACIS Register files moved to DONE directory!\n");
		strcpy(dn,DNHist);
		strcat(dn,"\\");
		char *pdn=dn+strlen(dn);
		L.Log("Move transaction files to %s directory!\n",DNHist);
		for(char stage=0;stage<2;stage++) {
			gotfile=true;
			strcpy(n,(stage==0) ? DNDone : DNWork);
			p=n+strlen(n)+1;
			strcat(n,"\\*");
			if ((hf=FindFirstFile(n,&fd))!=INVALID_HANDLE_VALUE) { //At least 1 file is there
				while (gotfile) {
					if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
						strcpy(p,fd.cFileName);
						strcpy(pdn,fd.cFileName);
						if (!(CopyFile(n,dn,false))) {//copy to hist wasn't made
							L.LogE("ERROR: CopyFile has been failed! %s -> %s Error code=%d\n"
								,n,dn,GetLastError());
						}
						else {
							if (!(DeleteFile(n))) {//Deleting from done was failed
								L.LogE("ERROR: DeleteFile has been failed! (%s) Error code=%d\n"
									,n,GetLastError());
							}
						}
					}
					gotfile=(FindNextFile(hf,&fd)!=0);
				}
			}
			FindClose(hf);
		}
		delete [] dn;
		L.Log("Transaction files MOVED to %s directory!\n",DNHist);
	}
	return true;
	


}

bool Chk4Flexys_Closure_File()
{
	char n[MAX_PATH];
	long retval=0;
	char GilbF[100];
	char *p=NULL;
	char *wn;	//Workname (filename in work directory)
	char *in;
	bool blockautopos=true;
//	SYSTEMTIME t;
	DWORD fs;
	WIN32_FIND_DATA fd;
	HANDLE hf;
	FILE* F;
	bool found=false;

	if (!FuelFunction) return false;
	sprintf(n,"%s\\zaras-*.xml",FlexDnIn);
	p=n+strlen(n)+1;
	if ((hf=FindFirstFile(n,&fd))!=INVALID_HANDLE_VALUE) { //At least 1 file is there
		if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
						strcpy(p,fd.cFileName);
						fs=fd.nFileSizeLow;
						found=true;
		}
		else *p=0;
		while (FindNextFile(hf,&fd)) {
			if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
							strcpy(p,fd.cFileName);
							fs=fd.nFileSizeLow;
							found=true;
			}
		}
	}
	FindClose(hf);
	
	if (p==NULL) return false; //No good filename has been found
	sprintf(GilbF,"%s",p);
	GilbF[5]=0;
	int j=strlen(p);
	if (!found) return false; //Ther isn't a new file
	if (found && (_stricmp(GilbF,"zaras")!=0)) {//No good filename has been found 
		if (p[j-5]!='*')
			L.Log("No good filename found!%s\n",p);
		return false;
	}
	TDRefresh();
	L.Log("Info: Chk4FlexysTran: Got new closure file to process: %s.\n",p);
	wn=new char[strlen(DNWork)+strlen(p)+2];
	in=new char[strlen(FlexDnIn)+strlen(p)+2];
	strcpy(wn,DNWork);
	strcat(wn,"\\");
	strcat(wn,p);
	sprintf(in,"%s\\%s",FlexDnIn,p);
	if (!(CopyFile(in,wn,false))) {
		delete [] wn;
		delete [] in;
		return false; //Copy was not successful - probably still locked.
	}
	if ((F=fopen(wn,"rt"))==NULL) {
		L.LogE("ERROR: Opening copy was unsuccessful but CopyFile finished with success!\n  File: %s\n",wn);
		DeleteFile(wn);
		delete [] wn;
		delete [] in;
		return false; //Copied file cannot be opened;
	}
	//workfile opened
	fseek(F,0,SEEK_END);
	fs=ftell(F);
	fseek(F,0,SEEK_SET);
	if (fs==0) {
		if (FuelZeroSizeRetries==0) {
			FuelZeroSizeRetries++;
			L.LogE("Warning: Workfile (%s) size is 0! There will be %d retries before dropping.\n"
				,wn,FuelZeroSizeRetriesMax);
			fclose(F);
			DeleteFile(wn);
			delete [] wn;
			delete [] in;
			return false; //Copied file cannot be opened;
		}
		else {
			if (FuelZeroSizeRetries==FuelZeroSizeRetriesMax) {
				FuelZeroSizeRetries=0;
				L.LogE("ERROR: Workfile (%s) size is still 0! No more retries left. File will be parsed.\n"
					,wn,FuelZeroSizeRetriesMax);
			}
			else {
				L.LogE("Warning: Workfile (%s) size is still 0! (Retry #%d)\n"
					,wn,FuelZeroSizeRetries);
				FuelZeroSizeRetries++;
				fclose(F);
				DeleteFile(wn);
				delete [] wn;
				delete [] in;
				return false; //Copied file cannot be opened;
			}
		}
	}
	//workfile size valid or no more retries left
	if (!(DeleteFile(in))) {//deleting incoming file was not successful
		char retries=15;
		L.LogE("ERROR: Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
			,retries,in);
		Sleep(2000);
		while (!(DeleteFile(in))) {//deleting incoming file was not successful
			L.LogE("ERROR: FLEXYS Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
				,retries,in);
			if (!(--retries)) {//no more retries left
				fclose(F);
				DeleteFile(wn);
				delete [] wn;
				delete [] in;
				return false;
			}
			Sleep(2000);
		}
	}
	//Original file was deleted
	fclose(F);
	retval=1;
	while((KeepLooping)&&(retval!=0)) {
		retval=FLEXSYS_ProcClose(wn);
		if (retval!=0) {
			L.LogE("Warning: FLEXYS Chk4Tran: ProcClose needs retry! (%d)\n",retval);
			Sleep(4000);
		}
		char *dn=new char[MAX_PATH];
		bool gotfile=true;
		strcpy(dn,DNDone);
		strcat(dn,"\\");
		FindClose(hf);
		strcpy(dn,DNHist);
		strcat(dn,"\\");
		char *pdn=dn+strlen(dn);
		L.Log("Move transaction files to %s directory!\n",DNHist);
		for(char stage=0;stage<2;stage++) {
			gotfile=true;
			strcpy(n,(stage==0) ? DNDone : DNWork);
			p=n+strlen(n)+1;
			strcat(n,"\\*");
			if ((hf=FindFirstFile(n,&fd))!=INVALID_HANDLE_VALUE) { //At least 1 file is there
				while (gotfile) {
					if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
						strcpy(p,fd.cFileName);
						strcpy(pdn,fd.cFileName);
						if (!(CopyFile(n,dn,false))) {//copy to hist wasn't made
							L.LogE("ERROR: CopyFile has been failed! %s -> %s Error code=%d\n"
								,n,dn,GetLastError());
						}
						else {
							if (!(DeleteFile(n))) {//Deleting from done was failed
								L.LogE("ERROR: DeleteFile has been failed! (%s) Error code=%d\n"
									,n,GetLastError());
							}
						}
					}
					gotfile=(FindNextFile(hf,&fd)!=0);
				}
			}
			FindClose(hf);
		}
		delete [] dn;
		L.Log("Transaction files MOVED to %s directory!\n",DNHist);
		
	}
	if (retval!=0) {
		L.LogE("ERROR: FLEXYS Chk4Tran: ProcSale retry aborted - manual intervention required!\n");
	}
	if (retval==0) {
		char *dn=new char[strlen(DNDone)+strlen(p)+2];
		strcpy(dn,DNDone);
		strcat(dn,"\\");
		strcat(dn,p);
		if (!(MoveFile(wn,dn))) {//Moving to done was not successful
			L.LogE("ERROR: Moving processed file to Done directory was unsuccessful!\n");
		}
		else 
			L.Log("Moving processed file to Done directory was successful!%s %s\n",wn,dn);

		delete [] dn;
	}
	delete [] wn;
	delete [] in;
	return true;

}



bool Chk4Flexys_Tran()	//Get a file to process (find the best one by name, cache the content
{
	char n[MAX_PATH];
	long retval=0;
	char GilbF[100];
	char *p=NULL;
	char *wn;	//Workname (filename in work directory)
	char *in;
	bool blockautopos=true;
	SYSTEMTIME t;
	DWORD fs;
	WIN32_FIND_DATA fd;
	HANDLE hf;
	FILE* F;
	bool found=false;

	if (!FuelFunction) return false;
	if (ItemTotals.GetCount()>0) blockautopos=false;
	else {
		GetLocalTime(&t);
		long l=t.wHour*3600+t.wMinute*60+t.wSecond;
		if ((AutoOpenEnabled>0)&&(l>=AutoOpenStartTime)&&(l<AutoOpenEndTime)) blockautopos=false;
	}
	sprintf(n,"%s\\nyusza-*.xml",FlexDnIn);
	p=n+strlen(n)+1;
	if ((hf=FindFirstFile(n,&fd))!=INVALID_HANDLE_VALUE) { //At least 1 file is there
		if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
					//if ((FileNameOk(fd.cFileName,blockautopos))&&(!fd.nFileSizeHigh)) {
						strcpy(p,fd.cFileName);
						fs=fd.nFileSizeLow;
						found=true;
					//}
					//else *p=0;
		}
		else *p=0;
		while (FindNextFile(hf,&fd)) {
			if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
						/*if (*p) {
							long l=12;
							char *p1=p+1;
							char *p2=fd.cFileName+1;
							l=_stricmp (p1,p2);
							if (l>0) {//new one is better
								strcpy(p,fd.cFileName);	
								fs=fd.nFileSizeLow;
								found=true;
							}
						}
						else {*/
							strcpy(p,fd.cFileName);
							fs=fd.nFileSizeLow;
							found=true;
						//}
			}
		}
	}
	FindClose(hf);
	
	if (p==NULL) return false; //No good filename has been found
	//if (!(*p)) return false; //No good filename has been found
	sprintf(GilbF,"%s",p);
	GilbF[6]=0;
	int j=strlen(p);
	if (!found) return false; //Ther isn't a new file
	if (found && (_stricmp(GilbF,"nyusza")!=0)) {//No good filename has been found 
		//if (_stricmp(p,"T_*.*")!=0) 
		if (p[j-5]!='*')
			L.Log("No good filename found!%s\n",p);
		return false;
	}
	TDRefresh();
	if (TDNoGo) return false; //No good trading day condition
	L.Log("Info: Chk4FlexysTran: Got new file to process: %s.\n",p);
	wn=new char[strlen(DNWork)+strlen(p)+2];
	in=new char[strlen(FlexDnIn)+strlen(p)+2];
	strcpy(wn,DNWork);
	strcat(wn,"\\");
	strcat(wn,p);
	sprintf(in,"%s\\%s",FlexDnIn,p);
	if (!(CopyFile(in,wn,false))) {
		//delete [] wn;
		//delete [] in;
		return false; //Copy was not successful - probably still locked.
	}
	if ((F=fopen(wn,"rt"))==NULL) {
		L.LogE("ERROR: Opening copy was unsuccessful but CopyFile finished with success!\n  File: %s\n",wn);
		DeleteFile(wn);
		//delete [] wn;
		//delete [] in;
		return false; //Copied file cannot be opened;
	}
	//workfile opened
	fseek(F,0,SEEK_END);
	fs=ftell(F);
	fseek(F,0,SEEK_SET);
	if (fs==0) {
		if (FuelZeroSizeRetries==0) {
			FuelZeroSizeRetries++;
			L.LogE("Warning: Workfile (%s) size is 0! There will be %d retries before dropping.\n"
				,wn,FuelZeroSizeRetriesMax);
			fclose(F);
			DeleteFile(wn);
		//	delete [] wn;
		//	delete [] in;
			return false; //Copied file cannot be opened;
		}
		else {
			if (FuelZeroSizeRetries==FuelZeroSizeRetriesMax) {
				FuelZeroSizeRetries=0;
				L.LogE("ERROR: Workfile (%s) size is still 0! No more retries left. File will be parsed.\n"
					,wn,FuelZeroSizeRetriesMax);
			}
			else {
				L.LogE("Warning: Workfile (%s) size is still 0! (Retry #%d)\n"
					,wn,FuelZeroSizeRetries);
				FuelZeroSizeRetries++;
				fclose(F);
				DeleteFile(wn);
		//		delete [] wn;
		//		delete [] in;
				return false; //Copied file cannot be opened;
			}
		}
	}
	//workfile size valid or no more retries left
	if (!(DeleteFile(in))) {//deleting incoming file was not successful
		char retries=15;
		L.LogE("ERROR: Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
			,retries,in);
		Sleep(2000);
		while (!(DeleteFile(in))) {//deleting incoming file was not successful
			L.LogE("ERROR: FLEXYS Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
				,retries,in);
			if (!(--retries)) {//no more retries left
				fclose(F);
				DeleteFile(wn);
		//		delete [] wn;
		//		delete [] in;
				return false;
			}
			Sleep(2000);
		}
	}
	//Original file was deleted
	fclose(F);
	retval=1;
	while((KeepLooping)&&(retval!=0)) {
		retval=FLEXSYS_ProcSale(wn);
		if (retval!=0) {
			L.LogE("Warning: FLEXYS Chk4Tran: ProcSale needs retry! (%d)\n",retval);
			Sleep(4000);
		}
	}
	if (retval!=0) {
		L.LogE("ERROR: FLEXYS Chk4Tran: ProcSale retry aborted - manual intervention required!\n");
	}
	if (retval==0) {
		char *dn=new char[strlen(DNDone)+strlen(p)+2];
		strcpy(dn,DNDone);
		strcat(dn,"\\");
		strcat(dn,p);
		if (!(MoveFile(wn,dn))) {//Moving to done was not successful
			L.LogE("ERROR: Moving processed file to Done directory was unsuccessful!\n");
		}
		else 
			L.Log("Moving processed file to Done directory was successful!%s %s\n",wn,dn);

		delete [] dn;
	}
	delete [] wn;
	delete [] in;
	return true;
}


bool Chk4NAMOS_Tran()	//Get a file to process (find the best one by name, cache the content
{
	char n[MAX_PATH];
	long retval=0;
	int retval_counter=0;
	char GilbF[100];
	char *p=NULL;
	char *wn;	//Workname (filename in work directory)
	char *in,*in_txt;
	bool blockautopos=true;
	SYSTEMTIME t;
	DWORD fs;
	WIN32_FIND_DATA fd;
	HANDLE hf;
	FILE* F;
	bool found=false;
	GetLocalTime(&t);
	long l=t.wHour*3600+t.wMinute*60+t.wSecond;
	if (!FuelFunction) return false;
	if (ItemTotals.GetCount()>0) blockautopos=false;
	else {
		
		if ((AutoOpenEnabled>0)&&(l>=AutoOpenStartTime)&&(l<AutoOpenEndTime)) blockautopos=false;
	}
	sprintf(n,"%s\\TIC_*.xml",DNIn);
	p=n+strlen(n)+1;
	if ((hf=FindFirstFile(n,&fd))!=INVALID_HANDLE_VALUE) { //At least 1 file is there
		if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
					//if ((FileNameOk(fd.cFileName,blockautopos))&&(!fd.nFileSizeHigh)) {
						strcpy(p,fd.cFileName);
						fs=fd.nFileSizeLow;
						found=true;
					//}
					//else *p=0;
		}
		else *p=0;
		while (FindNextFile(hf,&fd)) {
			if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
						/*if (*p) {
							long l=12;
							char *p1=p+1;
							char *p2=fd.cFileName+1;
							l=_stricmp (p1,p2);
							if (l>0) {//new one is better
								strcpy(p,fd.cFileName);	
								fs=fd.nFileSizeLow;
								found=true;
							}
						}
						else {*/
							strcpy(p,fd.cFileName);
							fs=fd.nFileSizeLow;
							found=true;
						//}
			}
		}
	}
	FindClose(hf);
	
	if (p==NULL) return false; //No good filename has been found
	//if (!(*p)) return false; //No good filename has been found
	sprintf(GilbF,"%s",p);
	GilbF[4]=0;
	int j=strlen(p);
	if (!found) return false; //Ther isn't a new file
	if (found && (_stricmp(GilbF,"TIC_")!=0)) {//No good filename has been found 
		//if (_stricmp(p,"T_*.*")!=0) 
		if (p[j-5]!='*')
			L.Log("No good filename found!%s\n",p);
		return false;
	}
	TDRefresh();
	if (TDNoGo) return false; //No good trading day condition
	L.Log("Info: Chk4NamosTran: Got new file to process: %s.\n",p);
	wn=new char[strlen(DNWork)+strlen(p)+2];
	in=new char[strlen(DNIn)+strlen(p)+2];
	in_txt=new char[strlen(DNIn)+strlen(p)+6];
	
	strcpy(wn,DNWork);
	strcat(wn,"\\");
	strcat(wn,p);
	sprintf(in,"%s\\%s",DNIn,p);
	sprintf(in_txt,"%s\\%s.old",DNIn,p);
	if (!(CopyFile(in,wn,false))) {
		//delete [] wn;
		//delete [] in;
		return false; //Copy was not successful - probably still locked.
	}
	if ((F=fopen(wn,"rt"))==NULL) {
		L.LogE("ERROR: Opening copy was unsuccessful but CopyFile finished with success!\n  File: %s\n",wn);

		DeleteFile(wn);
		//delete [] wn;
		//delete [] in;
		return false; //Copied file cannot be opened;
	}
	//workfile opened
	fseek(F,0,SEEK_END);
	fs=ftell(F);
	fseek(F,0,SEEK_SET);
	if (fs==0) {
		if (FuelZeroSizeRetries==0) {
			FuelZeroSizeRetries++;
			L.LogE("Warning: Workfile (%s) size is 0! There will be %d retries before dropping.\n"
				,wn,FuelZeroSizeRetriesMax);
			fclose(F);
			DeleteFile(wn);
		//	delete [] wn;
		//	delete [] in;
			return false; //Copied file cannot be opened;
		}
		else {
			if (FuelZeroSizeRetries==FuelZeroSizeRetriesMax) {
				FuelZeroSizeRetries=0;
				L.LogE("ERROR: Workfile (%s) size is still 0! No more retries left. File will be parsed.\n"
					,wn,FuelZeroSizeRetriesMax);
			}
			else {
				L.LogE("Warning: Workfile (%s) size is still 0! (Retry #%d)\n"
					,wn,FuelZeroSizeRetries);
				FuelZeroSizeRetries++;
				fclose(F);
				DeleteFile(wn);
		//		delete [] wn;
		//		delete [] in;
				return false; //Copied file cannot be opened;
			}
		}
	}
	CopyFile(in,in_txt,false);
	//workfile size valid or no more retries left
	if (!(DeleteFile(in))) {//deleting incoming file was not successful
		char retries=15;
		L.LogE("ERROR: Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
			,retries,in);
		Sleep(2000);
		while (!(DeleteFile(in))) {//deleting incoming file was not successful
			L.LogE("ERROR: NAMOS Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
				,retries,in);
			if (!(--retries)) {//no more retries left
				fclose(F);
				DeleteFile(wn);
		//		delete [] wn;
		//		delete [] in;
				return false;
			}
			Sleep(2000);
		}
	}
	//Original file was deleted
	fclose(F);
	retval=1;
	while((KeepLooping)&&(retval!=0)) {
		retval=NAMOS_ProcSale(wn,p);
		if (retval==-99) {
				char *dn=new char[strlen(DNDuplicate)+strlen(p)+2];
				strcpy(dn,DNDuplicate);
				strcat(dn,"\\");
				strcat(dn,p);
				strcat(dn,".");
				ltoa(l,GilbF,10);
				strcat(dn,GilbF);
				if (!(MoveFile(wn,dn))) {//Moving to done was not successful
					L.LogE("ERROR: NAMOS Moving processed file to Duplicate directory was unsuccessful! %s %s\n",wn,dn);
				}
				else 
					L.Log("NAMOS Moving processed file to Duplicate directory was successful!%s %s\n",wn,dn);
				retval=0;
				delete [] dn;
		}
		if (retval==-98) {//Bad void transactions
				char *dn=new char[strlen(DNBad)+strlen(p)+2];
				strcpy(dn,DNBad);
				strcat(dn,"\\");
				strcat(dn,p);
				strcat(dn,".");
				ltoa(l,GilbF,10);
				strcat(dn,GilbF);
				if (!(MoveFile(wn,dn))) {//Moving to done was not successful
					L.LogE("ERROR: NAMOS Moving processed file to Bad directory was unsuccessful! %s %s\n",wn,dn);
				}
				else 
					L.Log("NAMOS Moving processed file to Bad directory was successful!%s %s\n",wn,dn);
				retval=0;
				delete [] dn;
		}
		if (retval==-97) {//Day Close
			char *dn=new char[MAX_PATH];
			//char *AIN=new char[MAX_PATH];
			//char *tt=new char[MAX_PATH];
			bool gotfile=true;
			FindClose(hf);
			strcpy(dn,DNDone);
			strcat(dn,"\\");
			//L.Log("Move ACIS Register files moved to DONE directory!\n");
			strcpy(dn,DNHist);
			strcat(dn,"\\");
			char *pdn=dn+strlen(dn);
			L.Log("Move transaction files to %s directory!\n",DNHist);
			for(char stage=0;stage<2;stage++) {
				gotfile=true;
				strcpy(n,(stage==0) ? DNDone : DNWork);
				p=n+strlen(n)+1;
				strcat(n,"\\*");
				if ((hf=FindFirstFile(n,&fd))!=INVALID_HANDLE_VALUE) { //At least 1 file is there
					while (gotfile) {
						if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
							strcpy(p,fd.cFileName);
							strcpy(pdn,fd.cFileName);
							if (!(CopyFile(n,dn,false))) {//copy to hist wasn't made
								L.LogE("ERROR: CopyFile has been failed! %s -> %s Error code=%d\n"
									,n,dn,GetLastError());
							}
							else {
								if (!(DeleteFile(n))) {//Deleting from done was failed
									L.LogE("ERROR: DeleteFile has been failed! (%s) Error code=%d\n"
										,n,GetLastError());
								}
							}
						}
						gotfile=(FindNextFile(hf,&fd)!=0);
					}
				}
				FindClose(hf);
			}
			delete [] dn;
			retval=0;
			L.Log("Transaction files MOVED to %s directory!\n",DNHist);
		}
		if (retval!=0 && retval_counter<5) {
			retval_counter++;
			L.LogE("Warning: NAMOS Chk4Tran: ProcSale needs retry! (%d)\n",retval);
			Sleep(4000);
		}
	}
	if(retval_counter>4){
		return true;
	}
	if (retval!=0) {
		L.LogE("ERROR: NAMOS Chk4Tran: ProcSale retry aborted - manual intervention required!\n");
	}
	if (retval==0) {
		char *dn=new char[strlen(DNDone)+strlen(p)+2];
		strcpy(dn,DNDone);
		strcat(dn,"\\");
		strcat(dn,p);
		if (!(MoveFile(wn,dn))) {//Moving to done was not successful
			L.LogE("ERROR: Moving processed file to Done directory was unsuccessful!\n");
		}
		else 
			L.Log("Moving processed file to Done directory was successful!%s %s\n",wn,dn);

		delete [] dn;
	}
	delete [] wn;
	delete [] in;
	return true;
}

bool Chk4IGASS_Tran()	//Get a file to process (find the best one by name, cache the content
{
	char n[MAX_PATH];
	long retval=0;
	char GilbF[100];
	char *p=NULL;
	char *wn;	//Workname (filename in work directory)
	char *in;
	bool blockautopos=true;
	SYSTEMTIME t;
	DWORD fs;
	WIN32_FIND_DATA fd;
	HANDLE hf;
	FILE* F;
	bool found=false;
	GetLocalTime(&t);
	long l=t.wHour*3600+t.wMinute*60+t.wSecond;
	if (!FuelFunction) return false;
	if (FuelClose==1) return false;
	if (ItemTotals.GetCount()>0) blockautopos=false;
	else {
		if ((AutoOpenEnabled>0)&&(l>=AutoOpenStartTime)&&(l<AutoOpenEndTime)) blockautopos=false;
	}
	sprintf(n,"%s\\ITIC_*.xml",DNIGASSIn);
	p=n+strlen(n)+1;
	if ((hf=FindFirstFile(n,&fd))!=INVALID_HANDLE_VALUE) { //At least 1 file is there
		if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
					//if ((FileNameOk(fd.cFileName,blockautopos))&&(!fd.nFileSizeHigh)) {
						strcpy(p,fd.cFileName);
						fs=fd.nFileSizeLow;
						found=true;
					//}
					//else *p=0;
		}
		else *p=0;
		/*while (FindNextFile(hf,&fd)) {
			if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
						/*if (*p) {
							long l=12;
							char *p1=p+1;
							char *p2=fd.cFileName+1;
							l=_stricmp (p1,p2);
							if (l>0) {//new one is better
								strcpy(p,fd.cFileName);	
								fs=fd.nFileSizeLow;
								found=true;
							}
						}
						else {
							strcpy(p,fd.cFileName);
							fs=fd.nFileSizeLow;
							found=true;
						//}
			}
		}*/
	}
	FindClose(hf);
	
	if (p==NULL) return false; //No good filename has been found
	//if (!(*p)) return false; //No good filename has been found
	sprintf(GilbF,"%s",p);
	GilbF[4]=0;
	int j=strlen(p);
	if (!found) return false; //Ther isn't a new file
	if (found && (_stricmp(GilbF,"ITIC")!=0)) {//No good filename has been found 
		//if (_stricmp(p,"T_*.*")!=0) 
		if (p[j-5]!='*')
			L.Log("No good IGASS filename found!%s\n",p);
		return false;
	}
	TDRefresh();
	if (TDNoGo) return false; //No good trading day condition
	L.Log("Info: Chk4IGASTran: Got new file to process: %s.\n",p);
	wn=new char[strlen(DNWork)+strlen(p)+2];
	in=new char[strlen(DNIGASSIn)+strlen(p)+2];
	strcpy(wn,DNWork);
	strcat(wn,"\\");
	strcat(wn,p);
	sprintf(in,"%s\\%s",DNIGASSIn,p);
	if (!(CopyFile(in,wn,false))) {
		//delete [] wn;
		//delete [] in;
		return false; //Copy was not successful - probably still locked.
	}
	if ((F=fopen(wn,"rt"))==NULL) {
		L.LogE("ERROR: IGASS Opening copy was unsuccessful but CopyFile finished with success!\n  File: %s\n",wn);
		DeleteFile(wn);
		//delete [] wn;
		//delete [] in;
		return false; //Copied file cannot be opened;
	}
	//workfile opened
	fseek(F,0,SEEK_END);
	fs=ftell(F);
	fseek(F,0,SEEK_SET);
	if (fs==0) {
		if (FuelZeroSizeRetries==0) {
			FuelZeroSizeRetries++;
			L.LogE("Warning: IGASS Workfile (%s) size is 0! There will be %d retries before dropping.\n"
				,wn,FuelZeroSizeRetriesMax);
			fclose(F);
			DeleteFile(wn);
		//	delete [] wn;
		//	delete [] in;
			return false; //Copied file cannot be opened;
		}
		else {
			if (FuelZeroSizeRetries==FuelZeroSizeRetriesMax) {
				FuelZeroSizeRetries=0;
				L.LogE("ERROR: IGASS Workfile (%s) size is still 0! No more retries left. File will be parsed.\n"
					,wn,FuelZeroSizeRetriesMax);
			}
			else {
				L.LogE("Warning: IGASS Workfile (%s) size is still 0! (Retry #%d)\n"
					,wn,FuelZeroSizeRetries);
				FuelZeroSizeRetries++;
				fclose(F);
				DeleteFile(wn);
		//		delete [] wn;
		//		delete [] in;
				return false; //Copied file cannot be opened;
			}
		}
	}
	//workfile size valid or no more retries left
	if (!(DeleteFile(in))) {//deleting incoming file was not successful
		char retries=15;
		L.LogE("ERROR: IGASS Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
			,retries,in);
		Sleep(2000);
		while (!(DeleteFile(in))) {//deleting incoming file was not successful
			L.LogE("ERROR: IGASS Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
				,retries,in);
			if (!(--retries)) {//no more retries left
				fclose(F);
				DeleteFile(wn);
		//		delete [] wn;
		//		delete [] in;
				return false;
			}
			Sleep(2000);
		}
	}
	//Original file was deleted
	fclose(F);
	retval=1;
	while((KeepLooping)&&(retval!=0)) {
		retval=NAMOS_ProcSale(wn,p);
		if (retval==-99) {
				char *dn=new char[strlen(DNDuplicate)+strlen(p)+2];
				strcpy(dn,DNDuplicate);
				strcat(dn,"\\");
				strcat(dn,p);
				strcat(dn,".");
				ltoa(l,GilbF,10);
				strcat(dn,GilbF);
				if (!(MoveFile(wn,dn))) {//Moving to done was not successful
					L.LogE("ERROR: NAMOS Moving processed file to Duplicate directory was unsuccessful! %s %s\n",wn,dn);
				}
				else 
					L.Log("IGAS Moving processed file to Duplicate directory was successful!%s %s\n",wn,dn);
				retval=0;
				delete [] dn;
		}
		if (retval!=0) {
			L.LogE("Warning: IGASS Chk4Tran: ProcSale needs retry! (%d)\n",retval);
			Sleep(4000);
		}
	}
	if (retval!=0) {
		L.LogE("ERROR: IGASS Chk4Tran: ProcSale retry aborted - manual intervention required!\n");
	}
	if (retval==0) {
		char *dn=new char[strlen(DNDone)+strlen(p)+2];
		strcpy(dn,DNDone);
		strcat(dn,"\\");
		strcat(dn,p);
		if (!(MoveFile(wn,dn))) {//Moving to done was not successful
			L.LogE("ERROR: IGASS Moving processed file to Done directory was unsuccessful!\n");
		}
		else 
			L.Log("Moving IGASS processed file to Done directory was successful!%s %s\n",wn,dn);

		delete [] dn;
	}
	delete [] wn;
	delete [] in;
	return true;
}



bool Chk4NewACISTran(int station)	//Get a file to process (find the best one by name, cache the content
{
	char n[MAX_PATH];
	long retval=0;
	char GilbF[100];
	char *p=NULL;
	char wn[200];	//Workname (filename in work directory)
	char in[200];
	bool blockautopos=true;
	SYSTEMTIME t;
	DWORD fs;
	WIN32_FIND_DATA fd;
	HANDLE hf;
	FILE* F;
	bool found=false;

	if (!FuelFunction) return false;
	if (ItemTotals.GetCount()>0) blockautopos=false;
	else {
		GetLocalTime(&t);
		long l=t.wHour*3600+t.wMinute*60+t.wSecond;
		if ((AutoOpenEnabled>0)&&(l>=AutoOpenStartTime)&&(l<AutoOpenEndTime)) blockautopos=false;
	}
	
	sprintf(n,"%s\\XML_export_%d\\PCATS_TRAN_*.xml",NewACISDir,station);
	if (ACIS_BBOX==1)
		sprintf(n,"%s\\XML_export\\PCATS_TRAN_*.xml",NewACISDir);
	//strcpy(p,"file");
	p=n+strlen(n)+1;
	//printf("New Acis file name:%s \n",n);
	//strcat(n,"\\PCATS_TRAN_*.xml");
	if ((hf=FindFirstFile(n,&fd))!=INVALID_HANDLE_VALUE) { //At least 1 file is there
		if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
					//if ((FileNameOk(fd.cFileName,blockautopos))&&(!fd.nFileSizeHigh)) {
						strcpy(p,fd.cFileName);
						fs=fd.nFileSizeLow;
						found=true;
					//}
					//else *p=0;
		}
		else *p=0;
		while (FindNextFile(hf,&fd) && !found) {
			if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
						/*if (*p) {
							long l=12;
							char *p1=p+1;
							char *p2=fd.cFileName+1;
							l=_stricmp (p1,p2);
							if (l>0) {//new one is better
								strcpy(p,fd.cFileName);	
								fs=fd.nFileSizeLow;
								found=true;
							}
						}
						else {
							strcpy(p,fd.cFileName);
							fs=fd.nFileSizeLow;
							found=true;
						}*/
						strcpy(p,fd.cFileName);
						fs=fd.nFileSizeLow;
						found=true;
			}
		}
	}
	FindClose(hf);
	if (p==NULL) return false; //No good filename has been found
	//if (!(*p)) return false; //No good filename has been found
	sprintf(GilbF,"%s",p);
	GilbF[11]=0;
	int j=strlen(p);
	if (!found) return false; //Ther isn't a new file
	
	if (ACIS_BBOX) {
		if (found && 
			(_stricmp(GilbF,"PCATS_TRAN_")!=0)  
			) {//No good filename has been found 
		//if (_stricmp(p,"T_*.*")!=0) 
			if (p[j-5]!='*')
				L.Log("No good filename found!%s\n",p);
				return false;
			
		}		
	}
		else {
		if (found && ((_stricmp(GilbF,"PCATS_TRAN_")!=0) || (strlen(p)!=strlen("PCATS_TRAN_20121123134732.xml")))) {//No good filename has been found 
			//if (_stricmp(p,"T_*.*")!=0) 
			if (p[j-5]!='*')
				L.Log("No good filename found!%s\n",p);
			return false;
	}	
		
		}
	
	TDRefresh();
	if (TDNoGo) return false; //No good trading day condition
	L.Log("Info: Chk4NewACISTran: Got new file to process: %s.\n",p);
	//wn=new char[strlen(DNWork)+strlen(p)+2];
	//in=new char[strlen(NewACISDir)+strlen(p)+2];
	strcpy(wn,DNWork);
	strcat(wn,"\\");
	strcat(wn,p);
	L.Log("Info : Work file path: %s\n",wn);
	if (ACIS_BBOX)
		sprintf(in,"%s\\XML_export\\%s",NewACISDir,p);
	else 
		sprintf(in,"%s\\XML_export_%d\\%s",NewACISDir,station,p);
	L.Log("Info : Input file path: %s\n",in);
	if (!(CopyFile(in,wn,false))) {
		//delete [] wn;
		//delete [] in;
		L.LogE("ERROR: Cannot copy file %s to work: %s directory %d\n",in,wn,GetLastError());
		return false; //Copy was not successful - probably still locked.
	}
	L.Log("INFO: %s file copied to %s\n",in,wn);
	if ((F=fopen(wn,"rt"))==NULL) {
		L.LogE("ERROR: Opening copy was unsuccessful but CopyFile finished with success!\n  File: %s\n",wn);
		DeleteFile(wn);
		//delete [] wn;
		//delete [] in;
		return false; //Copied file cannot be opened;
	}
	L.Log("INFO - Opened the file %s\n",wn);
	//workfile opened
	fseek(F,0,SEEK_END);
	fs=ftell(F);
	fseek(F,0,SEEK_SET);
	if (fs==0) {
		if (FuelZeroSizeRetries==0) {
			FuelZeroSizeRetries++;
			L.LogE("Warning: Workfile (%s) size is 0! There will be %d retries before dropping.\n"
				,wn,FuelZeroSizeRetriesMax);
			fclose(F);
			DeleteFile(wn);
		//	delete [] wn;
		//	delete [] in;
			return false; //Copied file cannot be opened;
		}
		else {
			if (FuelZeroSizeRetries==FuelZeroSizeRetriesMax) {
				FuelZeroSizeRetries=0;
				L.LogE("ERROR: Workfile (%s) size is still 0! No more retries left. File will be parsed.\n"
					,wn,FuelZeroSizeRetriesMax);
			}
			else {
				L.LogE("Warning: Workfile (%s) size is still 0! (Retry #%d)\n"
					,wn,FuelZeroSizeRetries);
				FuelZeroSizeRetries++;
				fclose(F);
				DeleteFile(wn);
		//		delete [] wn;
		//		delete [] in;
				return false; //Copied file cannot be opened;
			}
		}
	}
	//workfile size valid or no more retries left
	if (!(DeleteFile(in))) {//deleting incoming file was not successful
		char retries=15;
		L.LogE("ERROR: Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
			,retries,in);
		Sleep(2000);
		while (!(DeleteFile(in))) {//deleting incoming file was not successful
			L.LogE("ERROR: Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
				,retries,in);
			if (!(--retries)) {//no more retries left
				fclose(F);
				DeleteFile(wn);
		//		delete [] wn;
		//		delete [] in;
				return false;
			}
			Sleep(2000);
		}
	}
	L.Log("INFO - Deleted file %s\n",in);
	//Original file was deleted
	fclose(F);
	retval=1;
	while((KeepLooping)&&(retval!=0)) {
		retval=NewACIS_ProcSale(wn);
		if (retval!=0) {
			L.LogE("Warning: Chk4Tran: ProcSale needs retry! (%d)\n",retval);
			Sleep(4000);
		}
	}
	if (retval!=0) {
		L.LogE("ERROR: Chk4Tran: ProcSale retry aborted - manual intervention required!\n");
	}
	if (retval==0) {
		char *dn=new char[strlen(DNDone)+strlen(p)+2];
		strcpy(dn,DNDone);
		strcat(dn,"\\");
		strcat(dn,p);
		if (!(MoveFile(wn,dn))) {//Moving to done was not successful
			L.LogE("ERROR: Moving processed file to Done directory was unsuccessful!\n");
		}
		else 
			L.Log("Moving processed file to Done directory was successful!%s %s\n",wn,dn);

		//delete [] dn;
	}
//	delete [] wn;
//	delete [] in;
	return true;
}



bool Chk4ACISTran(void)	//Get a file to process (find the best one by name, cache the content
{
	char n[MAX_PATH];
	long retval=0;
	char GilbF[100];
	char *p;
	char *wn;	//Workname (filename in work directory)
	bool blockautopos=true;
	SYSTEMTIME t;
	DWORD fs;
	WIN32_FIND_DATA fd;
	HANDLE hf;
	FILE* F;
	if (!FuelFunction) return false;
	if (ItemTotals.GetCount()>0) blockautopos=false;
	else {
		GetLocalTime(&t);
		long l=t.wHour*3600+t.wMinute*60+t.wSecond;
		if ((AutoOpenEnabled>0)&&(l>=AutoOpenStartTime)&&(l<AutoOpenEndTime)) blockautopos=false;
	}
	strcpy(n,ACISDNIn);
	p=n+strlen(n)+1;
	strcat(n,"\\T_*.*");
	if ((hf=FindFirstFile(n,&fd))!=INVALID_HANDLE_VALUE) { //At least 1 file is there
		if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
					//if ((FileNameOk(fd.cFileName,blockautopos))&&(!fd.nFileSizeHigh)) {
						strcpy(p,fd.cFileName);
						fs=fd.nFileSizeLow;
					//}
					//else *p=0;
		}
		else *p=0;
		while (FindNextFile(hf,&fd)) {
			if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
					//if ((FileNameOk(fd.cFileName,blockautopos))&&(!fd.nFileSizeHigh)) {
						if (*p) {
							long l=12;
							bool b=false;
							char *p1=p+1;
							char *p2=fd.cFileName+1;
							if ((*p|32)=='z') {
								b=true; //prefer new file
								if ((*fd.cFileName|32)=='z') l=6;
								else l=2;
							}
							else if ((*fd.cFileName|32)=='z') l=2;
							if ((*p1|32)=='i') p1++;
							if ((*p2|32)=='i') p2++;
							l=strncmp (p1,p2,l);
							if ((l>0)||((l==0)&&b)) {//new one is better
								strcpy(p,fd.cFileName);	
								fs=fd.nFileSizeLow;
							}
						}
						else {
							strcpy(p,fd.cFileName);
							fs=fd.nFileSizeLow;
						}
					//}
			}
		}
	}
	FindClose(hf);
	if (!(*p)) return false; //No good filename has been found
	/***********  ACIS REGISTER FILES  ************/
	sprintf(GilbF,"%s",p);
	GilbF[2]=0;
/*	//L.Log("New ACIS file arrived! %s	GilbF:%s\n",p, GilbF);
	//if ( (_stricmp(GilbF,"R_")==0) && (_stricmp(fd.cFileName+strlen(fd.cFileName)-3,"dat")==0) ) {
	if (_stricmp(GilbF,"R_")==0) {
		L.Log("ACIS register file arrived! %s\n",p);
		char *dn=new char[strlen(DNDone)+strlen(p)+2];
		wn=new char[strlen(ACISDNIn)+strlen(p)+2];
		strcpy(dn,DNDone);
		strcat(dn,"\\");
		strcat(dn,p);
		strcpy(wn,ACISDNIn);
		strcat(wn,"\\");
		strcat(wn,p);

		if (!(MoveFile(wn,dn))) {//Moving to done was not successful
			if (GetLastError()==183) {
				DeleteFile(dn);
				L.LogE("ERROR -- The file already exists in the target directory! %s\n",dn);
				if (!(MoveFile(wn,dn)))
					L.LogE("ERROR: Moving ACIS REGISTER file to Done directory was unsuccessful! Error_code: %d\n",GetLastError());
			}
			else
				L.LogE("ERROR: Moving ACIS REGISTER file to Done directory was unsuccessful! Error_code: %d\n",GetLastError());
		}
		delete [] dn;
		delete [] wn;
		return false;
	}*/
	int j=strlen(p);
	if ((_stricmp(GilbF,"T_")!=0) || (strlen(p)<7)) {//No good filename has been found 
		if (_stricmp(p,"T_*.*")!=0) L.Log("No good filename found!%s\n",p);
		return false;
	}
	TDRefresh();
	if (TDNoGo) return false; //No good trading day condition
	wn=new char[strlen(DNWork)+strlen(p)+2];
	strcpy(wn,DNWork);
	strcat(wn,"\\");
	strcat(wn,p);
	if (!(CopyFile(n,wn,false))) {
		delete [] wn;
		return false; //Copy was not successful - probably still locked.
	}
	if ((F=fopen(wn,"rt"))==NULL) {
		L.LogE("ERROR: Opening copy was unsuccessful but CopyFile finished with success!\n  File: %s\n",wn);
		DeleteFile(wn);
		delete [] wn;
		return false; //Copied file cannot be opened;
	}
	//workfile opened
	fseek(F,0,SEEK_END);
	fs=ftell(F);
	fseek(F,0,SEEK_SET);
	if (fs==0) {
		if (FuelZeroSizeRetries==0) {
			FuelZeroSizeRetries++;
			L.LogE("Warning: Workfile (%s) size is 0! There will be %d retries before dropping.\n"
				,wn,FuelZeroSizeRetriesMax);
			fclose(F);
			DeleteFile(wn);
			delete [] wn;
			return false; //Copied file cannot be opened;
		}
		else {
			if (FuelZeroSizeRetries==FuelZeroSizeRetriesMax) {
				FuelZeroSizeRetries=0;
				L.LogE("ERROR: Workfile (%s) size is still 0! No more retries left. File will be parsed.\n"
					,wn,FuelZeroSizeRetriesMax);
			}
			else {
				L.LogE("Warning: Workfile (%s) size is still 0! (Retry #%d)\n"
					,wn,FuelZeroSizeRetries);
				FuelZeroSizeRetries++;
				fclose(F);
				DeleteFile(wn);
				delete [] wn;
				return false; //Copied file cannot be opened;
			}
		}
	}
	//workfile size valid or no more retries left
	if (!(DeleteFile(n))) {//deleting incoming file was not successful
		char retries=15;
		L.LogE("ERROR: Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
			,retries,n);
		Sleep(2000);
		while (!(DeleteFile(n))) {//deleting incoming file was not successful
			L.LogE("ERROR: Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
				,retries,n);
			if (!(--retries)) {//no more retries left
				fclose(F);
				DeleteFile(wn);
				delete [] wn;
				return false;
			}
			Sleep(2000);
		}
	}
	//Original file was deleted
/*	L.Log("Info: Chk4ACISTran: Got new file to process: %s.\n",p);
	/***********  ACIS REGISTER FILES  ************/
/*	sprintf(GilbF,"%s",p);
	GilbF[2]=0;
	L.Log("New ACIS file arrived (2)! %s	GilbF:%s\n",fd.cFileName, GilbF);
	//if ( (_stricmp(GilbF,"R_")==0) && (_stricmp(fd.cFileName+strlen(fd.cFileName)-3,"dat")==0) ) {
	if (_stricmp(GilbF,"R_")==0) {
		L.Log("ACIS register file arrived (2)! %s\n",p);
		char *dn=new char[strlen(DNDone)+strlen(p)+2];
		wn=new char[strlen(ACISDNIn)+strlen(p)+2];
		strcpy(dn,DNDone);
		strcat(dn,"\\");
		strcat(dn,p);
		strcpy(wn,ACISDNIn);
		strcat(wn,"\\");
		strcat(wn,p);

		if (!(MoveFile(wn,dn))) {//Moving to done was not successful
			if (GetLastError()==183) {
				DeleteFile(dn);
				if (!(MoveFile(wn,dn)))
					L.LogE("ERROR: Moving ACIS REGISTER file to Done directory was unsuccessful! Error_code: %d\n",GetLastError());
			}
			else
				L.LogE("ERROR: Moving ACIS REGISTER file to Done directory was unsuccessful! Error_code: %d\n",GetLastError());
		}
		delete [] dn;
		delete [] wn;
		return false;
	}
*/
	if ((*p|32)=='z') ProcClose(F);
	else {
			if ((p[1]|32)=='i') {
				retval=1;
				while((KeepLooping)&&(retval!=0)) {
					retval=ProcLogOnOff(F);
					if (retval!=0) {
						L.LogE("Warning: Chk4Tran: ProcLogOnOff needs retry! (%d)\n",retval);
						fseek(F,0,SEEK_SET);
						Sleep(4000);
					}
				}
				if (retval!=0) {
					L.LogE("ERROR: Chk4Tran: ProcLogOnOff retry aborted - manual intervention required!\n");
				}
			}
			else {
				retval=1;
				while((KeepLooping)&&(retval!=0)) {
					retval=ProcSale(F,true);
					if (retval!=0) {
						L.LogE("Warning: Chk4Tran: ProcSale needs retry! (%d)\n",retval);
						fseek(F,0,SEEK_SET);
						Sleep(4000);
					}
				}
				if (retval!=0) {
					L.LogE("ERROR: Chk4Tran: ProcSale retry aborted - manual intervention required!\n");
				}
			}
	}
	fclose(F);
	if (retval==0) {
		char *dn=new char[strlen(DNDone)+strlen(p)+2];
		strcpy(dn,DNDone);
		strcat(dn,"\\");
		strcat(dn,p);
		if (!(MoveFile(wn,dn))) {//Moving to done was not successful
			L.LogE("ERROR: Moving processed file to Done directory was unsuccessful! %d\n",GetLastError());
		}
		else 
			L.Log("Moving processed file to Done directory was successful!%s %s\n",wn,dn);

		delete [] dn;
	}
	delete [] wn;
	if ((*p|32)=='z') {//Closure -> Moving files from done to history
		char *dn=new char[MAX_PATH];
		strcpy(dn,DNHist);
		strcat(dn,"\\");
		char *pdn=dn+strlen(dn);
		for(char stage=0;stage<2;stage++) {
			bool gotfile=true;
			strcpy(n,(stage==0) ? DNDone : DNWork);
			p=n+strlen(n)+1;
			strcat(n,"\\*");
			if ((hf=FindFirstFile(n,&fd))!=INVALID_HANDLE_VALUE) { //At least 1 file is there
				while (gotfile) {
					if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
						strcpy(p,fd.cFileName);
						strcpy(pdn,fd.cFileName);
						if (!(CopyFile(n,dn,false))) {//copy to hist wasn't made
							L.LogE("ERROR: CopyFile has been failed! %s -> %s Error code=%d\n"
								,n,dn,GetLastError());
						}
						else {
							if (!(DeleteFile(n))) {//Deleting from done was failed
								L.LogE("ERROR: DeleteFile has been failed! (%s) Error code=%d\n"
									,n,GetLastError());
							}
						}
					}
					gotfile=(FindNextFile(hf,&fd)!=0);
				}
			}
			FindClose(hf);
		}
		delete [] dn;
	}
	return true;
}


bool Chk4RVMTran(void)	//Get a file to process (find the best one by name, cache the content
{
	char n[MAX_PATH];
	long retval=0;
	char retries=0;
	bool closure=false;
	char cc;
	char *p;
	char *wn;	//Workname (filename in work directory)
	DWORD fs;
	WIN32_FIND_DATA fd;
	HANDLE hf= INVALID_HANDLE_VALUE;
	
	FILE* F;
	if (!RVMFunction) return false;
	strcpy(n,RDNIn);
	p=n+strlen(n)+1;
	strcat(n,"\\*.*T");
	if ((hf=FindFirstFile(n,&fd))!=INVALID_HANDLE_VALUE) { //At least 1 file is there
		if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
			if ((RVMFileNameOk(fd.cFileName))&&(!fd.nFileSizeHigh)) {
				strcpy(p,fd.cFileName);
				fs=fd.nFileSizeLow;
			}
			else *p=0;
		}
		else *p=0;
		while (FindNextFile(hf,&fd)) {
			if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
				if ((RVMFileNameOk(fd.cFileName))&&(!fd.nFileSizeHigh)) {
					if (*p) {
						//long l=12;
						long l=19; //YYYYMMDDHHMMSS.TTTI
						bool b=false;
						char *p1=p+1;
						char *p2=fd.cFileName+1;
						//if ((*p|32)=='z') {
						//	b=true; //prefer new file
						//	if ((*fd.cFileName|32)=='z') l=6;
						//	else l=2;
						//}
						//else if ((*fd.cFileName|32)=='z') l=2;
						//if ((*p1|32)=='i') p1++;
						//if ((*p2|32)=='i') p2++;
						l=strncmp (p1,p2,l);
						if (l>0) {//new one is better
							strcpy(p,fd.cFileName);	
							fs=fd.nFileSizeLow;
						}
					}
					else {
						strcpy(p,fd.cFileName);
						fs=fd.nFileSizeLow;
					}
				}
			}
		}
	}
	FindClose(hf);
	if (!(*p)) return false; //No good filename has been found
	wn=new char[strlen(RDNWork)+strlen(p)+2];
	strcpy(wn,RDNWork);
	strcat(wn,"\\");
	strcat(wn,p);
	if (!(CopyFile(n,wn,false))) {
		delete [] wn;
		return false; //Copy was not successful
	}
	if ((F=fopen(wn,"rt"))==NULL) {
		DeleteFile(wn);
		delete [] wn;
		return false; //Copied file cannot be opened;
	}
	//workfile opened
	if (!(DeleteFile(n))) {//deleting incoming file was not successful
		retries=15;
		RL.LogE("ERROR: Chk4RVMTran: Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
			,retries,n);
		Sleep(2000);
		while (!(DeleteFile(n))) {//deleting incoming file was not successful
			RL.LogE("ERROR: Chk4RVMTran: Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
				,retries,n);
			if (!(--retries)) {//no more retries left
				fclose(F);
				DeleteFile(wn);
				delete [] wn;
				return false;
			}
			Sleep(2000);
		}
	}
	//Original file was deleted
	RL.Log("Info: Chk4RVMTran: Got new file to process: %s.\n",p);
	retval=-1;
	fseek(F,0,SEEK_SET);
	cc=fgetc(F);
	if (cc=='H') {
		closure=false;
		if (PendingClosure) {
			RL.LogE("ERROR: Chk4RVMTran: Another RVM closure file was expected! Forcing closure end!\n");
			ForcedRVMClosure();
		}
	}
	else {
		if (cc=='D') closure=true;
		else {
			RL.LogE("ERROR: Chk4RVMTran: RVM file invalid - does not start with D or H!\n");
			retval=0x00010001;
		}
	}
	fseek(F,0,SEEK_SET);
	retries=60;
	while (retval<0) {
		if (closure) retval=ProcRVMClose(F);
		else retval=ProcRVMTran(F);
		if (retval<0) {
			if (retries) {
				RL.LogE("Warning: Chk4RVMTran: ProcRVMFile needs retry - (%d) left! (%d)\n"
					,retries,-retval);
				fseek(F,0,SEEK_SET);
				Sleep(5000);
				retries--;
			}
			else {
				RL.LogE("ERROR: Chk4RVMTran: ProcRVMFile maximum retry elapsed - manual intervention required!\n");
				fclose(F);
				return false;  //Like there wasn't any file
			}
		}
	}
	fclose(F);
	if (retval==0) {
		char *dn=new char[strlen(RDNDone)+strlen(p)+2];
		strcpy(dn,RDNDone);
		strcat(dn,"\\");
		strcat(dn,p);
		if (!(MoveFile(wn,dn))) {//Moving to done was not successful
			RL.LogE("ERROR: Moving processed file to Done directory was unsuccessful!\n");
		}
		else
			L.Log("Moving processed file to Done directory was successful!%s %s\n",wn,dn);
		delete [] dn;
	}
	delete [] wn;
	if (closure) {//Closure -> Moving files from done to history
		char *dn=new char[MAX_PATH];
		strcpy(dn,RDNHist);
		strcat(dn,"\\");
		char *pdn=dn+strlen(dn);
		for(char stage=0;stage<2;stage++) {
			bool gotfile=true;
			strcpy(n,(stage==0) ? RDNDone : RDNWork);
			p=n+strlen(n)+1;
			strcat(n,"\\*");
			if ((hf=FindFirstFile(n,&fd))!=INVALID_HANDLE_VALUE) { //At least 1 file is there
				while (gotfile) {
					if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
						strcpy(p,fd.cFileName);
						strcpy(pdn,fd.cFileName);
						if (!(CopyFile(n,dn,false))) {//copy to hist wasn't made
							RL.LogE("ERROR: CopyFile has been failed! %s -> %s Error code=%d\n"
								,n,dn,GetLastError());
						}
						else {
							if (!(DeleteFile(n))) {//Deleting from done was failed
								RL.LogE("ERROR: DeleteFile has been failed! (%s) Error code=%d\n"
									,n,GetLastError());
							}
						}
					}
					gotfile=(FindNextFile(hf,&fd)!=0);
				}
			}
			FindClose(hf);
		}
		delete [] dn;
	}
	return true;
}

bool Chk4TomraTran(void)	//Get a file to process (find the best one by name, cache the content
{
	char n[MAX_PATH];
	long retval=0;
	char retries=0;
	bool closure=false;
	char cc;
	char *p;
	char *wn;	//Workname (filename in work directory)
	DWORD fs;
	WIN32_FIND_DATA fd;
	HANDLE hf= INVALID_HANDLE_VALUE;
	//YYMMDDhhmmss_fffffffarrrrrrpppppp.ler
	FILE* F;
	if (!TomraFunction) return false;
	strcpy(n,RDNIn);
	p=n+strlen(n)+1;
	strcat(n,"\\*.ler");
	RL.Log("TOMRA -- Search files %s\n",n);
	if ((hf=FindFirstFile(n,&fd))!=INVALID_HANDLE_VALUE) { //At least 1 file is there
		if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
			if ((TomraFileNameOk(fd.cFileName))&&(!fd.nFileSizeHigh)) {
				strcpy(p,fd.cFileName);
				fs=fd.nFileSizeLow;
			}
			else *p=0;
		}
		else *p=0;
		while (FindNextFile(hf,&fd)) {
			if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
				if ((TomraFileNameOk(fd.cFileName))&&(!fd.nFileSizeHigh)) {
					strcpy(p,fd.cFileName);	
					/*if (*p) {
						//long l=12;
						long l=37; //YYMMDDhhmmss_fffffffarrrrrrpppppp.ler
						bool b=false;
						char *p1=p+1;
						char *p2=fd.cFileName+1;
						//if ((*p|32)=='z') {
						//	b=true; //prefer new file
						//	if ((*fd.cFileName|32)=='z') l=6;
						//	else l=2;
						//}
						//else if ((*fd.cFileName|32)=='z') l=2;
						//if ((*p1|32)=='i') p1++;
						//if ((*p2|32)=='i') p2++;
						l=strncmp (p1,p2,l);
						if (l>0) {//new one is better
							strcpy(p,fd.cFileName);	
							fs=fd.nFileSizeLow;
						}
					}
					else {
						strcpy(p,fd.cFileName);
						fs=fd.nFileSizeLow;
					}*/
				}
			}
		}
	}
	FindClose(hf);
	if (!(*p)) return false; //No good filename has been found
	wn=new char[strlen(RDNWork)+strlen(p)+2];
	strcpy(wn,RDNWork);
	strcat(wn,"\\");
	strcat(wn,p);
	if (!(CopyFile(n,wn,false))) {
		delete [] wn;
		return false; //Copy was not successful
	}
	if ((F=fopen(wn,"rt"))==NULL) {
		DeleteFile(wn);
		delete [] wn;
		return false; //Copied file cannot be opened;
	}
	//workfile opened
	if (!(DeleteFile(n))) {//deleting incoming file was not successful
		retries=15;
		RL.LogE("ERROR: Chk4TomraTran: Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
			,retries,n);
		Sleep(2000);
		while (!(DeleteFile(n))) {//deleting incoming file was not successful
			RL.LogE("ERROR: Chk4TomraTran: Deleting copied file was unsuccessful! %d retries left.\n  File: %s\n"
				,retries,n);
			if (!(--retries)) {//no more retries left
				fclose(F);
				DeleteFile(wn);
				delete [] wn;
				return false;
			}
			Sleep(2000);
		}
	}
	//Original file was deleted
	RL.Log("Info: Chk4TomraTran: Got new file to process: %s.\n",p);
	retval=-1;
	fseek(F,0,SEEK_SET);
	cc=fgetc(F);
	closure=false;
	/*if (cc=='H') {
		closure=false;
		if (PendingClosure) {
			RL.LogE("ERROR: Chk4TomraTran: Another Tomra closure file was expected! Forcing closure end!\n");
			ForcedTomraClosure();
		}
	}
	else {
		if (cc=='D') closure=true;
		else {
			RL.LogE("ERROR: Chk4TomraTran: Tomra file invalid - does not start with D or H!\n");
			retval=0x00010001;
		}
	}*/
	fseek(F,0,SEEK_SET);
	retries=60;
	while (retval<0) {
		//if (closure) retval=ProcTomraClose(F);
		//else 
		retval=ProcTOMRATran(F);
		if (retval<0) {
			if (retries) {
				RL.LogE("Warning: Chk4TomraTran: ProcTomraFile needs retry - (%d) left! (%d)\n"
					,retries,-retval);
				fseek(F,0,SEEK_SET);
				Sleep(5000);
				retries--;
			}
			else {
				RL.LogE("ERROR: Chk4TomraTran: ProcTomraFile maximum retry elapsed - manual intervention required!\n");
				fclose(F);
				return false;  //Like there wasn't any file
			}
		}
	}
	fclose(F);
	if (retval==0) {
		char *dn=new char[strlen(RDNDone)+strlen(p)+2];
		strcpy(dn,RDNDone);
		strcat(dn,"\\");
		strcat(dn,p);
		if (!(MoveFile(wn,dn))) {//Moving to done was not successful
			RL.LogE("ERROR: Moving processed file to Done directory was unsuccessful!\n");
		}
		else
			L.Log("Moving processed file to Done directory was successful!%s %s\n",wn,dn);
		delete [] dn;
	}
	delete [] wn;
	if (closure) {//Closure -> Moving files from done to history
		char *dn=new char[MAX_PATH];
		strcpy(dn,RDNHist);
		strcat(dn,"\\");
		char *pdn=dn+strlen(dn);
		for(char stage=0;stage<2;stage++) {
			bool gotfile=true;
			strcpy(n,(stage==0) ? RDNDone : RDNWork);
			p=n+strlen(n)+1;
			strcat(n,"\\*");
			if ((hf=FindFirstFile(n,&fd))!=INVALID_HANDLE_VALUE) { //At least 1 file is there
				while (gotfile) {
					if (!(fd.dwFileAttributes&(FILE_ATTRIBUTE_DIRECTORY|FILE_ATTRIBUTE_TEMPORARY))) {
						strcpy(p,fd.cFileName);
						strcpy(pdn,fd.cFileName);
						if (!(CopyFile(n,dn,false))) {//copy to hist wasn't made
							RL.LogE("ERROR: CopyFile has been failed! %s -> %s Error code=%d\n"
								,n,dn,GetLastError());
						}
						else {
							if (!(DeleteFile(n))) {//Deleting from done was failed
								RL.LogE("ERROR: DeleteFile has been failed! (%s) Error code=%d\n"
									,n,GetLastError());
							}
						}
					}
					gotfile=(FindNextFile(hf,&fd)!=0);
				}
			}
			FindClose(hf);
		}
		delete [] dn;
	}
	return true;
}

long Insert_live_stamp(){
	char sql[250];
//	long res;
	sprintf(sql,"INSERT INTO `Penzugy2`.`prog_check` (`prog_nev`,`ido`) VALUES ('aupossim',now()) on duplicate key update `ido`=now()");
	long ret_ins =Insert_MYSQL(sql);
	if(!ret_ins){
		return 0;
	}
	else
		return -1;
}

bool Chk4Upd_namos(void)
{
	bool res=false;
	SYSTEMTIME t;
	FILE *F;
	GetLocalTime(&t);
	char *dst=NULL;
	char a[250],b[50];
//	char ft1 [5];
	TIItems Items[20000];
	int items_counter=0;
	TIUpdate ItemUpd[50];
	TIUpdate ItemUpd_igas[5000];
	TIUpdate FuelUpd[50];
	int iupd_cnt=0,fupd_cnt=0,niupd_cnt=0;
//	char StoreLocId[2];
//	char Dept[4];
	int igas_upd_counter=0;
	Reg r(false, C_RegKey);
	long seq_num,sell_price=0;
	int i;
	/****  VEV� Friss�t�s  **********/
	/*if((t.wMinute % 2)==0){
		if (refresh==1 && FuelFunction==1){ //2 percenk�nt megy a friss�t�se a vev�knekw
			GetCSCFromDB();
			refresh=0;
		}
	}
	else
		refresh=1;
	*/
	
	if(t.wMinute<10 && t.wHour==0){
		/****  Ir�ny�t�sz�m bek�r�se st�tusz  **********/
		GetPostCodeStatusFromDB();
		/****  Akci�s �r lek�rdez�se  **********/
		GetDiscountPrice();
		if(is_disc_price){
			res=CopyFile("c:\\aufuelif\\fhaucp00.fuel",FNItem,true);		
			while(!res){
				L.LogE("ERROR -- Cannot copy c:\\aufuelif\\fhaucp00.fuel file to %s\n",FNItem);
				i++;
				Sleep(30000);
				res=CopyFile("c:\\aufuelif\\fhaucp00.fuel",FNItem,true);		
				if(i>10) {
					res=true;
					is_disc_price=false;
				}
			}
		}
	}
	if((t.wMinute % 10)==0){
		if(Insert_live_stamp()!=0) L.LogE("ERROR -- Write stamp to database is unsuccesful\n");
	}

	//No csc file, use filename for item update if exists the input file
	if ((F=fopen(FNItem,"rt"))!=NULL) {
		long fuelitems=0;
		long rvmitems=0;
		long rvmlinks=0;
		
		FILE *D;
		TSList fl={0,NULL,NULL};
		TSList rl={0,NULL,NULL};
		TSList *lp=NULL;
		res=true;
		L.Log("Info: Chk4Upd: Found a new item update file to process...\n");
		long l;
		long cnt=0;
		bool warn;
		char buf[200];
		char cr[108];
		char rcr[32];
		char lcr[32];
		char dept[5];
		char rdept[5];
		
		sprintf(dept,"%04d",ItemDept[0]);
		sprintf(rdept,"%04d",RItemDept[0]);
		memset(cr+1,'0',103);
		cr[0]='A'; //record type
		cr[104]=13; //string ending cr
		cr[105]=10; //string ending lf
		cr[106]=0; //string end
		memset(cr+51,' ',6);//item_desc.fill_end
		memset(cr+82,' ',7);//SWW_code.fill_end
		rcr[0]='U'; //record type
		rcr[14]='0';//missing leading zeroes from price
		rcr[15]='0';//...
		rcr[26]=13; //string ending cr
		rcr[27]=10; //string ending lf
		rcr[28]=0; //string end
		lcr[0]='R'; //record type
		lcr[27]=13; //string ending cr
		lcr[28]=10; //string ending lf
		lcr[29]=0; //string end
		int fuel=0,item=0;
		bool found=false;
		int xx=0;
		FILE *IF;
		char seps[]   = "|\n";
		char *token;
		int counter=0;
		char ttt[13];
		strcpy(ttt,"             ");
		if(namos){  //Milyen term�kek �rv�ltoz�sa mehet a NAMOS fel�
			if ((IF=fopen(FNItemList,"rt"))!=NULL) {
				if( fseek( IF,0, SEEK_SET) )	{
					L.LogE("ERROR - Fseek failed %s\n",FNItemList );
				}
				else   {
					while (!feof(IF))
					{
					  fgets( a,250,IF);
					  if(strcmp(a,ttt)!=0){
						token = strtok( a, seps );
						strcpy(ttt,token);
						memcpy(Items[items_counter].EAN,token,13);
						counter = 0;
						while( token != NULL )	{
							counter++;
							token = strtok( NULL, seps );
							switch(counter) {
								case 1: memcpy(Items[items_counter].Unit,token,4); break;
								case 2: Items[items_counter].isFuel = atoi(token); break;
							}
							if(Items[items_counter].isFuel==1) L.Log("Fuel:%s\n",Items[items_counter].EAN);
						}
						items_counter++;
					  }
					}
				}
				fclose(IF);
			}
		}
		char ean_tmp[14];
		char price[10];
		SYSTEMTIME t;
		SYSTEMTIME st;
		GetLocalTime(&t);
		long time;
		if(t.wHour*3600+t.wMinute*60+t.wSecond>FuelUpdateTimeSill)
			time=t.wHour*3600+t.wMinute*60+t.wSecond+NamosISSTimeDIfferent+FuelPlusTimeAfterSill;
		else
			time=t.wHour*3600+t.wMinute*60+t.wSecond+NamosISSTimeDIfferent+FuelPlusTimeBeforeSill;
		SysTimeFromLong(&st,time);		

		sprintf(ttt,"%.4d%.2d%.2d%.2d%.2d",t.wYear,t.wMonth,t.wDay,st.wHour,st.wMinute);
		wchar_t *wText;
		char *ansiText;
		bool fuel_found=false;
			seq_num=r.ValueDW("Fuel.NextUpdateNr",0);
			seq_num++;




			sprintf(a,"%.4d%.2d%.2d%.2d%.2d00",t.wYear,t.wMonth,t.wDay,st.wHour,st.wMinute);
			TiXmlDocument docwrite_igas;  
			TiXmlElement* msg_igas;
 			TiXmlDeclaration* decl_igas = new TiXmlDeclaration( "1.0", "UTF-8", "" );  
			docwrite_igas.LinkEndChild( decl_igas ); 
			
			/**********  NAMOS ITEM UPDATE  ********************/
			TiXmlElement * root_igas = new TiXmlElement( "Data" );
				root_igas->SetAttribute("TxDev","SIM");
				root_igas->SetAttribute("TxDevNum","0");
				root_igas->SetAttribute("RxDev","NAM");
				root_igas->SetAttribute("RxDevNum","0");
				root_igas->SetAttribute("SerGroup","50");
				root_igas->SetAttribute("SerCode","0");
				root_igas->SetAttribute("Prio","1");
				root_igas->SetAttribute("SeqNum",seq_num);
				root_igas->SetAttribute("Time",a);
				root_igas->SetAttribute("Version","2,23");
			docwrite_igas.LinkEndChild( root_igas );

			TiXmlElement * Items_igas = new TiXmlElement( "Items" );  
				Items_igas->SetAttribute("InitialLoad","0");
		
		while (fgets(buf,199,F)) {//loop to read logali records
			l=strlen(buf);
			while (buf[l-1]<' ') buf[--l]=0;
			warn=(l<115);
			while (l<127) {buf[l]=' '; buf[++l]=0;}
			if ((l>=127)&&((buf[20]=='1')||(buf[20]=='2')) ) {//good update record
				if(GilBarco || FLEXYS){
					item=ItemOnList(buf+21);
					fuel=FuelOnList(buf+21);
				}
				if(namos || igass ){
					item=false;
					xx=0;
					while ( xx<items_counter){
						strncpy(ean_tmp,buf+21,13);
						ean_tmp[13]=0;
						Items[xx].EAN[13]=0;
						remove_lchar(ean_tmp,"0");
						if(strcmp(Items[xx].EAN,ean_tmp)==0){
							igas_upd_counter++;
							ItemUpd[iupd_cnt].isFuel=Items[xx].isFuel;
							memcpy(ItemUpd[iupd_cnt].Unit,Items[xx].Unit,4);
							if(Items[xx].isFuel==1){
								xx=0;
								//iupd_cnt++;
								fuel_found=false;
								memcpy(price,buf+71,10);
								price[8]=0;
								while(FuelPriceArray[xx].price>0){//Be�rom az �j �rat a registrybe
									if(strcmp(FuelPriceArray[xx].ean,ean_tmp)==0){
										memcpy(FuelPriceArray[xx].date,ttt,12);
										if(atoi(price)>0){
											FuelPriceArray[xx].price=atoi(price);
										}
										else {
											sell_price=FuelPriceArray[xx].price;
											if(is_disc_price){
												if(FuelPriceArray[xx].disc_price<FuelPriceArray[xx].price && FuelPriceArray[xx].disc_price!=0)
													sell_price=FuelPriceArray[xx].disc_price;
												else
													sell_price=FuelPriceArray[xx].price;
											}
										}
										//FuelPriceArray[xx].disc_price=atoi(disc_price);
										fuel_found=true;
									}
									xx++;
								}
								if(!fuel_found && atoi(price)>0){ //Ha nincs ez a term�k a registry-ben
//									if(xx==1)  //Ha egy term�k sincs m�g a REGISTRY-ben
//										xx=0;
									sprintf(FuelPriceArray[xx].ean,"%s",ean_tmp);
									sprintf(FuelPriceArray[xx].date,"%s",ttt);
									FuelPriceArray[xx].price=atoi(price);
									//FuelPriceArray[xx].disc_price=atoi(disc_price);
									FuelPriceArray[xx].date[12]=0;
									//if(xx>0) FuelPriceArray[xx-1].date[12]=0;
									FuelPriceArray[xx].ean[13]=0;
								}
							}
							item=true;
							xx=items_counter+1;
						}
						xx++;
					} 
				}
				if (item || fuel ) {//in fuel department
					strncpy(ean_tmp,buf+21,13);
						DoItemSubst(buf+21,true);
					memcpy(cr+8,buf+21,13);//item_code
					
					memcpy(cr+21,buf+41,30);//item_desc
					cr[66]='1';//item_dept, always 0000000001
					memcpy(cr+67,buf+92,2);//VAT rate_id
					long idx=10*(cr[67]-'0')+(cr[68]-'0');
					cr[67]='0';
					if ((idx<0)||(((unsigned long)idx)>=strlen(VatConvTable))) {//under or overindexing
						cr[68]='2';
					}
					else {
						cr[68]=VatConvTable[idx];
						if ((cr[68]<'0')||(cr[68]>'4')) cr[68]='2';
					}
					cr[68]++; //Inc VAT rate ID to conform fuel station interface req.
					memcpy(cr+69,buf+114,13);//SWW_code
					if (PriceByCentiLiters==0) {//keep price coming on the iss interface
						memcpy(cr+94,buf+71,10);//price (ending 10 only)
					}
					else {//price by centiliters -> multiply price by 100
						memcpy(cr+94,buf+73,8);//price (ending 10 only)
						cr[102]='0';
						cr[103]='0';
					}
					
					if (item) {
						memcpy(ItemUpd[iupd_cnt].Auchan_code,buf+14,6);
						ItemUpd[iupd_cnt].Auchan_code[6]=0;
						memcpy(ItemUpd[iupd_cnt].EAN,buf+21,13);
						ItemUpd[iupd_cnt].EAN[13]=0;
						memcpy(ItemUpd_igas[iupd_cnt].EAN,ean_tmp,13);
						ItemUpd_igas[iupd_cnt].EAN[13]=0;
						memcpy(ItemUpd[iupd_cnt].Description,buf+41,30);
						ItemUpd[iupd_cnt].Description[30]=0;
						wText = CodePageToUnicode(852,ItemUpd[iupd_cnt].Description);
						ansiText = UnicodeToCodePage(65001,wText);	
						strncpy(ItemUpd[iupd_cnt].Description,ansiText,30);
				
						//ConvChar852ToWin((unsigned char *)ItemUpd[iupd_cnt].Description);
						remove_rchar(ItemUpd[iupd_cnt].Description," ");
						ItemUpd[iupd_cnt].Description[30]=0;
						//cr[68]--;
						ItemUpd[iupd_cnt].Tax_id[0]=cr[68];
						ItemUpd[iupd_cnt].Tax_id[1]=0;
						if (PriceByCentiLiters==0) {//keep price coming on the iss interface
							memcpy(ItemUpd[iupd_cnt].Price,buf+71,10);//price (ending 10 only)
						}
						else {//price by centiliters -> multiply price by 100
							memcpy(ItemUpd[iupd_cnt].Price,buf+73,8);//price (ending 10 only)
						}
						ItemUpd[iupd_cnt].Price[8]=0;
						remove_lchar(ItemUpd[iupd_cnt].Price,"0");
						if(is_disc_price)
							sprintf(ItemUpd[iupd_cnt].Price,"%d",sell_price);
						memcpy(ItemUpd[iupd_cnt].Dept_Id,buf+37,4);
						ItemUpd[iupd_cnt].Dept_Id[4]=0;
						memcpy(ItemUpd[iupd_cnt].Linked_ean,buf+94,13);
						ItemUpd[iupd_cnt].Linked_ean[13]=0;
						remove_lchar(ItemUpd[iupd_cnt].Linked_ean,"0");
						sprintf(ItemUpd[iupd_cnt].VTSZ,buf+111);
						RemoveLetter(ItemUpd[iupd_cnt].VTSZ);
						iupd_cnt++;
					}
					if (fuel) {
						memcpy(FuelUpd[fupd_cnt].EAN,buf+21,13);
						FuelUpd[fupd_cnt].EAN[13]=0;
						memcpy(FuelUpd[fupd_cnt].Description,buf+41,30);
						FuelUpd[fupd_cnt].Description[30]=0;
						ConvChar852ToWin((unsigned char *)FuelUpd[fupd_cnt].Description);
						remove_rchar(FuelUpd[fupd_cnt].Description," ");
						//cr[68]++;
						FuelUpd[fupd_cnt].Tax_id[0]=cr[68];
						FuelUpd[fupd_cnt].Tax_id[1]=0;
						if (PriceByCentiLiters==0) {//keep price coming on the iss interface
							memcpy(FuelUpd[fupd_cnt].Price,buf+71,8);//price (ending 10 only)
							FuelUpd[fupd_cnt].Price[8]='.';
							memcpy(FuelUpd[fupd_cnt].Price+9,buf+79,2);//price (ending 10 only)
							FuelUpd[fupd_cnt].Price[11]=0;
						}
						else {//price by centiliters -> multiply price by 100
							memcpy(FuelUpd[fupd_cnt].Price,buf+73,8);//price (ending 10 only)
							FuelUpd[fupd_cnt].Price[8]=0;
						}
						remove_lchar(FuelUpd[fupd_cnt].Price,"0");
						if(is_disc_price)
							sprintf(ItemUpd[iupd_cnt].Price,"%d",sell_price);
						memcpy(FuelUpd[fupd_cnt].Dept_Id,buf+37,4);
						FuelUpd[fupd_cnt].Dept_Id[4]=0;
						sprintf(FuelUpd[fupd_cnt].VTSZ,buf+110);
						RemoveLetter(FuelUpd[fupd_cnt].VTSZ);
						fupd_cnt++;
					}
					buf[34]=0;
					buf[71]=0;
					for (l=70;(l>41)&&(buf[l]==' ');l--) buf[l]=0;
					lp=fl.n;
					if ((fl.n=new TSList)==NULL) {
						L.LogE("ERROR: Chk4Upd: Item list cannot be extended, one item (%s) was lost!\n",buf+21);
						fl.n=lp;
					}
					else {
						if ((fl.n->p=new char[108])==NULL) {
							L.LogE("ERROR: Chk4Upd: Item data cannot be stored, one item (%s) was lost!\n",buf+21);
							delete fl.n;
							fl.n=lp;
						}
						else {
							fl.n->n=lp;
							fl.n->id=1;
							memcpy(fl.n->p,cr,108);
							fuelitems++;
						}
					}
					if (warn)
						L.LogE("Warning: Chk4Upd: SWW code is empty for %s! (%s)\n",buf+21,buf+41);
					else 
						L.Log("Update EAN:%s\n",ItemUpd[iupd_cnt-1].EAN);
				if ((FuelFunction) && ((namos!=0 ) || (igass!=0 ))) {
			
				//for(i=0;i<igas_upd_counter;i++) 
				if(igas_upd_counter>0){
					i=0;iupd_cnt=0;
					msg_igas = new TiXmlElement( "Item" );
					msg_igas->SetAttribute("ItemNr",ItemUpd[i].Auchan_code);
					msg_igas->SetAttribute("BarCode",ItemUpd_igas[i].EAN);
					msg_igas->SetAttribute("ShortText",ItemUpd[i].Description);
					if(is_disc_price){
						msg_igas->SetAttribute("SalesPrice",ItemUpd[i].Price);
					}
					else{
						msg_igas->SetAttribute("SalesPrice",ItemUpd[i].Price);
					}
					msg_igas->SetAttribute("SalesUnit",ItemUpd[i].Unit);
					msg_igas->SetAttribute("Locked","0");
					if(ItemUpd[i].isFuel)
						msg_igas->SetAttribute("ItemType","2");
					else 
						msg_igas->SetAttribute("ItemType","1");
					itoa(atoi(ItemUpd[i].Tax_id),b,10);
					msg_igas->SetAttribute("VATNr",b);
					msg_igas->SetAttribute("ItemSpec",ItemUpd[i].VTSZ);
					msg_igas->SetAttribute("ProviderID","0");
					msg_igas->SetAttribute("Description1","0");
					msg_igas->SetAttribute("Description2","0");
					msg_igas->SetAttribute("DiscountLock","0");
					msg_igas->SetAttribute("PriceOverLock","1");
					msg_igas->SetAttribute("DepartmentNr","0");
					msg_igas->SetAttribute("ProductCode","0");
					msg_igas->SetAttribute("EKWNr","0");
					msg_igas->SetAttribute("ItemGroupNr",ItemUpd[i].Dept_Id);
					msg_igas->SetAttribute("LoyaltyGroupNr","0");
					msg_igas->SetAttribute("TaxGroupNr","0");
					if(strlen(ItemUpd[i].Linked_ean)>1){
						msg_igas->SetAttribute("LinkItemType","3");
						msg_igas->SetAttribute("LinkItemNr1",Chk4AuchanCode(ItemUpd[i].Linked_ean));
						msg_igas->SetAttribute("LinkItemQuant1","1");
					}
					else {
						msg_igas->SetAttribute("LinkItemType","0");
						msg_igas->SetAttribute("LinkItemNr1","0");
						msg_igas->SetAttribute("LinkItemQuant1","0");
					}
					msg_igas->SetAttribute("LinkItemNr2","0");
					msg_igas->SetAttribute("LinkItemQuant2","0");
					msg_igas->SetAttribute("PurchPrice","0");
					msg_igas->SetAttribute("LoyaltyMult","0");
					msg_igas->SetAttribute("ContentFactor","0");
					msg_igas->SetAttribute("CompanyItem","0");
					msg_igas->SetAttribute("InvoiceFlag","0");
					Items_igas->LinkEndChild( msg_igas );
				}
			root_igas->LinkEndChild( Items_igas );
		//	igas_upd_counter=0;
			//DeleteFile(a);
		}


				}
				
				if (strncmp(buf+37,rdept,4)==0) {//in Tomra department
					memcpy(rcr+1,buf+21,13);//item_code
					memcpy(rcr+16,buf+71,10);//price (ending 10 only)
					buf[34]=0;
					buf[71]=0;
					lp=rl.n;
					if ((rl.n=new TSList)==NULL) {
						RL.LogE("ERROR: Chk4Upd: Item list cannot be extended, one item (%s) was lost!\n",buf+21);
						rl.n=lp;
					}
					else {
						if ((rl.n->p=new char[32])==NULL) {
							RL.LogE("ERROR: Chk4Upd: Item data cannot be stored, one item (%s) was lost!\n",buf+21);
							delete rl.n;
							rl.n=lp;
						}
						else {
							rl.n->n=lp;
							rl.n->id=2;
							memcpy(rl.n->p,rcr,32);
							rvmitems++;
						}
					}
				}
/*
0  0       1        22            33 3   4                             7         8         99 9            0      1
0  3       1        01            45 7   1                             1         1         12 4            7      4
99920030413000452534154490000667561013242FANTA VADM�LNA RPET 2,0 L     00000209000000020900202000000000008300001102202100000   
99920030413000999083100000000008331009999COCA REF 2 L                  0000006000000000600020200000000000000000110            

99920030413000481019159982845102381013123TOKAJI H�RSLEVELe 0.75 L      00000259000000025900202000000000006500001102204217900   
99920030413000999065100000000006591009999RAJNAI 0.75                   0000002800000000280020200000000000000000110            

99920030413000480050159951003447011013132�VA VERMOUTH �DES ALMA 0.7    00000359000000035900202000000000007200001102206005101   				
99920030413000999072100000000007271009999ZWACK FRANCIA 0.7             0000004000000000400020200000000000000000110
^-store    ^-BOcode ^-updete      ^-update                             ^-price   ^-usualprice ^-linked_item       ^-sww/BTO
   ^-date            ^-barcode     ^-code_type                                             ^-decimals      ^-flags
                                     ^-department                                                            
									     ^-item_name
*/
				if (RSendLinks) {//Item links will be sent to Tomra
					if (strncmp(buf+94,"0000000000000",13)!=0) {//Got link
						memcpy(lcr+1,buf+21,13);//item_code
						memcpy(lcr+14,buf+94,13);//linked item code
						lp=rl.n;
						if ((rl.n=new TSList)==NULL) {
							RL.LogE("ERROR: Chk4Upd: Item list cannot be extended, one item link (%s) was lost!\n",buf+21);
							rl.n=lp;
						}
						else {
							if ((rl.n->p=new char[32])==NULL) {
								RL.LogE("ERROR: Chk4Upd: Item link cannot be stored, one item link (%s) was lost!\n",buf+21);
								delete rl.n;
								rl.n=lp;
							}
							else {
								rl.n->n=lp;
								rl.n->id=2;
								memcpy(rl.n->p,lcr,32);
								rvmlinks++;
							}
						}
					}
				}
			}
		}
			TiXmlElement * FuelPrice = new TiXmlElement( "FuelPriceChanges" );  
				FuelPrice->SetAttribute("InitialLoad","0");
				i=0;
				while(FuelPriceArray[i].price>0){
					sell_price=FuelPriceArray[i].price;
					if(FuelPriceArray[i].price>FuelPriceArray[i].disc_price && FuelPriceArray[i].disc_price!=0)
						sell_price=FuelPriceArray[i].disc_price;
					msg_igas = new TiXmlElement( "FuelPriceChange" );
					msg_igas->SetAttribute("ItemNr",FuelPriceArray[i].ean);
					msg_igas->SetAttribute("Price",sell_price);
					msg_igas->SetAttribute("StartTime",a);
					FuelPrice->LinkEndChild( msg_igas );
					i++;
				}
			root_igas->LinkEndChild( FuelPrice );
			GetLocalTime(&t);
			r.SetValueDW("Fuel.NextUpdateNr",seq_num);
			sprintf(b,"C:\\DAT_000_%.6d.xml",seq_num);
			docwrite_igas.SaveFile( b );
			L.Log("NAMOS ITEM update file created! %s\n",a);
			if(namos>0 && igas_upd_counter>0){
				sprintf(a,"%s\\DAT_000_%.6d.xml",DNUpdO,seq_num);	
				for(i=0;i<10;i++){
					if(!CopyFile(b,a,false)){
						L.LogE("ERROR -- Cannot move NAMOS update file %s! Error: %d\n",a,GetLastError());
						Sleep(2000);
					}
					else i=10;
				}
			}
			if(igass>0 && igas_upd_counter>0){
				sprintf(a,"%s\\DAT_000_%.6d.xml",IgassUpdO,seq_num);	
				for(i=0;i<10;i++){
					if(!CopyFile(b,a,false)){
						L.LogE("ERROR -- Cannot move IGASS update file %s! Error: %d\n",a,GetLastError());
						Sleep(2000);
					}
					else i=10;
				}
			}

		fclose(F);
		set_fuel_price();
		/*if (FuelFunction && (!GilBarco)) {
			if (fuelitems) {
				if ((D=fopen(dst,"wb"))!=NULL) {
					lp=fl.n;
					while (lp) {
						if (lp->id==1) {
							fputs(lp->p,D);
						}
						lp=lp->n;
					}
					fclose(D);
					L.Log("Info: Namos Chk4Upd: Item update processing successfully generated %d update records.\n",fuelitems);
				}
				else {
					L.LogE("ERROR: Namos Chk4Upd: Fuel item update destination file cannot be created! %s\n",dst);
				}
			}
			else L.Log("Info: Chk4Upd: There was no items for the fuel department (%04d).\n",ItemDept[0]);
		}*/
		if ((RVMFunction)&&(RPosCount)) {
			if (rvmitems) {
				char * rdst;
				if ((rdst=new char[strlen(RDNUpdO)+30])==NULL)
					RL.LogE("ERROR: Chk4Upd: Cannot get memory for Tomra item update destination filename!\n");
				else {
					for (l=0;l<RPosCount;l++) {
						sprintf(rdst,"%s\\%04d%02d%02d%02d%02d%02d.%03d%d",RDNUpdO,t.wYear,t.wMonth,t.wDay
							,t.wHour,t.wMinute,t.wSecond,t.wMilliseconds,l+1);
						if ((D=fopen(rdst,"wb"))==NULL) {
							RL.LogE("ERROR: Chk4Upd: Tomra item update #%d destination file cannot be created! %s\n"
								,l+1,rdst);
						}
						else {
							lp=rl.n;
							while (lp) {
								if (lp->id==2) {
									fputs(lp->p,D);
								}
								lp=lp->n;
							}
							fclose(D);
							if (RSendLinks)	RL.Log("Info: Chk4Upd: Item update processing successfully generated %d update records.\n",rvmitems);
							else RL.Log("Info: Chk4Upd: Item update processing successfully generated %d update & %d link records.\n",rvmitems,rvmlinks);
						}
					}
					delete [] rdst;
				}
			}
			else RL.Log("Info: Chk4Upd: There was no items for the bottle department (%04d).\n",RItemDept[0]);
		}
		remove(FNItem);
		while (fl.n) {
			lp=fl.n;
			fl.n=lp->n;
			delete [] lp->p;
			delete lp;
		}
	
	}
	is_disc_price=false;
	delete [] dst;
	return res;

}


bool Chk4Upd(void)
{
	bool res=false;
	SYSTEMTIME t;
	FILE *F;
	GetLocalTime(&t);
	char *dst=NULL;
	char a[250],b[50];
	char ft1 [5];
	TIItems Items[20000];
	int items_counter=0;
	TIUpdate ItemUpd[50];
	TIUpdate ItemUpd_igas[5000];
	TIUpdate FuelUpd[50];
	int iupd_cnt=0,fupd_cnt=0,niupd_cnt=0;
	char StoreLocId[2];
	char Dept[4];
	int igas_upd_counter=0;
	Reg r(false, C_RegKey);
	long seq_num,sell_price=0;
	int i;
	/*if((t.wMinute % 2)==0){
		if (refresh==1 && FuelFunction==1){ //2 percenk�nt megy a friss�t�se a vev�knekw
			GetCSCFromDB();
			refresh=0;
		}
	}
	else
		refresh=1;
	*/
	if((t.wMinute % 10)==0){
		if(Insert_live_stamp()!=0) L.LogE("ERROR -- Write stamp to database is unsuccesful\n");
	}



	MoveFile(FNCSCI,FNCSCW);
	if ((F=fopen(FNCSCW,"rt"))!=NULL) {//success on csc workfile opening
		res=true;
		FILE *D;
		char in[500];
		L.Log("Info: Chk4Upd: Found a new customer update file to process...\n");
		if(!GilBarco && !FLEXYS && !namos){
			dst=new char[strlen(DNUpdO)+30];
			sprintf(dst,"%s\\%04d%02d%02d.%02d%02d%02d.%03d",DNUpdO,t.wYear,t.wMonth,t.wDay
				,t.wHour,t.wMinute,t.wSecond,t.wMilliseconds);

			if ((D=fopen(dst,"wb"))==NULL) {
				L.LogE("Error: Chk4Upd.Customers: New customer file cannot be opened. File/path: %s\n"
					,dst);
				fclose(F);
			}
			else {
				//line=0;
				long chg=0;
				while (fgets(in,499,F)!=NULL) {
					while ((*in)&&(in[strlen(in)-1]<32)) in[strlen(in)-1]=0;
					if ((*in=='C')&&(in[1]=='0')) {
						chg++;
						fprintf(D,"%s\r\n",in);
					}
				}
				L.Log("Info: Chk4Upd.Customers: A new customer change file has been put to output directory. (%d changes) File/path: %s\n"
					,chg,dst);
				fclose(F);
				fclose(D);
				DeleteFile(FNCSCW);
			}
		}
		/********************  GILBARCO **********************/
		else{
			if(GilBarco){
				char tmp[100],tmp1[100],csc[81];
				int leng=0,leng1=0;
				char licnr[10];
				fseek(F,0,SEEK_SET);
				/*****  ACIS CSC file   ******/
				TiXmlDocument ACISdocwrite;  
				TiXmlElement* msg;
 				TiXmlDeclaration* ACISdecl = new TiXmlDeclaration( "1.0", "windows-1250", "" );  
				ACISdocwrite.LinkEndChild( ACISdecl ); 
				TiXmlElement * ACISwroot = new TiXmlElement( "GVRXML-MaintenanceRequest" );  
				ACISdocwrite.LinkEndChild( ACISwroot );
				ACISwroot->SetAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
				ACISwroot->SetAttribute("xmlns:xsd","http://www.w3.org/2001/XMLSchema");
				ACISwroot->SetAttribute("version","3.3");
				ACISwroot->SetAttribute("xmlns","http://www.gilbarco.com/POSBO/Vocabulary/2006-04-18");
		/******************* TransmissionHeader   ******************/
				TiXmlElement * ACISTransHead = new TiXmlElement( "TransmissionHeader" );  
				ACISwroot->LinkEndChild( ACISTransHead );
				ACISTransHead->SetAttribute("xmlns","http://www.naxml.org/POSBO/Vocabulary/2003-10-16");
				msg = new TiXmlElement( "StoreLocationID" );  
				msg->LinkEndChild( new TiXmlText( "HUNGARY" ));  
				ACISTransHead->LinkEndChild( msg );  
				msg = new TiXmlElement( "VendorName" );  
				msg->LinkEndChild( new TiXmlText( "HUNGARY" ));  
				ACISTransHead->LinkEndChild( msg ); 
				msg = new TiXmlElement( "VendorModelVersion" );  
				msg->LinkEndChild( new TiXmlText( "1.0" ));  
				ACISTransHead->LinkEndChild( msg ); 
		/******************* CustomerMaintenance   ******************/
				TiXmlElement * ACISCustMaint = new TiXmlElement( "CustomerMaintenance" );  
				ACISwroot->LinkEndChild( ACISCustMaint);
				TiXmlElement * ACISTableAct = new TiXmlElement( "TableAction" );  
				ACISCustMaint->LinkEndChild( ACISTableAct );
				ACISTableAct->SetAttribute("type","initialize");
				
				TiXmlElement * ACISRecAct = new TiXmlElement( "RecordAction" );  
				ACISCustMaint->LinkEndChild( ACISRecAct );
				ACISRecAct->SetAttribute("type","create");
				
				
				while (fgets(a,499,F)!=NULL) {
					ConvChar852ToWin((unsigned char *)a);
					TiXmlElement * ACISCSTDetail = new TiXmlElement( "CSTDetail" );  
					ACISCustMaint->LinkEndChild( ACISCSTDetail );
					msg = new TiXmlElement( "RecordAction" );
					if (*a=='D')
						msg->SetAttribute( "type","delete" );
					else
						msg->SetAttribute( "type","create" );
					ACISCSTDetail->LinkEndChild( msg ); 
					/*memcpy(tmp,a+5,16);
					tmp[16]=0;*/
					//ACIS csak 10 karakteren tudja fogadni, ha kisebb, akkor baj van
					int h=6;
					for(int z=0;z<7;z++) {
						if (a[z+5]!='0') {
							h=z;
							z=12;
						}
					}
					memcpy(tmp,a+5+h,16-h);
					tmp[16-h]=0;
					//leng=remove_lchar(tmp,"0");
					msg = new TiXmlElement( "CustomerID" );  
					msg->LinkEndChild( new TiXmlText( tmp )); 
					ACISCSTDetail->LinkEndChild( msg );
					memcpy(licnr,a+45,6);
					licnr[6]=0;
					memset(csc,' ',48);
					memcpy(tmp,a+51,30);
					tmp[30]=0;
					leng=remove_lchar(tmp," ");
					leng=remove_rchar(tmp," ");
					memcpy(csc,tmp,leng);
					memcpy(tmp1,a+21,23);
					tmp1[23]=0;
					leng1=remove_lchar(tmp1," ");
					leng1=remove_rchar(tmp1," ");
					if ((leng>0) && (strcmp(tmp," ")!=0)) {
						memcpy(csc+leng," ",1);
						memcpy(csc+leng+1,tmp1,leng1);
						if (leng+leng1+1>48)
							csc[48]=0;
						else
							csc[leng+leng1+1]=0;
					}
					else { 
						if (leng==1) leng=0;
						memcpy(csc+leng,tmp1,leng1);
						csc[leng1]=0;
					}
					
					msg = new TiXmlElement( "CustomerName" );  
					msg->LinkEndChild( new TiXmlText( csc )); 
					ACISCSTDetail->LinkEndChild( msg );	

					memcpy(tmp,a+201,15);
					tmp[15]=0;
					leng=remove_rchar(tmp," ");
					if (leng>1) {
						msg = new TiXmlElement( "FiscalCode" );  
						msg->LinkEndChild( new TiXmlText( tmp )); 
						ACISCSTDetail->LinkEndChild( msg );
					}
					memset(csc,' ',24);
					memcpy(tmp,a+121,40);
					tmp[40]=0;
					leng=remove_lchar(tmp," ");
					leng=remove_rchar(tmp," ");
					memcpy(csc,tmp,leng);
					memcpy(tmp1,a+161,40);
					tmp1[40]=0;
					leng1=remove_lchar(tmp1," ");
					leng1=remove_rchar(tmp1," ");
					if ((leng>0) && (strcmp(tmp," ")!=0)) {
						memcpy(csc+leng," ",1);
						memcpy(csc+leng+1,tmp1,leng1);
						if (leng+leng1+1>24)
							csc[24]=0;
						else
							csc[leng+leng1]=0;
					}
					else { 
						if (leng==1) leng=0;
						memcpy(csc+leng,tmp1,leng1);
						csc[leng1]=0;
					}
					msg = new TiXmlElement( "Address" );  
					msg->LinkEndChild( new TiXmlText( csc )); 
					ACISCSTDetail->LinkEndChild( msg );						
					
					memcpy(tmp,a+81,10);
					tmp[6]=0;
					leng=remove_rchar(tmp," ");
					msg = new TiXmlElement( "ZIPCode" );  
					msg->LinkEndChild( new TiXmlText( tmp )); 
					ACISCSTDetail->LinkEndChild( msg );						
					
					memcpy(tmp,a+91,30);
					tmp[20]=0;
					leng=remove_rchar(tmp," ");
					msg = new TiXmlElement( "City" );  
					msg->LinkEndChild( new TiXmlText( tmp )); 
					ACISCSTDetail->LinkEndChild( msg );						
					/*memcpy(tmp,"0     ",6);
					tmp[6]=0;
					if(strcmp(licnr,tmp)==0) 
						licnr[0]=0;
					if (strlen(licnr)>0) {
						TiXmlElement * Vehicle = new TiXmlElement( "Vehicle" );  
						CSTDetail->LinkEndChild( Vehicle);
						leng=remove_rchar(licnr," ");
						msg = new TiXmlElement( "VehicleRegistrationNumber" );  
						msg->LinkEndChild( new TiXmlText( licnr )); 
						Vehicle->LinkEndChild( msg );
					}*/
				}
				

			GetLocalTime(&t);
			sprintf(tmp,"%d",t.wYear);
			
			strcpy(ft1,"null");
			fseek(F,0,SEEK_SET);
//			char s[1];
			TiXmlDocument docwrite;  
			//TiXmlElement* msg;
 			TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "windows-1250", "" );  
			docwrite.LinkEndChild( decl ); 
			TiXmlElement * wroot = new TiXmlElement( "GVRXML-MaintenanceRequest" );  
			docwrite.LinkEndChild( wroot );
			wroot->SetAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
			wroot->SetAttribute("xmlns:xsd","http://www.w3.org/2001/XMLSchema");
			wroot->SetAttribute("version","3.3");
			wroot->SetAttribute("xmlns","http://www.gilbarco.com/POSBO/Vocabulary/2006-04-18");
	/******************* TransmissionHeader   ******************/
			TiXmlElement * TransHead = new TiXmlElement( "TransmissionHeader" );  
			wroot->LinkEndChild( TransHead );
			TransHead->SetAttribute("xmlns","http://www.naxml.org/POSBO/Vocabulary/2003-10-16");
			msg = new TiXmlElement( "StoreLocationID" );  
			msg->LinkEndChild( new TiXmlText( "HUNGARY" ));  
			TransHead->LinkEndChild( msg );  
			msg = new TiXmlElement( "VendorName" );  
			msg->LinkEndChild( new TiXmlText( "HUNGARY" ));  
			TransHead->LinkEndChild( msg ); 
			msg = new TiXmlElement( "VendorModelVersion" );  
			msg->LinkEndChild( new TiXmlText( "1.0" ));  
			TransHead->LinkEndChild( msg ); 
	/******************* CustomerMaintenance   ******************/
			TiXmlElement * CustMaint = new TiXmlElement( "CustomerMaintenance" );  
			wroot->LinkEndChild( CustMaint);
			TiXmlElement * TableAct = new TiXmlElement( "TableAction" );  
			CustMaint->LinkEndChild( TableAct );
			TableAct->SetAttribute("type","initialize");
			
			TiXmlElement * RecAct = new TiXmlElement( "RecordAction" );  
			CustMaint->LinkEndChild( RecAct );
			RecAct->SetAttribute("type","create");
			
			
			while (fgets(a,499,F)!=NULL) {
				ConvChar852ToWin((unsigned char *)a);
				TiXmlElement * CSTDetail = new TiXmlElement( "CSTDetail" );  
				CustMaint->LinkEndChild( CSTDetail );
				msg = new TiXmlElement( "RecordAction" );
				if (*a=='D')
					msg->SetAttribute( "type","delete" );
				else
					msg->SetAttribute( "type","create" );
				CSTDetail->LinkEndChild( msg ); 
				memcpy(tmp,a+5,16);
				tmp[16]=0;
				leng=remove_lchar(tmp,"0");
				
				msg = new TiXmlElement( "CustomerID" );  
				msg->LinkEndChild( new TiXmlText( tmp )); 
				CSTDetail->LinkEndChild( msg );
				memcpy(licnr,a+45,6);
				licnr[6]=0;
				memset(csc,' ',48);
				memcpy(tmp,a+51,30);
				tmp[30]=0;
				leng=remove_lchar(tmp," ");
				leng=remove_rchar(tmp," ");
				memcpy(csc,tmp,leng);
				memcpy(tmp1,a+21,23);
				tmp1[23]=0;
				leng1=remove_lchar(tmp1," ");
				leng1=remove_rchar(tmp1," ");
				if ((leng>0) && (strcmp(tmp," ")!=0)) {
					memcpy(csc+leng," ",1);
					memcpy(csc+leng+1,tmp1,leng1);
					if (leng+leng1+1>48)
						csc[48]=0;
					else
						csc[leng+leng1+1]=0;
				}
				else { 
					if (leng==1) leng=0;
					memcpy(csc+leng,tmp1,leng1);
					csc[leng1]=0;
				}
				
				msg = new TiXmlElement( "CustomerName" );  
				msg->LinkEndChild( new TiXmlText( csc )); 
				CSTDetail->LinkEndChild( msg );	

				memcpy(tmp,a+201,15);
				tmp[15]=0;
				leng=remove_rchar(tmp," ");
				if (leng>1) {
					msg = new TiXmlElement( "FiscalCode" );  
					msg->LinkEndChild( new TiXmlText( tmp )); 
					CSTDetail->LinkEndChild( msg );
				}
				memset(csc,' ',24);
				memcpy(tmp,a+121,40);
				tmp[40]=0;
				leng=remove_lchar(tmp," ");
				leng=remove_rchar(tmp," ");
				memcpy(csc,tmp,leng);
				memcpy(tmp1,a+161,40);
				tmp1[40]=0;
				leng1=remove_lchar(tmp1," ");
				leng1=remove_rchar(tmp1," ");
				if ((leng>0) && (strcmp(tmp," ")!=0)) {
					memcpy(csc+leng," ",1);
					memcpy(csc+leng+1,tmp1,leng1);
					if (leng+leng1+1>24)
						csc[24]=0;
					else
						csc[leng+leng1]=0;
				}
				else { 
					if (leng==1) leng=0;
					memcpy(csc+leng,tmp1,leng1);
					csc[leng1]=0;
				}
				msg = new TiXmlElement( "Address" );  
				msg->LinkEndChild( new TiXmlText( csc )); 
				CSTDetail->LinkEndChild( msg );						
				
				memcpy(tmp,a+81,10);
				tmp[6]=0;
				leng=remove_rchar(tmp," ");
				msg = new TiXmlElement( "ZIPCode" );  
				msg->LinkEndChild( new TiXmlText( tmp )); 
				CSTDetail->LinkEndChild( msg );						
				
				memcpy(tmp,a+91,30);
				tmp[20]=0;
				leng=remove_rchar(tmp," ");
				msg = new TiXmlElement( "City" );  
				msg->LinkEndChild( new TiXmlText( tmp )); 
				CSTDetail->LinkEndChild( msg );						
				memcpy(tmp,"0     ",6);
				tmp[6]=0;
				if(strcmp(licnr,tmp)==0) 
					licnr[0]=0;
				if (strlen(licnr)>0) {
					TiXmlElement * Vehicle = new TiXmlElement( "Vehicle" );  
					CSTDetail->LinkEndChild( Vehicle);
					leng=remove_rchar(licnr," ");
					msg = new TiXmlElement( "VehicleRegistrationNumber" );  
					msg->LinkEndChild( new TiXmlText( licnr )); 
					Vehicle->LinkEndChild( msg );
				}
			}
			GetLocalTime(&t);
			sprintf(a,"%s\\PCATS_PCM_%.4d%.2d%.2d%.2d%.2d.xml",DNUpdO,t.wYear,t.wMonth,t.wDay,t.wHour,t.wMinute);
			docwrite.SaveFile( "c:\\GILBcsc.xml" );
			if (MoveFile("c:\\GILBcsc.xml",a)!=0)
				L.Log("Info: Chk4Upd.Customers: A new GILBARCO customer change file has been put to output directory. File/path: %s\n",a);
			else
				L.LogE("ERROR: Chk4Upd.Customers: Cannot put new GILBARCO customer change file to output directory. File/path: %s  Error code: %d\n",a,GetLastError());
			fclose(F);
			DeleteFile(FNCSCW);

		}
		delete [] dst;
		return true;
		}
	}
	//No csc file, use filename for item update if exists the input file
	if ((F=fopen(FNItem,"rt"))!=NULL) {
		long fuelitems=0;
		long rvmitems=0;
		long rvmlinks=0;
		
		FILE *D;
		TSList fl={0,NULL,NULL};
		TSList rl={0,NULL,NULL};
		TSList *lp=NULL;
		res=true;
		L.Log("Info: Chk4Upd: Found a new item update file to process...\n");
		long l;
		long cnt=0;
		bool warn;
		char buf[200];
		char cr[108];
		char rcr[32];
		char lcr[32];
		char dept[5];
		char rdept[5];
		
		sprintf(dept,"%04d",ItemDept[0]);
		sprintf(rdept,"%04d",RItemDept[0]);
		memset(cr+1,'0',103);
		cr[0]='A'; //record type
		cr[104]=13; //string ending cr
		cr[105]=10; //string ending lf
		cr[106]=0; //string end
		memset(cr+51,' ',6);//item_desc.fill_end
		memset(cr+82,' ',7);//SWW_code.fill_end
		rcr[0]='U'; //record type
		rcr[14]='0';//missing leading zeroes from price
		rcr[15]='0';//...
		rcr[26]=13; //string ending cr
		rcr[27]=10; //string ending lf
		rcr[28]=0; //string end
		lcr[0]='R'; //record type
		lcr[27]=13; //string ending cr
		lcr[28]=10; //string ending lf
		lcr[29]=0; //string end
		int fuel=0,item=0;
		bool found=false;
		int xx=0;
		FILE *IF;
		char seps[]   = "|\n";
		char *token;
		int counter=0;
		char ttt[13];
		strcpy(ttt,"             ");
		if(namos){
			if ((IF=fopen(FNItemList,"rt"))!=NULL) {
				if( fseek( IF,0, SEEK_SET) )	{
					L.LogE("ERROR - Fseek failed %s\n",FNItemList );
				}
				else   {
					while (!feof(IF))
					{
					  fgets( a,250,IF);
					  if(strcmp(a,ttt)!=0){
						token = strtok( a, seps );
						strcpy(ttt,token);
						memcpy(Items[items_counter].EAN,token,13);
						counter = 0;
						while( token != NULL )	{
							counter++;
							token = strtok( NULL, seps );
							switch(counter) {
								case 1: memcpy(Items[items_counter].Unit,token,4); break;
								case 2: Items[items_counter].isFuel = atoi(token); break;
							}
							if(Items[items_counter].isFuel==1) L.Log("Fuel:%s\n",Items[items_counter].EAN);
						}
						items_counter++;
					  }
					}
				}
				fclose(IF);
			}
		}
		char ean_tmp[14];
		char price[10];
		SYSTEMTIME t;
		GetLocalTime(&t);
		sprintf(ttt,"%.4d%.2d%.2d%.2d%.2d",t.wYear,t.wMonth,t.wDay,t.wHour,t.wMinute);
		wchar_t *wText;
		char *ansiText;
		bool fuel_found=false;
		
		while (fgets(buf,199,F)) {//loop to read logali records
			l=strlen(buf);
			while (buf[l-1]<' ') buf[--l]=0;
			warn=(l<115);
			while (l<127) {buf[l]=' '; buf[++l]=0;}
			if ((l>=127)&&((buf[20]=='1')||(buf[20]=='2')) ) {//good update record
				if(GilBarco || FLEXYS){
					item=ItemOnList(buf+21);
					fuel=FuelOnList(buf+21);
				}
				if(namos || igass ){
					item=false;
					xx=0;
					while ( xx<items_counter){
						strncpy(ean_tmp,buf+21,13);
						ean_tmp[13]=0;
						Items[xx].EAN[13]=0;
						remove_lchar(ean_tmp,"0");
						if(strcmp(Items[xx].EAN,ean_tmp)==0){
							igas_upd_counter++;
							ItemUpd[iupd_cnt].isFuel=Items[xx].isFuel;
							memcpy(ItemUpd[iupd_cnt].Unit,Items[xx].Unit,4);
							if(Items[xx].isFuel==1){
								xx=0;
								//iupd_cnt++;
								fuel_found=false;
								memcpy(price,buf+71,10);
								price[8]=0;
								while(FuelPriceArray[xx].price>0){//Be�rom az �j �rat a registrybe
									if(strcmp(FuelPriceArray[xx].ean,ean_tmp)==0){
										memcpy(FuelPriceArray[xx].date,ttt,12);
										FuelPriceArray[xx].price=atoi(price);
										//FuelPriceArray[xx].disc_price=atoi(disc_price);
										fuel_found=true;
									}
									xx++;
								}
								if(!fuel_found){ //Ha nincs ez a term�k a registry-ben
//									if(xx==1)  //Ha egy term�k sincs m�g a REGISTRY-ben
//										xx=0;
									sprintf(FuelPriceArray[xx].ean,"%s",ean_tmp);
									
									sprintf(FuelPriceArray[xx].date,"%s",ttt);
									FuelPriceArray[xx].price=atoi(price);
									//FuelPriceArray[xx].disc_price=atoi(disc_price);
									FuelPriceArray[xx].date[12]=0;
									//if(xx>0) FuelPriceArray[xx-1].date[12]=0;
									FuelPriceArray[xx].ean[13]=0;
								}
							}
							item=true;
							xx=items_counter+1;
						}
						xx++;
					} 
				}
				if (item || fuel ) {//in fuel department
					strncpy(ean_tmp,buf+21,13);
						DoItemSubst(buf+21,true);
					memcpy(cr+8,buf+21,13);//item_code
					
					memcpy(cr+21,buf+41,30);//item_desc
					cr[66]='1';//item_dept, always 0000000001
					memcpy(cr+67,buf+92,2);//VAT rate_id
					long idx=10*(cr[67]-'0')+(cr[68]-'0');
					cr[67]='0';
					if ((idx<0)||(((unsigned long)idx)>=strlen(VatConvTable))) {//under or overindexing
						cr[68]='2';
					}
					else {
						cr[68]=VatConvTable[idx];
						if ((cr[68]<'0')||(cr[68]>'4')) cr[68]='2';
					}
					cr[68]++; //Inc VAT rate ID to conform fuel station interface req.
					memcpy(cr+69,buf+114,13);//SWW_code
					if (PriceByCentiLiters==0) {//keep price coming on the iss interface
						memcpy(cr+94,buf+71,10);//price (ending 10 only)
					}
					else {//price by centiliters -> multiply price by 100
						memcpy(cr+94,buf+73,8);//price (ending 10 only)
						cr[102]='0';
						cr[103]='0';
					}
					
					if (item) {
						memcpy(ItemUpd[iupd_cnt].Auchan_code,buf+14,6);
						ItemUpd[iupd_cnt].Auchan_code[6]=0;
						memcpy(ItemUpd[iupd_cnt].EAN,buf+21,13);
						ItemUpd[iupd_cnt].EAN[13]=0;
						memcpy(ItemUpd_igas[iupd_cnt].EAN,ean_tmp,13);
						ItemUpd_igas[iupd_cnt].EAN[13]=0;
						memcpy(ItemUpd[iupd_cnt].Description,buf+41,30);
						ItemUpd[iupd_cnt].Description[30]=0;
						wText = CodePageToUnicode(852,ItemUpd[iupd_cnt].Description);
						ansiText = UnicodeToCodePage(65001,wText);	
						strncpy(ItemUpd[iupd_cnt].Description,ansiText,30);
				
						//ConvChar852ToWin((unsigned char *)ItemUpd[iupd_cnt].Description);
						remove_rchar(ItemUpd[iupd_cnt].Description," ");
						ItemUpd[iupd_cnt].Description[30]=0;
						//cr[68]--;
						ItemUpd[iupd_cnt].Tax_id[0]=cr[68];
						ItemUpd[iupd_cnt].Tax_id[1]=0;
						if (PriceByCentiLiters==0) {//keep price coming on the iss interface
							memcpy(ItemUpd[iupd_cnt].Price,buf+71,10);//price (ending 10 only)
						}
						else {//price by centiliters -> multiply price by 100
							memcpy(ItemUpd[iupd_cnt].Price,buf+73,8);//price (ending 10 only)
						}
						ItemUpd[iupd_cnt].Price[8]=0;
						remove_lchar(ItemUpd[iupd_cnt].Price,"0");
						memcpy(ItemUpd[iupd_cnt].Dept_Id,buf+37,4);
						ItemUpd[iupd_cnt].Dept_Id[4]=0;
						memcpy(ItemUpd[iupd_cnt].Linked_ean,buf+94,13);
						ItemUpd[iupd_cnt].Linked_ean[13]=0;
						remove_lchar(ItemUpd[iupd_cnt].Linked_ean,"0");
						sprintf(ItemUpd[iupd_cnt].VTSZ,buf+113);
						RemoveLetter(ItemUpd[iupd_cnt].VTSZ);
						iupd_cnt++;
					}
					if (fuel) {
						memcpy(FuelUpd[fupd_cnt].EAN,buf+21,13);
						FuelUpd[fupd_cnt].EAN[13]=0;
						memcpy(FuelUpd[fupd_cnt].Description,buf+41,30);
						FuelUpd[fupd_cnt].Description[30]=0;
						ConvChar852ToWin((unsigned char *)FuelUpd[fupd_cnt].Description);
						remove_rchar(FuelUpd[fupd_cnt].Description," ");
						//cr[68]++;
						FuelUpd[fupd_cnt].Tax_id[0]=cr[68];
						FuelUpd[fupd_cnt].Tax_id[1]=0;
						if (PriceByCentiLiters==0) {//keep price coming on the iss interface
							memcpy(FuelUpd[fupd_cnt].Price,buf+71,8);//price (ending 10 only)
							FuelUpd[fupd_cnt].Price[8]='.';
							memcpy(FuelUpd[fupd_cnt].Price+9,buf+79,2);//price (ending 10 only)
							FuelUpd[fupd_cnt].Price[11]=0;
						}
						else {//price by centiliters -> multiply price by 100
							memcpy(FuelUpd[fupd_cnt].Price,buf+73,8);//price (ending 10 only)
							FuelUpd[fupd_cnt].Price[8]=0;
						}
						remove_lchar(FuelUpd[fupd_cnt].Price,"0");
						
						memcpy(FuelUpd[fupd_cnt].Dept_Id,buf+37,4);
						FuelUpd[fupd_cnt].Dept_Id[4]=0;
						sprintf(FuelUpd[fupd_cnt].VTSZ,buf+110);
						RemoveLetter(FuelUpd[fupd_cnt].VTSZ);
						fupd_cnt++;
					}
					buf[34]=0;
					buf[71]=0;
					for (l=70;(l>41)&&(buf[l]==' ');l--) buf[l]=0;
					lp=fl.n;
					if ((fl.n=new TSList)==NULL) {
						L.LogE("ERROR: Chk4Upd: Item list cannot be extended, one item (%s) was lost!\n",buf+21);
						fl.n=lp;
					}
					else {
						if ((fl.n->p=new char[108])==NULL) {
							L.LogE("ERROR: Chk4Upd: Item data cannot be stored, one item (%s) was lost!\n",buf+21);
							delete fl.n;
							fl.n=lp;
						}
						else {
							fl.n->n=lp;
							fl.n->id=1;
							memcpy(fl.n->p,cr,108);
							fuelitems++;
						}
					}
					if (warn)
						L.LogE("Warning: Chk4Upd: SWW code is empty for %s! (%s)\n",buf+21,buf+41);
					else 
						L.Log("Update EAN:%s\n",ItemUpd[iupd_cnt-1].EAN);
				}
				
				if (strncmp(buf+37,rdept,4)==0) {//in Tomra department
					memcpy(rcr+1,buf+21,13);//item_code
					memcpy(rcr+16,buf+71,10);//price (ending 10 only)
					buf[34]=0;
					buf[71]=0;
					lp=rl.n;
					if ((rl.n=new TSList)==NULL) {
						RL.LogE("ERROR: Chk4Upd: Item list cannot be extended, one item (%s) was lost!\n",buf+21);
						rl.n=lp;
					}
					else {
						if ((rl.n->p=new char[32])==NULL) {
							RL.LogE("ERROR: Chk4Upd: Item data cannot be stored, one item (%s) was lost!\n",buf+21);
							delete rl.n;
							rl.n=lp;
						}
						else {
							rl.n->n=lp;
							rl.n->id=2;
							memcpy(rl.n->p,rcr,32);
							rvmitems++;
						}
					}
				}
/*
0  0       1        22            33 3   4                             7         8         99 9            0      1
0  3       1        01            45 7   1                             1         1         12 4            7      4
99920030413000452534154490000667561013242FANTA VADM�LNA RPET 2,0 L     00000209000000020900202000000000008300001102202100000   
99920030413000999083100000000008331009999COCA REF 2 L                  0000006000000000600020200000000000000000110            

99920030413000481019159982845102381013123TOKAJI H�RSLEVELe 0.75 L      00000259000000025900202000000000006500001102204217900   
99920030413000999065100000000006591009999RAJNAI 0.75                   0000002800000000280020200000000000000000110            

99920030413000480050159951003447011013132�VA VERMOUTH �DES ALMA 0.7    00000359000000035900202000000000007200001102206005101   				
99920030413000999072100000000007271009999ZWACK FRANCIA 0.7             0000004000000000400020200000000000000000110
^-store    ^-BOcode ^-updete      ^-update                             ^-price   ^-usualprice ^-linked_item       ^-sww/BTO
   ^-date            ^-barcode     ^-code_type                                             ^-decimals      ^-flags
                                     ^-department                                                            
									     ^-item_name
*/
				if (RSendLinks) {//Item links will be sent to Tomra
					if (strncmp(buf+94,"0000000000000",13)!=0) {//Got link
						memcpy(lcr+1,buf+21,13);//item_code
						memcpy(lcr+14,buf+94,13);//linked item code
						lp=rl.n;
						if ((rl.n=new TSList)==NULL) {
							RL.LogE("ERROR: Chk4Upd: Item list cannot be extended, one item link (%s) was lost!\n",buf+21);
							rl.n=lp;
						}
						else {
							if ((rl.n->p=new char[32])==NULL) {
								RL.LogE("ERROR: Chk4Upd: Item link cannot be stored, one item link (%s) was lost!\n",buf+21);
								delete rl.n;
								rl.n=lp;
							}
							else {
								rl.n->n=lp;
								rl.n->id=2;
								memcpy(rl.n->p,lcr,32);
								rvmlinks++;
							}
						}
					}
				}
			}
		}
		fclose(F);
		set_fuel_price();
		if (FuelFunction && (!GilBarco)) {
			if (fuelitems) {
				if ((D=fopen(dst,"wb"))!=NULL) {
					lp=fl.n;
					while (lp) {
						if (lp->id==1) {
							fputs(lp->p,D);
						}
						lp=lp->n;
					}
					fclose(D);
					L.Log("Info: Chk4Upd: Item update processing successfully generated %d update records.\n",fuelitems);
				}
				else {
					L.LogE("ERROR: Chk4Upd: Fuel item update destination file cannot be created! %s\n",dst);
				}
			}
			else L.Log("Info: Chk4Upd: There was no items for the fuel department (%04d).\n",ItemDept[0]);
		}
		if ((RVMFunction)&&(RPosCount)) {
			if (rvmitems) {
				char * rdst;
				if ((rdst=new char[strlen(RDNUpdO)+30])==NULL)
					RL.LogE("ERROR: Chk4Upd: Cannot get memory for Tomra item update destination filename!\n");
				else {
					for (l=0;l<RPosCount;l++) {
						sprintf(rdst,"%s\\%04d%02d%02d%02d%02d%02d.%03d%d",RDNUpdO,t.wYear,t.wMonth,t.wDay
							,t.wHour,t.wMinute,t.wSecond,t.wMilliseconds,l+1);
						if ((D=fopen(rdst,"wb"))==NULL) {
							RL.LogE("ERROR: Chk4Upd: Tomra item update #%d destination file cannot be created! %s\n"
								,l+1,rdst);
						}
						else {
							lp=rl.n;
							while (lp) {
								if (lp->id==2) {
									fputs(lp->p,D);
								}
								lp=lp->n;
							}
							fclose(D);
							if (RSendLinks)	RL.Log("Info: Chk4Upd: Item update processing successfully generated %d update records.\n",rvmitems);
							else RL.Log("Info: Chk4Upd: Item update processing successfully generated %d update & %d link records.\n",rvmitems,rvmlinks);
						}
					}
					delete [] rdst;
				}
			}
			else RL.Log("Info: Chk4Upd: There was no items for the bottle department (%04d).\n",RItemDept[0]);
		}
		remove(FNItem);
		while (fl.n) {
			lp=fl.n;
			fl.n=lp->n;
			delete [] lp->p;
			delete lp;
		}
	
	
    
//	char atoken[200];
//	char *token;
	if ((FuelFunction) && (FLEXYS)) {
		TiXmlDocument docwrite;  
		TiXmlElement* msg;
 		TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "UTF-8", "" );  
		docwrite.LinkEndChild( decl ); 
		if(fupd_cnt>0) {
			/**********  FLEXYS ITEM UPDATE  ********************/
			TiXmlElement * root = new TiXmlElement( "arvaltozas" );
			docwrite.LinkEndChild( root );
			GetLocalTime(&t);
			sprintf(a,"%.4d-%.2d-%.2d",t.wYear,t.wMonth,t.wDay);
			msg = new TiXmlElement( "datum" );  
			msg->LinkEndChild( new TiXmlText( a));  
			root->LinkEndChild( msg );

			sprintf(a,"%.2d:%.2d:00",t.wHour,t.wMinute);
			msg = new TiXmlElement( "idopont" );  
			msg->LinkEndChild( new TiXmlText( a));  
			root->LinkEndChild( msg );
			
			TiXmlElement * tetelek = new TiXmlElement( "tetelek" );
			root->LinkEndChild( tetelek );
			
			for(i=0;i<fupd_cnt;i++) {
				TiXmlElement * tetel = new TiXmlElement( "tetel" );
				sprintf(a,"%d",(i+1));
				tetel->SetAttribute( "id",a );
				tetelek->LinkEndChild( tetel );
				msg = new TiXmlElement( "cikkszam" );
				msg->LinkEndChild( new TiXmlText( FuelUpd[i].EAN ));  
				tetel->LinkEndChild( msg );

				msg = new TiXmlElement( "megnevezes" );
				msg->LinkEndChild( new TiXmlText( FuelUpd[i].Description  ));  
				tetel->LinkEndChild( msg );

				msg = new TiXmlElement( "bruttoar" );
				msg->LinkEndChild( new TiXmlText( FuelUpd[i].Price));  
				tetel->LinkEndChild( msg );

			}
			GetLocalTime(&t);
			sprintf(a,"%s\\arvalt-%.4d%.2d%.2d-%.2d%.2d.xml",FlexDnOut,t.wYear,t.wMonth,t.wDay,t.wHour,t.wMinute);
			docwrite.SaveFile( a ); 
			L.Log("FLEXYS ITEM update file created! %s\n",a);
		}
	}
	
	if ((FuelFunction) && (GilBarco!=0 )) {
			TiXmlDocument docwrite;  
			TiXmlElement* msg;
 			TiXmlDeclaration* decl = new TiXmlDeclaration( "1.0", "Windows-1250", "" );  
			docwrite.LinkEndChild( decl ); 
			L.Log("1\n");
		if(iupd_cnt>0) {
L.Log("2\n");			
			/**********  GILBARCO ITEM UPDATE  ********************/
			TiXmlElement * root = new TiXmlElement( "NAXML-MaintenanceRequest" );
				root->SetAttribute("xmlns","http://www.naxml.org/POSBO/Vocabulary/2003-10-16");
				root->SetAttribute("xmlns:gvr","http://www.gilbarco.com/POSBO/Vocabulary/2006-04-18");
				root->SetAttribute("xmlns:ixr","http://www.nrf-arts.org/IXRetail/namespace");
				root->SetAttribute("xmlns:psd","http://www.pcats.org/schema/schemadoc");
				root->SetAttribute("xmlns:html","http://www.w3.org/1999/xhtml");
				root->SetAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
				root->SetAttribute("version","3.3.4");
			docwrite.LinkEndChild( root );
L.Log("3\n");
			TiXmlElement* TransHead = new TiXmlElement( "TransmissionHeader" );  
				msg = new TiXmlElement( "StoreLocationID" ); 
				sprintf(StoreLocId,"%.3d",StoreNum);
				L.Log("2 - %s\n",StoreLocId);
				msg->LinkEndChild( new TiXmlText( StoreLocId )); 
				TransHead->LinkEndChild( msg );
				L.Log("3\n");
				msg = new TiXmlElement( "VendorName" );  
				msg->LinkEndChild( new TiXmlText( "AUCHAN" )); 
				L.Log("4\n");
				TransHead->LinkEndChild( msg );
				L.Log("5\n");
				msg = new TiXmlElement( "VendorModelVersion" );  
				msg->LinkEndChild( new TiXmlText( "REL1.0" )); 
				L.Log("6\n");
				TransHead->LinkEndChild( msg );
				L.Log("7\n");
			root->LinkEndChild( TransHead );
L.Log("4b\n");
			TiXmlElement * iwroot = new TiXmlElement( "ItemMaintenance" );  
			root->LinkEndChild( iwroot );
			msg = new TiXmlElement( "TableAction" );  
			msg->SetAttribute("type","update");
			iwroot->LinkEndChild( msg );  
			msg = new TiXmlElement( "RecordAction" );  
			msg->SetAttribute("type","addchange");
			iwroot->LinkEndChild( msg );  
			for(i=0;i<iupd_cnt;i++) {
				TiXmlElement * Detail = new TiXmlElement( "ITTDetail" );  
				iwroot->LinkEndChild( Detail );
				msg = new TiXmlElement( "RecordAction" );  
				msg->SetAttribute( "type","addchange" );  
				Detail->LinkEndChild( msg );
				TiXmlElement * ItemCode = new TiXmlElement( "ItemCode" );  
				Detail->LinkEndChild( ItemCode );
				msg = new TiXmlElement( "POSCodeFormat" );  
				msg->SetAttribute( "format","ean13" );  
				msg->SetAttribute( "checkDigit","absent" );  
				ItemCode->LinkEndChild( msg );
				msg = new TiXmlElement( "POSCode" );  
				msg->LinkEndChild( new TiXmlText( ItemUpd[i].EAN ));  
				ItemCode->LinkEndChild( msg );
				msg = new TiXmlElement( "POSCodeModifier" );  
				msg->LinkEndChild( new TiXmlText( "0" ));  
				ItemCode->LinkEndChild( msg );
				msg = new TiXmlElement( "InventoryItemID" );  
				ItemCode->LinkEndChild( msg );
				
				TiXmlElement * ITTData = new TiXmlElement( "ITTData" );  
				Detail->LinkEndChild( ITTData );
				msg = new TiXmlElement( "ActiveFlag" );  
				msg->SetAttribute( "value","yes" );  
				ITTData->LinkEndChild( msg );
				msg = new TiXmlElement( "InventoryValuePrice" );  
				msg->SetAttribute( "currency","HUF" );  
				msg->LinkEndChild( new TiXmlText( ItemUpd[i].Price ));
				ITTData->LinkEndChild( msg );
				msg = new TiXmlElement( "MerchandiseCode" );  
/******  OLASZOK miatt 3 karakteren kell �tadni a department �rt�k�t  ********************/
				sprintf(Dept,"%s",ItemUpd[i].Dept_Id);
				if((ItemUpd[i].Dept_Id[0]=='0') && (ItemUpd[i].Dept_Id[1]=='0'))
					sprintf(Dept,"%.3d",atoi(ItemUpd[i].Dept_Id));
				msg->LinkEndChild( new TiXmlText( Dept ));
				ITTData->LinkEndChild( msg );
				msg = new TiXmlElement( "RegularSellPrice" );  
				msg->LinkEndChild( new TiXmlText( ItemUpd[i].Price ));
				ITTData->LinkEndChild( msg );
				msg = new TiXmlElement( "Description" );  
				msg->LinkEndChild( new TiXmlText( ItemUpd[i].Description ));
				ITTData->LinkEndChild( msg );
				msg = new TiXmlElement( "SellingUnits" );  
				msg->SetAttribute( "uom","each" );
				msg->LinkEndChild( new TiXmlText( "1" ));
				ITTData->LinkEndChild( msg );
				msg = new TiXmlElement( "TaxStrategyID" );
				msg->LinkEndChild( new TiXmlText( ItemUpd[i].Tax_id ));
				ITTData->LinkEndChild( msg );
				msg = new TiXmlElement( "gvr:ReceiptDescription" );  
				msg->LinkEndChild( new TiXmlText( ItemUpd[i].Description ));
				ITTData->LinkEndChild( msg );
				msg = new TiXmlElement( "gvr:DiscountableFlg" );  
				msg->SetAttribute( "value","no" );
				ITTData->LinkEndChild( msg );
				msg = new TiXmlElement( "gvr:QuantityAllowedFlg" );  
				msg->SetAttribute( "value","yes" );
				ITTData->LinkEndChild( msg );
				msg = new TiXmlElement( "gvr:QuantityRequiredFlg" );  
				msg->SetAttribute( "value","no" );
				ITTData->LinkEndChild( msg );
			}
			L.Log("3\n");
			GetLocalTime(&t);
			seq_num=r.ValueDW("Fuel.NextUpdateNr",0);
			sprintf(a,"%s\\PXML_IM_%d_%d_%.4d%.2d%.2d_%.2d%.2d.xml",DNUpdO,GilBarco,seq_num,t.wYear,t.wMonth,t.wDay,t.wHour,t.wMinute);
			docwrite.SaveFile( a ); 
			L.Log("GILBARCO ITEM update file created! %s\n",a);
		}
		if (fupd_cnt>0) {
		L.Log("5\n");
			TiXmlDocument Gilbwrite;
			TiXmlDeclaration* fuel = new TiXmlDeclaration( "1.0", "windows-1250", "" );  
			Gilbwrite.LinkEndChild( fuel ); 
			/**********  GILBARCO FUEL UPDATE  ********************/
			TiXmlElement * froot = new TiXmlElement( "NAXML-MaintenanceRequest" );
				froot->SetAttribute("xmlns","http://www.naxml.org/POSBO/Vocabulary/2003-10-16");
				froot->SetAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
				froot->SetAttribute("xsi:schemaLocation","http://www.naxml.org/POSBO/Vocabulary/2003-10-16 X:\\Comps\\ImportExport_Net\\GVR.ImportExport\\GVR.ImportExport.Logic\\xsd\\pcats\\naxml-pbimaintenance34.xsd");
				froot->SetAttribute("release","3.3.4");
				froot->SetAttribute("version","Text");
			Gilbwrite.LinkEndChild( froot );
L.Log("6\n");
			TiXmlElement * fTransHead = new TiXmlElement( "TransmissionHeader" );  
				msg = new TiXmlElement( "StoreLocationID" );
				sprintf(StoreLocId,"%.2d",StoreNum);
				msg->LinkEndChild( new TiXmlText( StoreLocId ));
				fTransHead->LinkEndChild( msg );
				msg = new TiXmlElement( "VendorName" );  
				msg->LinkEndChild( new TiXmlText( "AUCHAN" )); 
				fTransHead->LinkEndChild( msg );
				msg = new TiXmlElement( "VendorModelVersion" );  
				msg->LinkEndChild( new TiXmlText( "1.0" )); 
				fTransHead->LinkEndChild( msg );
			froot->LinkEndChild( fTransHead );

			TiXmlElement * fwroot = new TiXmlElement( "FuelGradeMaintenance" );  
			froot->LinkEndChild( fwroot );
			msg = new TiXmlElement( "TableAction" );  
			msg->SetAttribute("type","update");
			fwroot->LinkEndChild( msg );  
			msg = new TiXmlElement( "RecordAction" );  
			msg->SetAttribute("type","addchange");
			fwroot->LinkEndChild( msg );  
			for(i=0;i<fupd_cnt;i++) {
				TiXmlElement * Detail = new TiXmlElement( "FGTDetail" );  
				fwroot->LinkEndChild( Detail );
				msg = new TiXmlElement( "FuelGradeID" );  
				msg->LinkEndChild( new TiXmlText( FuelUpd[i].EAN ));  
				Detail->LinkEndChild( msg );
				msg = new TiXmlElement( "DescriptionID" );  
				msg->LinkEndChild( new TiXmlText(FuelUpd[i].Description ));  
				Detail->LinkEndChild( msg );
				msg = new TiXmlElement( "FuelProductID" );  
				msg->LinkEndChild( new TiXmlText(FuelUpd[i].EAN ));  
				Detail->LinkEndChild( msg );
				msg = new TiXmlElement( "MerchandiseCode" );
				/******  OLASZOK miatt 3 karakteren kell �tadni a department �rt�k�t  ********************/
				sprintf(Dept,"%s",FuelUpd[i].Dept_Id);
				if((FuelUpd[i].Dept_Id[0]=='0') && (FuelUpd[i].Dept_Id[1]=='0'))
					sprintf(Dept,"%.3d",atoi(FuelUpd[i].Dept_Id));
				msg->LinkEndChild( new TiXmlText( Dept ));
				Detail->LinkEndChild( msg );
				msg = new TiXmlElement( "FuelGradeActiveFlag" );
				msg->SetAttribute("value","yes");
				Detail->LinkEndChild( msg );
				
				TiXmlElement * ITTData = new TiXmlElement( "FGTData" );  
				Detail->LinkEndChild( ITTData );
				msg = new TiXmlElement( "PriceTierCode" );  
				msg->LinkEndChild( new TiXmlText( "1" )); 
				ITTData->LinkEndChild( msg );
				msg = new TiXmlElement( "ServiceLevelCode" );  
				msg->LinkEndChild( new TiXmlText( "full" ));
				ITTData->LinkEndChild( msg );
				msg = new TiXmlElement( "TimeTierCode" );  
				msg->LinkEndChild( new TiXmlText( "1" ));
				ITTData->LinkEndChild( msg );
				msg = new TiXmlElement( "FuelProductBlendPercent" );  
				msg->LinkEndChild( new TiXmlText( "100" ));
				ITTData->LinkEndChild( msg );
				msg = new TiXmlElement( "RegularSellPrice" );  
				msg->LinkEndChild( new TiXmlText( FuelUpd[i].Price ));
				ITTData->LinkEndChild( msg );
			}
			GetLocalTime(&t);
			sprintf(a,"%s\\PXML_SP_%d_%d_%.4d%.2d%.2d_%.2d%.2d.xml",DNUpdO,GilBarco,seq_num,t.wYear,t.wMonth,t.wDay,t.wHour,t.wMinute);
			Gilbwrite.SaveFile( a );  		
			L.Log("GILBARCO FUEL update file created! %s\n",a);

			/**********  ACIS FUEL UPDATE  ********************/
			L.Log("Create acis fuel update file! Counter %d\n",fupd_cnt);
			//if (db>0) {
				TiXmlDocument ACISwrite;
				TiXmlDeclaration* acis = new TiXmlDeclaration( "1.0", "Windows-1252", "" );  
				ACISwrite.LinkEndChild( acis ); 
				TiXmlElement * aroot = new TiXmlElement( "ItemMaintenance" );
					aroot->SetAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
					aroot->SetAttribute("xmlns:xsd","http://www.w3.org/2001/XMLSchema");
					aroot->SetAttribute("xmlns","http://www.nrf-arts.org/IXRetail/namespace/");
				ACISwrite.LinkEndChild( aroot );

				TiXmlElement * awroot = new TiXmlElement( "Batch" );  
				aroot->LinkEndChild( awroot );
				msg = new TiXmlElement( "BatchID" );  
				msg->LinkEndChild( new TiXmlText( "1" ));
				awroot->LinkEndChild( msg );  
				msg = new TiXmlElement( "Description" );  
				msg->LinkEndChild( new TiXmlText( "2.1" ));
				awroot->LinkEndChild( msg );  
				for(i=0;i<fupd_cnt;i++) {
					counter=0;	
					L.Log("ACIS fuel update row %d\n",i+1);
/*					for (int x=1;x<=db;x++) {
						if(_stricmp( FuelUpd[i].EAN, AStruct[x].ean )==0) 
							counter=x;
					}
*/
					if(!ACIS_BBOX){
						TiXmlElement * Detail = new TiXmlElement( "Item" );
						
						Detail->SetAttribute("AuthorizedForSaleFlag","true");
						Detail->SetAttribute("ItemCategory","gvr:Oil");
						Detail->SetAttribute("ItemSubCategory","Gvr:HGift");
						awroot->LinkEndChild( Detail );
						msg = new TiXmlElement( "ItemID" );  
						sprintf(a,"0000000%s",FuelUpd[i].EAN);a[20]=0;
						//msg->LinkEndChild( new TiXmlText( AStruct[counter].ItemID )); 
						msg->LinkEndChild( new TiXmlText( a )); 
						Detail->LinkEndChild( msg );
						msg = new TiXmlElement( "Name" );
						sprintf(a,"%s",FuelUpd[i].Description);
						a[40]=0;
						//msg->LinkEndChild( new TiXmlText( AStruct[counter].Description )); 
						msg->LinkEndChild( new TiXmlText( a )); 
						Detail->LinkEndChild( msg );
						msg = new TiXmlElement( "Description" );  
						//msg->LinkEndChild( new TiXmlText( AStruct[counter].Description )); 
						msg->LinkEndChild( new TiXmlText( a)); 
						Detail->LinkEndChild( msg );
						msg = new TiXmlElement( "ShortDescription" );
						a[10]=0;
						//msg->LinkEndChild( new TiXmlText( AStruct[counter].Description)); 
						msg->LinkEndChild( new TiXmlText( a )); 
						Detail->LinkEndChild( msg );
						msg = new TiXmlElement( "ItemPrice" );  
						msg->SetAttribute("ValueTypeCode","RegularSalesUnitPrice");
						sprintf(a,"%.4d-%.2d-%.2dT%.2d:%.2d:%.2d",t.wYear,t.wMonth,t.wDay,t.wHour,t.wMinute,t.wSecond);
						//a[29]=0;
						msg->SetAttribute("ActivationDate",a);
						//msg->SetAttribute("StatisticCounter",AStruct[counter].VTSZ);
						sprintf(a,"%s000000000000",FuelUpd[i].VTSZ);
						a[11]=0;
						msg->SetAttribute("StatisticCounter",a);
						msg->LinkEndChild( new TiXmlText( FuelUpd[i].Price )); 
						Detail->LinkEndChild( msg );
						
						TiXmlElement * TAX = new TiXmlElement( "TaxInformation" );  
						Detail->LinkEndChild( TAX );
						msg = new TiXmlElement( "TaxID" );  
						msg->LinkEndChild( new TiXmlText(FuelUpd[i].Tax_id  )); 
						TAX->LinkEndChild( msg );
					}
					else {
						TiXmlElement * Detail = new TiXmlElement( "Item" );
						Detail->SetAttribute("ItemCategory","gvr:Oil");
						awroot->LinkEndChild( Detail );
						msg = new TiXmlElement( "ItemID" );  
						sprintf(a,"%s",FuelUpd[i].EAN);a[20]=0;
						//msg->LinkEndChild( new TiXmlText( AStruct[counter].ItemID )); 
						msg->LinkEndChild( new TiXmlText( a )); 
						Detail->LinkEndChild( msg );
						msg = new TiXmlElement( "ItemPrice" );  
						msg->SetAttribute("ValueTypeCode","RegularSalesUnitPrice");
						msg->SetAttribute("ServiceLevel","1");
						sprintf(a,"%.4d-%.2d-%.2dT%.2d:%.2d:%.2d",t.wYear,t.wMonth,t.wDay,t.wHour,t.wMinute,t.wSecond);
						//a[29]=0;
						msg->SetAttribute("ActivationDate",a);
						msg->LinkEndChild( new TiXmlText( FuelUpd[i].Price )); 
						Detail->LinkEndChild( msg );
					}
				}
				GetLocalTime(&t);
				char tmp[5];
				sprintf(tmp,"%.4d",t.wYear);
				tmp[4]=0;
				strcpy(ft1,"null");
				ACISwrite.SaveFile( "c:\\ACISupdate.xml" );
				if(!ACIS_BBOX){
					if (_strnicmp(ACISDNOut,ft1,4)!=0) { 
						sprintf(a,"%s\\IXML_IM_%.2d%.2d%s%.2d%.2d%.2d_1.xml",ACISDNOut,t.wDay,t.wMonth,tmp+2,t.wHour,t.wMinute,t.wSecond);
						if (CopyFile("c:\\ACISupdate.xml",a,true)!=0)
							L.Log("Info: Chk4Upd.ItemUPD: A ACIS item change file has been put to output directory. File/path: %s\n",a);
						else 
							L.LogE("ERROR: Chk4Upd.ItemUpd: Cannot put ACIS item change file to output directory. File/path: %s  Error code:%d\n",a,GetLastError());
					}			
					if (_strnicmp(NewACISDNOut,ft1,4)!=0) { 
						sprintf(a,"%s\\IXML_IM_%.2d%.2d%s%.2d%.2d%.2d_1.xml",NewACISDNOut,t.wDay,t.wMonth,tmp+2,t.wHour,t.wMinute,t.wSecond);
						if (MoveFile("c:\\ACISupdate.xml",a)!=0)
							L.Log("Info: Chk4Upd.ItemUpd: A new ACIS item change file has been put to output directory. File/path: %s\n",a);
						else 
							L.LogE("ERROR: Chk4Upd.ItemUpd: Cannot put new ACIS item change file to output directory. File/path: %s  Error code:%d\n",a,GetLastError());
					}
				}
				else {
					if (_strnicmp(NewACISDNOut,ft1,4)!=0) { 
						sprintf(a,"%s\\IXML_PCR_%.5d_%.4d%.2d%.2d_%.2d%.2d%.2d.xml",NewACISDNOut,GilBarco,t.wYear,t.wMonth,t.wDay,t.wHour,t.wMinute,t.wSecond);
						if (MoveFile("c:\\ACISupdate.xml",a)!=0)
							L.Log("Info: Chk4Upd.ItemUpd: A new ACIS item change file has been put to output directory. File/path: %s\n",a);
						else 
							L.LogE("ERROR: Chk4Upd.ItemUpd: Cannot put new ACIS item change file to output directory. File/path: %s  Error code:%d\n",a,GetLastError());
					}
				
				
				}
				//sprintf(a,"%s\\IXML_IM_%.2d%.2d%s%.2d%.2d%.2d_1.xml",ACISDNOut,t.wDay,t.wMonth,tmp+2,t.wHour,t.wMinute,t.wSecond);
				//ACISwrite.SaveFile( a );  		
				//L.Log("ACIS FUEL update file created! %s\n",a);
			//}
		}

	}
	}


	if ((FuelFunction) && ((namos!=0 ) || (igass!=0 ))) {
			TiXmlDocument docwrite_igas;  
			TiXmlElement* msg_igas;
 			TiXmlDeclaration* decl_igas = new TiXmlDeclaration( "1.0", "UTF-8", "" );  
			docwrite_igas.LinkEndChild( decl_igas ); 
			
		if(igas_upd_counter>0) {
			GetLocalTime(&t);
			seq_num=r.ValueDW("Fuel.NextUpdateNr",0);
			seq_num++;
			sprintf(a,"%.4d%.2d%.2d%.2d%.2d00",t.wYear,t.wMonth,t.wDay,t.wHour,t.wMinute);
			
			/**********  NAMOS ITEM UPDATE  ********************/
			TiXmlElement * root_igas = new TiXmlElement( "Data" );
				root_igas->SetAttribute("TxDev","SIM");
				root_igas->SetAttribute("TxDevNum","0");
				root_igas->SetAttribute("RxDev","NAM");
				root_igas->SetAttribute("RxDevNum","0");
				root_igas->SetAttribute("SerGroup","50");
				root_igas->SetAttribute("SerCode","0");
				root_igas->SetAttribute("Prio","1");
				root_igas->SetAttribute("SeqNum",seq_num);
				root_igas->SetAttribute("Time",a);
				root_igas->SetAttribute("Version","2,23");
			docwrite_igas.LinkEndChild( root_igas );

			TiXmlElement * Items_igas = new TiXmlElement( "Items" );  
				Items_igas->SetAttribute("InitialLoad","0");
				for(i=0;i<igas_upd_counter;i++) {
					msg_igas = new TiXmlElement( "Item" );
					msg_igas->SetAttribute("ItemNr",ItemUpd[i].Auchan_code);
					msg_igas->SetAttribute("BarCode",ItemUpd_igas[i].EAN);
					msg_igas->SetAttribute("ShortText",ItemUpd[i].Description);
					msg_igas->SetAttribute("SalesPrice",ItemUpd[i].Price);
					msg_igas->SetAttribute("SalesUnit",ItemUpd[i].Unit);
					msg_igas->SetAttribute("Locked","0");
					if(ItemUpd[i].isFuel)
						msg_igas->SetAttribute("ItemType","2");
					else 
						msg_igas->SetAttribute("ItemType","1");
					itoa(atoi(ItemUpd[i].Tax_id),b,10);
					msg_igas->SetAttribute("VATNr",b);
					msg_igas->SetAttribute("ItemSpec",ItemUpd[i].VTSZ);
					msg_igas->SetAttribute("ProviderID","0");
					msg_igas->SetAttribute("Description1","0");
					msg_igas->SetAttribute("Description2","0");
					msg_igas->SetAttribute("DiscountLock","0");
					msg_igas->SetAttribute("PriceOverLock","1");
					msg_igas->SetAttribute("DepartmentNr","0");
					msg_igas->SetAttribute("ProductCode","0");
					msg_igas->SetAttribute("EKWNr","0");
					msg_igas->SetAttribute("ItemGroupNr",ItemUpd[i].Dept_Id);
					msg_igas->SetAttribute("LoyaltyGroupNr","0");
					msg_igas->SetAttribute("TaxGroupNr","0");
					if(strlen(ItemUpd[i].Linked_ean)>1){
						msg_igas->SetAttribute("LinkItemType","3");
						msg_igas->SetAttribute("LinkItemNr1",Chk4AuchanCode(ItemUpd[i].Linked_ean));
						msg_igas->SetAttribute("LinkItemQuant1","1");
					}
					else {
						msg_igas->SetAttribute("LinkItemType","0");
						msg_igas->SetAttribute("LinkItemNr1","0");
						msg_igas->SetAttribute("LinkItemQuant1","0");
					}
					msg_igas->SetAttribute("LinkItemNr2","0");
					msg_igas->SetAttribute("LinkItemQuant2","0");
					msg_igas->SetAttribute("PurchPrice","0");
					msg_igas->SetAttribute("LoyaltyMult","0");
					msg_igas->SetAttribute("ContentFactor","0");
					msg_igas->SetAttribute("CompanyItem","0");
					msg_igas->SetAttribute("InvoiceFlag","0");
					Items_igas->LinkEndChild( msg_igas );
				}
			root_igas->LinkEndChild( Items_igas );
			TiXmlElement * FuelPrice = new TiXmlElement( "FuelPriceChanges" );  
				FuelPrice->SetAttribute("InitialLoad","1");
				i=0;
				while(FuelPriceArray[i].price>0){
					sell_price=FuelPriceArray[i].price;
					if(FuelPriceArray[i].price>FuelPriceArray[i].disc_price && FuelPriceArray[i].disc_price!=0)
						sell_price=FuelPriceArray[i].disc_price;
					msg_igas = new TiXmlElement( "FuelPriceChange" );
					msg_igas->SetAttribute("ItemNr",FuelPriceArray[i].ean);
					msg_igas->SetAttribute("Price",sell_price);
					msg_igas->SetAttribute("StartTime",a);
					FuelPrice->LinkEndChild( msg_igas );
					i++;
				}
			root_igas->LinkEndChild( FuelPrice );
			GetLocalTime(&t);
			r.SetValueDW("Fuel.NextUpdateNr",seq_num);
			sprintf(b,"C:\\DAT_000_%.6d.xml",seq_num);
			docwrite_igas.SaveFile( b );
			L.Log("NAMOS ITEM update file created! %s\n",a);
			if(namos>0){
				sprintf(a,"%s\\DAT_000_%.6d.xml",DNUpdO,seq_num);	
				for(i=0;i<10;i++){
					if(!CopyFile(b,a,false)){
						L.LogE("ERROR -- Cannot move NAMOS update file %s! Error: %d\n",a,GetLastError());
						Sleep(2000);
					}
					else i=10;
				}
			}
			if(igass>0 && igas_upd_counter>0){
				sprintf(a,"%s\\DAT_000_%.6d.xml",IgassUpdO,seq_num);	
				for(i=0;i<10;i++){
					if(!CopyFile(b,a,false)){
						L.LogE("ERROR -- Cannot move IGASS update file %s! Error: %d\n",a,GetLastError());
						Sleep(2000);
					}
					else i=10;
				}
			}
			//DeleteFile(a);
		}


	}

	if ((!res)&&(InitCSC!=0)) {
		if (InitCSC==2) sprintf(dst,"%s\\FullCSC.dmp",DNUpdO);
		if ((F=fopen(dst,"wb"))!=NULL) {
			UINT16 bufid,mode,precision;
			ISS_T_BOOLEAN unknown;
			ISS_T_REPLY rc;
			long records=0;
			char buf[208];
			memset(buf+1,' ',200);
			buf[201]=13;
			buf[202]=10;
			buf[203]=0;
			{
				Reg r(false,C_RegKey);
				r.SetValueDW(C_InitCSC,0);
				InitCSC=0;
			}
			L.Log("Chk4Upd: DumpCSC: Started dumping customer records...\n");
			mode=ISS_C_DB_FIRST;
			while ((rc=ReadISS(&bufid,false,"CSC",0,NULL,mode,ISS_C_DB_NO_LOCK,"CUSTOMER_ID"
				,21,buf,false))==ISS_SUCCESS) {
				records++;
				buf[0]='C';
				mode=ISS_C_DB_NEXT;
				if ((rc=iss_db_extract_field(bufid,"NOM",30,0,buf+21,&precision,&unknown))
					!=ISS_SUCCESS) 
					L.LogE("ERROR: Chk4Upd: DumpCSC.ExtractField.NOM: Failed with error code %d!\n"
						,rc);
				else {
					long l=strlen(buf+21);
					if (l<30) memset(buf+21+l,' ',30-l);
				}
				if (rc==ISS_SUCCESS) {
					if ((rc=iss_db_extract_field(bufid,"PRENOM",30,0,buf+51,&precision,&unknown))
						!=ISS_SUCCESS) 
						L.LogE("ERROR: Chk4Upd: DumpCSC.ExtractField.PRENOM: Failed with error code %d!\n"
							,rc);
					else {
						long l=strlen(buf+51);
						if (l<30) memset(buf+51+l,' ',30-l);
					}
				}
				if (rc==ISS_SUCCESS) {
					if ((rc=iss_db_extract_field(bufid,"CODE_POSTAL",10,0,buf+81,&precision
						,&unknown))!=ISS_SUCCESS) 
						L.LogE("ERROR: Chk4Upd: DumpCSC.ExtractField.CODE_POSTAL: Failed with error code %d!\n"
							,rc);
					else {
						long l=strlen(buf+81);
						if (l<10) memset(buf+81+l,' ',10-l);
					}
				}
				if (rc==ISS_SUCCESS) {
					if ((rc=iss_db_extract_field(bufid,"VILLE",30,0,buf+91,&precision,&unknown))
						!=ISS_SUCCESS) 
						L.LogE("ERROR: Chk4Upd: DumpCSC.ExtractField.VILLE: Failed with error code %d!\n"
							,rc);
					else {
						long l=strlen(buf+91);
						if (l<30) memset(buf+91+l,' ',30-l);
					}
				}
				if (rc==ISS_SUCCESS) {
					if ((rc=iss_db_extract_field(bufid,"ADRESSE",30,0,buf+121,&precision,&unknown))
						!=ISS_SUCCESS) 
						L.LogE("ERROR: Chk4Upd: DumpCSC.ExtractField.ADRESSE.0: Failed with error code %d!\n"
							,rc);
					else {
						long l=strlen(buf+121);
						if (l<30) memset(buf+121+l,' ',30-l);
					}
				}
				if (rc==ISS_SUCCESS) {
					if ((rc=iss_db_extract_field(bufid,"ADRESSE",30,1,buf+161,&precision,&unknown))
						!=ISS_SUCCESS) 
						L.LogE("ERROR: Chk4Upd: DumpCSC.ExtractField.ADRESSE.1: Failed with error code %d!\n"
							,rc);
					else {
						long l=strlen(buf+161);
						if (l<30) memset(buf+161+l,' ',30-l);
					}
				}
				if (rc==ISS_SUCCESS) {//write record to dest file
					fputs(buf,F);
				}
				if ((rc=iss_db_free_buffer(&bufid))!=ISS_SUCCESS)
					L.LogE("ERROR: Chk4Upd: DumpCSC.FreeBuffer: Failed with error code %d!\n",rc);
			}
			if (rc!=ISS_E_SDBMS_NOREC) {
				L.LogE("ERROR: Chk4Upd: DumpCSC: Dumping customer records (count=%d) finished with unexpected error code: %d!\n"
					,records,rc);
			}
			else L.Log("Chk4Upd: DumpCSC: Dumping all (=%d )customer records successfully finished.\n",records);
			if (ftell(F)>0) {
				res=true;
				fclose(F);
			}
			else {
				fclose(F);
				remove(dst);
			}
		}
	}
	delete [] dst;
	return res;

}

BOOL CtrlHandler(DWORD fdwCtrlType) 
{ 
    switch (fdwCtrlType) 
    { 
        case CTRL_C_EVENT: 
        case CTRL_BREAK_EVENT: 
        case CTRL_LOGOFF_EVENT: 
        case CTRL_SHUTDOWN_EVENT: 
        case CTRL_CLOSE_EVENT: 
			KeepLooping=false; //safe shutdown
			if (FuelFunction)
				L.Log("Info: AuPosSim user shutdown initiated. (Ctrl_event:%d)\n",fdwCtrlType);
			if (RVMFunction)
				RL.Log("Info: AuPosSim user shutdown initiated. (Ctrl_event:%d)\n",fdwCtrlType);
			Sleep(5000);
			if (FuelFunction)
				L.Log("Info: AuPosSim user shutdown delay passed.\n");
			if (RVMFunction)
				RL.Log("Info: AuPosSim user shutdown delay passed.\n");
            return TRUE; 
        default: 
			if (FuelFunction)
				L.Log("Info: AuPosSim unhandled control event passed! %d\n",fdwCtrlType);
			if (RVMFunction)
				RL.Log("Info: AuPosSim unhandled control event passed! %d\n",fdwCtrlType);
            return FALSE; 
    } 
} 

int ProcessLoop(void)
{
	Reg r(false, C_RegKey);
	SYSTEMTIME t;
    if (!SetConsoleCtrlHandler((PHANDLER_ROUTINE) CtrlHandler, TRUE)) {
		if (FuelFunction)
			L.LogE("ERROR: Registering console control handler failed!\n");
		if (RVMFunction)
			RL.LogE("ERROR: Registering console control handler failed!\n");
	}
	else {
		TDRefresh(true);
		while (KeepLooping) {
			RL.Log("KeepLooping %s\n",RDNIn);
			if (Chk4TomraTran()) continue;
			if (Chk4RVMTran()) continue;
			if(GilBarco)
				if (Chk4FuelTran()) 
					FuelClose=r.ValueDW("Fuel.System.Close",0);
			if(FLEXYS){
				GetLocalTime(&t);
				long l=t.wHour*3600+t.wMinute*60+t.wSecond;
				if ((AutoOpenEnabled>0)&&(l>=AutoOpenStartTime)&&(l<AutoOpenEndTime)){
					r.SetValueDW("Fuel.System.Close",0);
					r.SetValueDW("Fuel.System.Close",0); //Engedem a benzink�t f�jlok feldolgoz�s�t	
					FuelClose=0;
				}
				if(!FuelClose)
					if (Chk4Flexys_Tran()) continue;
				if (Chk4Flexys_Closure_File()) continue;
			}
			if(namos){
				GetLocalTime(&t);
				long l=t.wHour*3600+t.wMinute*60+t.wSecond;
				if ((AutoOpenEnabled>0)&&(l>=AutoOpenStartTime)&&(l<AutoOpenEndTime)){
					r.SetValueDW("Fuel.System.Close",0);
					r.SetValueDW("Fuel.System.Close",0); //Engedem a benzink�t f�jlok feldolgoz�s�t	
					FuelClose=0;
				}
				if (Chk4NAMOS_Tran()) continue;
				if (Chk4Flexys_Closure_File()) continue;
			}
			if (!FuelClose){ //Ha m�g nem j�tt z�r�s f�jl, akkor elfogadja 
				if (Chk4ACISTran()) continue;
				if (Chk4IGASS_Tran()) continue;
				if(ACIS_BBOX==0){
					for (int i=1;i<=ACISCounter;i++)
						if (Chk4NewACISTran(i)) continue;
				}
				else 
					if (Chk4NewACISTran(1)) continue;
			}
			if (LogNoTran) {
				if (FuelFunction)
					L.Log("Info: No transaction file to process.\n");
				else if (RVMFunction)
					RL.Log("Info: No transaction file to process.\n");
			}
			if(namos) if(Chk4Upd_namos()) continue;
			else 
				if (Chk4Upd()) continue;
			if (Chk4RVMIdle()) {
				if (ExitOnNoTran) KeepLooping=false; //This one is for testing only
				else Sleep(ProcessLoopDelay);
			}
		}
	}
    if (!SetConsoleCtrlHandler((PHANDLER_ROUTINE) CtrlHandler, FALSE)) {
		if (FuelFunction)
			L.LogE("ERROR: Unregistering console control handler failed!\n");
		if (RVMFunction)
			RL.LogE("ERROR: Unregistering console control handler failed!\n");
	}
	return 0;
}
 
int main(int argc, char**argv)
{
	int res=0;
	
	L.PutBreakLine("<=====================>",true);
	L.LogE("Auchan Fuel Station sales data injector interface %s started...\n",C_Ver);
	L.Log("Loading last time saved status and system parameters...\n");
	RL.PutBreakLine("<=====================>",true);
	RL.LogE("Auchan RVM/Bottle Return data injector interface %s started...\n",C_Ver);
	RL.Log("Loading last time saved status and system parameters...\n");
	res=LoadVar();
	if (isRunning("AuPosSim.exe")) {
			L.LogE("EROOR -- Cannot start Aupossim.exe in 2 copies!\n" );
			return 1;
		}

//	int zz=RegCSC("19","Falikut","Falikut��������� 2","C�m","Cim���������","V�ros","2600","1234567-2-34","AJR111");
	if (!res) {
		res=PreFetchData();
	}
	if (!res) {
		get_fuel_prices(FuelPrices); //Bet�lt�m az �zemanyag �rakat
		GetDiscountPrice();  //Ha van kedvezm�nyes �r, akkor azt is beolvasom
		set_fuel_price();  //Be�rom a registry-be az �rakat
	
		if ((FuelFunction)||(RVMFunction)) {
			if (!res) {
				if (FuelFunction) L.Log("Parameters have been loaded successfully.\n");
				if (RVMFunction) RL.Log("Parameters have been loaded successfully.\n");
				res=GetProgressStatus();
			}
//L.Log("Forced exit after checkings done.\n");
//return res;
			SignalCleanExit2False();
			res=ProcessLoop();
			if (res) {
				SaveStatus();
				RSaveStatus();
			}
			else {
				res=SaveStatus();
				RSaveStatus();
			}
		}
		else {
			L.Log("Info: No active interface functions, exiting...\n");
			RL.Log("Info: No active interface functions, exiting...\n");
		}
	}
	if (FuelFunction) L.Log("Cleaning up memory...\n");
	if (RVMFunction) RL.Log("Cleaning up memory...\n");
	CleanUp();
	if (FuelFunction) L.Log("Clean exit with result code %d\n",res);
	if (RVMFunction) RL.Log("Clean exit with result code %d\n",res);
	return res;
}